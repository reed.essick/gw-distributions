.. _event:

.. contents::
    :depth: 2

Event Objects
==================================================

.. automodule:: gwdistributions.event
    :members:
