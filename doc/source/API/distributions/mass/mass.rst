.. _api-distributions-mass:

gwdistributions.distributions.mass
==================================================

.. toctree::
    :maxdepth: 2

    joint
    individual
    pairing

.. automodule:: gwdistributions.distributions.mass
    :members:
