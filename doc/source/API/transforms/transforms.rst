.. _api-transforms:

gwdistributions.transforms
==================================================

.. toctree::
    :maxdepth: 2

    base
    detection
    location
    mass
    spin

.. automodule:: gwdistributions.transforms
    :members:

