.. _installation:

Installation
==================================================

Currently, GWDistributions is only available from-source.
To download and install the code, do something like the following::

    >> git clone https://git.ligo.org/reed.essick/gw-distributions
    >> cd gw-distributions
    >> python setup.py install --prefix /path/to/install

Make sure to update your PATH and PYTHONPATH environment variables to make your installation discoverable.
You can test that you've correctly installed GWDistributions via::

    >> python -c "import gwdistributions ; print(gwdistributions.__version__)"
    0.0.0
