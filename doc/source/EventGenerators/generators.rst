.. _generators:

.. contents::
    :depth: 2

Generators (Joint Distributions)
==================================================

Basic Modular Joint Distributions
--------------------------------------------------

.. autoclass:: gwdistributions.generators.EventGenerator

.. autoclass:: gwdistributions.generators.Parameters

Specialized Joint Distributions
--------------------------------------------------

.. autoclass:: gwdistributions.generators.MonteCarloIntegrator
