.. GWDistributions documentation master file, created by
   sphinx-quickstart on Fri Nov 13 14:45:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GWDistributions's documentation!
==================================================

This library provides flexible, modular distributions for single-event parameters of gravitational-wave sources such as masses, spins, and redshifts.
GWDistributions defines a series of probability distributions, from which one can sample single-event parameters or evaluate the probability of particular single-event parameters.
Individual distributions for a particular set of variates may be conditioned on other variates, with automatic sanity-checking implemented to make sure the resulting joint distribution is sensible.
Joint distributions are supported, and samples drawn from joint distributions can additionally be updated automatically to contain transformed versions of the variates over which the distributions are defined (e.g., transform component masses into chirp mass and mass ratio).

You can read more about how this is accomplished below.

.. toctree::
    :maxdepth: 1

    overview
    Event/event
    SamplingDistributions/distributions
    AttributeTransformations/transforms
    EventGenerators/generators

We additionally provide tutorials, a brief summary of the executables included in the library, as well as a full description of the API.

.. toctree::
    :caption: User Guide and Detailed References
    :maxdepth: 1

    Tutorials/tutorials
    Executables/executables
    API/api
    Installation/installation

Indices and tables
--------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
