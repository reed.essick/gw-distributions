.. _transforms:

.. contents::
    :depth: 2

Transformations
==================================================

.. toctree::
    :caption: AttributeTransformations
    :maxdepth: 2

    base
    mass
    spin
    location
    detection

