.. _transforms-detection:

Detection Transformations
==================================================

.. automodule:: gwdistributions.transforms.detection
    :members:
