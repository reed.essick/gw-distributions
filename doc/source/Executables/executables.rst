.. _executables:

.. contents::
    :depth: 2

Executables
==================================================

GWDistributions provides a lot of functionality, which is expected to be of general use in a variety of code-bases.
Therefore, we strive to make the library easy to incorporate within other repositories.
Part of that expectation includes the likelihood that specific use cases will be tailored to different repositories, and we therefore only provide a few of the most general possible operations through command-line executables.

We summarize the supported executables below, with which one can draw samples from a distribution or compute the probability of each sample in a set based on a distribution.

gwdistributions-sample
--------------------------------------------------

gwdistributions-weigh
--------------------------------------------------
