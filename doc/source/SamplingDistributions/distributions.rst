.. _distributions:

.. contents::
    :depth: 2

Distributions
==================================================

Description of SamplingDistributions

  * what they require
  * what they can do

.. toctree::
    :maxdepth: 2

    base
    external
    mass/mass
    spin
    time
    orientation
    location
    eccentricity
