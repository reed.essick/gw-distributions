.. _distributions_base:

Basic Distributions
==================================================

.. automodule:: gwdistributions.distributions.base
    :members: SamplingDistribution
