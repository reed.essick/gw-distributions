.. _distributions-mass-joint:

Joint Mass Distributions
==================================================

.. automodule:: gwdistributions.distributions.mass.joint
    :members:

