.. _distributions-mass:

Mass Distributions
==================================================

.. automodule:: gwdistributions.distributions.mass
    :members:

The mass distributions are further divided into several subsets.

.. toctree::
    :caption: Mass Distributions
    :maxdepth: 2

    joint
    individual
    pairing
