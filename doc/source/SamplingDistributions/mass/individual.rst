.. _distributions-mass-individual:

Individual Mass Distributions
==================================================

.. automodule:: gwdistributions.distributions.mass.individual
    :members:

.. automodule:: gwdistributions.distributions.mass.pairing
    :members:

.. automodule:: gwdistributions.distributions.mass.joint
    :members:

