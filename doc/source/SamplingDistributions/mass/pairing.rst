.. _distributions-mass-pairing:

Pairing Functions to Combine Individual Mass Distributions
==========================================================

.. automodule:: gwdistributions.distributions.mass.pairing
    :members:

