#!/usr/bin/env python
__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Reed Essick <reed.essick@gmail.com>, Chris Pankow <chris.pankow@ligo.org>, Eve Chase <eve.chase@ligo.org>"

#-------------------------------------------------

from setuptools import (setup, find_packages)
import glob

#-------------------------------------------------

# set up arguments
scripts = glob.glob('bin/*') + glob.glob('bin-tests/*')

packages = find_packages()
package_data = {
    'gwdistributions.distributions.location.angles.zodiac': ['*txt', '*.csv.gz'], ### constellations
}

# set up requirements
requires = []
### FIXME: specify requirements? are any of these actually required?
#   numpy?
#   scipy?
#   h5py?
#   jax?
#   lal?
#   lalsimulation?
#   gwdetectors? https://git.ligo.org/reed.essick/gw-detectors

#------------------------

# install
setup(
    name = 'gw-distributions',
    version = '0.0.0',
    url = 'https://git.ligo.org/reed.essick/gw-distributions',
    author = __author__,
    author_email = 'reed.essick@gmail.com',
    description = __description__,
    license = 'MIT',
    scripts = scripts,
    packages = packages,
    package_data = package_data,
    requires = requires,
)
