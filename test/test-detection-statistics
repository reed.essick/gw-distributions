#!/bin/bash

### a simple wrapper around tests of (some) detection statistic transformations
### Reed Essick (reed.essick@gmail.com)

#-------------------------------------------------

. test-settings

OUTDIR="detection-statistics"

#------------------------

# NOTE
#  this is chosen so that we explore some of the interesting behavior associated with the fact that
#  some detection statistics are positive definite. This is also why the --snr-range and --threshold-snr-net
#  are set dynamically for test-detection-probability (chosen so that the detection probability is always a 
#  reasonable number that's not too big or too small)
SINGLE_IFO_THRESHOLD=3

#-------------------------------------------------

for N in $(seq 1 3) ### different network sizes
do

    echo '--------------------------------------------------'
    echo "            $N detector observed snr"
    echo '--------------------------------------------------'

    #echo \
    ./test-observed-snr \
        --num-trials $NUM_TRIALS \
        --num-samples $NUM_SAMPLES \
        --num-detectors $N \
        --snr-range 0.0 20.0 \
        $SEED \
        --output-dir $OUTDIR \
        --tag "${N}det" \
        --backend $BACKEND \
        $VERBOSE \
    || exit 1

    echo '--------------------------------------------------'
    echo "            $N detector detection probability"
    echo '--------------------------------------------------'

    #echo \
    ./test-detection-probability \
        --num-trials $NUM_TRIALS \
        --num-samples $NUM_SAMPLES \
        --num-detectors $N \
        --threshold-snr $SINGLE_IFO_THRESHOLD \
        --threshold-snr-net $(python -c "print($SINGLE_IFO_THRESHOLD * $N**0.5)") \
        --snr-range $(($SINGLE_IFO_THRESHOLD - 1)) $(($SINGLE_IFO_THRESHOLD + 1)) \
        $SEED \
        --output-dir $OUTDIR \
        --tag "${N}det" \
        --backend $BACKEND \
        $VERBOSE \
    || exit 1

done
