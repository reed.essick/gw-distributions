#!/bin/bash

### test distributions over component masses (mass1_source, mass2_source)
### Reed Essick (reed.essick@gmail.com)

#-------------------------------------------------

. test-settings ### establish a few parameters

OUTDIR="component-mass"

#-------------------------------------------------

#echo \
gwdist-test-distribution \
    FixedComponentMass \
    --param-prior mass1 1.6 2.0 \
    --param-prior mass2 1.2 1.4 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag FixedComponentMass \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1

#echo \
gwdist-test-distribution \
    PowerLawComponentMass \
    --param-prior min_mass1_source 2.0 5.0 \
    --param-prior max_mass1_source 5.0 10.0 \
    --param-prior pow_mass1_source -2.0 +2.0 \
    --param-prior min_mass2_source 1.0 2.0 \
    --param-prior max_mass2_source 3.0 5.0 \
    --param-prior pow_mass2_source -2.0 +2.0 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag PowerLawComponentMass \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1

#echo \
gwdist-test-distribution \
    PowerLawComponentMass \
    --param-prior min_mass1_source 2.0 5.0 \
    --param-prior max_mass1_source 5.0 10.0 \
    --param-prior pow_mass1_source -1.0 -1.0 \
    --param-prior min_mass2_source 1.0 2.0 \
    --param-prior max_mass2_source 3.0 5.0 \
    --param-prior pow_mass2_source -2.0 +2.0 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag PowerLawComponentMass-mass1_pow-1 \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1

#echo \
gwdist-test-distribution \
    PowerLawComponentMass \
    --param-prior min_mass1_source 2.0 5.0 \
    --param-prior max_mass1_source 5.0 10.0 \
    --param-prior pow_mass1_source -2.0 +2.0 \
    --param-prior min_mass2_source 1.0 2.0 \
    --param-prior max_mass2_source 3.0 5.0 \
    --param-prior pow_mass2_source -1.0 -1.0 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag PowerLawComponentMass-mass2_pow-1 \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1

#echo \
gwdist-test-distribution \
    PowerLawComponentMass \
    --param-prior min_mass1_source 2.0 5.0 \
    --param-prior max_mass1_source 5.0 10.0 \
    --param-prior pow_mass1_source -1.0 -1.0 \
    --param-prior min_mass2_source 1.0 2.0 \
    --param-prior max_mass2_source 3.0 5.0 \
    --param-prior pow_mass2_source -1.0 -1.0 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag PowerLawComponentMass-mass1_pow-1-mass2_pow-1 \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1

#echo \
gwdist-test-distribution \
    TruncatedGaussianComponentMass \
    --param-prior mean_mass_source 0.5 5.0 \
    --param-prior stdv_mass_source 1.0 3.0 \
    --param-prior min_mass1_source 2.0 3.0 \
    --param-prior max_mass1_source 4.0 10.0 \
    --param-prior min_mass2_source 0.5 2.0 \
    --param-prior max_mass2_source 3.0 7.0 \
    --num-samples $NUM_SAMPLES \
    --num-trials $NUM_TRIALS \
    --num-grid-points $NUM_GRID_2D \
    $SEED \
    --plot \
    --output-dir $OUTDIR \
    --tag TruncatedGaussianComponentMass \
    --backend $BACKEND \
    $VERBOSE \
|| exit 1
