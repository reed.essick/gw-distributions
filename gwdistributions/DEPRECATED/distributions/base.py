"""a module that houses various basic functions for popluation models
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions import backends as be
from gwdistributions.utils import Parameters

#-------------------------------------------------
# Sampling Distribution
#-------------------------------------------------

class SamplingDistribution(object):
    """a thin wrapper around some functionality to compute basic attributes of distributions
    """
    _normalized = False
    _variates = ()
    _params = ()
    _required = ()

    def __init__(self, *args, **kwargs):
        self._init_values(*args, **kwargs)
        self._init_param2ind()
        self._parameters = Parameters(self)

    def _init_values(self):
        self._values = be.vector([], dtype=float)

    def _init_param2ind(self):
        self._param2ind = dict((name, ind) for ind, name in enumerate(self._params)) ### FIXME: need to make this automatic for all instantiations...
        self._Nparams = len(self._params)

    @property
    def normalized(self):
        return self._normalized

    @property
    def variates(self):
        return self._variates

    @property
    def required(self):
        return self._required

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==len(self._values)
        self._values[:] = new

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, new):
        '''expects the new parameters to be a dictionary, which we then pass to self.update
        '''
        self.update(**new)

    def __getitem__(self, name):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        return self._values[self._param2ind[name]]

    def __setitem__(self, name, value):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        self._values[self._param2ind[name]] = value

    def __items__(self):
        return list(zip(self._params, self._values))

    def update(self, **kwargs):
        '''update the parameters of this sampling distribution'''
        for name, param in kwargs.items():
            self[name] = param

    @staticmethod
    def _check_variates(variates, args):
        """checks whether args has the correct format. Will also manipulate some common formats into the correct format if possible.
        returns corectly_formated_args (suitable to be passed to _prob, _logprob, etc)
        """
        if len(args) == len(variates):
            if len(variates)==1:
                try: ### see if we can __getitem__ like a dictionary
                    args = [args[0][variates[0]]]
                    args = SamplingDistribution._check_variates(variates, args)

                except Exception:
                    if not isinstance(args[0], (int, float, be.ArrayType)): ### see if we already have the right type (no named columns)
                        raise RuntimeError('''\
could not interpret args. Note that len(variates)==1. Please pass either a single array-like argument 
or an object from which such an argument can be extracted via args[variates[0]]''')
                    else: ### data should be acceptable
                        pass

            elif not all([isinstance(_, (int, float, be.ArrayType)) for _ in args]): ### see if all args are in the right type
                raise RuntimeError('''\
when multiple variates are passed (and len(variates)!=1), each variate must be instances of int, float, or be.ArrayType''')

        elif len(args)==1: ### try extracting the parameters as EventGenerator would from a dict, numpy structured array, pandas dataframe, etc
            args = args[0]
            try:
                args = [args[_] for _ in variates] ### access variable names like args is a dict
                args = SamplingDistribution._check_variates(variates, args)
            except Exception:
                raise RuntimeError('''\
could not interpret arguments as passed. Please either pass a list of array-like arguments
in the same order as in variates or an object from which such a list an be extracted via [args[_] for _ in variates]''')

        else:
            raise RuntimeError('''
could not interpret args. Please either pass a list of array-like arguments \
in the same order as in variates or an object from which such a list an be extracted via [args[_] for _ in variates]''')

        return args

    def rvs(self, size=1, *args):
        """draw a specified number of realizations from this distribution
        """
        return self._rvs(size, *self._check_variates(self._required, args))

    def _rvs(self, size=1, *required):
        raise NotImplementedError('child classes should override this')

    def prob(self, *args):
        """return the value of the distribution at the specified point
        signature should be prob(self, *variates, *required)
        """
        return self._prob(*self._check_variates(self._variates+self._required, args))

    def _prob(self, *args):
        return be.exp(self._logprob(*args))

    def logprob(self, *args):
        """return the natural log of the value of the distribution at the specified point
        signature should be logprob(self, *variates, *required)
        """
        return self._logprob(*self._check_variates(self._variates+self._required, args))

    def _logprob(self, *args):
        raise NotImplementedError('child classes should override this')

    def jacobian(self, *args):
        """return the jacobian of the distribution at the specified point
        signature should be jacobian(self, *variates, *required)
        """
        return self._jacobian(*self._check_variates(self._variates+self._required, args))

    def _jacobian(self, *args):
        raise NotImplementedError('child classes should override this')

    def hessian(self, *args):
        """return the hessian of the distribution at the specified point
        signature should be hessian(self, *variates, *required)
        """
        return self._hessian(*self._check_variates(self._variates+self._required, args))

    def _hessian(self, *args):
        raise NotImplementedError('child classes should override this')

#-------------------------------------------------

class EvolveXwithY(SamplingDistribution):
    """a class that generates new SamplingDistributions that's parameters evolve based on the variates produced by another SamplingDistribution
    """

    def _init_values(self, DependentSamplingDistribution, IndependentSamplingDistribution):
        self._dependent = DependentSamplingDistribution
        self._independent = IndependentSamplingDistribution
        raise NotImplementedError('''\
FIXME!
write a factory that automatically generates distributions that are conditioned on other distributions.
Need to set
    _variates
    _required
    _params (take these from DependentSamplingDistribution, but possibly augmented if there is going to be some sort of evolution based on the IndependentSamplingDistribution)
when we evaluate this thing (or sample from it), we need to update the DependentSamplingDistribution based on the variates from the IndependentSamplingDistribution, then we can just delegate to the DependentSamplingDistribution for everything...
''')

    def parameter_map(self, vals):
        '''a basic function that encodes how the conditional distribution will "evolve" the parameters of the DependentSamplingDistribution with the specific variates produced by the IndependentSamplingDistribution
        '''
        return self._params ### in the base class, don't do anything

class EvolveXwithTaylorY(EvolveXwithY):
    """a class that generates new SamplingDistributions that's parameters evolve according to a power law in another SamplingDistribution's variates
    """

    def parameter_map(self, vals):
        '''determine the parameter for the DependentSamplingDistribution as a Taylor Series in the IndependentSamplingDistribution's variates
        '''
        raise NotImplementedError

class EvolveXwithSigmoidY(EvolveXwithY):
    """a class that models the evolution of the distribution for X as a sigmoid in the variates of Y
    """

    def parameter_map(self, vals):
        '''compute the parameter to use based on a sigmoid in vals
        '''
        raise NotImplementedError

class EvolveXwithStepsY(EvolveXwithY):
    """a class that models the evolution of the distribution for X as a series of independent "bins" based on the variates of Y
    """

    def parameter_map(self, vals):
        '''figure out which parameter to use based on vals
        '''
        raise NotImplementedError

#-------------------------------------------------

class Mixture(SamplingDistribution):
    """a class that allows users to generate mixture model distributions based on individual distributions
    """

    def _init_values(self, *SubDistributions):
        raise NotImplementedError('''this is not supported in a general way yet...''')
