"""a specific set of modules meant to model distributions over the component masses of GW binaries. Because there are numerous ways of parametrizing such distirbuitons, we divide them up a bit more herein for the sake of maintainability
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .joint import * ### basic 2D MassDistributions

from .individual import * ### separate ditributions over "mass1" and "mass2"
from .pairing import * ### combine Mass1Distributions via pairing functions, which emulate MassDistribution objects
