"""source distributions for how events masses are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from numpy import pi

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions import backends as be

#-------------------------------------------------
# Distributions not in scipy
#-------------------------------------------------

def negative_power(a, b, idx, size=1):
    if a==b: ### this is effectively a delta-function
        return a*be.ones(size, dtype=float)

    if idx < 0:
        assert b > a > 0
    else:
        assert b > a

    if idx == -1:
        norm = b / a
        rvs = be.random(low=0, high=1, size=size)
        return norm**rvs * a

    norm = b**(idx+1) - a**(idx+1)
    rvs = be.random(low=0, high=1, size=size)
    # cdf = (rvs**(idx+1) - a**(idx+1)) / norm
    return (norm * rvs + a**(idx+1))**(1.0/(idx+1))

#-------------------------------------------------

class MassDistribution(SamplingDistribution):
    """a child class used to declare things are mass distributions
    """
    _variates = ("mass1", "mass2")

#-------------------------------------------------

class FixedComponentMass(MassDistribution):
    """Generate mass pairs with specific values.
    """
    _normalized = True
    _params = ('mass1', 'mass2')

    def _init_values(self, mass1=10., mass2=10.):
        self._values = be.vector([mass1, mass2], dtype=float)

    @property
    def mass1(self):
        return self._values[0]

    @mass1.setter
    def mass1(self, new):
        self._values[0] = new

    @property
    def mass2(self):
        return self._values[1]

    @mass2.setter
    def mass2(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        ans = be.empty((size, 2), dtype=float)
        ans[:,0] = self.mass1
        ans[:,1] = self.mass2
        return ans

    def _prob(self, mass1, mass2):
        return be.where((mass1==self.mass1)*(mass2==self.mass2), 1.0, 0.0)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2):
        return be.zeros(2, type=float)

    def _hessian(self, mass1, mass2):
        return be.zeros((2,2), dtype=float)

class UniformComponentMass(MassDistribution):
    _normalized = True
    _params = ('min_primary', 'max_primary', 'min_secondary', 'max_secondary')

    def _init_values(self, min_primary=1.0, max_primary=100.0, min_secondary=1.0, max_secondary=100.0):
        self._values = be.vector([min_primary, max_primary, min_secondary, max_secondary], dtype=float)

    @property
    def min_primary(self):
        return self._values[0]

    @min_primary.setter
    def min_primary(self, new):
        self._values[0] = new

    @property
    def max_primary(self):
        return self._values[1]

    @max_primary.setter
    def max_primary(self, new):
        self._values[1] = new

    @property
    def min_secondary(self):
        return self._values[2]

    @min_secondary.setter
    def min_secondary(self, new):
        self._values[2] = new

    @property
    def max_secondary(self):
        return self._values[3]

    @max_secondary.setter
    def max_secondaryy(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        ans = be.empty((size, 2), dtype=float)
        N = 0
        while N < size:
            m1 = be.random(self.min_primary, self.max_primary, size=size)
            m2 = be.random(self.min_secondary, self.max_secondary, size=size)
            selected = m1 >= m2
            n = min(be.sum(selected), size-N)
            ans[N:N+n,0] = m1[selected][:n]
            ans[N:N+n,1] = m2[selected][:n]
            N += n
        return ans

    def _prob(self, mass1, mass2):
        nonzero = (mass1 >= mass2)*(self.min_primary <= mass1)*(mass1 < self.max_primary)*(self.min_secondary <= mass2)*(mass2 < self.max_secondary)
        return be.where(nonzero, 1./self.norm, 0.)

    @property
    def norm(self):
        min_primary, max_primary, min_secondary, max_secondary = self._values
        if min_primary < min_secondary:
            min_primary = min_secondary
        if max_secondary > max_primary:
            max_secondary = max_primary
        return (min_primary - min_secondary)*(max_primary - min_primary) + (max_secondary - min_primary)*(max_primary - max_secondary) + 0.5*(max_secondary - min_primary)**2

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2):
        base = (self.min_primary <= mass1)*(mass1 < self.max_primary)*(self.min_secondary <= mass2)*(mass2 < self.max_secondary)
        ans = base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary))
        wer = base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.max_secondary)**2)
        return be.vector((ans, -ans, wer, -wer), dtype=float)

    def _hessian(self, mass1, mass2):
        base = (self.min_primary <= mass1)*(mass1 < self.max_primary)*(self.min_secondary <= mass2)*(mass2 < self.max_secondary)
        ans = be.empty((4,4), dtype=float)

        ans[0,0] = 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[0,1] = ans[1,0] = - 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[0,2] = ans[2,0] = +base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[0,3] = ans[3,0] = -base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[1,1] = 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[1,2] = ans[2,1] = -base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[1,3] = ans[3,1] = +base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[2,2] = 2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.min_secondary)**3)
        ans[2,3] = ans[3,2] = -2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.max_secondary)**3)
        ans[3,3] = 2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.min_secondary)**3)

        return ans

class NormalComponentMass(MassDistribution):
    _params = ('mean_primary', 'std_primary', 'mean_secondary', 'std_secondary')

    def _init_values(self, mean_primary=1.4, std_primary=0.1, mean_secondary=1.4, std_secondary=0.1):
        self._values = be.vector([mean_primary, std_primary, mean_secondary, std_secondary], dtype=float)

    @property
    def mean_primary(self):
        return self._values[0]

    @mean_primary.setter
    def mean_primary(self, new):
        self._values[0] = new

    @property
    def std_primary(self):
        return self._values[1]

    @std_primary.setter
    def std_primary(self, new):
        self._values[1] = new

    @property
    def mean_secondary(self):
        return self._values[2]

    @mean_secondary.setter
    def mean_secondary(self, new):
        self._values[2] = new

    @property
    def std_secondary(self):
        return self._values[3]

    @std_secondary.setter
    def std_secondary(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        ans = be.empty((size, 2), dtype=float)
        N = 0
        while N < size:
            m1 = be.normal(self.mean_primary, self.std_primary, size=size)
            m2 = be.normal(self.mean_secondary, self.std_secondary, size=size)
            selected = (m1 >= m2) & (m2 >= 0)
            n = min(be.sum(selected), size-N)
            ans[N:N+n,0] = m1[selected][:n]
            ans[N:N+n,1] = m2[selected][:n]
            N += n
        return ans

    @property
    def norm(self):
        raise NotImplementedError

    def _prob(self, mass1, mass2):
        return be.where((mass1 >= mass2)*(mass2 >= 0), be.exp(-0.5*(mass1-self.mean_primary)**2/self.std_primary**2 -0.5*(mass2-self.mean_secondary)**2/self.std_secondary**2), 0.)

        return be.exp(-0.5*(mass1-self.mean_primary)**2/self.std_primary**2 -0.5*(mass2-self.mean_secondary)**2/self.std_secondary**2) / (2*pi * self.std_primary * self.std_secondary)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2):
        pdf = self.prob(mass1, mass2)
        return be.vector((
                pdf * (mass1-self.mean_primary)/self.std_primary**2,
                pdf * (-1/self.std_primary + (mass1-self.mean_primary)**2/self.std_primary**3),
                pdf * (mass2-self.mean_secondary)/self.std_secondary**2,
                pdf * (-1/self.std_secondary + (mass1-self.mean_secondary)**2/self.std_secondary**3),
            ),
            dtype=float
        )

    def _hessian(self, mass1, mass2):
        ans = be.empty((4,4), dtype=float)
        pdf = self.prob(mass1, mass2)

        ans[0,0] = pdf * (mass1-self.mean_primary)**2/self.std_primary**2 - pdf/self.std_primary**2
        ans[0,1] = ans[1,0] = -3 * pdf * (mass1-self.mean_primary)/self.std_primary**3 \
            + pdf * (mass1-self.mean_primary)**3/self.std_primary**5
        ans[0,2] = ans[2,0] = pdf * (mass1-self.mean_primary)/self.std_primary**2 * (mass2-self.mean_secondary)/self.std_secondary**2
        ans[0,3] = ans[3,0] = -pdf/self.std_secondary**2 \
            + pdf * (mass2-self.mean_secondary)**2/self.std_secondary**3 * (mass1-self.mean_primary)/self.std_primary**2
        ans[1,1] = pdf * 2/self.std_primary**2 \
            - 2 * pdf * (mass1-self.mean_primary)**2/self.std_primary**4 \
            + pdf * (mass1-self.mean_primary)**4/self.std_primary**6 \
            - 3 * pdf * (mass1-self.mean_primary)**2/self.std_primary**4
        ans[1,2] = ans[2,1] = -pdf/self.std_primary**2 \
            + pdf * (mass1-self.mean_primary)**2/self.std_primary**3 * (mass2-self.mean_secondary)/self.std_secondary**2
        ans[1,3] = ans[3,1] = pdf/(self.std_primary*self.std_secondary) \
            - pdf * (mass1-self.mean_primary)/(self.std_secondary*self.std_primary**3) \
            - pdf * (mass2-self.mean_secondary)/(self.std_primary*self.std_secondary**3) \
            + pdf * (mass1-self.mean_primary)**2/self.std_primary**3 * (mass2-self.mean_secondary)**2/self.std_secondary**3
        ans[2,2] = pdf * (mass2-self.mean_secondary)**2/self.std_secondary**2 - pdf/self.std_secondary**2
        ans[2,3] = ans[3,2] = -3 * pdf * (mass2-self.mean_secondary)/self.std_secondary**3 \
            + pdf * (mass2-self.mean_secondary)**3/self.std_secondary**5
        ans[4,4] = pdf * 2/self.std_secondary**2 \
            - 2 * pdf * (mass2-self.mean_secondary)**2/self.std_secondary**4 \
            + pdf * (mass2-self.mean_secondary)**4/self.std_secondary**6 \
            - 3 * pdf * (mass2-self.mean_secondary)**2/self.std_secondary**4

        return ans

class AstroComponentMass(MassDistribution):
    _params = ('min_primary', 'max_primary', 'mean_secondary', 'std_secondary', 'power_idx')

    def _init_values(self, min_primary=5.0, max_primary=100.0, mean_secondary=1.4, std_secondary=0.1, power_idx=-2.3):
        self._values = be.vector([min_primary, max_primary, mean_secondary, std_secondary, power_idx], dtype=float)

    @property
    def min_primary(self):
        return self._values[0]

    @min_primary.setter
    def min_primary(self, new):
        self._values[0] = new

    @property
    def max_primary(self):
        return self._values[1]

    @max_primary.setter
    def max_primary(self, new):
        self._values[1] = new

    @property
    def mean_secondary(self):
        return self._values[2]

    @mean_secondary.setter
    def mean_secondary(self, new):
        self._values[2] = new

    @property
    def std_secondary(self):
        return self._values[3]

    @std_secondary.setter
    def std_secondary(self, new):
        self._values[3] = new

    @property
    def power_idx(self):
        return self._values[4]

    @power_idx.setter
    def power_idx(self, new):
        self._values[4] = new

    def _rvs(self, size=1):
        return be.matrix(
            zip(
                negative_power(self.min_primary, self.max_primary, self.power_idx, size=size),
                be.normal(self.mean_secondary, self.std_secondary, size=size)
            ),
            dtype=float
        )

    def _prob(self, mass1, mass2):
        ans = 1.
        ans *= (self.min_primary <= mass1)*(mass1 < self.max_primary) * (self.power_idx+1) * mass1**self.power_idx / (self.max_primary**(self.power_idx+1) - self.min_primary**(self.power_idx+1))
        ans *= be.exp(-0.5*(mass2-self.mean_secondary)**2/self.std_secondary**2) / ((2*pi)**0.5*self.std_secondary)
        return ans

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2):
        pdf = self.prob(mass1, mass2)
        return be.vector((
            +pdf * (self.power_idx+1)*self.min_primary**self.power_idx / (self.max_primary**(self.power_idx+1)-self.min_primary**(self.power_idx+1)),
            -pdf * (self.power_idx+1)*self.max_primary**self.power_idx / (self.max_primary**(self.power_idx+1)-self.min_primary**(self.power_idx+1)),
            +pdf * (mass2-self.mean_secondary)/self.std_secondary**2,
            -pdf / self.std_secondary + pdf * (mass2-self.mean_secondary)**2 / self.std_secondary**3,
            +pdf / (self.power_idx+1) + pdf * be.log(mass1) - pdf * (self.max_primary**(self.power_idx+1)*be.log(self.max_primary) - self.min_primary**(self.power_idx+1)*be.log(self.min_primary)) / (self.max_primary**(self.power_idx+1) - self.min_primary**(self.power_idx+1)),
            ),
            dtype=float,
        )

    def _hessian(self, mass1, mass2):
        raise NotImplementedError

class ParetoFlatComponentMass(MassDistribution):
    """Generate BBH masses. Primary is a power law (-2.3) in 5 - 50 and the secondary is generated according to a uniform distribution in mass ratio such that min_q < q < m_max_q.
    """
    _params = ('min_bh_mass', 'max_bh_mass', 'min_q', 'max_q', 'power_idx')

    def _init_values(self, min_bh_mass=10, max_bh_mass=100., min_q=0.1, max_q=1.0, power_idx=-2.3):
        assert 0 < min_q
        assert min_q <= max_q
        assert max_q <= 1
        self._values = be.vector([min_bh_mass, max_bh_mass, min_q, max_q, power_idx], dtype=float)

    @property
    def min_bh_mass(self):
        return self._values[0]

    @min_bh_mass.setter
    def min_bh_mass(self, new):
        self._values[0] = new

    @property
    def max_bh_mass(self):
        return self._values[1]

    @max_bh_mass.setter
    def max_bh_mass(self, new):
        self._values[1] = new

    @property
    def min_q(self):
        return self._values[2]

    @min_q.setter
    def min_q(self, new):
        self._values[2] = new

    @property
    def max_q(self):
        return self._values[3]

    @max_q.setter
    def max_q(self, new):
        self._values[3] = new

    @property
    def power_idx(self):
        return self._values[4]

    @power_idx.setter
    def power_idx(self, new):
        self._values[4] = new

    def _rvs(self, size=1):
        m1 = negative_power(self.min_bh_mass, self.max_bh_mass, self.power_idx, size=size)
        return be.matrix(
            zip(
                m1,
                be.random(low=self.min_q, high=self.max_q, size=size) * m1
            ),
            dtype=float
        )

    def _prob(self, mass1, mass2):
        q = 1.0*mass2/mass1
        ans = mass1**self.power_idx ### pareto distrib for mass1
        ans *= (self.min_bh_mass <= mass1)*(mass1 < self.max_bh_mass)*(self.min_q<=q)*(q<self.max_q)
        ans *= (self.power_idx+1)/(self.max_bh_mass**(self.power_idx+1) - self.min_bh_mass**(self.power_idx+1))
        ans *= 1./mass1 ### jacobian between flat-in-q and mass2
        ans *= 1./(self.max_q - self.min_q) ### prior normalization from q
        return ans

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2):
        pdf = self.prob(mass1, mass2)
        return be.vector((
                +pdf * (self.power_idx+1)*self.min_bh_mass**self.power_idx / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1)),
                -pdf * (self.power_idx+1)*self.max_bh_mass**self.power_idx / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1)),
                +pdf / (self.max_q - self.min_q),
                -pdf / (self.max_q - self.min_q),
                +pdf / (self.power_idx+1) + pdf * be.log(mass1) - pdf * (self.max_bh_mass**(self.power_idx+1) * be.log(self.max_bh_mass) - self.min_bh_mass**(self.power_idx)*be.log(self.min_bh_mass)) / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1))
            ),
            dtype=float,
        )

    def _hessian(self, mass1, mass2):
        raise NotImplementedError

#-------------------------------------------------
# Mass Generators conditioned on Redshift
#-------------------------------------------------

class ParetoFlatComponentMassConditionedOnRedshift(ParetoFlatComponentMass):
    """a "smart" distribution that combines the ParetoFlatComponentMass distribution with the FishbachRedshift distribution in an attempt to sample more efficiently
    """
    _required = ('z',)

    _max_redshift = 2.0 ### the maximum redshift for which we expect our approximation in z2min_bh_mass to hold

    def _init_values(self, min_bh_mass=10, max_bh_mass=100., min_q=0.1, max_q=1.0, power_idx=-2.3):
        ParetoFlatComponentMass._init_values(self, min_bh_mass=min_bh_mass, max_bh_mass=max_bh_mass, min_q=min_q, max_q=max_q, power_idx=power_idx)
        self._static_min_bh_mass = min_bh_mass ### hold this outside of the parent object's structures so we can swap it back and forth
        self._update()

    def z2min_bh_mass(self, z):
        """a helper function that figures out the minimum BH mass from a redshift

        **NOTE**, this is a naive (hopefully conservative) estimate based on a larger monte-carlo study
        """
        assert z <= self._max_redshift, 'our approximate scaling relation for the minimum BH mass as a function of z is only valid up to z=%.3d'%self._max_redshift
        return min(self.max_bh_mass, max(self._static_min_bh_mass, 50 * z)) ### this relation should hopefully be valid up to z~1.5

    def _juggle_min_bh_mass_(self, foo, *args, **kwargs):
        args, z = args[:-1], args[-1] ### this should always be true
        try:
            self.min_bh_mass = self.z2min_bh_mass(z)
            return foo(self, *args, **kwargs) ### call the parent class's method
        except: ### do this so we always re-set the component mass
            raise
        finally:
            self.min_bh_mass = self._static_min_bh_mass ### re-set this to the original value

    def _rvs(self, size=1, *args):
        """args is specified like this for syntactic reasons, but it should just be the redshift
        """
        z = args[0]
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._rvs, size, z)

    def _prob(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._pdf, mass1, mass2, z)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._jacobian, mass1, mass2, z)

    def _hessian(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._hessian, mass1, mass2, z)

