"""a module that support classes that build 2D mass distributions out of 1D mass distributions and pairing probabilities
"""
__author__ = "Reed Essick <reed.essick@gmail.com>, Maya Fishbach <maya.fishbach@ligo.org>"

#-------------------------------------------------

from scipy.special import betaln as scipy_betaln

from gwdistributions import backends as be
from .joint import MassDistribution

#-------------------------------------------------

### more general pairing probabilities that draw mass1 and mass2 separately from a SingleMassDistribution

class RandomMassPairing(MassDistribution):
    '''an object that automatically combines single-mass distributions to create a joint distribution according to a pairing probability
    '''
    _extra_params = ()
    _extra_required = ()

    def _init_values(self, massdist):
        self._massdist = massdist
        self._values = massdist._values ### NOTE: we hold on to a shared reference here!
        self._params = self.massdist._params + self._extra_params ### store all the params, including the massdist params
        self._required = self.massdist._required + self._extra_required

    @property
    def massdist(self):
        return self._massdist

    @property
    def params(self):
        return self._values ### NOTE: we have to redeclare this because (for some reason) it is not inherited correctly from SamplingDistribution

    @params.setter
    def params(self, new):
        assert len(new) == len(self.params)
        self._values = new
        self.massdist.params = new[:self.massdist.Nparams]

    def update(self, **kwargs):
        for name, param in kwargs.items():
            if name in self._extra_params:
                self[name] = param
            elif name in self.massdist._params:
                self.massdist[name] = self[name] = param
            else:
                pass

    ### sampling
    def _rvs(self, size=1.):
        '''draw samples via rejection sampling
        '''
        ans = be.empty((size, 2), dtype=float)
        N = 0
        while N < size:
            # draw from SinglMassDistribution
            mas = self.massdist._rvs(size=size)
            mbs = self.massdist._rvs(size=size)

            # map into m1, m2 based on relative size
            m1s = be.max(mas, mbs)
            m2s = be.min(mas, mbs)

            # compute weights
            weights = self.pairing(m1s, m2s)
            weights /= be.sum(weights) ### approximately normalize the weights

            # draw random subset from weights
            selected = be.random(size=size) < weights

            ### record the samples
            n = min(be.sum(selected), size-N)

            ans[N:N+n, 0] = m1s[selected][:n]
            ans[N:N+n, 1] = m2s[selected][:n]

            # increment the number of samples found
            N += n

        return ans

    ### pairing functions
    def pairing(self, m1, m2):
        return be.exp(self.logpairing(m1, m2))

    def logpairing(self, m1, m2):
        return be.where(m1 >= m2, 0., -be.infty) ### requirement that m1 >= m2

    ### probability distributions
    def _logprob(self, m1, m2):
        return self.massdist.logprob(m1) + self.massdist.logprob(m2) + self.logpairing(m1, m2)

    def _jacobian(self, *args):
        raise NotImplementedError

    def _hessian(self, *args):
        raise NotImplementedError

#------------------------

class PowerLawQMassPairing(RandomMassPairing):
    '''takes in any SingleMassDistribution and adds a parameter (betaq), so that the two masses drawn separately from the SingleMassDistribution 
are paired according to (m2/m1)**betaq, where m1 >= m2
    '''
    _extra_params = ('betaq',)

    def _init_values(self, massdist, betaq):
        RandomMassPairing._init_values(self, massdist)
        self._betaq = betaq
        self._values = be.vector(tuple(massdist._values) + (betaq,), dtype=float)

    @property
    def betaq(self):
        return self._betaq

    ### pairing functions
    def logpairing(self, m1, m2):
        return self.betaq * (be.log(m2) - be.log(m1))

#------------------------

class PowerLawQMtotMassPairing(RandomMassPairing): 
    '''takes in any SingleMassDistribution and adds parameters (betaq and betam), so that the two masses drawn from separately from the SingleMassDistribution are paired according to (m2/m1)**betaq * (m1+m2)**betam, where m1 >= m2
    '''
    _extra_params = ('betaq', 'betam')
    
    def _init_values(self, massdist, betaq, betam):
        RandomMassPairing._init_values(self, massdist)
        self._betaq = betaq
        self._betam = betam
        self._values = be.vector(tuple(self.massdist._values) + (betaq, betam), dtype=float)

    @property
    def betaq(self):
        return self._betaq

    @property
    def betam(self):
        return self._betam

    ### pairing functions
    def logpairing(self, m1, m2):
        return self.betaq * (be.log(m2) - be.log(m1)) + self.betam * (m1+m2)

#------------------------

class MixtureMassPairing(RandomMassPairing):
    '''takes in any SingleMassDistribution and adds parameters f, betaq, qmin, mu, sigma so that the pairing probability is given by
    f * (betaq+1)/(1-qmin**(betaq+1)) * (m2/m1)**betaq + (1-f) * Gamma(a+b)/(Gamma(a)*Gamma(b)) * q**(a-1) * (1-q)**b-1
with
    a = (1-mu)*mu**2/sigma**2 - mu
    b = a * (1-mu)/mu
    '''
    _extra_params = ('f', 'betaq', 'qmin', 'mu', 'sigma')

    def __init__(self, massdist, f, betaq, qmin, mu, sigma):
        RandomMassPairing(self, massdist)
        self._f = f
        self._betaq = betaq
        self._qmin = qmin
        self._mu = mu
        self._sigma = sigma
        self._value = be.vector(tuple(massdist._values) + (f, betaq, qmin, mu, sigma), dtype=float)

    @property
    def f(self):
        return self._f

    @property
    def betaq(self):
        return self._betaq

    @property
    def qmin(self):
        return self._qmin

    @property
    def mu(self):
        return self._mu

    @property
    def sigma(self):
        return self._sigma

    ### pairing functions
    def logpairing(self, m1, m2):
        if (self.mu - self.mu**2 > self.sigma**2) and (0 <= self.f) and (self.f <= 1): ### sane parameters
            return _logpairing_mixture(m1, m2, self.f, self.betaq, self.qmin, self.mu, self.sigma)
        else:
            return -be.infty

def _logpairing_mixture(m1, m2, f, beta, qmin, mu, sigma):
    q = m2/m1

    if beta == -1:
        logpower = be.where(q >= qmin, be.log(f) + be.log(qmin) - be.log(q), -be.infty)
    else:
        logpower = be.where(q >= qmin, be.log(f) + be.log(be.abs(beta+1.)) - be.log(be.abs(1.-qmin**(beta+1.))) + beta*be.log(q), -be.infty)

    logbeta = be.log(1-f) + logbeta_distribution(q, mu, sigma)
    return be.where(logpower > logbeta, logpower + be.log(1 + be.exp(logbeta-logpower)), logbeta + be.log(1 + be.exp(logpower-logbeta)))

def logbeta_distribution(x, mu, sigma):
    a = (1.-mu) * mu**2 / sigma**2 - mu
    b = a * (1. - mu) / mu
    return (a-1.)*be.log(x) + (b-1.)*be.log(1.-x) - scipy_betaln(a, b)
