"""a module that houses basic single-mass distributions, from which we can construct more complicated joint distributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>, Maya Fishbach <maya.fishbach@ligo.org>"

#-------------------------------------------------

import copy
import warnings

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions import backends as be

#------------------------

### set up a custom warning for the mass orderings so that we can make Python always print that warning

class MassOrderWarning(RuntimeWarning):
    pass

warnings.simplefilter('always', MassOrderWarning)

#-------------------------------------------------

class Mass1Distribution(SamplingDistribution):
    """a SamplingDistribution that describes how a single mass (mass1) is distributed. Can be combined through pairing functions to make regular MassDistribution objects, or can be used with a Mass2Distribution to emulate the behavior of a MassDistribution
    """
    _variates = ('mass1',)
    _required = ()
    _params = ()

class Mass2Distribution(SamplingDistribution):
    """a SamplingDistribution that describes how a single mass (mass2) is distributed, conditioned on (mass1). Can be combined with a Mass1Distribution to be equivalent to a regular MassDistribution
    """
    _variates = ('mass2',)
    _required = ('mass1',)
    _params = ()

#-------------------------------------------------

class NPiecePowerLawMass1(Mass1Distribution):
    """a simple power law with hard cut-off and abrupt transitions between components
    mass1 ~ \prod_0^{N-1} (break_i <= mass1)*( (mass1 < break_(i+1)) * (mass_1/break_1) + (mass1 >= break_(i+1))*(break_(i+1)/break_i) )**alpha_i
    """
    _normalized = True

    def _init_values(self, warn=True, mmin=1, mmax=100, alpha0=-3, **kwargs):
        '''dynamically parse kwargs to figure out how many power-law components there are
        '''
        self.warn = warn ### whether to print warnings about mass orderings

        # check how many kwargs we got
        self._params = ['mmin', 'alpha0']
        self._values = [mmin, alpha0]
        N = 1
        self._break_mass_idxs = [0]
        while len(kwargs):
            try:
                params = ['break%d'%N, 'alpha%d'%N]
                values = [kwargs.pop(param) for param in params]
                self._break_mass_idxs.append(len(self._params)) ### record where the next break mass lives
                self._params += params
                self._values += values
                N += 1
            except KeyError:
                break

        if len(kwargs):
            raise RuntimeError('could not interpret all kwargs!')

        self._Ncomponents = len(self._params)//2

        self._break_mass_idxs.append(len(self._params)) ### record where the next break mass will go
        self._params.append('mmax')
        self._values.append(mmax)

        ### cast these to be the types we expect more generally
        self._params = tuple(self._params)
        self._values = be.vector(self._values, dtype=float)

        # check the mass ordering
        self._check_break_masses()

    @property
    def break_masses(self):
        ''' grabs just the break masses from _values 
        '''
        return self._values[self._break_mass_idxs]

    def _check_break_masses(self):
        ''' checks to make sure the break masses are in ascending order and creates a flag
        which is 1 if break masses are in ascending order and 0 otherwise.
        '''
        brk_masses = self.break_masses
        self._ascending_break_masses = all(brk_masses[i+1] >= brk_masses[i] for i in range(len(brk_masses)-1))
        if self.warn and (not self._ascending_break_masses):
            warnings.warn('break masses not in ascending order! Probability will be set to zero', MassOrderWarning)

    @property
    def params(self): # redefine this here so that we can overwrite the setter
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==len(self._values)
        self._values[:] = new
        self._check_break_masses() ### update the flag for sensible break masses

    def update(self, **kwargs):
        '''update the parameters of this sampling distribution'''
        for name, param in kwargs.items():
            self[name] = param    
        self._check_break_masses() ### update the flag for sensible break masses

    def _rvs(self, size=1):
        segs = be.vector(self._norm_segs())
        segs /= be.sum(vector)
        segs = be.cumsum(segs)
        inds = be.arange(len(segs))

        ans = be.empty((size,1), dtype=float)
        for ind, r in enumerate(be.random(size=size)):
            i = inds[segs > r][0] ### pick the smallest index as the "segment" where this sample lives
            ans[ind,0] = self._rvs_segment(*self._values[2*i:2*i+3], size=1)

        return ans

    @staticmethod
    def _rvs_segment(mmin, alpha, mmax, size=1):
        r = be.random(size=size)
        if alpha == -1:
            ans = mmin * be.exp(r * (be.log(mmax) - be.log(mmin)))
        else:
            alpha1 = alpha+1
            ans = (r * (mmax**alpha1 - mmin**alpha1)/alpha1 + mmin**alpha1)**(1./alpha1)
        return ans

    def _prob(self, *args):
        return be.exp(self._logprob(*args))

    def _norm_segs(self):
        if self.warn and (not self._ascending_break_masses):
            warnings.warn('break masses are not in ascending order! normalization may be nonsensical!', MassOrderWarning)

        segs = []
        for i in range(self._Ncomponents): ### note, this may give nonsensical results if mmin > mmax
            mmin, alpha, mmax = self._values[2*i:2*i+3]
            if alpha == -1:
                segs.append( be.log(mmax) - be.log(mmin) )
            else:
                alpha1 = alpha+1
                segs.append( (mmax**alpha1 - mmin**alpha1) / alpha1 )
        return segs

    def _logprob(self, mass1):

        # first check that mass hyperparaemters are in ascending order
        if not self._ascending_break_masses:
            return mass1 - be.infty ### will make sure we have the right shape (based on mass1) and the right value (-infty)

        # now check the absolute boundaries
        mmin = self._values[0] ### the absolute minimum mass
        mmax = self._values[2*self._Ncomponents] ### look this up in this way because children may add other parameters to the end
        logpdf = be.where((mmin <= mass1)*(mass1 < mmax), 0., -be.infty) ### if mass is outside bounds, zero the probability


        if be.any(logpdf > -be.infty): ### if we have any masses that are within the bounds
            for i in range(self._Ncomponents):
                mmin, alpha, mmax = self._values[2*i:2*i+3]

                logpdf += be.where((mmin <= mass1), be.where(mass1 < mmax, alpha*(be.log(mass1) - be.log(mmin)), alpha*(be.log(mmax) - be.log(mmin))), 0.)

            return logpdf - be.log(be.sum(be.vector(self._norm_segs())))

        else: ### zero probabilities for everyone, so we don't have to do any other work!
            return logpdf

class PowerLawMass1(NPiecePowerLawMass1):
    """a special case of the NPiecePowerLaw that we define for convenience
    """

    def _init_values(self, warn=True, mmin=1, mmax=100, alpha0=-3):
        NPiecePowerLawMass1._init_values(self, warn=warn, mmin=mmin, mmax=mmax, alpha0=alpha0)

#------------------------

class NPieceBandpassPowerLawMass1(NPiecePowerLawMass1):
    """a simple power law that's band-passed instead of with hard cut-offs; still retains abrupt transitions between components, though
    """
    _normalized = False

    def _init_values(self, minknee=1, minn=6, maxknee=40, maxn=6, **kwargs):
        NPiecePowerLawMass1._init_values(self, **kwargs)
        self._params += ('minknee', 'minn', 'maxknee', 'maxn')
        self._values = be.vector(list(self._values)+[minknee, minn, maxknee, maxn], dtype=float)

    def _rvs(self, size=1, *required):
        ans = be.empty((size,1), dtype=float)
        N = 0
        while N < size:
            ### draw from underlying piece-wise power law
            mass1 = NPiecePowerLawMass1._rvs(size=size-N)[:,0]

            ### keep these according to rejection sampling based on the bandpass
            selected = be.random(size) > be.exp(self._logbandpass(mass1))

            ### fill in the selected samples
            n = min(be.sum(selected), size-N)
            ans[N:N+n] = mass1[selected][:n]

            N += n

        return ans

    def _prob(self, *args):
        return be.exp(self._logprob(*args))

    def _logprob(self, mass1):
        return NPiecePowerLawMass1._logprob(self, mass1) + self._logbandpass(mass1)

    def _logbandpass(self, mass1):
        minknee, minn, maxknee, maxn = self._values[-4:]
        return -be.where(mass1 > 0, be.log(1 + (minknee/mass1)**minn) + be.log(1 + (mass1/maxknee)**maxn), -be.infty)

class BandpassPowerLawMass1(NPieceBandpassPowerLawMass1):
    """a special case of NPieceBandpassPowerLaw defined for convenience
    """

    def _init_values(self, warn=True, minknee=1, minn=6, maxknee=40, maxn=6, mmin=1, mmax=100, alpha0=-3):
        NPieceBandpassPowerLawMass1._init_values(self, warn=warn, minknee=minknee, minn=minn, maxknee=maxknee, maxn=maxn, mmin=mmin, mmax=mmax, alpha0=alpha0)

#------------------------

class NPieceSmoothPowerLawMass1(NPiecePowerLawMass1):
    """a power law that's bandpassed instead of with hard cut-offs and smoothed transitions between components
    inherits from NPiecePowerLawMass1 so that we can access the sanity-checking for how the break masses are ordered
    """

    def _init_values(self, warn=True, mmin=1, mmax=100, alpha0=-3, **kwargs):
        '''dynamically parse kwargs to figure out how many power-law components there are
        '''
        self.warn = warn ### whether to display warnings about mass ordering

        # check how many kwargs we got
        self._params = ['mmin', 'alpha0']
        self._values = [mmin, alpha0]
        self._break_mass_idxs = [0]
        N = 1
        while len(kwargs):
            try:
                params = ['break%d'%N, 'alpha%d'%N, 'delta%d'%N]
                values = [kwargs.pop(param) for param in params]
                self._break_mass_idxs.append(len(self._params)) ### record where the break mass lives
                self._params += params
                self._values += values
                N += 1
            except KeyError:
                break
        if len(kwargs):
            raise RuntimeError('could not interpret all kwargs!')

        self._Ncomponents = 1 + (len(self._params)-2)//3

        self._break_mass_idxs.append(len(self._params)) ### record where the break mass lives
        self._params.append('mmax')
        self._values.append(mmax)

        # cast these to the expected types
        self._params = tuple(self._params)
        self._values = be.vector(self._values, dtype=float)

        # check the mass ordering
        self._check_break_masses()

    def _rvs(self, size=1, *required):
        raise NotImplementedError('implement rejection sampling?')

    def _prob(self, *args):
        return be.exp(self._logprob(*args))

    def _logprob(self, mass1):
        '''
        based off of the astropy function:
            https://docs.astropy.org/en/stable/api/astropy.modeling.powerlaws.SmoothlyBrokenPowerLaw1D.html
        '''

        # first check that mass hyperparaemters are in ascending order
        if not self._ascending_break_masses:
            return mass1 - be.infty ### will make sure we have the right shape (based on mass1) and the right value (-infty)

        ### check that we're within the total mass bounds
        mmin, alpha0, mbrk = self._values[:3]
        mmax = self._values[2+(self._Ncomponents-1)*3] ### look up this way so child classes that append more params don't break this
        logpdf = be.where((mmin <= mass1)*(mass1 < mmax), alpha0*(be.log(mass1) - be.log(mbrk)), -be.infty) ### first component

        ### only do more work if there is more work to be done
        if be.any(logpdf > -be.infty):
            for i in range(self._Ncomponents-1): ### the rest of them, treated as incremental filters
                mbrk, alpha, delta = self._values[2*i+2:2*i+5]
                logpdf += (alpha - alpha0) * delta * be.log(1.0 + (mass1/mbrk)**(1./delta))
                alpha0 = alpha

        return logpdf

#------------------------

class MatterMattersMass1(Mass1Distribution):
    """the single-mass distribution considered in Fishbach, Essick, Holz. Does Matter Matter? ApJ Lett 899, 1 (2020) : arXiv:2006.13178
    """
    _params = ('A', 'NSmin', 'NSmax', 'BHmin', 'BHmax', 'n0', 'n1', 'n2', 'n3', 'mbreak', 'alpha1', 'alpha2')

    def _init_values(self, A=0.5, NSmin=1, NSmax=2.5, BHmin=7, BHmax=40, n0=6, n1=6, n2=6, n3=6, mbreak=20, alpha1=-3, alpha2=-3):
        self._values = be.vector([A, NSmin, NSmax, BHmin, BHmax, n0, n1, n2, n3, mbreak, alpha1, alpha2], dtype=float)

    def _rvs(self, size=1, *required):
        raise NotImplementedError('implement rejection sampling?')

    def _prob(self, *args):
        return be.exp(self._logprob(*args))

    def _logprob(self, mass1):
        A, NSmin, NSmax, BHmin, BHmax, n0, n1, n2, n3, mbreak, alpha1, alpha2 = self._values
        return be.where(mass1 >= 0, -be.log(1 + (NSmin/mass1)**n0) + be.log(1.0 - A/((1 + (NSmax/mass1)**n1) * (1 + (mass1/BHmin)**n2))) \
            - be.log(1 + (mass1/BHmax)**n3) + be.where(mass1 <= mbreak, alpha1, alpha2)*(be.log(mass1) - be.log(mbreak)),
            - be.infty)

#-------------------------------------------------

### mass2 distributions conditioned on mass1

class PowerLawMass2(Mass2Distribution):
    """implements a power law distribution for m2 given m1
    """
    _normalized = True
    _params = ('mmin2', 'beta')

    def _init_values(self, mmin2, beta):
        self._values = be.vector([mmin2, beta], dtype=float)

    @property
    def beta(self):
        return self._values[1]

    @property
    def mmin2(self):
        return self._values[0]

    # sampling
    def _rvs(self, m1, size=1):
        return self.mmin2 + (m1 - self.mmin2) * be.random(size=(size,1))**(1./(self.beta+1))

    # mass distributions
    def _logprob(self, m2, m1):
        b1 = 1 + self.beta
        return be.where((m1 >= m2)*(m2 >= self.mmin2), be.log(b1) + self.beta*be.log(m2) - be.log(m1**b1 - self.mmin2**b1), -be.infty)
