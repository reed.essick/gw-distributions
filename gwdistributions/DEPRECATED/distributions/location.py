"""source distributions for how events are distributed through space
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import numpy as np

import scipy
from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d

from gwdistributions import backends as be
from .base import SamplingDistribution

#-------------------------------------------------

# Redshift distributions derived from SFRs
def _madau_dickinson_sfr(z, coef=0.015, num_exp=2.7, den_coef=1/2.9, den_exp=5.6):
    return coef * (1 + z)**num_exp / (1 + (den_coef * (1 + z))**den_exp)

# Form CDF over complete range of "validity"
_md_zvals = np.linspace(0, 10, 1000)
# CDF
_md_cdf = cumtrapz(_madau_dickinson_sfr(_md_zvals), _md_zvals)
# Interpolated inverse CDF
_md_invcdf = interp1d(_md_cdf, _md_zvals[:-1])
# Interpolated CDF
_md_cdf = interp1d(_md_zvals[:-1], _md_cdf)

# This is to hack around the painfully slow cosmology calculator from astropy
def init_cosmology(min_log10_z=-3, max_log10_z=1):
    """Damn astropy, you slow.
    """
    from astropy.cosmology import Planck15

    _com_z_cache = np.logspace(min_log10_z, max_log10_z, 1000)
    _com_z_cache = np.concatenate(([0], _com_z_cache))
    _com_cache = Planck15.comoving_volume(_com_z_cache).value

    _inv_com_cache = interp1d(_com_cache, _com_z_cache)
    com_v = interp1d(_com_z_cache, _com_cache)
    _com_d_cache = Planck15.comoving_distance(_com_z_cache).value
    com_d = lambda z: _com_cache[be.searchsorted(_com_d_cache, z)]
    return com_v, com_d, _inv_com_cache

# NOTE: One will have to recall this idiom to reset the internal interpolators
_com, _com_dist, _inv_com = init_cosmology()

#-------------------------------------------------

class RADecDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over RA and Dec
    """
    _variates = ("right_ascension", "declination",)

#-------------------------------------------------

class RandomRADec(RADecDistribution):
    """random event orientation
    """
    _parmas = ()

    def _rvs(self, size=1):
        ra = be.random(0, 2 * be.pi, size=size)
        dec = be.arcsin(be.random(-1, 1, size=size))
        incl = be.arccos(be.random(-1, 1, size=size))
        psi = be.random(0, be.pi, size=size)
        coa_phase = be.random(0, 2 * be.pi, size=size)
        return be.matrix(zip(ra, dec, incl, psi, coa_phase), dtype=float)

    def _prob(self, ra, dec, incl, psi, coa_pahse):
        ans = 1.
        ans *= (2*be.pi)**-1   ### normalizatoin from ra
        ans *= be.cos(dec)/2   ### dec
        ans *= be.sin(incl)/2  ### incl
        ans *= be.pi**-1       ### psi
        ans *= (2*be.pi)**-1   ### coa_phase
        return ans

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, ra, dec, incl, psi, coa_phase):
        return be.vector([], dtype=float)

    def _hessian(self, ra, dec, incl, psi, coa_phase):
        return be.matrix([[]], dtype=float)

class OptimalRADec(RADecDistribution):
    """assumes the detector arms are aligned with the coordinate system, so zenith is at RA=0, Dec=pi/2
    """
    _params = ()

    def _rvs(self, size=1):
        # Inclination, coa_phase (ignored), polarization (ignored)
        ans = be.zeros((size, len(self._variates)), dtype=float)
        ans[:,1] = be.pi/2
        return ans

    def _prob(self, ra, dec):
        if (ra==0) and (dec==be.pi/2):
            return 1.
        else:
            return 0.

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, ra, dec):
        return be.vector([], dtype=float)

    def _hessian(self, ra, dec):
        return be.matrix([[]], dtype=float)

#-------------------------------------------------

class DistanceDistribution(SamplingDistribution):
    """a child class used to declare things are distance distributions
    """
    _variates = ("distance",)

#--------------------------------------------------

class UniformVolumeDistance(DistanceDistribution):
    """
    Uniform in Euclidean volume. E. g. p(r) \propto r^2
    """
    _params = ('d_max',)

    def _init_values(self, d_max=1000):
        self._values = be.vector([d_max], dtype=float)

    @property
    def d_max(self):
        return self._values[0]

    @d_max.setter
    def d_max(self, new):
        self._values[0] = new

    def _rvs(self, size=1):
        return be.random(0., self.d_max**3, size=(size, 1))**(1./3)

    def _prob(self, d):
        return 3 * d**2 / self.d_max**3

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, d):
        return be.vector([-9*d**2 / self.d_max**4], dtype=float)

    def _hessian(self, d):
        return be.matrix([[36*d**2 / self.d_max**5]], dtype=float)

class UniformComovingDistance(DistanceDistribution):
    """uniform in comoving volume distribution, returning luminosity distance
    """
    _params = ()

    def _rvs(self, size=1):
        from astropy.cosmology import Planck15
        return Planck15.luminosity_distance(UniformComovingRedshift.rvs(self, size=(size, 1)))

    def _prob(self, d):
        """(dVc/dz) /  (dDL/dz)
        """
        raise NotImplementedError('''need access to some stuff from astropy.cosmo''')

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, d):
        return be.vector([], dtype=float)

    def _hessian(self, d):
        return be.matrix([[]], dtype=float)

#--------------------------------------------------

class RedshiftDistribution(SamplingDistribution):
    """a child class used to declare things are redshift distributions
    """
    _variates = ("z",)

#-------------------------------------------------

class UniformRedshift(RedshiftDistribution):
    """Uniform in redshift between zmin=0 (default) and zmax.
    """
    _params = ('z_min', 'z_max')

    def _init_values(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = be.vector([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return be.random(self.z_min, self.z_max, size=(size, 1))

    def _prob(self, z):
        return (self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        ans = (self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)**2
        return be.vector((ans, -ans), dtype=float)

    def _hessian(self, z):
        ans = 2*(self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)**3
        return be.matrix(((ans, -ans), (-ans, ans)), dtype=float)

class UniformLogRedshift(RedshiftDistribution):
    """uniform in log(1+z)
    """
    _params = ('z_min', 'z_max')

    def _init_values(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = be.vector([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return be.exp(be.random(be.log(1+self.z_max), be.log(1+self.z_min), size=(size, 1))) - 1

    def _prob(self, z):
        return (self.z_min<=z)*(z<self.z_max) / ((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min)))

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        pdf = self.prob(z)
        return be.vector((
                + pdf / ((be.log(1+self.z_max)-be.log(1+self.z_min))*(1+self.z_min))
                - pdf / ((be.log(1+self.z_max)-be.log(1+self.z_min))*(1+self.z_max))
            ),
            dtype=float,
        )

    def _hessian(self, z):
        ans = be.empty((2,2), dtype=float)
        ans[0,0] = 2./((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min))**3*(1+self.z_min)**2) - 1./((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min))**2*(1+self.z_min)**2)
        ans[0,1] = ans[1,0] = -2./((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min))**3*(1+self.z_max)*(1+self.z_min)),
        ans[1,1] = 2./((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min))**3*(1+self.z_max)**2) + 1./((1+z)*(be.log(1+self.z_max)-be.log(1+self.z_min))**2*(1+self.z_max)**2)
        return ans

class UniformComovingRedshift(RedshiftDistribution):
    """Obtain redshift distributed as uniform in comoving volume.
    """
    _params = ('z_min', 'z_max')

    def _init_values(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = be.vector([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new
        self._update()

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new
        self._update()

    def _update(self):
        self._fmin = _com(self.z_min) if z_min > 0 else 0
        self._fmax = _com(self.z_max)
        assert self._fmin < self._fmax

    def _rvs(self, size=1):
        f = be.random(self._fmin, self._fmax, size=(size, 1))
        return _inv_com(f)

    def _prob(self, z):
        """dVc/dz
        """
        raise NotImplementedError('''need access to dVc/dz from astropy.cosmo''')

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        """(dVc/dz|z)/(Vc(zmax) - Vc(zmin)**2 *(dVc/dz|zmin) , -(dVc/dz|z)/(Vc(zmax) - Vc(zmin))**2 *(dVc/dz|zmax)
        """
        raise NotImplementedError('''need access to dVc/dz from astropy.cosmo''')

    def _hessian(self, z):
        raise NotImplementedError('''need access to dVc/dz, d^2Vc/dz^2 from astropy.cosmo''')


class ParetoRedshift(RedshiftDistribution):
    """Obtain redshift distributed as (1+z)^index
    """
    _params = ('z_min', 'z_max', 'index')

    def _init_values(self, z_min=0, z_max=10, index=3):
        assert z_min < z_max
        if index < 0:
            assert z_min > 0
        self._values = be.vector([z_min, z_max, index], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    @property
    def index(self):
        return self._values[2]

    @index.setter
    def index(self, new):
        self._values[2] = new

    def _rvs(self, size=1):
        a, b, idx = self._values
        norm = (1 + b)**(idx+1) - (1 + a)**(idx+1)
        rvs = be.random(0, 1, size=(size, 1))
        return (norm * rvs + (1 + a)**(idx+1))**(1.0/(idx+1)) - 1
        # Despite its name, it can handle positive powers too.
        #return (negative_power(z_min, z_max, index),)

    def _prob(self, z):
        return (self.index+1) * (1+z)**self.index / ((1 + self.z_max)**(self.index+1) - (1 + self.z_min)**(self.index+1))

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        m = 1 + self.z_min
        M = 1 + self.z_max
        n1 = self.index + 1
        z1 = 1 + z
        return be.vector((
            +n1**2 * z1**self.index * m**self.index / (M**n1 - m**n1)**2, ### dp/dz_min
            -n1**2 * z1**self.index * M**self.index / (M**n1 - m**n1)**2, ### dp/dz_max
            z1**self.index / (M**n1 - m**n1) + n1*z1**self.index*be.log(z1)/(M**n1 - m**n1) - n1*z1**self.index/(M**n1 - m**n1)**2 * (M**n1 * be.log(M) - m**n1 * be.log(m)), ### dp/dindex
        ), dtype=float)        

    def _hessian(self, z):
        m = 1 + self.z_min
        M = 1 + self.z_max
        n1 = self.index + 1
        z1 = 1 + z

        ans = be.empty((3,3), dtype=float)
        ans[0,0] = n1**2*z1**self.index * self.index * m**(self.index-1) / (M**n1 - m**n1)**2 \
            + 2*n1**3 * z1**self.index * m**(2*self.index) / (M**n1 - m**n1)**3
        ans[0,1] = ans[1,0] = -2 * n1**3 * z1**self.index * M**self.index * m**self.index / (M**n1 - m**n1)**3
        ans[0,2] = ans[2,0] = + 2*n1 * z1**self.index * m**self.index / (M**n1 - m**n1)**2 \
            + n1**2 * z1**self.index * be.log(z1) * m**self.index / (M**n1 - m**n1)**2 \
            + n1**2 * z1**self.index * m**self.index * be.log(m) / (M**n1 - m**n1)**2 \
            + 2*n1**2 * z1**self.index * m**self.index * (M**n1 * be.log(M) - m**n1 * be.log(m))/ (M**n1 - m**n1)**3
        ans[1,1] = - n1**2*z1**self.index * self.index * M**(self.index-1) / (M**n1 - m**n1)**2 \
            + 2*n1**3 * z1**self.index * M**(2*self.index) / (M**n1 - m**n1)**3
        ans[1,2] = ans[2,1] = - 2*n1 * z1**self.index * M**self.index / (M**n1 - m**n1)**2 \
            - n1**2 * z1**self.index * be.log(z1) * M**self.index / (M**n1 - m**n1)**2 \
            - n1**2 * z1**self.index * M**self.index * be.log(M) / (M**n1 - m**n1)**2 \
            + 2*n1**2 * z1**self.index * M**self.index * (M**n1 * be.log(M) - m**n1 * be.log(m))/ (M**n1 - m**n1)**3
        ans[2,2] = z1**self.index * be.log(z1) / (M**n1 - m**n1) \
            - z1**self.index * (M**n1 * be.log(M) - m**n1 * be.log(m)) / (M**n1 - m**n1) \
            + z1**self.index * be.log(z1) / (M**n1 - m**n1) \
            + n1 * z1**self.index * be.log(z1)**2 / (M**n1 - m**n1) \
            - n1 * z1**self.index * be.log(z1) * (M**n1 * be.log(M) - m**n1 * be.log(m)) / (M**n1 - m**n1)**2 \
            - z1**self.index * (M**n1 * be.log(M) - m**n1 * be.log(m)) / (M**n1 - m**n1)**2 \
            - n1 * z1**self.index * be.log(z1) * (M**n1 * be.log(M) - m**n1 * be.log(m)) / (M**n1 - m**n1)**2 \
            - n1 * z1**self.index * (M**n1 * be.log(M)**2 - m**n1 * be.log(m)**2) / (M**n1 - m**n1)**2 \
            + 2 * n1 * z1**self.index * (M**n1 * be.log(M) - m**n1 * be.log(m))**2 / (M**n1 - m**n1)**3

        return ans

class FishbachRedshift(RedshiftDistribution):
    """Redshift distribution ``dV_dz*(1+z)**(a-1)``.
    """
    _params = ('z_max', 'a')

    def _init_values(self, z_max=2, a=3):
        self._values = be.vector([z_max, a], dtype=float)
        self._update()

    @property
    def z_max(self):
        return self._values[0]

    @z_max.setter
    def z_max(self, new):
        self._values[0] = new
        self._update()

    @property
    def a(self):
        return self._values[1]

    @a.setter
    def a(self, new):
        self._values[1] = new
        self._update()

    def _update(self):
        from astropy.cosmology import Planck15
        import astropy.units as u

        zi = np.expm1(np.linspace(np.log(1), np.log(1+self.z_max), 1024))
        pi = Planck15.differential_comoving_volume(zi).to(u.Gpc**3/u.sr).value*(1+zi)**(self.a-1)
        ci = cumtrapz(pi, zi, initial=0)
        pi /= ci[-1]
        ci /= ci[-1]

        self._invcdf_interpolator = interp1d(ci, zi)
        self._pdf_interpolator = interp1d(zi, pi)

    def _rvs(self, size=1):
        return self._invcdf_interpolator(np.random.random(size=(size, 1)))

    def _prob(self, z):
        return self._pdf_interpolator(z)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        raise NotImplementedError("haven't computed Jacobian yet")

    def _hessian(self, z):
        return NotImplementedError("haven't computed Hessian yet")

class MadauDickinsonRedshift(RedshiftDistribution):
    """Obtain redshift distributed as the SFR inferred from Madau and Dickinson (2014), eqn 15.
    """
    _params = ('z_min', 'z_max')

    def _init_values(self, z_min=0, z_max=10):
        assert z_min < z_max
        if z_max >= _md_zvals[-1]:
            raise ValueError("Invalid maximum redshift (z={0:f}), MD SFR only valid up to z ~ 8-10.".format(z_max))
        self._values = np.array([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        n1 = _md_cdf(self._z_max)
        n2 = _md_cdf(self._z_min)
        rv = np.random.uniform(n1, n2, size=(size, 1))
        return _md_invcdf(rv)

    def _prob(self, z):
        return _madau_dickinson_sfr(z) / (_md_cdf(self._z_max) - _md_cdf(self._z_min))

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, z):
        base = _madau_dickinson_sfr(z) / (_md_cdf(self._z_max) - _md_cdf(self._z_min))**2
        return np.array((
                +base * _madau_dickinson_sfr(self.z_min),
                -base * _madau_dickinson_sfr(self.z_max)
            ),
            dtype=float
        )

    def _hessian(self, z):
        raise NotImplementedError('''need access to the second derivative of the SFR''')
