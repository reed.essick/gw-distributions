"""housing for distributions for single-event GW parameters
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import *
from .external import *

from .time import *
from .eccentricity import *
from .location import *
from .mass import *
from .orientation import *
from .spin import *

#-------------------------------------------------
# Reference instantiations
#-------------------------------------------------

# Astrophysical object binary generators
#UNIFORM_BNS_COMP_MASS = UniformComponentMass(min_primary=1.0, max_primary=2.0, min_secondary=1.0, max_secondary=2.0)
#UNIFORM_BBH_COMP_MSS = UniformComponentMass(min_primary=3.0, max_primary=100.0, min_secondary=3.0, max_secondary=100.0)
#UNIFORM_NSBH_COMP_MASS = UniformComponentMass(min_primary=3.0, max_primary=50.0, min_secondary=1.0, max_secondary=2.0)

# From Ozel and Freire (2016): https://arxiv.org/pdf/1603.02698.pdf
#NORMAL_BNS_COMP_MASS = NormalComponentMass(mean_primary=1.33, std_primary=0.09, mean_secondary=1.33, std_secondary=0.09)
#ASTRO_NSBH_COMP_MASS = AstroComponentMass(min_primary=3.0, max_primary=50.0, mean_secondary=1.33, std_secondary=0.09, power_idx=-2.3)

# A few representative spin distributions
# NSBH, NS non-spinning
#ASTRO_SPIN = UniformSpin(a2_max=0.0)
