"""source distributions for how events masses are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from numpy import pi

from .base import SamplingDistribution
from gwdistributions import backends as be

#-------------------------------------------------

class SpinDistribution(SamplingDistribution):
    """a child class used to declare things are spin distributions
    """
    _variates = ("spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z")

#-------------------------------------------------

class ZeroSpin(SpinDistribution):
    _params = ()

    def _rvs(self, size=1):
        return be.zeros((size, 6), dtype=float)

    def _prob(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        if (spin1x==0) and (spin1y==0) and (spin1z==0) and (spin2x==0) and (spin2y==0) and (spin2z==0):
            return 1.
        else:
            return 0.

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        return be.vector([], dtype=float)

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        return be.matrix([[]], dtype=float)

class UniformSpin(SpinDistribution):
    """Generate isotropically distributed spins. The spin components magnitudes are uniform.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def _init_values(self, a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
        self._values = be.vector([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        a1 = be.random(low=self.a1_min, high=self.a1_max, size=size)
        a2 = be.random(low=self.a2_min, high=self.a2_max, size=size)

        th1 = be.arcsin(be.random(low=-1, high=1, size=size))
        th2 = be.arcsin(be.random(low=-1, high=1, size=size))

        ph1 = be.random(low=0, high=2*pi, size=size)
        ph2 = be.random(low=0, high=2*pi, size=size)

        costh1 = be.cos(th1)
        s1x = a1 * costh1 * be.cos(ph1)
        s1y = a1 * costh1 * be.sin(ph1)
        s1z = a1 * be.sin(th1)

        costh2 = be.cos(th2)
        s2x = a2 * costh2 * be.cos(ph2)
        s2y = a2 * costh2 * be.sin(ph2)
        s2z = a2 * be.sin(th2)

        return be.matrix(zip(s1x, s1y, s1z, s2x, s2y, s2z), dtype=float)

    def _prob(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = (spin1x**2 + spin1y**2 + spin1z**2)**0.5
        a2 = (spin2x**2 + spin2y**2 + spin2z**2)**0.5
        return (self.a1_min <= a1)*(a1 < self.a1_max)*(self.a2_min <= a2)*(a2 < self.a2_max) / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return be.vector((
                - pdf/(self.a1_max-self.a1_min),
                - pdf/(self.a2_max-self.a2_min),
                + pdf/(self.a1_max-self.a1_min),
                + pdf/(self.a2_max-self.a2_min),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)

        ans = be.matrix((4,4), dtype=float)

        ans[0,0] = 2*pdf/(self.a1_max-self.a1_min)**2
        ans[0,1] = ans[1,0] = pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[0,2] = ans[2,0] = -2*pdf / (self.a1_max-self.a1_min)**2
        ans[0,3] = ans[3,0] = -pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[1,1] = 2*pdf/(self.a2_max-self.a2_min)**2
        ans[1,2] = ans[2,1] = -pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[1,3] = ans[3,1] = -2*pdf / (self.a2_max-self.a2_min)**2
        ans[2,2] = 2*pdf/(self.a1_max-self.a1_min)**2
        ans[2,3] = ans[3,2] = pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[3,3] = 2*pdf/(self.a2_max-self.a2_min)**2

        return ans

class UniformAlignedSpin(SpinDistribution):
    """Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed uniformly up to maximum.
    """
    _params = ('a1_max', 'a2_max')

    def _init_values(self, a1_max=1.0, a2_max=1.0):
        self._values = be.vector([a1_max, a2_max], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        ans = be.zeros((size, 6), dtype=float)
        ans[:,2] = be.random(low=-self.a1_max, high=self.a1_max, size=size)
        ans[:,5] = be.random(low=-self.a2_max, high=self.a2_max, size=size)
        return ans

    def _prob(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        if (spin1x==0) and (spin1y==0) and (be.abs(spin1z)<self.a1_max) and (spin2x==0) and (spin2y==0) and (be.abs(spin2z)<self.a2_max):
            return 0.25/(self.a1_max*self.a2_max)
        else:
            return 0.

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return be.vector((-pdf/self.a1_max, -pdf/self.a2_max), dtype=float)

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        ans = be.empty((2,2), dtype=float)
        ans[0,0] = 2*pdf/self.a1_max**2
        ans[0,1] = ans[1,0] = pdf/(self.a1_max*self.a2_max)
        ans[1,1] = 2*pdf/self.a2_max**2
        return ans

class IsotropicSpin(SpinDistribution):
    """Generate isotropically distributed spins. The spin components magnitudes are isotropic. E.g. this will favor large spin magnitudes.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def _init_values(self, a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
        self._values = be.vector([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self, new):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        a1 = ((self.a1_max**3 - self.a1_min**3) * be.random(size=size) + self.a1_min**3)**(1./3)
        a2 = ((self.a2_max**3 - self.a2_min**3) * be.random(size=size) + self.a1_min**3)**(1./3)

        th1 = be.arcsin(be.random(low=-1, high=1, size=size))
        th2 = be.arcsin(be.random(low=-1, high=1, size=size))

        ph1 = be.random(low=0, high=2*pi, size=size)
        ph2 = be.random(low=0, high=2*pi, size=size)

        costh1 = be.cos(th1)
        s1x = a1 * costh1 * be.cos(ph1)
        s1y = a1 * costh1 * be.sin(ph1)
        s1z = a1 * be.sin(th1)

        costh2 = be.cos(th2)
        s2x = a2 * costh2 * be.cos(ph2)
        s2y = a2 * costh2 * be.sin(ph2)
        s2z = a2 * be.sin(th2)

        return be.matrix(zip(s1x, s1y, s1z, s2x, s2y, s2z), dtype=float)

    def _prob(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = (spin1x**2 + spin1y**2 + spin1z**2)**0.5
        a2 = (spin2x**2 + spin2y**2 + spin2z**2)**0.5
        return (self.a1_min <= a1)*(a1 < self.a1_max)*(self.a2_min <= a2)*(a2 < self.a2_max) * 9 * a1**2 * a2**2 / ((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3))

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return be.vector((
                -pdf * 3*self.a1_max**2 / (self.a1_max**3 - self.a1_min**3),
                -pdf * 3*self.a2_max**2 / (self.a2_max**3 - self.a2_min**3),
                -pdf * 3*self.a1_min**2 / (self.a1_max**3 - self.a1_min**3),
                +pdf * 3*self.a2_min**2 / (self.a2_max**3 - self.a2_min**3),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.prob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        ans = be.empty((4,4), dtype=float)

        ans[0,0] = -6*self.a1_max*pdf/(self.a1_max**3 - self.a1_min**3) + 6*self.a1_max**4*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[0,1] = ans[1,0] = +9*self.a1_max**2*self.a2_max**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[0,2] = ans[2,0] = 18*self.a1_max**2*self.a1_min**2*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[0,3] = ans[3,0] = -9*self.a1_max**2*self.a2_min**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[1,1] = -6*self._a2_max*pdf/(self.a2_max**3 - self.a2_min**3) + 6*self.a2_max**4*pdf/(self.a2_max**3 - self.a2_min**3)**2,
        ans[1,2] = ans[2,1] = -9*self.a1_min**2*self.a2_max**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[1,3] = ans[3,1] = 18*self.a2_max**2*self.a2_min**2*pdf/(self.a2_max**3 - self.a2_min**3)**2,
        ans[2,2] = +6*self.a1_min*pdf/(self.a1_max**3 - self.a1_min**3) + 6*self.a1_min**4*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[2,3] = ans[3,2] = +9*self.a1_min**2*self.a2_min**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[3,3] = +6*self._a2_min*pdf/(self.a2_max**3 - self.a2_min**3) + 6*self.a2_min**4*pdf/(self.a2_max**3 - self.a2_min**3)**2,

        return ans

class IsotropicAlignedSpin(SpinDistribution):
    """Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed as if the spin vectors were isotropic but the in-plane spin component is ignored.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def _init_values(self, a1_max=1.0, a2_max=1.0, a1_min=0., a2_min=0.):
        self._values = be.vector([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self, new):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        ans = be.zeros((size, 6), dtype=float)
        a1 = ((self.a1_max**3 - self.a1_min**3) * be.random(size=size) + self.a1_min**3)**(1./3)
        a2 = ((self.a2_max**3 - self.a2_min**3) * be.random(size=size) + self.a1_min**3)**(1./3)
        ans[:,2] = a1 * be.arcsin(be.random(low=-1, high=1, size=size))
        ans[:,5] = a2 * be.arcsin(be.random(low=-1, high=1, size=size))
        return ans

    def _prob(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = be.abs(spin1z)
        a2 = be.abs(spin2z)
        if (spin1x==0) and (spin1y==0) and (a1<self.a1_max) and (spin2x==0) and (spin2y==0) and (a2<self.a2_max):
            return 0.75**2 * (self.a1_max**2 - be.max(a1, self.a1_min)**2) * (self.a2_max**2 - be.max(a2, self.a2_min)**2) / ((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3-self.a2_min**3))
        else:
            return 0.

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = be.abs(spin1z)
        a2 = be.abs(spin2z)
        return be.vector((
                0.75**2*(self.a2_max**2 - be.max(a2, self.a2_min)**2)/(self.a2_max**3-self.a2_min**3)*( 2*self.a1_max/(self.a1_max**3 - self.a1_min**3) - (self.a1_max**2 - be.max(a1, self.a1_min)**2)*3*self.a1_max**2/(self.a1_max**3 - self.a1_min**3)**2 ),
                0.75**2*(self.a1_max**2 - be.max(a1, self.a1_min)**2)/(self.a1_max**3-self.a1_min**3)*( 2*self.a2_max/(self.a2_max**3 - self.a2_min**3) - (self.a2_max**2 - be.max(a2, self.a2_min)**2)*3*self.a2_max**2/(self.a2_max**3 - self.a2_min**3)**2 ),
                0.75**2*(self.a2_max**2 - be.max(a2, self.a2_min)**2)/(self.a2_max**3-self.a2_min**3)*( (self.a1_max**2 - be.max(a1, self.a1_min)**2)*3*self.a1_min**2/(self.a1_max**3 - self.a1_min**3)**2 - (self.a1_min > a1)*2*self.a1_min/(self.a1_max**3 - self.a1_min**3) ),
                0.75**2*(self.a1_max**2 - be.max(a1, self.a1_min)**2)/(self.a1_max**3-self.a1_min**3)*( (self.a2_max**2 - be.max(a2, self.a2_min)**2)*3*self.a2_min**2/(self.a2_max**3 - self.a2_min**3)**2 - (self.a2_min > a2)*2*self.a2_min/(self.a2_max**3 - self.a2_min**3) ),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        raise NotImplementedError
