"""source distributions for how events are distributed through time
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import SamplingDistribution
from gwdistributions import backends as be

#-------------------------------------------------

class TimeDistribution(SamplingDistribution):
    """a child class used to declare things are distributions over time
    """
    _variates = ("time",)

#-------------------------------------------------

class UniformEventTime(TimeDistribution):
    """Uniform event time (seconds) over a year (arbitrarily defined).
    """
    _params = ('t0', 'dur')

    def _init_values(self, t0=1e9, dur=86400*365):
        self._values = be.vector([t0, dur], dtype=float)

    @property
    def t0(self):
        return self._values[0]

    @t0.setter
    def t0(self, new):
        self._values[0] = new

    @property
    def dur(self):
        return self._values[1]

    @dur.setter
    def dur(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return self.t0 + be.random(size=(size, 1))*self.dur ### FIXME: make backend compatible

    def _prob(self, t):
        return float((self.t0<=t)*(t<self.t0+self.dur))/self.dur

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, t):
        if self.prob(t) > 0:
            return be.vector((0., -1./self.dur**2), dtype=float)
        else:
            return be.vector((0., 0.), dtype=float)

    def _hessian(self, t):
        if self.prob(t) > 0:
            return be.matrix(((0., 0.), (0., 1./self.dur**3)), dtype=float)
        else:
            return be.matrix((2,2), dtype=float)

