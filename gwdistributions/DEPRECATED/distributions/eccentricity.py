"""source distributions for how events' eccentricies are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import SamplingDistribution
from gwdistributions import backends as be

#-------------------------------------------------

class EccentricityDistribution(SamplingDistribution):
    """a child class used to declar things are distributions over eccentricity
    """
    _variates = ("eccentricity",)

#-------------------------------------------------

class ZeroEccentricity(EccentricityDistribution):
    """zero eccentricity
    """
    _params = ()

    def _rvs(self, size=1):
        return be.zeros((size, 1), dtype=float)

    def _prob(self, e):
        if e==0:
            return 1.
        else:
            return 0.

    def _logprob(self, e):
        if e==0:
            return 0.
        else:
            return -be.infty

    def _jacobian(self, e):
        return be.vector([], dtype=float)

    def _hessian(self, e):
        return be.matrix([[]], dtype=float)
