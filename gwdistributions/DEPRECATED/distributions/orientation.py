"""source distributions for how events' orientations are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import numpy as np
from numpy import pi

from .base import SamplingDistribution
from gwdistributions import backends as be

#-------------------------------------------------

class OrientationDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over orientation
    """
    _variates = ("inclination", "polarization", "coa_phase")

#-------------------------------------------------

class RandomOrientation(OrientationDistribution):
    """random event orientation
    """
    _parmas = ()

    def _rvs(self, size=1):
        incl = be.arccos(be.random(low=-1, high=1, size=size))
        psi = be.random(low=0, high=pi, size=size)
        coa_phase = be.random(low=0, high=2*pi, size=size)
        return be.matrix(zip(incl, psi, coa_phase), dtype=float)

    def _prob(self, incl, psi, coa_pahse):
        ans = 1.
        ans *= be.sin(incl)/2  ### incl
        ans *= pi**-1       ### psi
        ans *= (2*pi)**-1   ### coa_phase
        return ans

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, incl, psi, coa_phase):
        return be.vector([], dtype=float)

    def _hessian(self, incl, psi, coa_phase):
        return be.matrix([[]], dtype=float)

class OptimallyOriented(OrientationDistribution):
    """assumes the detector arms are aligned with the coordinate system, so zenith is at RA=0, Dec=pi/2
    """
    _params = ()

    def _rvs(self, size=1):
        # Inclination, coa_phase (ignored), polarization (ignored)
        ans = be.zeros((size, len(self._variates)), dtype=float)
        return ans

    def _prob(self, incl, psi, coa_pahse):
        if (incl==0) and (psi==0) and (coa_phase==0):
            return 1.
        else:
            return 0.

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, incl, psi, coa_pahse):
        return be.vector([], dtype=float)

    def _hessian(self, incl, psi, coa_pahse):
        return be.matrix([[]], dtype=float)
