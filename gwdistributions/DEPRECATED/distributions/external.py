"""a module that houses distributions based on external lists of events
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import numpy as np
from scipy.stats import gaussian_kde

from gwdistributions import io as gwdio
from gwdistributions import backends as be
from .base import SamplingDistribution

#-------------------------------------------------

class ExternalDistribution(SamplingDistribution):
    """a child class that imports samples from an external source and provides samples based on these
    """

    def _init_values(self, path=None, weight_column=None, weight_column_is_log=False, **columns):
        """columns kwargs are maps from the variates (keys) to the column names in path (values)
        """
        if path is None:
            raise ValueError('please supply a path to the external samples!')
        self._path = path
        self._variates = tuple(sorted(columns.keys())) ### store the variates
        self._data = self.load(path, [columns[variate] for variate in self._variates])
        self._num_samples = len(data)

        ### figure out weights
        if weight_column is not None:
            self._weights = self.load(path, [weight_column])
            if weight_column_is_log:
                self._weights = be.exp(self._weights - be.max(self._weights))

        else:
            self._weights = be.ones(self._num_samples, dtype=float)

        self._weights /= be.sum(self._weights)
        self._cumweights = be.cumsum(self._weights)

    @staticmethod
    def load(path, variates):
        events = gwdio.file2events(path)
        return gwdio._events2array(events, self._variates)

    def _draw_from_weights(self, size=1):
        """return indecies corresponding to the weights chosen herein
        we draw with replacement
        """
        # compute a cdf and draw from it
        # we take the ceiling because cumweights starts above zero and ends at 1, so random samples should be assigned to the
        # index just above them in the cumulative sum
        return be.ceil(be.interp(be.random(size=size), self._cumweights, be.arange(self._num_samples))).astype(int)

    def _rvs(self, size=1, *args):
        ans = be.empty((size, len(self._variates)), dtype=float)
        for i, j in enumerate(self._draw_from_weights(size=size)):
            ans[i,:] = self._data[j,:]
        return ans

    def _prob(self, *args):
        truth = be.ones(self._num_samples, dtype=int)
        for i, arg in enumerate(args):
            truth *= arg==self._data[:,i]
        return be.sum(self._weights[truth>0])

    def _logprob(self, *args):
        return be.log(self._prob(*args))

    def _jacobian(self, *args):
        return be.vector([], dtype=float)

    def _hessian(self, *args):
        return be.matrix([[]], dtype=float)

#------------------------

DEFAULT_WHITENED_BANDWIDTH = 0.1
class ExternalKDEDistribution(ExternalDistribution):
    """a child class that imports samples from an external source and provides samples based on a Kernel Density Estimate constructed with these
    """

    def _init_values(self, path=None, whitened_bandwidth=DEFAULT_WHITENED_BANDWIDTH, weight_column=None, **columns):
        ExternalDistribution._init_values(self, path=path, **columns)
        if weight_column is None:
            self._weights = be.ones(len(self._data), dtype=float)/len(self._data)
        else:
            self._weights = self.load(self._path, [weight_column])
            self._weights /= be.sum(self._weights)

        self._whitener_params = [self.build_whitener(self._data[:,i], self._weights) for i in range(len(self._variates))]

        ### update data to be whitened data
        for i, params in enumerate(self._whitener_params):
            self._data[:,i] = self.whiten(self._data[:,i], *params)

        ### build the kde
        self._kde = self.build_kde(
            self._data,
            self._weights,
            bandwidth=whitened_bandwidth,
        )

    @staticmethod
    def build_whitener(data, weights):
        norm = be.sum(weights)
        m1 = be.sum(data * weights) / norm
        m2 = be.sum(data**2 * weights) / norm
        return m1, (m2 - m1**2)**0.5

    @staticmethod
    def whiten(data, mean, stdv):
        return (data - mean) / stdv

    @staticmethod
    def color(data, mean, stdv):
        return (data * stdv) + mean

    @staticmethod
    def build_kde(data, weights, bandwidth=DEFAULT_WHITENED_BANDWIDTH):
        if scipy.__version__ < '1.3.0':
            if be.any(weights[0]!=weights):
                raise NotImplementedError('scipy.stats.gaussian_kde only supports non-trivial weights with __version__>=1.3.0')
            return gaussian_kde(data.transpose(), bw_method=bandwidth) ### FIXME: make this compatible with all backends
        else:
            return gaussian_kde(data.transpose(), weights=weights, bw_method=bandwidth) ### FIXME:make this compatible with all backends

    def _rvs(self, size=1, *args):
        ans = self._kde.resample(size=size) ### sample in the whitened data
        ans = ans.transpose()

        for i, params in enumerate(self._whitener_params): ### color the result
            ans[:,i] = self.color(ans[:,i], *params)
        return ans

    def _prob(self, *args):
        # just evaluate the kde at the whitened args
        return self._kde.pdf(*[self.whiten(arg, *params) for arg, params in zip(args, self._whitener_params)])

    def _logprob(self, *args):
        return be.log(self._prob(*args))
