"""a module that defines objects that can sample from distributions of GW events, encoded as lists of Event objects
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import os
import sys
import warnings
import time

from functools import partial

from .backends import numerics as be
from .backends import names

from .utils import Parameters
from .event import Event
from gwdistributions import gwdio

from gwdistributions import distributions

#-------------------------------------------------

# used to determine default progress bar size
def terminal_size():
    try: # try to retrieve this dynamicall
        size = os.get_terminal_size()[0]
    except:
        size = 105
    finally:
        return size

#-------------------------------------------------
# EventGenerator classes
#-------------------------------------------------

class EventGenerator(object):
    """a workhorse object that wraps parameter distributions together to generate events
    """

    ### instantiation and maintenance

    def __init__(
            self,
            distributions=[],
            transforms=[],
            conditionals=[],
            store_logprob=True,
            store_factored_logprob=False,
            delete_vector_attributes=True, ### delete the expensive parts of waveform generation
        ):

        self._store_logprob = store_logprob
        self._store_factored_logprob = store_factored_logprob

        self._delete_vector_attributes = delete_vector_attributes

        self._events = []
        self._total_generated = 0

        ### list of things used to generate event parameters
        self._distributions = []
        for dist in distributions:
            self.append_distribution(dist)
        
        ### list of transofmrations to apply when events are generated
        self._transforms = []
        for trans in transforms:
            self.append_transform(trans)

        # These are functions called to do rejection sampling of generated events.
        self._conditionals = []
        for cond in conditionals:
            self.append_conditional(cond)

        # create a helper class to expose "parameters" in a dictionary-like form
        # this is done for compatibility with gwpopulations
        self._parameters = Parameters(self)

    ### helper functions to manage what is contained in the object

    def flush(self):
        """reset object. \
        return events, distributions, transforms, conditionals
        """
        return self.flush_events(), self.flush_distributions(), self.flush_transforms(), self.flush_conditionals()

    def flush_events(self):
        """empty the internal list of events
        """
        events = self.events
        self._events = []
        self._total_generated = 0
        return events

    def flush_distributions(self):
        """empty the internal list of distributions
        """
        dists = self.distributions
        self._distributions = []
        return dists

    def flush_transforms(self):
        """empty the internal list of transforms
        """
        trans = self.transforms
        self._transforms = []
        return trans

    def flush_conditionals(self):
        """empty the internal list of conditionals
        """
        conds = self.conditionals
        self._conditionals = []
        return conds

    ### general properies and/or access control to the object

    def __iter__(self):
        for e in self._events:
            yield e

    def __len__(self):
        return len(self._events)

    @property
    def normalized(self):
        """implemented for compatibility within Mixture SamplingDistributions
        """
        return all(dist.normalized for dist in self.distributions)

    @property
    def variates(self):
        """implemented for compatibility within Mixture SamplingDistributions
        """
        return self.distribution_variates

    @property
    def scalar_variates(self):
        """implemented for compatibility within Mixture SamplingDistributions
        """
        return self.distribution_scalar_variates

    @property
    def vector_variates(self):
        """implemented for compatibility within Mixture SamplingDistributions
        """
        return self.distribution_vector_variates

    @property
    def required(self):
        """implemented for compatibility within Mixture SamplingDistributions
        """
        return () ### EventGenerators cannot require anything because we check that as we append distributions

    def domain(self, *required):
        """implemented for compatibility within Mixture SamplingDistributions. Note, if any distribution requires variates, \
the domain will be set to the entire real line for each of the variates produced by that distribution.
        """
        ans = dict()
        for dist in self.distributions: ### concatenate dict of all domains
            if len(dist.required): ### we can't compute the domain of things that require variates without passing them, so just return the whole real line
                ans.update(dict((variate, (-be.infty, +be.infty)) for variate in dist.variates))
            else: ### delegate in case there is special logic that we can exploit in the absence of required variates
                ans.update(dist.domain())
        return ans

    @property
    def _params(self):
        """implemented for compatibility within Mixture SamplingDistribution
        """
        names = []
        for dist in self.distributions:
            names += list(dist._params)
        return tuple(names)

    @property
    def params(self):
        """implemented for compatibility within Mixture SamplingDistribution
        """
        values = []
        for dist in self.distributions:
            values += list(dist.params)
        return be.array(values)

    @property
    def distributions(self):
        return self._distributions

    def variate2distribution(self, variate):
        for dist in self.distributions:
            if names.name(variate) in dist.variates:
                return dist
        else:
            raise RuntimeError('could not find a distribution that produces : '+variate)

    @property
    def distribution_variates(self):
        return self.distribution_scalar_variates + self.distribution_vector_variates

    @property
    def distribution_scalar_variates(self):
        variates = ()
        for dist in self.distributions:
            variates += dist.scalar_variates
        return variates

    @property
    def distribution_vector_variates(self):
        variates = ()
        for dist in self.distributions:
            variates += dist.vector_variates
        return variates

    @property
    def transforms(self):
        return self._transforms

    @property
    def transform_variates(self):
        return self.transform_scalar_variates + self.transform_vector_variates

    @property
    def transform_scalar_variates(self):
        variates = ()
        for trans in self.transforms:
            variates += trans.scalar_variates
        return variates

    @property
    def transform_vector_variates(self):
        variates = ()
        for trans in self.transforms:
            variates += trans.vector_variates
        return variates

    @property
    def conditionals(self):
        return self._conditionals

    @property
    def events(self):
        return self._events

    @property
    def total_generated(self):
        return self._total_generated

    @property
    def attributes(self):
        return self.scalar_attributes | self.vector_attributes

    @property
    def scalar_attributes(self):
        attrs = set()
        if self._store_logprob:
            attrs.add(names.logprob_name()) ### include this as part of the event

        if self._store_factored_logprob:
            for dist in self.distributions:
                ### NOTE: these names may not automatically update within events...
                attrs.add(names.logprob_name(dist.variates, dist.required))

        for dist in self.distributions:
            attrs |= set(dist.scalar_variates)

        for trans in self.transforms:
            attrs |= set(trans.scalar_variates)

        return attrs

    @property
    def vector_attributes(self):
        attrs = set()
        for dist in self.distributions:
            attrs |= set(dist.vector_variates)

        for trans in self.transforms:
            attrs |= set(trans.vector_variates)

        return attrs

    def __getitem__(self, name):
        for dist in self.distributions:
            try:
                return dist[name]
            except KeyError:
                pass
        else:
            for trans in self.transforms:
                try:
                    return trans[name]
                except KeyError:
                    pass
            else:
                raise KeyError('could not find parameter=%s'%name)

    def __setitem__(self, name, value):
        found = False
        for dist in self.distributions:
            try:
                dist[name] = value
                found = True
            except KeyError:
                pass

        for trans in self.transforms:
            try:
                trans[name] = value
                found = True
            except KeyError:
                pass

        if not found:
            raise KeyError('could not find parameter=%s'%name)

    def __items__(self):
        items = []
        for dist in self.distributions:
            for name in dist._params:
                items.append((name, dist[name]))
        return items

    @property
    def parameters(self):
        '''this is done to support calls from gwpopulations that attempt to access a "parameters" attribute \
(expecting a dict) in order to update it
        we instead return a copy of the EventGenerator object, which also supports calls to "update"
        '''
        return self._parameters

    @parameters.setter
    def parameters(self, new):
        '''expects the new parameters to be a dictionary, which we then pass to self.update
        '''
        self.update(**new)

    ### add more events to this distribution's internal list

    def extend(self, events):
        self._events.extend(events)

    def append(self, event):
        self._events.append(event)

    ### sanity-checking

    def _check_distribution(self, obj):
        """check to make sure we don't already have any distribution that will generate one of obj.variates \
and that we have a distribution that will produce obj.required
        """
        variates = self.distribution_variates
        self._check_variates(variates, obj.variates)
        self._check_required(variates, obj.required)

    def _check_transform(self, obj):
        """check to make sure we don't already have a distribution or transform that will generate one of \
obj.variates and that we have a distribution or transform that will produce obj.required"""
        variates = self.distribution_variates + self.transform_variates
        self._check_variates(variates, obj.variates)
        self._check_required(variates, obj.required)

    @staticmethod
    def _check_required(present, required):
        for variate in required:
            assert variate in present, 'required=%s not present!'%variate

    @staticmethod
    def _check_variates(present, variates):
        for variate in variates:
            assert variate not in present, 'variate=%s already produced!'%variate

    ### include more distribution, transforms, and conditionals

    def append_distribution(self, obj):
        """Add a distribution for a set of properties of the event. Automatically checks that the new distribution \
for consistency with existing distribution
        """
        self._check_distribution(obj)
        self.distributions.append(obj)

    def append_transform(self, obj):
        """Add a transform for a set of properties of the event. Typically, this is for properties which are not \
controlled by distributions, but are instead derived (e.g. total mass from component masses). \
 Automatically checks the new transform for consistency with the existing distributions and transforms.
        """
        self._check_transform(obj)
        self._transforms.append(obj)

    def append_conditional(self, cond, **kwargs):
        """Add a conditional function when generating properties of the event. This is useful to allow for a type \
of rejection sampling. An example would be rejecting events whose SNR falls below a certain threshold. The \
conditional function is `cond`, and `kwargs` is bound to the function via `functools.partial`. This is useful \
if the conditional itself has arguments which need to be set.
        """
        if kwargs:
            self._conditionals.append(partial(cond, **kwargs))
        else:
            self._conditionals.append(cond)

    ### I/O wrappers

    def write(self, *args, **kwargs):
       """automatically summarize information within the object and pass to io helpers to deal with different \
file formats.
NOTE: will overwrite the kwargs 'num_accepted' and 'total_generated' with information about the events generated
       """
       kwargs.update({ # pass this way so we can support dynamic naming
           names.name('num_accepted') : len(self),
           names.name('total_generated') : self.total_generated,
       })
       if self._delete_vector_attributes:
           if isinstance(self._delete_vector_attributes, bool):
               vectors = []
           else:
               vectors = [_ for _ in self.vector_attributes if _ not in self._delete_vector_attributes]
       else:
           vectors = self.vector_attributes

       gwdio.events2file(
           self.events,
           self.scalar_attributes,
           vectors,
           *args,
           **kwargs
        )

    ### functions to evaluate the probability distribution at an event's parameters

    def prob(self, *args, **kwargs):
        """evaluate the sampling pdf for the variates associated with this event
        """
        return be.exp(self.logprob(*args, **kwargs))

    def logprob(self, *args):
        """evaluate log(sampling pdf) for the variates associated with this event
        """
        logpdf = 0.
        for dist in self.distributions:
            logpdf += dist.logprob(*args)
        return logpdf

    def __call__(self, *args, **kwargs):
        """evaluate the probaiblity density encoded in the distributions
        """
        return self.logprob(*args, **kwargs)

    ### utility functions for drawning events from the distributions

    def rvs(self, size=1):
        """implemented for compatibility within Mixture SamplingDistribution. This delegates to generate, \
but then flushes all events and returns an array instead of a list of events
        """
        if self.scalar_variates and self.vector_variates:
            raise RuntimeError('cannot rvs both scalar and vector variates because of conflicts with array shape')

        events = self.generate(n_event=size) ### draw events
        self.flush_events() ### forget about those events

        # format answer for output
        variates = self.variates
        return be.array([[event[variate] for variate in variates] for event in events], dtype=float)

    def iterate(self, n_itr=be.infty, n_event=be.infty, verbose=False, Verbose=False):
        """repeatedly calls self.generate() and yields the result, as would be useful for an interation. \
In addition to yielding each event as it comes, this stores them internally within self._events \
        NOTE! the default behavior is to enter an infinite loop (n_iter=infty and n_event=infty)!
        """
        initial = self.total_generated
        while (self.total_generated < n_itr+initial) and (len(self) < n_event):
            remaining_itr = n_itr + initial - self.total_generated ### the number of allowed iterations remaining
            events = self.generate(n_itr=remaining_itr, n_event=1, verbose=verbose, Verbose=Verbose) ### automatically stores new events in self.events
            if events:
                yield events[0]

    def generate_regularly_spaced(
            self,
            start_time,
            end_time,
            n_event=1,
            verbose=False,
            Verbose=False,
        ):
        """generate a set of events that are regularly spaced in time. This is done by shifting the \
TimeDistribution and repeatedly calling self.generate
        """
        verbose |= Verbose

        ### identify a time distribution
        time_dist = self.variate2distribution(names.name('geocenter_time'))
        assert isinstance(time_dist, distributions.AdjustableTimeDistribution), \
            'TimeDistribution must be an instance of AdjustableTimeDistribution when regularly spacing events'

        mint, maxt = time_dist.domain()[names.name('geocenter_time')]
        dur = maxt - mint

        # figure out event spacing
        dt = float(end_time - start_time) / n_event ### spacing by which we shift central_time
        if dt < dur:
            warnings.warn('Event spacing (%.3f sec) is smaller than duration of TimeDistribution (%.3f sec) \
Events may overlap!'%(dt, dur))

        # initialize the time distribution
        time_dist.central_time = start_time + 0.5*(dt-dur) ### start away from edges of the time range

        ### now iterate through events
        if verbose:
            tmp = '\r %6d / %6d --> (%15.3f, %15.3f)' ### declare this here and re-use within the loop
            print('regularly spacing events within windows of duration %.3f sec \
shifted by %.3f sec between events' % (dur, dt))

        for event in self.iterate(n_event=n_event, Verbose=Verbose):
            t0 = time_dist.central_time

            if verbose:
                sys.stdout.write(tmp%(len(self), n_event, t0, t0+dur))
                sys.stdout.flush()

            time_dist.central_time = t0 + dt

        if verbose:
            sys.stdout.write('\n')
            sys.stdout.flush()

        ### return
        return self.events

    def generate(
            self,
            n_itr=be.infty,
            n_event=1,
            verbose=False,
            Verbose=False,
            progress_bar_size=None,
        ):
        """Loop over distributions, transforms, and conditionals to generate events. Store the generated events \
internally in addition to returning the list of newly generated events.
        """
        verbose |= Verbose

        if verbose:
            ### template for verbosity statements, save some time compared to constructing this each epoch
            tmp = "\r  iter = %6d" + " / %6.0f"%n_itr + " : event = %6d" + " / %6.f"%n_event
            if progress_bar_size is None:
                progress_bar_size = max(terminal_size() - 55, 0)
            if progress_bar_size > 0:
                tmp += ' : [%s%s]'

        if Verbose:
            attrs = sorted(self.attributes)

        initial = self.total_generated
        events = []
        num = len(events)

        while (self.total_generated < n_itr+initial) and (num < n_event):

            if verbose:
                tup = (self.total_generated-initial, num)
                if progress_bar_size > 0:
                    ### fraction of the way there
                    progress = int(max(1.*num/n_event, (1.*self.total_generated-initial)/n_itr) * progress_bar_size)
                    tup += (progress*'-', (progress_bar_size-progress)*' ')
                report = tmp%tup
                if Verbose:
                    print(report)
                else:
                    sys.stdout.write(report)
                    sys.stdout.flush()

            self._total_generated += 1 ### record that we made another attempt
            event = Event()

            # Generate user-specified randomized properties
            for dist in self.distributions:
                if Verbose:
                    t0 = time.time()

                ### size is always the first argument!
                rv = dist.rvs(*[event[variate] for variate in dist.required], size=1)[0]
                for l, v in zip(dist.variates, rv):
                    event[l] = v

                if Verbose:
                    print('    %.3e sec : %s'%(time.time()-t0, ', '.join(dist.variates)))

            # Generate "dependent" properties as transformations over the base properties
            for trans in self.transforms:
                if Verbose:
                    t0 = time.time()

                trans(event)

                if Verbose:
                    print('    %.3e sec : %s'%(time.time()-t0, ', '.join(trans.variates)))

            # Check that the event passes checks
            if Verbose:
                t0 = time.time()

            check = True
            for func in self.conditionals:
                check &= func(event)

            if Verbose: ### print properties of the event
                print('    %.3e sec : conditionals'%(time.time()-t0))

                for key in event.attributes:
                    print('    %s : %s'%(key, event[key]))
                print('    check = %s'%check)

            ### finish processing event
            if check:

                # store draw probabilities if requested
                if self._store_logprob: ### assign an attribute that stores the prob of drawing this sample
                    if Verbose:
                        t0 = time.time()

                    # cast to float to make sure it appears as a scalar
                    event[names.logprob_name()] = float(self.logprob(event))

                    if Verbose:
                        print('    %.3e sec : %s'%(time.time()-t0, names.logprob_name()))

                if self._store_factored_logprob:
                    if Verbose:
                        t0 = time.time()

                    for dist in self.distributions:
                        ### NOTE: these names may not dynamically update!
                        # cast to float to make sure it appears as a scalar
                        event[names.logprob_name(dist.variates, dist.required)] = float(dist.logprob(event))

                    if Verbose:
                        print('    %.3e sec : factored %s'%(time.time()-t0, names.logprob_name()))

                # clean up expensive parts of waveform to save on memory
                if self._delete_vector_attributes:
                    if isinstance(self._delete_vector_attributes, bool): # delete all attributes
                        del_attrs = event.vector_attributes
                    else:
                        del_attrs = self._delete_vector_attributes # delete a specific set of attributes

                    for attr in del_attrs:
                        event.delete_attr(attr, verbose=Verbose)

                # store the event
                events.append(event)
                num = len(events)

        if verbose:
            if not Verbose:
                sys.stdout.write('\n')
            print("%d events generated, %d retained."%(self.total_generated-initial, num))

        self.extend(events) ### store these internally

        return events ### return the newly generated ones

    def transform(self):
        """This applies all transforms on the event set. This may be useful in cases where the event list \
is statically generated, and the user wants a new set of columns that wasn't accessible during generation.
        """
        # Regenerate "dependent" properties via transforms
        for event in self._events:
            for trans in self._transforms:
                trans(event) ### this modifies the event in place

    ### modify the parameters of stored distributions, transforms

    def update(self, **kwargs):
        for key, value in kwargs.items():
            self[key] = value
