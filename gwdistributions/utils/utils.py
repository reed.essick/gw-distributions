"""a module that houses basic utilities that are useful throughout the library
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

def subclasses(klass):
    """discover and return all the subclasses of a particular class
    """
    ans = dict()
    for obj in klass.__subclasses__():
        ans[obj.__name__] = obj
        ans.update(subclasses(obj))
    return ans

def subclasses_tree(klass):
    """discovers and returns all subclasses of a particular class. Return these in a tree structure
    """
    ans = dict()
    for obj in klass.__subclasses__():
        ans[obj.__name__] = (obj, subclasses_tree(obj))
    return ans

#-------------------------------------------------

class Parameters(object):
    '''a helper class that will connect the parameters stored in "reference" with utilities that are usually available via dictionaries
    NOTE: this does *not* check for repeated parameter names (they may be allowed)
        * __getitem__ will only return the value associated with the first occurance.
        * __setitem__ will update all occurances.
    '''
    def __init__(self, reference):
        self._reference = reference

    @property
    def reference(self):
        return self._reference

    def __repr__(self):
        return "{Parameters: "+", ".join('%s=%s'%(key, value) for key, value in self.items()) + '}'

    def __iter__(self):
        for key in self.keys():
            yield key

    def __contains__(self, name):
        return name in self.keys()

    def __getitem__(self, name):
        return self.reference[name]

    def __setitem__(self, name, value):
        self.reference[name] = value

    def copy(self):
        ''' return a dict of all the parameter values so that this emulates the dict copy() method.
        note that this will not return the same type as the Parameters() class, contrary to what one might expect.'''
        return dict(self.items())

    def get(self, name, default=None):
        try:
            return self[name]
        except KeyError:
            return default

    def items(self):
        return self.reference.__items__()

    def keys(self):
        return [a for a, b in self.items()]

    def values(self):
        return [b for a, b in self.items()]

    def update(self, *args, **kwargs):
        for arg in args:
            assert isinstance(arg, dict)
            kwargs.update(arg)
        self.reference.update(**kwargs)
