"""a module that houses various I/O utility functions for reading/writing sets of events from disk
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import os
from shutil import move ### used to make file writing atomic
import time

import numpy as np
import h5py

try:
    import lal ### needed for xml support
    from glue.ligolw import lsctables, utils, ligolw, ilwd
    from glue.ligolw.utils import process as lw_process
except ImportError:
    lal = None ### flag this as not being available

from .backends import numerics as be
from .backends import names

from .event import Event

#-------------------------------------------------
# I/O associated with lists of Events
#-------------------------------------------------

def tmppath(path, tmpdir='/tmp'):
    return os.path.join(tmpdir, '.'+os.path.basename(path))

def date():
    return time.strftime("%H:%M:%S UTC %a %d %b %Y", time.gmtime())

#------------------------
# top-level helper funtions
#------------------------

# the file-types that we think we may be able to interpret
ASCII_SUFFIXES = ['txt', 'txt.gz', 'dat', 'dat.gz', 'csv', 'csv.gz']
ASCII_DELIMITER = {'csv':',', 'csv.gz':','} ### special case for delimeters

XML_SUFFIXES = ['xml', 'xml.gz']
HDF_SUFFIXES = ['hdf5', 'hdf', 'h5']

KNOWN_SUFFIXES = ASCII_SUFFIXES + HDF_SUFFIXES + XML_SUFFIXES

#-----------

def _ascii_delimiter(path):
    """figure out the appropriate delimiter based on the path name
    """
    for key, val in ASCII_DELIMITER.items():
        if path.endswith(key):
            return val
    return None

#-----------

def array2file(attrs, path, *args, **kwargs):
    """write an array to a file
    """
    if names.name('date') not in kwargs:
        kwargs[names.name('date')] = date()

    if any(path.endswith(_) for _ in ASCII_SUFFIXES):
        kwargs['delimiter'] = _ascii_delimiter(path)
        return array2ascii(attrs, path, *args, **kwargs)

    elif any(path.endswith(_) for _ in XML_SUFFIXES):
        return array2xml(attrs, path, *args, **kwargs)

    elif any(path.endswith(_) for _ in HDF_SUFFIXES):
        return array2hdf5(attrs, path, *args, **kwargs)

    else:
        raise ValueError('could not interpret the file type of path=%s. \
Suffix must be one of: %s'%(path, ', '.join(KNOWN_SUFFIXES)))

def events2file(events, scalars, vectors, path, *args, **kwargs):
    """write a list of events to a file
    """
    if names.name('date') not in kwargs:
        kwargs[names.name('date')] = date()

    if any(path.endswith(_) for _ in ASCII_SUFFIXES):
        kwargs['delimiter'] = _ascii_delimiter(path)
        return events2ascii(events, scalars, vectors, path, *args, **kwargs)

    elif any(path.endswith(_) for _ in XML_SUFFIXES):
        return events2xml(events, scalars, vectors, path, *args, **kwargs)

    elif any(path.endswith(_) for _ in HDF_SUFFIXES):
        return events2hdf5(events, scalars, vectors, path, *args, **kwargs)

    else:
        raise ValueError('could not interpret the file type of path=%s. \
Suffix must be one of: %s'%(path, ', '.join(KNOWN_SUFFIXES)))

#-----------

def file2array(path, *args, **kwargs):
    """reads an array from a file
    """
    if any(path.endswith(_) for _ in ASCII_SUFFIXES):
        kwargs['delimiter'] = _ascii_delimiter(path)
        return ascii2array(path, *args, **kwargs)

    elif any(path.endswith(_) for _ in XML_SUFFIXES):
        return xml2array(path, *args, **kwargs)

    elif any(path.endswith(_) for _ in HDF_SUFFIXES):
        return hdf52array(path, *args, **kwargs)

    else:
        raise ValueError('could not interpret the file type of path=%s. \
Suffix must be one of: %s'%(path, ', '.join(KNOWN_SUFFIXES)))

def file2events(path, *args, **kwargs):
    """read a list of events from a file
    """
    if any(path.endswith(_) for _ in ASCII_SUFFIXES):
        kwargs['delimiter'] = _ascii_delimiter(path)
        return ascii2events(path, *args, **kwargs)

    elif any(path.endswith(_) for _ in XML_SUFFIXES):
        return xml2events(path, *args, **kwargs)

    elif any(path.endswith(_) for _ in HDF_SUFFIXES):
        return hdf52events(path, *args, **kwargs)

    else:
        raise ValueError('could not interpret the file type of path=%s. \
Suffix must be one of: %s'%(path, ', '.join(KNOWN_SUFFIXES)))

#------------------------
# logic to map between arrays and lists of events
#------------------------

def events2array(events, attrs):
    """basic helper function that maps events into the arrays of the correct type
    """
    S50_ATTRS = [names.name(_) for _ in ['approximant', 'taper', 'domain']] ### dynamic/automatic name lookup
    attrs = [(names.name(attr), 'S50' if attr in S50_ATTRS else 'float') for attr in attrs]
    return be.array([tuple(event[attr] for attr, _ in attrs) for event in events], dtype=attrs)

def array2events(array):
    """basic helper function that maps a structured array to a list of events
    """
    # make sure data are reasonable types
    attrs = tuple(array.dtype.names)
    num = len(array)
    array = dict((attr, array[attr]) for attr in attrs)
    for k, v in array.items(): # try to cast these to reasonable types
        try:
            v = v.astype(float)
        except ValueError:
            v = v.astype(str)
        finally:
            array[k] = v

    # now wrap data into Event objects
    events = []
    for row in range(num):
        event = Event()
        for attr in attrs:
            ### call to event.__setitem__ will automatically rename via backends.names
            event[attr] = array[attr][row]
        events.append(event)
    return events

#------------------------
### csv (ascii)
#------------------------

def events2ascii(events, scalars, vectors, path, delimiter=',', **kwargs):
    """write a list of events to a ascii file
    vectors are ignored!
    """
    return array2ascii(events2array(events, scalars), path, delimiter=delimiter, **kwargs)

def array2ascii(samples, path, delimiter=',', **kwargs):
    """write a structured array to a ascii file
    """
    ### write the result to a temporary location
    if isinstance(path, str):
        tmp = tmppath(path)
    else:
        tmp = path ### assume this is a file object

    if delimiter is None:
        delimiter = ' '

    np.savetxt(
        tmp,
        samples,
        comments='',
        header=delimiter.join(samples.dtype.names),
        fmt=delimiter.join('%s' if name=='approximant' else '%.18e' for name in samples.dtype.names),
    )

    ### move to final location
    if isinstance(path, str):
        move(tmp, path)

def ascii2array(path, delimiter=','):
    """Load an array from a ascii file.
    """
    meta = dict() ### empty dict since we don't have a good way to store metadata in ascii
    return np.genfromtxt(path, names=True, delimiter=delimiter), meta

def ascii2events(path, delimiter=','):
    """Load a list of events generated from a ascii file.
    """
    data, meta = ascii2array(path, delimiter=delimiter)
    return array2events(data), meta

#------------------------
### xml
#------------------------

def _xml_column_mapping():
    """dynamically construct column mapping to support backends.names and mappings to multiple columns
    """
    mapping = dict((names.name(key), (val,)) for key, val in names.CONVENTIONS['ligolw_xml'].items())

    # map names for effective distance
    # NOTE: this might be fragile as it depends on how transforms.detection.EffectiveDistance mangles strings
    old_tmp = names.name('effective_luminosity_distance') + "_%s"
    new_tmp = names.CONVENTIONS['ligolw_xml']['effective_luminosity_distance'] + "_%s"
    for old, new in [('h', 'H'), ('l', 'L'), ('v', 'V')]:
        mapping[old_tmp%old] = (new_tmp%new,) ### support mapping to multiple columns

    return mapping

XML_ADDITIONAL_ATTRS = ["simulation_id", "process_id"]
def array2xml(array, path, include_empty_columns=False, **kwargs):
    """write an array to a LIGOLW XML file.
    """
    if lal is None:
        raise ImportError('lal and/or glue are not available')
    lsctables.use_in(ligolw.LIGOLWContentHandler)

    ### set up the document
    xmldoc = ligolw.Document()
    xmldoc.appendChild(ligolw.LIGO_LW())

    ### set up process table (how file was generated)
    proc_table = lw_process.register_to_xmldoc(xmldoc, __name__, kwargs)
    proc_id = proc_table.process_id

    #-------

    ### set up attributes to record within SimInspiral Table

    XML_COLUMN_MAPPING = _xml_column_mapping() ### supports dynamic naming and mapping to multiple columns

    dump_attrs = []
    time_attrs = []

    geocent_time_name = names.name('geocenter_time') ### look these up once before entering loop
    time_name = names.name('time')
    len_time_name = len(time_name)

    # iterate through attributes and figure out how we will try to write each
    for attr in array.dtype.names:
        # first handle special cases where we map to 2 columns
        if attr == geocent_time_name:
            name = names.CONVENTIONS['ligolw_xml']['geocenter_time']
            twoname = [name, name+"_ns"]
            time_attrs.append((attr, twoname))
            dump_attrs += twoname

        elif attr[:len_time_name] == time_name: # NOTE: this might be fragile as it depends on how
                                                # transforms.detection.TimeOfArrival mangles strings
            name = attr.split('_')[-1].lower() + '_' + names.CONVENTIONS['ligolw_xml']['time']
            twoname = [name, name+"_ns"]
            time_attrs.append((attr, twoname))
            dump_attrs += twoname

        else: # then handle the rest
            dump_attrs += XML_COLUMN_MAPPING.get(attr, [attr])

    # downselect attributes based on what is allowed in SimInspiralTable
    dump_attrs = list(set(lsctables.SimInspiralTable.validcolumns.keys()) & set(dump_attrs))

    # now "invert" dump_attrs to get list of attributes (current naming convention) we're going to write to disk
    event2xml = []
    for attr in array.dtype.names:
        if (attr not in time_attrs):
            for a in XML_COLUMN_MAPPING.get(attr, [attr]):
                if a in dump_attrs:
                    event2xml.append(attr)
                    break

    # downselect time_attr to match dump_attrs
    time_attrs = [(attr, (name_sec, name_ns)) for attr, (name_sec, name_ns) in time_attrs \
        if ((name_sec in dump_attrs) and (name_ns in dump_attrs))]

    # if desired, build a list of other attrs for which we fill in "None"
    empty_attrs = []
    if include_empty_columns:
        for key in lsctables.SimInspiralTable.validcolumns.keys():
            for _, attrs in time_attrs:
                if key in attrs: ### this is one of the time attrs
                    break
            else: ### not one of the time attrs
                if key not in dump_attrs + XML_ADDITIONAL_ATTRS: ### not being otherwise sepcified
                    empty_attrs.append(key) ### need to fill this in

    #-------

    # create table
    sim_table = lsctables.New(lsctables.SimInspiralTable, dump_attrs + XML_ADDITIONAL_ATTRS + empty_attrs)
    xmldoc.childNodes[0].appendChild(sim_table)

    # iterate over events, creating a row for each
    for event in array:
        row = sim_table.RowType()

        ### fill in IDs for this row
        row.process_id = proc_id                     # these names are recorded in XML_ADDITIONAL_ATTRS
        row.simulation_id = sim_table.get_next_id()  # may be fragile...

        ### fill in time attributes
        for attr, (name_sec, name_ns) in time_attrs:
            _t = lal.LIGOTimeGPS(event[attr])
            setattr(row, name_sec, _t.gpsSeconds) ### split into gps seconds and nanoseconds
            setattr(row, name_ns, _t.gpsNanoSeconds)

        ### fill in attributes specified for this event
        for a in event2xml:
            for A in XML_COLUMN_MAPPING.get(a, [a]): ### deal with mappings to multiple columns
                setattr(row, A, event[a])

        ### fill in default for everything else
        for a in empty_attrs:
            setattr(row, a, None)

        ### add to table
        sim_table.append(row)

    #-------

    ### write to a temporary path
    tmp = tmppath(path)
    try:
        utils.write_filename(xmldoc, tmp, gz=tmp.endswith("gz"))
        move(tmp, path) ### move to final location

    except Exception as e:
        os.remove(tmp)
        raise e

def events2xml(events, scalars, vectors, path, **kwargs):
    """write a list of events to a LIGOLW XML file.
    vectors are ignored!
    """
    return array2xml(events2array(events, scalars), path, **kwargs)

def xml2array(path):
    """read an array from a LIGOLW XML file. Must contain one and only one sim_inspiral table.
    """
    events, meta = xml2events(path)
    attrs = events[0].attrs if len(events) else []
    return events2array(events, attrs), meta

def xml2events(path):
    """Load a list of events from a LIGOLW XML file. Must contain one and only one sim_inspiral table.
    """
    if lal is None:
        raise ImportError('lal and/or glue are not available')
    lsctables.use_in(ligolw.LIGOLWContentHandler)

    xmldoc = utils.load_filename(path, contenthandler=ligolw.LIGOLWContentHandler)
    tbl = lsctables.SimInspiralTable.get_table(xmldoc)
    attrs = set(tbl.validcolumns.keys())

    # first grab what was available for each event
    event_dicts = []
    for xmlevent in tbl:
        event_dict = dict((k, getattr(xmlevent, k)) for k in attrs if hasattr(xmlevent, k))
        attrs.intersection_update(set(event_dict.keys())) ### only retain those keys that appear for everyone
        event_dicts.append(event_dict)

    # remove additional attrs that don't correspond to any physical params
    for attr in XML_ADDITIONAL_ATTRS:
        attrs.remove(attr)

    # figure out column mapping
    ### invert the mapping, assuming there is one name for each of these keys (no multiple columns here)
    XML_COLUMN_MAPPING = dict((val, key) for key, (val,) in _xml_column_mapping().items())

    ligolw_geocent_time_name = names.CONVENTIONS['ligolw_xml']['geocenter_time']
    ligolw_geocent_time_ns_name = ligolw_geocent_time_name+"_ns"

    has_geocent_sec = (ligolw_geocent_time_name in attrs)
    has_geocent_ns = (ligolw_geocent_time_ns_name in attrs)
    for name in [ligolw_geocent_time_name, ligolw_geocent_time_ns_name]: ### remove these because we handle them as special case
        if name in attrs:
            attrs.remove(name)

    # now iterate over those and make Event objects
    # at this point, attrs should be only the intersection of the attributes that everyone has
    events = []

    geocent_time_name = XML_COLUMN_MAPPING[ligolw_geocent_time_name]
    for event_dict in event_dicts:
        event = Event()

        # handle special case
        if has_geocent_sec: ### only include if the sec column is present
            event[geocent_time_name] = event_dict[ligolw_geocent_time_name]
            if has_geocent_ns:
                event[geocent_time_name] += 1e-9*event_dict[ligolw_geocent_time_ns_name]

        ### FIXME: do we want to extract end times at detectors?

        # now handle the rest
        for attr in attrs:
            event[XML_COLUMN_MAPPING[attr]] = event_dict[attr]

        events.append(event)

    meta = dict() ### NOTE: reture an empty dict because we don't want to parse the process params table

    return events, meta

#------------------------
### hdf5
#------------------------

def array2hdf5(array, path, name='events', root=None, **meta):
    """write an array to an HDF5 file.
    """
    tmp = tmppath(path)
    try:
        with h5py.File(tmp, "a") as hfile:
            if root is not None:
                if root in hfile.keys():
                    grp = hfile[root] ### assume this is a group and not a dataset...
                else:
                    grp = hfile.create_group(root)
            else:
                grp = hfile ### just use the top-level file

            for k, v in meta.items():
                ### if k already exists, this updates it in-place
                grp.attrs.create(
                    k,
                    v,
                    dtype='S%d'%(len(v)) if isinstance(v, str) else None, # deal with annoying Python3 issue
                )

            ### create one big dataset, which assume homogenous data
            grp.create_dataset(name, data=array)

        ### move to final location
        move(tmp, path)

    except Exception as e:
        os.remove(tmp)
        raise e

def hdf52array(path, name='events', root=None):
    """Load an array from an HDF5 file.
    """
    with h5py.File(path, "r") as hfile:
        if root is not None:
            grp = hfile[root]
        else:
            grp = hfile
        data = grp[name][...] ### copy everything out of the file
        meta = dict((k, grp.attrs[k]) for k in grp.attrs.keys())

    ### return
    return data, meta

#-----------

def _hdf_vector_name(name):
    return name + "_vectors"

def events2hdf5(events, scalars, vectors, path, name='events', root=None, **meta):
    """write a list of events to an HDF5 file.
    """
    tmp = tmppath(path)
    try:
        with h5py.File(tmp, "a") as hfile:
            if root is not None:
                if root in hfile.keys():
                    grp = hfile[root] ### assume this is a group and not a dataset...
                else:
                    grp = hfile.create_group(root)
            else:
                grp = hfile ### just use the top-level file

            for k, v in meta.items():
                ### if k already exists, this updates it in-place
                grp.attrs.create(
                    k,
                    v,
                    dtype='S%d'%(len(v)) if isinstance(v, str) else None, # deal with annoying Python3 issue
                )

            ### create one big dataset, which assume homogenous data
            grp.create_dataset(name, data=events2array(events, scalars))

            ### store vectors for each event, which may not be homogenous
            if vectors:
                grp = grp.create_group(_hdf_vector_name(name))
                for ind, event in enumerate(events):
                    GRP = grp.create_group(str(ind))
                    for attr in vectors:
                        GRP.create_dataset(attr, data=event[attr])

        ### move to final location
        move(tmp, path)

    except Exception as e:
        os.remove(tmp) ### make sure we delete the temporary file
        raise e

def hdf52events(path, name='events', root=None):
    """Load a list of events from an HDF5 file.
    """
    with h5py.File(path, "r") as hfile:
        if root is not None:
            grp = hfile[root]
        else:
            grp = hfile
        meta = dict((k, grp.attrs[k]) for k in grp.attrs.keys())
        events = array2events(grp[name][...]) ### copy everything out of the file

        vector_name = _hdf_vector_name(name)
        if vector_name in grp.keys(): ### vectors are present
            grp = grp[vector_name]
            for ind, event in enumerate(events):
                GRP = grp[str(ind)]
                for key in GRP.keys():
                    event[key] = GRP[key][:]

    ### return
    return events, meta
