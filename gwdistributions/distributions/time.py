"""source distributions for how events are distributed through time
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from numpy import genfromtxt ### used to load segments

from .base import (SamplingDistribution, Fixed, Uniform, Mixture)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class TimeDistribution(SamplingDistribution):
    """a child class used to declare things are distributions over time
    """
    _scalar_variates = ('geocenter_time',)

    @staticmethod
    def _domain():
        return {'geocenter_time': (-be.infty, +be.infty)}

#-------------------------------------------------

class AdjustableTimeDistribution(TimeDistribution):
    """a time distribution with the concept of a central_time property
    """

    @property
    def central_time_name(self):
        return self.param_names[0]

    @property
    def central_time(self):
        return self[self.central_time_name]

    @central_time.setter
    def central_time(self, new):
        self[self.central_time_name] = new

#------------------------

class FixedTime(AdjustableTimeDistribution, Fixed):
    """\
a distribution that returns a fixed time
    """
    _normalized = False
    _param_defaults = (1e9,)

    def _domain(self):
        t0 = self.params[0]
        return {'geocenter_time': (t0, t0)}

class UniformTime(AdjustableTimeDistribution):
    """\
Uniform event time
    p(t) = (1/dur)
where
    t0 <= t <= t0 + dur
    """
    _normalized = True
    _params = ('t0', 'dur')

    def _init_values(self, t0=1e9, dur=86400*365):
        self._values = [t0, dur]

    def _domain(self):
        t0, dur = self.params
        return {'geocenter_time': (t0, t0+dur)}

    def _rvs(self, size=1):
        t0, dur = self.params
        return Uniform._static_rvs(t0, t0+dur, size=size)

    @staticmethod
    def _logprob(t, t0, dur):
        return Uniform._logprob(t, t0, t0+dur)

#-------------------------------------------------

class UniformSegments(TimeDistribution):
    """\
Defines a mixture model over UniformTime distributions defined by separate segments.
We define mixing fractions proportional to the segment weights.
    """
    _normalized = True
    _params = ()

    def _init_values(self, path=None):
        if path is None:
            raise ValueError('must supply the path to a segments CSV file!')
        self._path = path
        self._mixture = Mixture(*[(UniformTime(start, end-start), end-start) \
            for start, end in self._load_segments(path)])
        self._values = []

    @property
    def path(self):
        return self._path

    @property
    def mixture(self):
        return self._mixture

    def _domain(self):
        start = be.infty
        end = -be.infty
        for dist in self.mixture.distributions:
            t0, dur = dist.params
            start = min(t0, start)
            end = max(t0+dur, end)
        return {'geocenter_time': (start, end)}

    @staticmethod
    def _load_segments(path):
        """expect segments to be passed as an ASCII file with 2 columns (start and end times)
        """
        if path.endswith('txt') or path.endswith('txt.gz'):
            segs = genfromtxt(path)
        elif path.endswith('csv') or path.endswith('csv.gz'):
            segs = genfromtxt(path, delimiter=',')
        else:
            raise ValueError('file type not recognized for: '+path)
        return segs

    def rvs(self, *args, **kwargs):
        return self.mixture.rvs(*args, **kwargs)

    def logprob(self, *args, **kwargs):
        return self.mixture.logprob(*args, **kwargs)
