"""a module that houses various basic functions for popluation models
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

from gwdistributions.utils import Parameters

#-------------------------------------------------

class SamplingDistributionArgumentError(Exception):
    """an exception raised when SamplingDistribution cannot parse the arguments passed to prob or logprob
    """

    def __init__(self, variates, args):
        message = '''
SamplingDistribution._check_variates could not interpret args!.
Please pass one of the following:
    a single 2D array-like argument with shape (num_variates, num_samples)
    a list of 1D array-like arguments (one for each variate)
    a single object from which a list of 1D array-like arguments can be extracted via [args[key] for key in variates]
if len(variates) == 1, you may also pass a single int or float.
You passed
    num_variates = %d
    variates = %s
    shape(args) = %s
    args = %s
    type(args) = %s'''%(len(variates), variates, be.shape(args), args, type(args))
        Exception.__init__(self, message)

#-------------------------------------------------
# Sampling Distribution
#-------------------------------------------------

class SamplingDistribution(object):
    """a thin wrapper around some functionality to compute basic attributes of distributions
    """
    cosmological = False ### used to control option lookup when parsing objects from INI
    _normalized = False
    _params = ()
    _scalar_variates = ()
    _vector_variates = ()
    _required = ()

    def __init__(self, *args, **kwargs):
        self._init_values(*args, **kwargs)
        self._Nvariates = len(self._scalar_variates)+len(self._vector_variates)
        self._init_param2ind()
        self._parameters = Parameters(self)

    def _init_values(self):
        self._values = []

    def _init_param2ind(self):
        self._param2ind = dict((name, ind) for ind, name in enumerate(self._params))
        self._Nparams = len(self._params)

    @property
    def normalized(self):
        return self._normalized

    @property
    def variates(self): ### support dynamic renaming
        return self.scalar_variates + self.vector_variates

    @property
    def scalar_variates(self):
        return tuple(names.name(_) for _ in self._scalar_variates)

    @property
    def vector_variates(self):
        return tuple(names.name(_) for _ in self._vector_variates)

    @property
    def num_variates(self):
        return self._Nvariates

    def domain(self, *required): ### support dynamic renaming
        return dict((names.name(key), val) for key, val in self._domain(*required).items())

    def _domain(self, *required):
        return dict((variate, (-be.infty, +be.infty)) for variate in self.scalar_variates)

    @property
    def required(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._required)

    @property
    def param_names(self):
        return self._params

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==self.num_params
        self._values[:] = new

    @property
    def num_params(self):
        return self._Nparams

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, new):
        '''expects the new parameters to be a dictionary, which we then pass to self.update
        '''
        self.update(**new)

    def __getitem__(self, name):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        return self._values[self._param2ind[name]]

    def __setitem__(self, name, value):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        self._values[self._param2ind[name]] = value

    def __items__(self):
        return list(zip(self._params, self._values))

    def update(self, **kwargs):
        '''update the parameters of this sampling distribution'''
        for name, param in kwargs.items():
            self[name] = param

    @staticmethod
    def _check_variates(variates, args):
        """checks whether args has the correct format. Will also manipulate some common formats into the correct format if possible.
        returns corectly_formated_args (suitable to be passed to _logprob)
        """
        num_variates = len(variates)
        num_args = len(args)

        if num_args == 1: ### a single argument
            args = args[0]

            if isinstance(args, (int, float, be.ArrayType)): # is this an int, float, or array?
                if num_variates == 1: # distribution is 1D
                    if isinstance(args, (int, float)):
                        args = [args]

                    elif be.ndim(args)<=1: # this is fine
                        if args.dtype.names: ### a structured array
                            args = [args[variates[0]]]
                        else:
                            args = [args]

                    elif (be.ndim(args) == 2) and (len(args) == 1): # shape(args) = (1, num_samples)
                        pass ### args is fine

                    else:
                        raise SamplingDistributionArgumentError(variates, args)

                else: ### try to interpret args as an array with shape (num_variates, num_samples)
                    if (isinstance(args, be.ArrayType) and (be.ndim(args)==2) and (len(args) == num_variates)):
                        pass # args should be good as-is

                    elif isinstance(args, be.ArrayType) and args.dtype.names: ### maybe a structured array
#                        args = [be.array(args[var]) for var in variates]
                        args = [args[var] for var in variates]

                    else:
                        raise SamplingDistributionArgumentError(variates, args)

            else: # try to access keys like a dictionary
                if hasattr(args, '__getitem__'):
#                    args = [be.array(args[var]) for var in variates] # cast so we recognize this on the next loop
                    args = [args[var] for var in variates] # cast so we recognize this on the next loop

                else:
                    raise SamplingDistributionArgumentError(variates, args)

        elif num_args == num_variates: ### expect each arg to be an int, float, or array
            if not all([isinstance(_, (int, float, be.ArrayType)) for _ in args]):
                raise SamplingDistributionArgumentError(variates, args)

            else:
                pass # args should be acceptable as-is

        else:
            raise SamplingDistributionArgumentError(variates, args)

        # now return args
        return args

    def rvs(self, *required, **kwargs):
        """draw a specified number of realizations from this distribution
        expect kwargs to just be "size", but signature is delcared this way to be compatible with static methods declared in some child classes
        """
        return self._rvs(*self._check_variates(self.required, required), **kwargs)

    def _rvs(self, *required, **kwargs):
        raise NotImplementedError('child classes should override this')

    def prob(self, *args):
        """return the value of the distribution at the specified point
        signature should be prob(self, *variates, *required)
        """
        return be.exp(self.logprob(*args))

    def logprob(self, *args):
        """return the natural log of the value of the distribution at the specified point
        signature should be logprob(self, *variates, *required)
        """
        args = tuple(self._check_variates(self.variates+self.required, args))
        args += tuple(self.params) ### pass this way so that autoconditioning is faster
        return self._logprob(*args)

    def _logprob(self, *args):
        raise NotImplementedError('child classes should override this')

#-------------------------------------------------
# a few standard 1D distributions
#-------------------------------------------------

class _StandardDistribution(SamplingDistribution):
    """a class for standard distributions
    """
    _scalar_variates = ('variate',)
    _param_prefixes = ()
    _param_defaults = ()

    @property
    def _params(self):
        param = self._scalar_variates[0]
        return tuple(param if prefix is None else '%s_%s'%(prefix, param) for prefix in self._param_prefixes)

    def _init_values(self, **kwargs):
        self._values = [kwargs.pop(key, default) for key, default in zip(self._params, self._param_defaults)]
        if kwargs: ### there were unrecognized kwargs
            raise TypeError('unknown kwargs : '+', '.join(kwargs.keys()))

#------------------------

class Fixed(_StandardDistribution):
    """a class that implments a fixed distribution (delta-function
    """
    _noramlized = False
    _param_prefixes = (None,)
    _param_defaults = (0,)

    def _rvs(self, size=1):
        return self._static_rvs(self.params[0], size=size)

    @staticmethod
    def _static_rvs(param, size=1):
        return param*be.ones((size,1))

    @staticmethod
    def _logprob(param, val):
        return be.where(param==val, 0., -be.infty)

#------------------------

class Zero(Fixed):
    """a class that implements a fixed distribution centered on zero
    """
    _params = ()
    _defaults = ()

    @staticmethod
    def _rvs(size=1):
        return be.zeros((size,1))

    @staticmethod
    def _logprob(param):
        return Fixed._logprob(param, 0.0)

#------------------------

class _BoundedDistribution(_StandardDistribution):
    """a class that implements a bounded distribution
    """
    _param_prefixes = ('min', 'max')
    _param_defaults = (0, 1)

    def _init_values(self, **kwargs):
        _StandardDistribution._init_values(self, **kwargs)
        assert self._values[1] >= self._values[0], 'maximum must be larger than minimum'

    def _domain(self):
        return dict([(self._scalar_variates[0], tuple(self.params[:2]))])

#------------------------

class Uniform(_BoundedDistribution):
    """a class that implements a uniform distribution between bounds
    """
    _normalized = True
    _param_prefixes = ('min', 'max')
    _param_defaults = (0, 1)

    def _rvs(self, size=1):
        return self._static_rvs(*self.params[:2], size=size)

    @staticmethod
    def _static_rvs(m, M, size=1):
        return m + (M-m)*be.random(size=(size,1))

    @staticmethod
    def _logprob(variate, min_variate, max_variate):
        return be.where(
            (min_variate <= variate) & (variate <= max_variate),
            -be.log(max_variate - min_variate),
            -be.infty
        )

#------------------------

class PowerLaw(_BoundedDistribution):
    """a class that implements a truncated power law
if pow_variate == -1:
    p(variate) = variate**-1 / log(max_variate/min_variate)
else
    p(variate) = (pow_variate+1) * variate**pow_variate / (max_variate**(pow_variate+1) - min_variate**(pow_variate+1))
with
    min_variate <= variate <= max_variate
    """
    _normalized = True
    _param_prefixes = ('min', 'max', 'pow')
    _param_defaults = (0, 1, 0.0)

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(m, M, p, size=1):
        if p == -1:
            return be.array(m * (M/m)**be.random(size=(size,1)))

        else:
            p1 = p+1
            if p1 < 0:
                m, M = M, m ### flip order for delegation to Uniform
            return Uniform._static_rvs(m**p1, M**p1, size=size)**(1./p1)

    @staticmethod
    def _logprob(variate, min_variate, max_variate, pow_variate):
        p1 = pow_variate + 1
        return be.where(
            (max_variate >= variate) & (variate >= min_variate),
            be.where(
                pow_variate == -1,
                -be.log(variate) - be.log(be.log(max_variate) - be.log(min_variate)),
                be.log(be.abs(p1)) + pow_variate*be.log(variate) - be.log(be.abs(max_variate**p1 - min_variate**p1))
            ),
            -be.infty
        )

#------------------------

class TruncatedGaussian(_BoundedDistribution):
    """a class that implements a truncated Gaussian
    p(param) ~ exp( -0.5 * (param-mean_param)**2 / stdv_param**2 ) * Theta(min_param <= param <= max_param)
where Theta() is the indicator function (1 when the argument is true, 0 else)
    """
    _normalized = True
    _param_prefixes = ('min', 'max', 'mean', 'stdv')
    _param_defaults = (0, 10, 5, 3)

    @staticmethod
    def _cdf(x, min_param, max_param, mean_param, stdv_param):
        logcdf = be.norm.logcdf((x-mean_param)/stdv_param)
        logcdf_low = be.norm.logcdf((min_param - mean_param)/stdv_param)
        logcdf_high = be.norm.logcdf((max_param - mean_param)/stdv_param)

        # NOTE, this is computed via logcdf to retain high precision when bounds are far away from the mean
        # and at the same time allow for logcdf_low -> -infty
        return be.where(
            min_param <= x,
            be.where(
                x <= max_param,
                be.exp(logcdf - logcdf_high) * (1 - be.exp(logcdf_low - logcdf)) / (1 - be.exp(logcdf_low - logcdf_high)),
                1.0,
            ),
            0.0,
        )

    @staticmethod
    def _log_normalization(min_param, max_param, mean_param, stdv_param):
        logcdf_low = be.norm.logcdf((min_param - mean_param)/stdv_param)
        logcdf_high = be.norm.logcdf((max_param - mean_param)/stdv_param)
        return logcdf_high + be.log(1 - be.exp(logcdf_low - logcdf_high))

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(m, M, mean, stdv, size=1):
        """acomplished via inverse transform sampling
        """
        ### preform inverse transform sampling
        # note, this might be sped up if we pre-computed CDF...
        x = be.linspace(m, M, max(101, int(50*(M-m)/stdv)))
        cdf = TruncatedGaussian._cdf(x, m, M, mean, stdv)
        return be.interp(be.random(size=size), cdf, x).reshape((size,1))

    @staticmethod
    def _logprob(param, min_param, max_param, mean_param, stdv_param):
        return be.where(
            (min_param <= param) & (param <= max_param),
            -0.5 * (param-mean_param)**2 / stdv_param**2 - 0.5*be.log(2*be.pi) - be.log(stdv_param) \
                - TruncatedGaussian._log_normalization(min_param, max_param, mean_param, stdv_param),
            -be.infty
        )

#------------------------

class TruncatedLogNormal(_BoundedDistribution):
    """a class that implements a truncated log-normal
    p(param) ~ (1/param) * exp( -0.5*(log(param)-mu)**2 / stdv**2 ) * Theta(min_param <= param <= max_param)
where Theta() is the indicator function (1 when the argument is true, 0 else)
    """
    _normalized = True
    _param_prefixes = ('min', 'max', 'mean_log', 'stdv_log')
    _param_defaults = (0, 10, 5, 3)

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(m, M, mean_log, stdv_log, size=1):
        """delegate to TruncatedGaussian
        """
        return be.exp(TruncatedGaussian._static_rvs(be.log(m), be.log(M), mean_log, stdv_log, size=size))

    @staticmethod
    def _logprob(param, min_param, max_param, mean_log_param, stdv_log_param):
        """delegate to TruncatedGaussian and then add the jacobian
        """
        return TruncatedGaussian._logprob(
            be.log(param),
            be.log(min_param),
            be.log(max_param),
            mean_log_param,
            stdv_log_param,
        ) - be.log(param) ### include jacobian

#------------------------

class Beta(_BoundedDistribution):
    """a beta distribution
    param ~ (param-min_param)**(alpha_param-1) * (max_param-(param-min_param))**(beta_param-1)
where
    min_param <= param <= max_param
    """
    _normalized = True
    _param_prefixes = ('min', 'max', 'alpha', 'beta')
    _param_defaults = (0, 1, 1, 1)

    def _init_values(self, **kwargs):
        _BoundedDistribution._init_values(self, **kwargs)
        assert self._values[-2] > 0, self._params[-2]+' must be larger than 0.0'
        assert self._values[-1] > 0, self._params[-1]+' must be larger than 0.0'

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(min_q, max_q, alpha_q, beta_q, size=1):
        return be.array(min_q + (max_q - min_q)*be.beta.rvs(alpha_q, beta_q, size=(size,1)))

    @staticmethod
    def _logprob(q, min_q, max_q, alpha_q, beta_q):
        x = (q-min_q) / (max_q - min_q)
        return be.where(
            (0<=x) & (x<=1),
            be.array(be.beta.logpdf(x, alpha_q, beta_q) - be.log(max_q - min_q)), ### include jacobian
            -be.infty,
        )

#-------------------------------------------------
# Mixture Models of SamplingDistributions
#-------------------------------------------------

class Mixture(SamplingDistribution):
    """a general class that implements mixture models of sampling distributions
    """
    _normalized = True ### we require all sub-models to be normalized, so this is normalized

    _param_tmp = '%s_component%d'
    _mixture_frac_tmp = 'mixture_frac%d'

    def __init__(self, *subpopulations):
        """pass subpopulations as separate arguments with the format: (SamplingDistribution, mixing_fraction). Can support an arbitrary number of subpopulations.
        """
        assert len(subpopulations), 'please provide at least 1 subpopulation!'

        # parse subpopulations into a coherent set of things
        distributions = []
        param2distribution_param = dict()
        frac_inds = []
        scalar_variates = None
        vector_variates = None
        required = set()
        values = []
        params = []
        for ind, (dist, frac) in enumerate(subpopulations):
            assert dist.normalized, 'all subpopulations within Mixture models must be normalized!'

            # check variates
            if scalar_variates is None: ### first element in the list
                scalar_variates = dist.scalar_variates ### call it like this to support EventGenerator and SamplingDistribution
                vector_variates = dist.vector_variates
            else:
                assert scalar_variates == dist.scalar_variates, \
                    'scalar_variates must be idential for all subpopulations!'
                assert vector_variates == dist.vector_variates, \
                    'scalar_variates must be idential for all subpopulations!'

            # take the union of what is required
            required = required.union(set(dist.required)) ### support EventGenerator and SamplingDistribution

            # concatenate paramters and their names
            values += list(dist.params) + [frac]
            for p in dist._params:
                new_p = self._param_tmp%(p, ind) ### new param name
                params.append(new_p)
                param2distribution_param[new_p] = (ind, p) ### map to look up parameter name in subpops easily
            params += [self._mixture_frac_tmp%ind] ### add the mixing fraction
            frac_inds.append(len(params)-1) ### store the indecies of mixing fractions

            # add the subpop to the list
            distributions.append(dist)

        # store the results
        self._scalar_variates = tuple(names.invname(name) for name in scalar_variates) ### undo naming convention
        self._vector_variates = tuple(names.invname(name) for name in vector_variates) ### undo naming convention
        self._Nvariates = len(scalar_variates) + len(vector_variates)

        self._required = tuple(names.invname(name) for name in required) ### undo naming convention

        self._values = list(values)
        self._params = tuple(params)

        self._num_components = len(distributions)
        self._distributions = tuple(distributions)
        self._frac_inds = frac_inds

        # set up the look-up logic for __getitem__, __setitem__, and other reference tools
        self._parameters = Parameters(self)
        self._init_param2ind()
        self._param2distribution_param = param2distribution_param

    #---

    @property
    def num_components(self):
        return self._num_components

    @property
    def distributions(self):
        return self._distributions

    @property
    def mixing_fractions(self):
        return be.array([self.params[ind] for ind in self._frac_inds])

    @property
    def normalized_mixing_fractions(self):
        fracs = self.mixing_fractions
        return fracs / be.sum(fracs) ### normalize these

    #---

    def _domain(self, *required):
        """compute the domain of each variate as the supremum of the domains from each subpopulation
        """
        required = dict(zip(self.required, required)) ### make this a dictionary
        domains = [dist.domain(*[required[key] for key in dist.required]) for dist in self.distributions]
        return dict((key, (min(d[key][0] for d in domains), max(d[key][1] for d in domains))) for key in self.scalar_variates)

    def __setitem__(self, name, new):
        SamplingDistribution.__setitem__(self, name, new) ### update the local reference
        ### also update the value in the subpopulation
        if name in self._param2distribution_param: ### handle the case where we update mixing fraction
            ind, param = self._param2distribution_param[name]
            self.distributions[ind][param] = new

    #---

    @staticmethod
    def _rvs_distributions(normalized_mixing_fractions, size=1):
        """sample from the distributions via inverse-transform sampling over the cumulative distribution defined by normalized_mixing_fractions
        """
        cweights = be.cumsum(normalized_mixing_fractions)
        inds = be.arange(len(normalized_mixing_fractions))
        ans = []
        for r in be.random(size=size):
            ans.append( inds[r <= cweights][0] )
        return ans

    def _rvs(self, *required, **kwargs):
        """obtained by first sampling subpopulations according to the mixing fractions and then sampling from each subpop as needed
        """
        # parse the input arguments
        required = dict(zip(self.required, required))
        required = [[required[key] for key in dist.required] for dist in self.distributions]

        size = kwargs.pop('size', 1) ### extract size from kwargs since it is a special case

        # set up the result
        ans = []

        # iterate, sampling according to mixing fractions and then from each subpop
        for ind in self._rvs_distributions(self.normalized_mixing_fractions, size=size):
            ans.append(self.distributions[ind].rvs(*required[ind], size=1)[0,:]) ### sample from the chosen distribution

        # return
        return be.array(ans)

    def logprob(self, *args):
        """obtained by computing the probability for each subpopuation and then taking a sum weighed by the mixing fractions
        """
        # iterate over subpops and compute logprob as a sum weighed by mixing fractions
        prob = 0.
        for frac, dist in zip(self.normalized_mixing_fractions, self.distributions):
            prob = prob + frac * dist.prob(*args) ### delegate for heavy lifting

        # return
        return be.log(prob)
