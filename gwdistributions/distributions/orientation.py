"""source distributions for how events' orientations are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import SamplingDistribution
from gwdistributions.distributions.base import Uniform
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class InclinationDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over inclination
    """
    _scalar_variates = ('inclination',)

    @staticmethod
    def _domain():
        return {'inclination': (0.0, be.pi)}

class PolarizationDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over polariztions
    """
    _scalar_variates = ('polarization',)

    @staticmethod
    def _domain():
        return {'polarization': (0.0, be.pi)}

class CoalescencePhaseDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over coalesence phase (coa_phase)
    """
    _scalar_variates = ('coalescence_phase',)

    @staticmethod
    def _domain():
        return {'coalescence_phase': (0.0, 2*be.pi)}

class OrientationDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over orientation
    """
    _scalar_variates = InclinationDistribution._scalar_variates \
        + PolarizationDistribution._scalar_variates \
        + CoalescencePhaseDistribution._scalar_variates

    @staticmethod
    def _domain():
        items = list(InclinationDistribution._domain().items()) \
            + list(PolarizationDistribution._domain().items()) \
            + list(CoalescencePhaseDistribution._domain().items())
        return dict(items)

#-------------------------------------------------

class IsotropicInclination(InclinationDistribution):
    """\
isotropic inclination
    p(inclination) = sin(inclination)  with 0 <= inclination <= pi
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return be.arccos(Uniform._static_rvs(-1, +1, size=size))

    @staticmethod
    def _logprob(inclination):
        return Uniform._logprob(be.cos(inclination), -1, +1) + be.log(be.clip(be.sin(inclination), 0, 1))

class RandomPolarization(PolarizationDistribution):
    """\
random polarization
    p(polarization) = 1/pi
with
    0 <= polarization <= pi
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return Uniform._static_rvs(0, be.pi, size=size)

    @staticmethod
    def _logprob(psi):
        return Uniform._logprob(psi, 0, be.pi)

class RandomCoalescencePhase(CoalescencePhaseDistribution):
    """\
random coalescence phase
    p(coa_phase) = 1/(2*pi)
with
    0 <= coa_phase <= 2*pi
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return Uniform._static_rvs(0, 2*be.pi, size=size)

    @staticmethod
    def _logprob(coa_phase):
        return Uniform._logprob(coa_phase, 0, 2*be.pi)

class RandomOrientation(OrientationDistribution):
    """\
random event orientation
    p(inclination, polarization, coa_phase) = p(inclination) * p(polarization) * p(coa_phase)
with
    p(inclination) = sin(inclination)  with 0 <= inclination <= pi
    p(polarization) = 1/pi             with 0 <= polarization <= pi
    p(coa_phase) = 1/(2*pi)            with 0 <= coa_phase <= 2*pi
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return be.transpose(be.array([
            IsotropicInclination._rvs(size=size)[:,0],
            RandomPolarization._rvs(size=size)[:,0],
            RandomCoalescencePhase._rvs(size=size)[:,0],
        ]))

    @staticmethod
    def _logprob(inclination, psi, coa_phase):
        return IsotropicInclination._logprob(inclination) \
            + RandomPolarization._logprob(psi) \
            + RandomCoalescencePhase._logprob(coa_phase)
