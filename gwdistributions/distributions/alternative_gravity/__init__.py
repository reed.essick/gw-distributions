"""a module housing distributions over parametric deviations from GR
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from .tiger_imrphenomxphm import *
