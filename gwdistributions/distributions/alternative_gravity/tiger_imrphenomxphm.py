"""a module containing distributions over the TIGER parameterization of deviations from GR based on the IMRPhenomXPHM waveform model
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from gwdistributions.distributions.base import (SamplingDistribution, Zero, Uniform, TruncatedGaussian)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class TIGERdChiDistribution(SamplingDistribution):
    """a distribution over the TIGER parametrization of deviations from GR
    """
    _scalar_variates = ('dchi',)

    def _domain(self):
        return {self._scalar_variates[0]: (-be.infty, +be.infty)}

#-------------------------------------------------

# child classes for each order

#-------------------------------------------------

class TIGERdChiMinus2Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchim2
    """
    _scalar_variates = ('dchim2',)

#-----------

class ZeroTIGERdChiMinus2(TIGERdChiMinus2Distribution, Zero):
    """zero deviation dchim2
    """

#-----------

class UniformTIGERdChiMinus2(TIGERdChiMinus2Distribution, Uniform):
    """\
uniform distribution over dchim2
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChiMinus2(TIGERdChiMinus2Distribution, TruncatedGaussian):
    """\
Generates dchim2 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi0Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi0
    """
    _scalar_variates = ('dchi0',)

#-----------

class ZeroTIGERdChi0(TIGERdChi0Distribution, Zero):
    """zero deviation dchi0
    """

#-----------

class UniformTIGERdChi0(TIGERdChi0Distribution, Uniform):
    """\
uniform distribution over dchi0
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi0(TIGERdChi0Distribution, TruncatedGaussian):
    """\
Generates dchi0 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi1Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi1
    """
    _scalar_variates = ('dchi1',)

#-----------

class ZeroTIGERdChi1(TIGERdChi1Distribution, Zero):
    """zero deviation dchi1
    """

#-----------

class UniformTIGERdChi1(TIGERdChi1Distribution, Uniform):
    """\
uniform distribution over dchi1
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi1(TIGERdChi1Distribution, TruncatedGaussian):
    """\
Generates dchi1 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi2Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi2
    """
    _scalar_variates = ('dchi2',)

#-----------

class ZeroTIGERdChi2(TIGERdChi2Distribution, Zero):
    """zero deviation dchi2
    """

#-----------

class UniformTIGERdChi2(TIGERdChi2Distribution, Uniform):
    """\
uniform distribution over dchi2
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi2(TIGERdChi2Distribution, TruncatedGaussian):
    """\
Generates dchi2 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi3Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi3
    """
    _scalar_variates = ('dchi3',)

#-----------

class ZeroTIGERdChi3(TIGERdChi3Distribution, Zero):
    """zero deviation dchi3
    """

#-----------

class UniformTIGERdChi3(TIGERdChi3Distribution, Uniform):
    """\
uniform distribution over dchi3
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi3(TIGERdChi3Distribution, TruncatedGaussian):
    """\
Generates dchi3 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi4Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi4
    """
    _scalar_variates = ('dchi4',)

#-----------

class ZeroTIGERdChi4(TIGERdChi4Distribution, Zero):
    """zero deviation dchi4
    """

#-----------

class UniformTIGERdChi4(TIGERdChi4Distribution, Uniform):
    """\
uniform distribution over dchi4
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi4(TIGERdChi4Distribution, TruncatedGaussian):
    """\
Generates dchi4 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi5Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi5
    """
    _scalar_variates = ('dchi5',)

#-----------

class ZeroTIGERdChi5(TIGERdChi5Distribution, Zero):
    """zero deviation dchi5
    """

#-----------

class UniformTIGERdChi5(TIGERdChi5Distribution, Uniform):
    """\
uniform distribution over dchi5
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi5(TIGERdChi5Distribution, TruncatedGaussian):
    """\
Generates dchi5 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#------------------------

class TIGERdChi5LDistribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi5L (log term)
    """
    _scalar_variates = ('dchi5L',)

#-----------

class ZeroTIGERdChi5L(TIGERdChi5LDistribution, Zero):
    """zero deviation dchi5L (log term)
    """

#-----------

class UniformTIGERdChi5L(TIGERdChi5LDistribution, Uniform):
    """\
uniform distribution over dchi5L (log term)
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi5L(TIGERdChi5LDistribution, TruncatedGaussian):
    """\
Generates dchi5L (log term) drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi6Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi6
    """
    _scalar_variates = ('dchi6',)

#-----------

class ZeroTIGERdChi6(TIGERdChi6Distribution, Zero):
    """zero deviation dchi6
    """

#-----------

class UniformTIGERdChi6(TIGERdChi6Distribution, Uniform):
    """\
uniform distribution over dchi6
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi6(TIGERdChi6Distribution, TruncatedGaussian):
    """\
Generates dchi6 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#------------------------

class TIGERdChi6LDistribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi6L (log term)
    """
    _scalar_variates = ('dchi6L',)

#-----------

class ZeroTIGERdChi6L(TIGERdChi6LDistribution, Zero):
    """zero deviation dchi6L (log term)
    """

#-----------

class UniformTIGERdChi6L(TIGERdChi6LDistribution, Uniform):
    """\
uniform distribution over dchi6L (log term)
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi6L(TIGERdChi6LDistribution, TruncatedGaussian):
    """\
Generates dchi6L (log term) drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class TIGERdChi7Distribution(TIGERdChiDistribution):
    """a distribution over the deviation : dchi7
    """
    _scalar_variates = ('dchi7',)

#-----------

class ZeroTIGERdChi7(TIGERdChi7Distribution, Zero):
    """zero deviation dchi7
    """

#-----------

class UniformTIGERdChi7(TIGERdChi7Distribution, Uniform):
    """\
uniform distribution over dchi7
    """

    def _domain(self):
        return Uniform._domain(self)

#-----------

class TruncatedGaussianTIGERdChi7(TIGERdChi7Distribution, TruncatedGaussian):
    """\
Generates dchi7 drawn from a truncated Gaussian distribution
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)
