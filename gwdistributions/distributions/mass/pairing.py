"""a module that support classes that build 2D mass distributions out of 1D mass distributions and pairing probabilities
"""
__author__ = "Reed Essick <reed.essick@gmail.com>, Maya Fishbach <maya.fishbach@ligo.org>"

#-------------------------------------------------

from .utils import (Mass1Distribution, ComponentMassDistribution)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class ComponentMassPairing(ComponentMassDistribution):
    '''an object that automatically combines single-mass distributions to create a joint distribution according to a pairing probability
    '''
    _normalized = False
    _extra_params = ()
    _extra_required = ()

    def _init_values(self, massdist=None):
        if massdist is None:
            raise ValueError('must supply a 1D mass distribution!')
        assert massdist.num_variates == 1, 'mass distribution must be one-dimensional'

        self._massdist = massdist
        self._values = massdist._values ### NOTE: we hold on to a shared reference here!
        self._params = self.massdist._params + self._extra_params ### store all the params, including the massdist params
        self._required = self.massdist._required + self._extra_required

    @property
    def massdist(self):
        return self._massdist

    def _domain(self, *required):
        tup = self.massdist._domain(*required)['mass1_source']
        return dict(mass1_source=tup, mass2_source=tup)

    @property
    def params(self):
        return self._values ### NOTE: we have to redeclare this because (for some reason) it is not inherited correctly from SamplingDistribution

    @params.setter
    def params(self, new):
        assert len(new) == len(self.params)
        self._values = new
        self.massdist.params = new[:self.massdist.num_params]

    def update(self, **kwargs):
        for name, param in kwargs.items():
            if name in self._extra_params:
                self[name] = param
            elif name in self.massdist._params:
                self.massdist[name] = self[name] = param
            else:
                pass

    ### sampling
    def _rvs(self, *required, size=1., max_iter=be.infty, max_draw=1e6):
        '''draw samples via rejection sampling. NOTE: this may be relatively slow...
        '''
        ans_m1 = []
        ans_m2 = []
        N = 0

        ### FIXME: may fail if dist.required != ()
        max_pairing = self._max_pairing(*self.massdist.domain(*required)['mass1_source'])

        it = 0
        num_draw = size ### the number of samples we draw each epoch
                        ### updated based on estimate of acceptance fraction
        max_draw = size if size > max_draw else max_draw ### never draw too many

        while (N < size) and (it < max_iter):

            # draw from Mass1Distribution separately for each component
            mas = self.massdist._rvs(*required, size=num_draw)[:,0]
            mbs = self.massdist._rvs(*required, size=num_draw)[:,0]

            # map into m1, m2 based on relative size
            ms = be.array([mas, mbs])
            m1s = be.max(ms, axis=0)
            m2s = be.min(ms, axis=0)

            # compute weights
            weights = self.pairing(m1s, m2s) / max_pairing ### normalize the weights
                                                           ### needed to make rejection sampling work correctly!

            # draw random subset from weights
            selected = be.random(size=num_draw) < weights

            ### record the samples
            n = min(be.sum(selected), size-N)

            ans_m1 += list(m1s[selected][:n])
            ans_m2 += list(m2s[selected][:n])

            # increment the number of samples found
            N += n

            # increment iteration number
            it += 1

            # update num_draw based on approximate acceptance rate
            frac = 1.* max(1, n) / num_draw
            num_draw = int(min(max_draw, (size-N) / frac)) ### never draw too many in a single epoch

        if it == max_iter:
            raise RuntimeError('too many iterations (%d) without drawing %d samples'%(max_iter, size))

        return be.transpose(be.array([ans_m1, ans_m2]))

    ### pairing functions
    def pairing(self, m1, m2):
        return be.exp(self.logpairing(m1, m2))

    def logpairing(self, m1, m2):
        return be.where(m1 >= m2, 0., -be.infty) ### requirement that m1 >= m2

    def _max_pairing(self, mass_min, mass_max):
        """used to normalize the weights within rejection sampling
        """
        return 1.0

    ### probability distributions
    def _logprob(self, m1, m2, *ignored):
        return self.massdist.logprob(m1) + self.massdist.logprob(m2) + self.logpairing(m1, m2)

#------------------------

class PowerLawAsymmetricMassRatioTotalMassPairing(ComponentMassPairing):
    '''takes in any Mass1Distribution and adds parameters (betaq and betam), so that the two masses drawn from separately from the Mass1Distribution are paired according to (m2/m1)**betaq * (m1+m2)**betam, where m1 >= m2
    '''
    _extra_params = ('pairing_pow_q', 'pairing_pow_m')
    
    def _init_values(self, massdist=None, pairing_pow_q=0.0, pairing_pow_m=0.0):
        ComponentMassPairing._init_values(self, massdist=massdist)
        self._values = list(self.massdist._values) + [pairing_pow_q, pairing_pow_m]

    ### pairing functions
    def logpairing(self, m1, m2):
        betaq, betam = self.params[-2:]
        return be.where(m1 >= m2, betaq * (be.log(m2) - be.log(m1)) + betam * be.log(m1 + m2), -be.infty)

    def _max_pairing(self, mass_min, mass_max):
        """used to normalize the weights within rejection sampling
        """
        betaq, betam = self.params[-2:]
        # NOTE, this may actually overestimate how big this could be (inefficient rejection sampling)
        #       biggest possible contrib from q           biggest possible contrib from total_mass
        return (mass_max / mass_min)**be.abs(betaq) * (2*(mass_max if betam > 0 else mass_min))**betam

#------------------------

class BinnedPowerLawAsymmetricMassRatioPairing(ComponentMassPairing):
    '''takes in any Mass1Distribution and adds parameters (betaq_low, betaq_high, mbreak_pairing), so that the two masses drawn separately from the Mass1Distribution are paired according to

  p_pairing = (m2/m1)**betaq_low if m2 <= mbreak_pairing else (m2/m1)**betaq_high

This is based on/inspired by : https://arxiv.org/abs/2111.03498
    '''
    _extra_params = ('pairing_pow_q_low', 'pairing_pow_q_high', 'pairing_mbreak')

    def _init_values(self, massdist=None, pairing_pow_q_low=0.0, pairing_pow_q_high=0.0, pairing_mbreak=5.0):
        ComponentMassPairing._init_values(self, massdist=massdist)
        self._values = list(self.massdist._values) + [pairing_pow_q_low, pairing_pow_q_high, pairing_mbreak]

    ### pairing functions
    def logpairing(self, m1, m2):
        beta_low, beta_high, mbreak = self.params[-3:]
        return be.where(
            m1 >= m2,
            be.where(m2 <= mbreak, beta_low, beta_high) * (be.log(m2) - be.log(m1)),
            -be.infty,
        )

    def _max_pairing(self, mass_min, mass_max):
        """used to normalize the weights within rejection sampling
        """
        beta_low, beta_high, mbreak = self.params[-3:]
        # NOTE, this may actually overestimate how big this could be (inefficient rejection sampling)
        return (mass_max / mass_min)**be.max(be.abs(be.array([beta_low, beta_high])), axis=0)

class ContinuousBinnedPowerLawAsymmetricMassRatioPairing(BinnedPowerLawAsymmetricMassRatioPairing):
    '''An extension of BinnedPowerLawAsymmetricMassRatioPairing that adds a function of mass1 to the pairing function to make it continuous over m2

  p_pairing = (m2/m1)**betaq_low if m2 <= mbreak_pairing else coef*(m2/m1)**betaq_high

where coef is chosen to make the pairing function continuous at mbreak_pairing
    '''

    ### pairing functions
    def logpairing(self, m1, m2):
        beta_low, beta_high, mbreak = self.params[-3:]
        return BinnedPowerLawAsymmetricMassRatioPairing.logpairing(self, m1, m2) \
            + be.where(m2 <= mbreak, 0.0, (beta_low-beta_high)*(be.log(mbreak) - be.log(m1)))

    def _max_pairing(self, mass_min, mass_max):
        beta_low, beta_high, mbreak = self.params[-3:]
        return BinnedPowerLawAsymmetricMassRatioPairing._max_pairing(self, mass_min, mass_max) \
            * be.exp(be.abs(be.log(mass_max) - be.log(mbreak)))**be.abs(beta_low-beta_high)
