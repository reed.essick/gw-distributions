"""basic definition of mass distribution classes
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import warnings
from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions.backends import numerics as be

#-------------------------------------------------

### set up a custom warning for the mass orderings so that we can make Python always print that warning

class MassOrderWarning(RuntimeWarning):
    pass

warnings.simplefilter('always', MassOrderWarning)

#-------------------------------------------------

class MassDistribution(SamplingDistribution):
    """a parent class for all mass distributions. Primarily useful for gwdist-which
    """

#-------------------------------------------------

class ComponentMassDistribution(MassDistribution):
    """a child class used to declare things are mass distributions
    """
    _scalar_variates = ('mass1_source', 'mass2_source')

    @staticmethod
    def _domain():
        return dict((key, (0.0, +be.infty)) for key in ComponentMassDistribution._scalar_variates)

#-------------------------------------------------

class Mass1Distribution(MassDistribution):
    """a SamplingDistribution that describes how a single mass (mass1) is distributed. Can be combined through pairing functions to make regular MassDistribution objects, or can be used with a Mass2Distribution to emulate the behavior of a MassDistribution
    """
    _scalar_variates = ('mass1_source',)

    @staticmethod
    def _domain():
        return {'mass1_source': (0.0, +be.infty)}

class Mass2Distribution(MassDistribution):
    """a SamplingDistribution that describes how a single mass (mass2) is distributed, conditioned on (mass1). Can be combined with a Mass1Distribution to be equivalent to a regular MassDistribution
    """
    _scalar_variates = ('mass2_source',)
    _required = ('mass1_source',)

    @staticmethod
    def _domain(mass1):
        return {'mass2_source': (0.0, mass1)}

#-------------------------------------------------

class AsymmetricMassRatioDistribution(MassDistribution):
    """a SamplingDistribution that describes how the asymmetric mass ratio (mass2/mass1) is distributed.
    """
    _scalar_variates = ('asymmetric_mass_ratio',)

    @staticmethod
    def _domain():
        return {'asymmetric_mass_ratio': (0.0, 1.0)}

class SymmetricMassRatioDistribution(MassDistribution):
    """a SamplingDistribution that describes how the symmetric mass ratio (mass1*mass2/(mass1+mass2)**2) is distributed.
    """
    _scalar_variates = ('symmetric_mass_ratio',)

    @staticmethod
    def _domain():
        return {'symmetric_mass_ratio': (0.0, 0.25)}
