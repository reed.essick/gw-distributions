"""source distributions for how events masses are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.backends import numerics as be

from .utils import ComponentMassDistribution
from gwdistributions.distributions.base import TruncatedGaussian

#-------------------------------------------------

class FixedComponentMass(ComponentMassDistribution):
    """Generate mass pairs with specific values.
    """
    _normalized = True
    _params = ('mass1', 'mass2')

    def _init_values(self, mass1=10., mass2=10.):
        self._values = [mass1, mass2]

    def _rvs(self, size=1):
        mass1, mass2 = self.params
        return be.array([[mass1, mass2] for _ in range(size)])

    def _prob(self, mass1, mass2, fixed_mass1, fixed_mass2):
        return be.where((mass1==fixed_mass1) & (mass2==fixed_mass2), 1.0, 0.0)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

class PowerLawComponentMass(ComponentMassDistribution):
    """\
Power law distribution over component masses
    p(mass1, mass2) ~ mass1**mass1_pow * mass2**mass2_pow
with
    mass1_min <= mass1 <= mass1_max
    mass2_min <= mass2 <= mass2_max
    mass2 <= mass1
    """
    _normalized = True
    _params = (
        'min_mass1_source',
        'max_mass1_source',
        'pow_mass1_source',
        'min_mass2_source',
        'max_mass2_source',
        'pow_mass2_source',
    )

    def _init_values(
            self,
            min_mass1_source=1.0,
            max_mass1_source=100.0,
            pow_mass1_source=0.0,
            min_mass2_source=1.0,
            max_mass2_source=100.0,
            pow_mass2_source=0.0,
        ):
        ### needed for distribution to have any support
        assert max_mass1_source > min_mass2_source, 'max_mass1_source must be larger than min_mass2_source'
        self._values = [
            min_mass1_source,
            max_mass1_source,
            pow_mass1_source,
            min_mass2_source,
            max_mass2_source,
            pow_mass2_source,
        ]

    def _domain(self):
        min_mass1, max_mass1, _, min_mass2, max_mass2, _ = self.params
        return {
            'mass1_source': (max(min_mass1, min_mass2), max_mass1),
            'mass2_source': (min_mass2, min(max_mass1, max_mass2)),
        }

    def _rvs(self, size=1):
        """rejection sample
        """
        mass1_min, mass1_max, mass1_pow, mass2_min, mass2_max, mass2_pow = self.params
        ans_m1 = []
        ans_m2 = []
        N = 0
        while N < size:
            rands = be.random(size=(size,2))
            if mass1_pow == -1:
                m1 = mass1_min * (mass1_max/mass1_min)**rands[:,0]
            else:
                m1 = (mass1_min**(mass1_pow+1) + rands[:,0]*(mass1_max**(mass1_pow+1) - mass1_min**(mass1_pow+1)))**(1./(mass1_pow+1))

            if mass2_pow == -1:
                m2 = mass2_min * (mass2_max/mass2_min)**rands[:,1]
            else:
                m2 = (mass2_min**(mass2_pow+1) + rands[:,1]*(mass2_max**(mass2_pow+1) - mass2_min**(mass2_pow+1)))**(1./(mass2_pow+1))

            selected = m1 >= m2
            n = min(be.sum(selected), size-N)
            ans_m1 += list(m1[selected][:n])
            ans_m2 += list(m2[selected][:n])
            N += n

        return be.transpose(be.array([ans_m1, ans_m2]))

    @staticmethod
    def _prob(mass1, mass2, mass1_min, mass1_max, mass1_pow, mass2_min, mass2_max, mass2_pow):
        return be.where(
            (mass1_min <= mass1) & (mass1 <= mass1_max) & (mass2_min <= mass2) & (mass2 <= mass2_max) & (mass2 <= mass1),
            mass1**mass1_pow * mass2**mass2_pow / PowerLawComponentMass._norm(mass1_max, mass2_max, mass1_min, mass2_min, mass1_pow, mass2_pow),
            0.0,
        )

    @staticmethod
    def _norm(mass1_max, mass2_max, mass1_min, mass2_min, mass1_pow, mass2_pow):
        """NOTE: assumes (mass1_min < mass1_max) and (mass2_min < mass2_max) and (mass2_min < mass1_max) so that we have nonvanishing support
More details of the derivation of this normalization can be found here:
    https://git.ligo.org/reed.essick/gw-distributions/-/blob/master/gwdistributions/distributions/mass/PowerLawComponentMass.pdf
The current implementation may break if mass1_pow and mass2_pow are arrays
        """
        ### cast these to arrays so we avoid cases where we raise zero (float) to negative powers
        ### this is needed because be.where will evaluate all arguments, and therefore may still try to raise a float to a negative power 
        ### even though that value will not be stored at the end of the day
        mass1_max = be.array(mass1_max, dtype=float)
        mass2_max = be.array(mass2_max, dtype=float)
        mass1_min = be.array(mass1_min, dtype=float)
        mass2_min = be.array(mass2_min, dtype=float)
        mass1_pow = be.array(mass1_pow, dtype=float)
        mass2_pow = be.array(mass2_pow, dtype=float)

        # figure out the boundaries of the distribution
        mass_min = be.max(be.array([mass1_min, mass2_min]), axis=0)
        mass_max = be.min(be.array([mass1_max, mass2_max]), axis=0)

        ### compute the normalization as the integral over a big rectangle minus a little triangle over the region excluded by (mass2 <= mass1)
        # start with the big rectangle, which means the joint integral factors
        big_rectangle = be.where(mass1_pow==-1, be.log(mass1_max/mass_min), (mass1_max**(mass1_pow+1) - mass_min**(mass1_pow+1))/(mass1_pow+1)) \
            * be.where(mass2_pow==-1, be.log(mass_max/mass2_min), (mass_max**(mass2_pow+1) - mass2_min**(mass2_pow+1))/(mass2_pow+1))

        # compute the result that we need to subtract
        lil_triangle = be.where(
            mass_min < mass_max, ### only compute this where the integral is positive
            be.where(
                mass2_pow==-1,
                be.where(
                    mass1_pow==-1,
                    be.log(mass_max)*be.log(mass_max/mass_min) - 0.5*(be.log(mass_max)**2 - be.log(mass_min)**2), ### mass1_pow == -1, mass2_pow == -1
                    (mass1_pow+1)**-1 * (mass_max**(mass1_pow+1) - mass_min**(mass1_pow+1)) * be.log(mass_max) \
                      - (mass1_pow+1)**-2 * (mass_max**(mass1_pow+1)*((mass1_pow+1)*be.log(mass_max) - 1) \
                          - mass_min**(mass1_pow+1)*((mass1_pow+1)*be.log(mass_min) -1)) ### mass1_pow != -1, mass2_pow == -1
                ),
                be.where(
                    mass1_pow==-1,
                    (mass2_pow+1)**-1 * mass_max**(mass2_pow+1) * be.log(mass_max/mass_min) \
                        - (mass2_pow+1)**-2 * (mass_max**(mass2_pow+1) - mass_min**(mass2_pow+1)), ### mass1_pow == -1, mass2_pow != -1
                    mass_max**(mass2_pow+1) * (mass_max**(mass1_pow+1) - mass_min**(mass1_pow+1)) / ((mass1_pow+1)*(mass2_pow+1)) \
                        - (mass2_pow+1)**-1 \
                        * be.where(
                            mass1_pow + mass2_pow == -2,
                            be.log(mass_max/ mass_min),
                            (mass1_pow+mass2_pow+2)**-1 * (mass_max**(mass1_pow+mass2_pow+2) - mass_min**(mass1_pow+mass2_pow+2))
                        ) ### mass1_pow != -1, mass2_pow != -1
                ),
            ),
            0.
        )

        # return the result
        return big_rectangle - lil_triangle

    def _logprob(self, *args, **kwargs):
        return be.log(self._prob(*args, **kwargs))

#-------------------------------------------------

class TruncatedGaussianComponentMass(ComponentMassDistribution):
    """implements a joint Gaussian distribution for masses that is defined between bounds
    p(mass1, mass2) ~ exp(-0.5*( (mass1 - mass_mean)**2 + (mass2 - mass_mean)**2 ) / mass_sigma**2 )
with
    mass1_min <= mass1 <= mass1_max
    mass2_min <= mass2 <= mass2_max
    mass2 <= mass1
    """
    _normalized = True
    _params = (
        'min_mass1_source',
        'max_mass1_source',
        'min_mass2_source',
        'max_mass2_source',
        'mean_mass_source',
        'stdv_mass_source',
    )

    def _init_values(
            self,
            min_mass1_source=0,
            max_mass1_source=2.5,
            min_mass2_source=0,
            max_mass2_source=2.5,
            mean_mass_source=1.33,
            stdv_mass_source=0.09,
        ):
        self._values = [
            min_mass1_source,
            max_mass1_source,
            min_mass2_source,
            max_mass2_source,
            mean_mass_source,
            stdv_mass_source,
        ]

    def _domain(self):
        mass1_min, mass1_max, mass2_min, mass2_max = self.params[:-2]
        return {
            "mass1_source": (max(mass1_min, mass2_min), mass1_max),
            "mass2_source": (mass2_min, min(mass1_max, mass2_max)),
        }

    def _rvs(self, size=1):
        """accomplished via rejection sampling
        """
        min1, max1, min2, max2, mean, sigma = self.params
        ans_m1 = []
        ans_m2 = []

        ### preform inverse transform sampling
        # note, this might be sped up if we pre-computed CDF...
        mass1 = be.linspace(min1, max1, int(50*(max1-min1)/sigma))
        cdf1 = TruncatedGaussian._cdf(mass1, min1, max1, mean, sigma)

        mass2 = be.linspace(min2, max2, int(50*(max2-min2)/sigma))
        cdf2 = TruncatedGaussian._cdf(mass2, min2, max2, mean, sigma)

        n = 0
        while n < size:
            m1 = be.interp(be.random(size=size), cdf1, mass1)
            m2 = be.interp(be.random(size=size), cdf2, mass2)
            keep = (m2 <= m1)
            N = be.sum(keep)
            ans_m1 += list(m1[keep])
            ans_m2 += list(m2[keep])
            n += N

        return be.transpose(be.array([ans_m1, ans_m2]))

    @staticmethod
    def _logprob(mass1, mass2, mass1_min, mass1_max, mass2_min, mass2_max, mean, sigma):
        return be.where(
            (mass1_min <= mass1) & (mass1 <= mass1_max) & (mass2_min <= mass2) & (mass2 <= mass2_max) & (mass2 <= mass1),
            -0.5 * ((mass1-mean)**2 + (mass2-mean)**2) / sigma**2 \
                - be.log(TruncatedGaussianComponentMass._norm(mass1_min, mass1_max, mass2_min, mass2_max, mean, sigma)),
            -be.infty,
        )

    @staticmethod
    def _norm(mass1_min, mass1_max, mass2_min, mass2_max, mean, sigma):
        """the normalization is computed using a similar approach to PowerLawComponentMass. We break the integral into a rectangular region 
and subtract a triangular region if necessary
        """
        mass_min = be.max(be.array([mass1_min, mass2_min]), axis=0)
        mass_max = be.min(be.array([mass1_max, mass2_max]), axis=0)

        ### compute the big rectangle, which factors nicely because of the assumed functional form
        logrectangle = TruncatedGaussian._log_normalization(mass1_min, mass1_max, mean, sigma) \
            + TruncatedGaussian._log_normalization(mass2_min, mass_max, mean, sigma)

        ### compute the triangular region
        logcdf_max = be.norm.logcdf((mass_max-mean)/sigma)
        logcdf_min = be.norm.logcdf((mass_min-mean)/sigma)

        logtriangle = be.where(mass_min < mass_max,
            2*logcdf_max + be.log((1 - be.exp(logcdf_min-logcdf_max)) - 0.5*(1 - be.exp(2*(logcdf_min-logcdf_max)))),
            -be.infty,
        )

        ### return the difference
        return be.exp(logrectangle)*(1 - be.exp(logtriangle-logrectangle)) * 2*be.pi * sigma**2
