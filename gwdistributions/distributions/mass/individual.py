"""a module that houses basic single-mass distributions, from which we can construct more complicated joint distributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>, Maya Fishbach <maya.fishbach@ligo.org>"

#-------------------------------------------------

import warnings

from .utils import (MassOrderWarning, Mass1Distribution, Mass2Distribution)
from gwdistributions.distributions.base import (PowerLaw, TruncatedGaussian, TruncatedLogNormal)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

### mass1 distributions

class PowerLawMass1(Mass1Distribution, PowerLaw):
    """a truncated power law in Mass1. This can be thought of as a special case of NPiecePowerLawMass1, but is impleme
nted separately
    """

    def _domain(self):
        return PowerLaw._domain(self) ### need to overwrite the inheritance from Mass1Distribution

#------------------------

class NPiecePowerLawMass1(Mass1Distribution):
    """a simple power law with hard cut-off and abrupt transitions between components
    mass1 ~ \prod_0^{N-1} (break_i <= mass1)*( (mass1 < break_(i+1)) * (mass_1/break_1) + (mass1 >= break_(i+1))*(break_(i+1)/break_i) )**alpha_i
    """
    _normalized = True

    def _init_values(self, warn=False, min_mass1_source=1, max_mass1_source=100, alpha0=-3, **kwargs):
        '''dynamically parse kwargs to figure out how many power-law components there are
        '''
        self.warn = warn ### whether to print warnings about mass orderings

        # check how many kwargs we got
        self._params = ['min_mass1_source', 'alpha0']
        self._values = [min_mass1_source, alpha0]
        N = 1
        while len(kwargs):
            try:
                params = ['break%d_mass1_source'%N, 'alpha%d'%N]
                values = [kwargs.pop(param) for param in params]
                self._params += params
                self._values += values
                N += 1
            except KeyError:
                break

        if len(kwargs):
            raise RuntimeError('Could not interpret all kwargs!\nCould not interpret : '+', '.join(kwargs.keys()))

        self._params.insert(0, 'Ncomp')
        self._values.insert(0, len(self._params)//2)

        self._params.append('max_mass1_source')
        self._values.append(max_mass1_source)

        ### cast these to be the types we expect more generally
        self._params = tuple(self._params)

        # check the mass ordering
        self._check_break_masses()

    @staticmethod
    def _break_masses(Ncomp, *params):
        ''' grabs just the break masses from list of parameters
        '''
        return params[0:int(Ncomp)*2+1:2]

    def _check_break_masses(self):
        ''' checks to make sure the break masses are in ascending order and creates a flag
        which is 1 if break masses are in ascending order and 0 otherwise.
        '''
        brk_masses = self._break_masses(*self.params)
        self._ascending_break_masses = be.all(brk_masses[1:] >= brk_masses[:-1])
        if self.warn and (not self._ascending_break_masses):
            warnings.warn('break masses not in ascending order! Probability will be set to zero', MassOrderWarning)

    @property
    def params(self): # redefine this here so that we can overwrite the setter
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==len(self._values)
        self._values[:] = new
        self._check_break_masses() ### update the flag for sensible break masses

    def update(self, **kwargs):
        '''update the parameters of this sampling distribution'''
        for name, param in kwargs.items():
            self[name] = param    
        self._check_break_masses() ### update the flag for sensible break masses

    def _domain(self):
        params = self.params
        Ncomp = params[0]
        masses = self._break_masses(*params)
        return {'mass1_source': (masses[0], masses[int(Ncomp)])}

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(*params, **kwargs): ### need declare signature this way to make parsing happy
        Ncomp = int(params[0])
        params = params[1:]
        size = kwargs.get('size', 1)

        segs = be.array(NPiecePowerLawMass1._norm_segs(Ncomp, *params))
        segs = segs / be.sum(segs)
        segs = be.cumsum(segs)
        inds = be.arange(Ncomp)

        ans = []
        for r in be.random(size=size):
            i = inds[segs > r][0] ### pick the smallest index as the "segment" where this sample lives
            mmin, alpha, mmax = params[2*i:2*i+3]
            ans.append( PowerLaw._static_rvs(mmin, mmax, alpha, size=1) )

        return be.array(ans).reshape((size, 1))

    @staticmethod
    def _norm_segs(Ncomp, *params):
        """assumes break masses are in ascending order
        """
        segs = []
        scale = 1.
        for i in range(int(Ncomp)): ### note, this may give nonsensical results if mmin > mmax
            mmin, alpha, mmax = params[2*i:2*i+3]
            alpha1 = be.array(alpha+1, dtype=float)
            new = be.where( ### use this syntax to make jax happy
                alpha == -1,
                be.log(mmax/mmin) * mmin,
                (mmax**alpha1 - mmin**alpha1) / (alpha1 * mmin**alpha),
            )

            segs.append( scale*new )
            scale *= (mmax/mmin)**alpha

        return segs

    @staticmethod
    def _logprob(mass1, Ncomp, *params):
        """assumes break masses are in ascending order
        """
        Ncomp = int(Ncomp)

        # now check the absolute boundaries
        mmin = params[0] ### the absolute minimum mass
        mmax = params[2*Ncomp] ### look this up in this way because children may add other parameters to the end
        logpdf = be.where((mmin <= mass1) & (mass1 < mmax), 0., -be.infty) ### if mass is outside bounds, zero the probability

        # previously we conditioned workflow on whether there were any finite values of logpdf
        # now we always iterate through because jax.jit compilation needs this
        # this does not appear to significantly worsen runtime performance
        for i in range(Ncomp):
            mmin, alpha, mmax = params[2*i:2*i+3]

            logpdf = logpdf + be.where(
                (mmin <= mass1), ### do not delegate to PowerLaw._logprob because upper boundary condition differs
                be.where(mass1 < mmax, alpha*(be.log(mass1) - be.log(mmin)), alpha*(be.log(mmax) - be.log(mmin))),
                0.
            )

        return logpdf - be.log(be.sum(be.array(NPiecePowerLawMass1._norm_segs(Ncomp, *params)), axis=0))

#------------------------

class ButterworthNotchNPiecePowerLawMass1(NPiecePowerLawMass1):
    """an extension of NPiecePowerLawMass1 that adds roll-offs instead of hard-cut-offs and adds a notch filter
based on : https://arxiv.org/abs/2111.03498
    """
    _normalized = False ### we cannot integrate through the bandpass filters
    _extra_params = (
        'highpass_mass_scale', 'highpass_exponent', # parameters of the high-pass filter at the lowest masses
        'lowpass_mass_scale', 'lowpass_exponent',   # parameters of the low-pass filter at the highest masses
        'notch_amplitude', 'notch_lowmass_scale', 'notch_lowmass_exponent',
        'notch_highmass_scale', 'notch_highmass_exponent', # notch filter
    )

    def _init_values(
            self,
            highpass_mass_scale=50, # parameters of the high-pass filter at the lowest masses
            highpass_exponent=10.0,
            lowpass_mass_scale=1.0, # parameters of the low-pass filter at the highest masses
            lowpass_exponent=50.0,
            notch_amplitude=0.5,    # notch filter
            notch_lowmass_scale=3.0,
            notch_lowmass_exponent=50.0,
            notch_highmass_scale=5.0,
            notch_highmass_exponent=50.0,
            **kwargs # NPiecePowerLawMass1 parameters
        ):
        NPiecePowerLawMass1._init_values(self, **kwargs)
        self._values = list(self._values) + [
            highpass_mass_scale, highpass_exponent, \
            lowpass_mass_scale, lowpass_exponent, \
            notch_amplitude, \
            notch_lowmass_scale, notch_lowmass_exponent, \
            notch_highmass_scale, notch_highmass_exponent, \
        ]
        self._params = tuple(list(self._params) + list(self._extra_params))

    @staticmethod
    def loglowpass(mass, scale, exponent):
        """the natural log of a low-pass filter when exponent > 0
        1./(1 + (mass/scale)**exponent)
        """
        return -be.log(1 + (mass/scale)**exponent)

    @staticmethod
    def loghighpass(mass, scale, exponent):
        """the natural log of a high-pass filter when exponent > 0
        1./(1 + (scale/mass)**exponent)
        """
        return ButterworthNotchNPiecePowerLawMass1.loglowpass(scale, mass, exponent)

    @staticmethod
    def lognotch(mass, amp, scale_low, exp_low, scale_high, exp_high):
        """the natural log of a notch filter composed of low- and high-pass filters and scaled by amp
        """
        low = ButterworthNotchNPiecePowerLawMass1.loglowpass(mass, scale_high, exp_high) # lowpass at high end
        hgh = ButterworthNotchNPiecePowerLawMass1.loghighpass(mass, scale_low, exp_low) # highpass at low end
        return be.log(1.0 - amp*be.exp(low + hgh))

    @staticmethod
    def logfilters(mass1, hps, hpe, lps, lpe, amp, sl, el, sh, eh):
        return ButterworthNotchNPiecePowerLawMass1.loghighpass(mass1, hps, hpe) \
            + ButterworthNotchNPiecePowerLawMass1.loglowpass(mass1, lps, lpe) \
            + ButterworthNotchNPiecePowerLawMass1.lognotch(mass1, amp, sl, el, sh, eh)

    #---

    def _rvs(self, size=1):
        """implemented via rejection sampling
draw samples from the corresponding NPiecePowerLawMass1 and then reject based on Butterworth filters
        """
        params = self.params[-9:] ### pull out extra params
        ans = []
        N = 0
        while N < size:
            mass = NPiecePowerLawMass1._rvs(self, size=size-N)[:,0]
            weights = be.exp(self.logfilters(mass, *params))
            sel = be.random(size=size-N) <= weights
            n = be.sum(sel)
            if n > 0:
                ans += list(mass[sel])
                N += n

        return be.array(ans, dtype=float).reshape((size,1))

    #---

    @staticmethod
    def _logprob(mass1, *params):
        return NPiecePowerLawMass1._logprob(mass1, *params) \
            + ButterworthNotchNPiecePowerLawMass1.logfilters(mass1, *params[-9:])

#-------------------------------------------------

### mass1 distributions conditioned on redshift
# NOTE: we may deprecate this later in favor of auto-conditioning

class NPiecePowerLawMass1GivenRedshift(NPiecePowerLawMass1):
    """a special case of NPiecePowerLaw that conditions mmin on redshift
    """
    _normalized = True
    _required = ('redshift',)

    def _init_values(self, warn=True, min_mmin=1, mmin0=1, dmmin_dz=0, max_mass1_source=100, alpha0=-3, **kwargs):
        '''dynamically parse kwargs to figure out how many power-law components there are
        '''
        NPiecePowerLawMass1._init_values( ### delegate for heavy lifting
            self,
            warn=warn,
            min_mass1_source=mmin0,
            max_mass1_source=max_mass1_source,
            alpha0=alpha0,
            **kwargs
        )

        ### now fix this to actually have the parameters we want
        self._params = list(self._params)
        self._params = [self._params[0]] + self._params[2:]

        self._values = list(self._values)
        self._values = [self._values[0]] + self._values[2:]

        ### add in parameters describing how to get mmin from z
        self._params += ['min_mmin', 'mmin0', 'dmmin_dz'] ### used to compute mmin(z) = mmin0 + dmmin_dz*z 
        self._values += [min_mmin, mmin0, dmmin_dz]

        ### cast these to be the types we expect more generally
        self._params = tuple(self._params)

    def _domain(self, z):
        params = self.params
        return {'mass1_source': (self.mmin(z, *params[-3:]), params[-4])}

    @staticmethod
    def mmin(z, min_mmin, mmin0, dmmin_dz):
        mmin = mmin0 + dmmin_dz * z
        return be.where(mmin < min_mmin, min_mmin, mmin) ### handle arrays

    def _rvs(self, z, size=1):
        return self._static_rvs(z, *self.params, size=size)

    @staticmethod
    def _static_rvs(*params, **kwargs): ### declare signature this way to make parser happy
        z = params[0]
        Ncomp = params[1]
        params = params[2:]
        size = kwargs.get('size', 1)

        mmin = NPiecePowerLawMass1GivenRedshift.mmin(z, *params[-3:])

        return NPiecePowerLawMass1._static_rvs(Ncomp, mmin, *params[:-3], size=size)

    @staticmethod
    def _logprob(mass1, z, Ncomp, *params):
        mmin = NPiecePowerLawMass1GivenRedshift.mmin(z, *params[-3:])
        return NPiecePowerLawMass1._logprob(mass1, Ncomp, mmin, *params[:-3])

#-------------------------------------------------

class TruncatedGaussianMass1(Mass1Distribution, TruncatedGaussian):
    """implements a Gaussian distribution between bounds
    p(mass1) ~ exp( -0.5 * (mass1-mass1_mean)**2 / mass1_sigma**2 ) * Theta(mass1_min <= mass1 <= mass1_max)
    """

    def _domain(self):
        return TruncatedGaussian._domain(self) ### overwrite inheritance from Mass1Distribution

class TruncatedLogNormalMass1(Mass1Distribution, TruncatedLogNormal):
    """implements a log-normal distribution for m1 between bounds
    """

    def _domain(self):
        return TruncatedLogNormal._domain(self) ### overwrite inheritance from Mass1Distribution

#-------------------------------------------------

### mass2 distributions conditioned on mass1

class PowerLawMass2(Mass2Distribution):
    """implements a power law distribution for m2 given m1
if beta == -1:
    p(mass2|mass1) = mass2**-1 / log(mass1/mass2_min)
else
    p(mass2|mass1) = (beta+1) * mass2**beta / (mass1**(beta+1) - mass2_min**(beta+1))
with
    mass2_min <= mass2 <= mass1
    """
    _normalized = True
    _params = ('min_mass2_source', 'pow_mass2_source')

    def _init_values(self, min_mass2_source=0.0, pow_mass2_source=0.0):
        self._values = [min_mass2_source, pow_mass2_source]

    def _domain(self, mass1):
        m2_min, beta = self.params
        return {'mass2_source': (m2_min, mass1)}

    # sampling
    def _rvs(self, m1, size=1):
        m2_min, beta = self.params
        return PowerLaw._static_rvs(m2_min, m1, beta, size=size)

    @staticmethod
    def _logprob(m2, m1, min_m2, pow_m2):
        return PowerLaw._logprob(m2, min_m2, m1, pow_m2)

class TruncatedGaussianMass2(Mass2Distribution):
    """implements a Gaussian within bounds for m2 given m1
    p(mass2|mass1) ~ exp(-0.5 * (mass2 - mass2_mean)**2 / mass2_sigma**2) * Theta(mass2_min <= mass2 <= min(mass1, mass2_max)
    """
    _normalized = True
    _params = ('min_mass2_source', 'max_mass2_source', 'mean_mass2_source', 'stdv_mass2_source')

    def _init_values(self, mean_mass2_source=1.33, stdv_mass2_source=0.09, min_mass2_source=0, max_mass2_source=2.5):
        self._values = [min_mass2_source, max_mass2_source, mean_mass2_source, stdv_mass2_source]

    def _domain(self, mass1):
        mmin, mmax = self.params[:2]
        return {'mass2_source':(mmin, min(mass1, mmax))}

    def _rvs(self, m1, size=1):
        """delegate to TruncatedGaussian._static_rvs
        """
        mmin, mmax, mean, sigma = self.params
        mmax = be.where(m1 <= mmax, m1, mmax)
        return TruncatedGaussian._static_rvs(mmin, mmax, mean, sigma, size=size)

    @staticmethod
    def _logprob(mass2, mass1, min_mass2, max_mass2, mass2_mean, mass2_sigma):
        """delegate to TruncatedGaussian._logprob
        """
        mass_max = be.where(mass1 <= max_mass2, mass1, max_mass2)
        return TruncatedGaussian._logprob(mass2, min_mass2, mass_max, mass2_mean, mass2_sigma)

class TruncatedLogNormalMass2(Mass2Distribution):
    """implements a log-normal distribution for m2 given m1
    """
    _normalized = True
    _params = ('min_mass2_source', 'max_mass2_source', 'mean_log_mass2_source', 'stdv_log_mass2_source')

    def _init_values(
            self,
            mean_log_mass2_source=be.log(1.33),
            stdv_log_mass2_source=0.1,
            min_mass2_source=1,
            max_mass2_source=2.5,
        ):
        self._values = [min_mass2_source, max_mass2_source, mean_log_mass2_source, stdv_log_mass2_source]

    def _domain(self, mass1):
        mmin, mmax = self.params[:2]
        return {'mass2_source':(mmin, min(mass1, mmax))}

    def _rvs(self, m1, size=1):
        """delegate to TruncatedLogNormal._static_rvs
        """
        mmin, mmax, mean, sigma = self.params
        mmax = be.where(m1 <= mmax, m1, mmax)
        return TruncatedLogNormal._static_rvs(mmin, mmax, mean, sigma, size=size)

    @staticmethod
    def _logprob(mass2, mass1, min_mass2, max_mass2, log_mass2_mean, log_mass2_sigma):
        """delegate to TruncatedGaussianMass1._logprob
        """
        mass_max = be.where(mass1 <= max_mass2, mass1, max_mass2)
        return TruncatedLogNormal._logprob(mass2, min_mass2, mass_max, log_mass2_mean, log_mass2_sigma)

