"""a module that implements distributions over mass ratios
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import (AsymmetricMassRatioDistribution, SymmetricMassRatioDistribution)
from gwdistributions.distributions.base import (PowerLaw, Beta)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

### Asymmetric Mass Ratio Distributions

class PowerLawAsymmetricMassRatio(AsymmetricMassRatioDistribution, PowerLaw):
    """a simple power law with hard cut-offs
    q ~ q**pow_q
where
    0.0 <= min_q <= q
    q <= max_q <= 1.0
    """

    def _domain(self):
        return PowerLaw._domain(self)

class BetaAsymmetricMassRatio(AsymmetricMassRatioDistribution, Beta):
    """a beta distribution for the asymmetric mass ratio
    q ~ (q-min_q)**(alpha_q-1) * (max_q-(q-min_q))**(beta_q-1)
where
    min_q <= q <= max_q
    """

    def _domain(self):
        return PowerLaw._domain(self)

#-------------------------------------------------

### FIXME: also implement SymmetricMassRatioDistributions?
