"""a module that houses distributions based on external lists of events
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import SamplingDistribution
from gwdistributions import gwdio
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class External(SamplingDistribution):
    """Represents a distribution by a set of discrete samples from an external source
    """
    _normalized = True
    _params = ()
    _values = ()

    def _init_values(self, path=None, weight_column=None, weight_column_is_log=False, **columns):
        """columns kwargs are maps from the variates (keys) to the column names in path (values)
        """
        if path is None: ### passed this way to make this plug into the more general 
                         ### SamplingDistribution.__init__ workflow
            raise ValueError('please supply a path to the external samples!')

        if not len(columns):
            raise ValueError('must specify at least one column to load!')

        self._path = path
        self._scalar_variates = tuple(sorted(columns.keys())) ### store the variates
        self._data = self.load(path, [columns[variate] for variate in self._scalar_variates])
        self._num_samples = len(self._data)

        ### figure out weights
        if weight_column is not None:
            self._weights = self.load(path, [weight_column])[:,0]
            if weight_column_is_log:
                self._weights = be.exp(self._weights - be.max(self._weights))
        else:
            self._weights = be.ones(self._num_samples, dtype=float)

        self._weights /= be.sum(self._weights)
        self._cumweights = be.cumsum(self._weights)

    @staticmethod
    def load(path, variates):
        array, meta = gwdio.file2array(path)
        return be.transpose([array[_] for _ in variates])

    def _rvs(self, size=1, *args):
        """accomplished by inverse transform sampling from the empirical CDF defined by weights
        """
        # compute a cdf and draw from it
        # we take the ceiling because cumweights starts above zero and ends at 1, so random samples 
        # should be assigned to the index just above them in the cumulative sum
        inds = be.interp(be.random(size=size), self._cumweights, be.arange(self._num_samples))
        return self._data[be.ceil(inds).astype(int)]

    def _logprob(self, *args):
        """Note, because it doesn't make sense to use this within auto-conditioning, and there are \
no _params, we do not declare this as a staticmethod
        """

        # check to see what type of arguments we have, make sure they're iterable
        return_float = isinstance(args[0], (int, float)) ### assume these all have the same shape
        if return_float:
            args = [[arg] for arg in args]
            N = 1
        else:
            N = len(args[0])

        # set up holders for results
        pdf = be.empty(N, dtype=float)
        truth = be.empty(self._num_samples, dtype=bool)

        # iterate over args
        for i, arg in enumerate(be.transpose(args)):
            ### arg should be a 1D array with length == len(self._data)
            truth[:] = True # reset this holder

            for j, val in enumerate(arg): # compare each dimension to self._data and require all 
                                          # dimensions to be a match
                truth &= val==self._data[:,j]

            pdf[i] = be.sum(self._weights[truth]) ### sum the weights for all samples that match in all dimensions

        ### convert back to a float if needed
        if return_float:
            pdf = pdf[0]

        ### return the log
        return be.log(pdf)
