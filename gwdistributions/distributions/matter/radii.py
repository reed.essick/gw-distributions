"""distributions for properties of compact objects that contain matter, such as tidal deformabilities and radii
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions.backends import numerics as be
from gwdistributions.backends.units import (G_CGS, MSUN_CGS, C_CGS)

from .utils import (path2branches, rvs_from_branches, logprob_from_branches, \
    PowerLawWithBoundedMass, FixedEoSSamplingDistribution)

#-------------------------------------------------

def Rschwarzschild_CGS(mass):
    """returns Schwarzschild radius in CGS units
    """
    return 2*G_CGS*(mass*MSUN_CGS)/C_CGS**2

#-------------------------------------------------

class R1Distribution(SamplingDistribution):
    """a child class that computes the radius conditioned on mass
    """
    _scalar_variates = ('radius1',)
    _required = ('mass1_source',)

    @staticmethod
    def _domain(*required):
        return {'radius1': (0.0, be.infty)}

    @staticmethod
    def default(mass1):
        return Rschwarzschild_CGS(mass1)

#-------------------------------------------------

### distributions for simple power laws of matter parameters bounded by basic ranges on the mass

class PowerLawR1(R1Distribution, PowerLawWithBoundedMass):
    """a power law for the radius between mass bounds. Radius is specified in cm (CGS)
if matter_mass_min <= mass1 <= matter_mass_max
    p(R1) ~ R1**R1_pow
else:
    p(R1) ~ delta(R1-Rschwarzschild(mass1)
"""
    _param_defaults = (6.0, 20.0, 0.0, 1.0, 3.0)

    @staticmethod
    def default(mass1):
        return Rschwarzschild_CGS(mass1)

#-------------------------------------------------

class FixedEoSR1(R1Distribution, FixedEoSSamplingDistribution):
    """determines R1 based on the mass given a fixed EoS
    """
    _normalized = False
    _params = ('eospath', 'mass_column', 'radius_column', 'central_density_column')

    def _init_values(
            self,
            path=None,
            mass_column='M',
            radius_column='R',
            central_density_column='central_baryon_density',
        ):
        if path is None:
            raise ValueError('must supply a path to the EoS')
        self._branches = path2branches(path, mass_column, central_density_column, columns=[radius_column])
        self._columns = [radius_column]
        self._values = [path, mass_column, radius_column, central_density_column]

    @property
    def branches(self):
        return self._branches

    @property
    def columns(self):
        return self._columns

    def _rvs(self, mass1, size=1):
        return rvs_from_branches(mass1, self.columns, [self.default(mass1)], self.branches, size=size)

    def _logprob(self, R1, mass1, *args): ### capture the unused arguments
        return logprob_from_branches(R1, mass1, self.columns, [self.default(mass1)], self.branches)

#-------------------------------------------------

### FIXME:
# implement mixture models over weighed sets of EoS? Should we just rely on the "default mixture model" 
# infrastructure and feed it many instances of FixedEoS?
