"""distributions for properties of compact objects that contain matter, such as tidal deformabilities and radii
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions.backends import numerics as be

from .utils import (path2branches, rvs_from_branches, logprob_from_branches, \
    PowerLawWithBoundedMass, FixedEoSSamplingDistribution)

#-------------------------------------------------

class Lambda1Distribution(SamplingDistribution):
    """a child class that computes the tidal deformability conditioned on mass
    """
    _scalar_variates = ('tidal_deformability1',)
    _required = ('mass1_source',)

    @staticmethod
    def _domain(*required):
        return {'tidal_deformability1': (0.0, be.infty)}

    @staticmethod
    def default(mass1):
        return 0.0 * mass1 ### gives the same shape as mass1

class Lambda1Lambda2Distribution(SamplingDistribution):
    """a child class that computes the tidal deformabilities for both components based on mass
    """
    _scalar_variates = ('tidal_deformability1', 'tidal_deformability2')
    _required = ('mass1_source', 'mass2_source')

    @staticmethod
    def _domain(*required):
        return dict((key, (0.0, be.infty)) for key in Lambda1Lambda2Distribution._scalar_variates)

    @staticmethod
    def default(mass1):
        return Lambda1Distribution.default(mass1)

#-------------------------------------------------

### distributions for simple power laws of matter parameters bounded by basic ranges on the mass

class PowerLawLambda1(Lambda1Distribution, PowerLawWithBoundedMass):
    """a power law for Lambda1 with matter_mass1_min < mass1 < matter_mass_max:
if matter_mass1_min < mass1 < matter_mass_max:
    p(lambda1) ~ lambda1**lambda_pow
else:
    p(lambda1) ~ delta(lambda1-0)
"""
    _param_defaults = (0.0, 5000.0, 0.0, 1.0, 3.0)

class PowerLawLambda1Lambda2(Lambda1Lambda2Distribution):
    """a power law for Lambda1, Lambda2 between matter_mass_min <= mass1, mass2 <= matter_mass_max:
returns the joint distribution based on independent PowerLawLambda1 for each component
"""
    _normlalized = False
    _params = (
        'min_tidal_deformability1',
        'max_tidal_deformability1',
        'pow_tidal_deformability1',
        'min_tidal_deformability2',
        'max_tidal_deformability2',
        'pow_tidal_deformability2',
        'min_matter_mass_source',
        'max_matter_mass_source',
    )

    def _init_values(
            self,
            min_tidal_deformability1=0,
            max_tidal_deformability1=5000,
            pow_tidal_deformability1=0,
            min_tidal_deformability2=0,
            max_tidal_deformability2=5000,
            pow_tidal_deformability2=0,
            min_matter_mass_source=0,
            max_matter_mass_source=2.5,
        ):
        self._values = [
            min_tidal_deformability1,
            max_tidal_deformability1,
            pow_tidal_deformability1,
            min_tidal_deformability2,
            max_tidal_deformability2,
            pow_tidal_deformability2,
            min_matter_mass_source,
            max_matter_mass_source,
        ]

    def _rvs(self, mass1, mass2, size=1):
        L1_min, L1_max, L1_pow, L2_min, L2_max, L2_pow, matter_mass_min, matter_mass_max = self.params
        L1 = PowerLawLambda1._static_rvs(
            mass1,
            self.default(mass1),
            L1_min,
            L1_max,
            L1_pow,
            matter_mass_min,
            matter_mass_max,
            size=size,
        )[:,0]
        L2 = PowerLawLambda1._static_rvs(
            mass2,
            self.default(mass2),
            L2_min,
            L2_max,
            L2_pow,
            matter_mass_min,
            matter_mass_max,
            size=size,
        )[:,0]
        return be.transpose(be.array([L1, L2]))

    def _logprob(
            self,
            lambda1,
            lambda2,
            mass1,
            mass2,
            lambda1_min,
            lambda1_max,
            lambda1_pow,
            lambda2_min,
            lambda2_max,
            lambda2_pow,
            matter_mass_min,
            matter_mass_max,
        ):
        logprob1 = PowerLawLambda1._logprob(
            self,
            lambda1,
            mass1,
            lambda1_min,
            lambda1_max,
            lambda1_pow,
            matter_mass_min,
            matter_mass_max,
        )
        logprob2 = PowerLawLambda1._logprob(
            self,
            lambda2,
            mass2,
            lambda2_min,
            lambda2_max,
            lambda2_pow,
            matter_mass_min,
            matter_mass_max,
        )
        return logprob1 + logprob2

#-------------------------------------------------

class FixedEoSLambda1(Lambda1Distribution, FixedEoSSamplingDistribution):
    """determines lambda1 based on the mass given a fixed EoS
    """
    _normalized = False
    _params = ('eospath', 'mass_column', 'lambda_column', 'central_density_column')

    def _init_values(
            self,
            path=None,
            mass_column='M',
            lambda_column='Lambda',
            central_density_column='central_baryon_density',
        ):
        if path is None:
            raise ValueError('must supply a path to the EoS')
        self._branches = path2branches(path, mass_column, central_density_column, columns=[lambda_column])
        self._columns = [lambda_column]
        self._values = [path, mass_column, lambda_column, central_density_column]

    @property
    def branches(self):
        return self._branches

    @property
    def columns(self):
        return self._columns

    def _rvs(self, mass1, size=1):
        return rvs_from_branches(mass1, self.columns, [self.default(mass1)], self.branches, size=size)

    def _logprob(self, lambda1, mass1, *args): ### capture the unused args
        return logprob_from_branches(lambda1, mass1, self.columns, [self.default(mass1)], self.branches)

class FixedEoSLambda1Lambda2(Lambda1Lambda2Distribution, FixedEoSSamplingDistribution):
    """determines lambda1 based on the mass given a fixed EoS
    """
    _normalized = False
    _params = ('eospath', 'mass_column', 'lambda_column', 'central_density_column')

    def _init_values(
            self,
            path=None,
            mass_column='M',
            lambda_column='Lambda',
            central_density_column='central_baryon_density',
        ):
        if path is None:
            raise ValueError('must supply a path to the EoS')
        self._branches = path2branches(path, mass_column, central_density_column, columns=[lambda_column])
        self._columns = [lambda_column]
        self._values = [path, mass_column, lambda_column, central_density_column]

    @property
    def branches(self):
        return self._branches

    @property
    def columns(self):
        return self._columns

    def _rvs(self, mass1, mass2, size=1):
        mass1 = rvs_from_branches(mass1, self.columns, [self.default(mass1)], self.branches, size=size)[:,0]
        mass2 = rvs_from_branches(mass2, self.columns, [self.default(mass2)], self.branches, size=size)[:,0]
        return be.transpose(be.array([mass1, mass2]))

    def _logprob(self, lambda1, lambda2, mass1, mass2, *args): ### capture the unused arguments
        return logprob_from_branches(lambda1, mass1, self.columns, [self.default(mass1)], self.branches) \
            + logprob_from_branches(lambda2, mass2, self.columns, [self.default(mass2)], self.branches)

#-------------------------------------------------

### FIXME:
# implement mixture models over weighed sets of EoS? Should we just rely on the "default mixture model" 
# infrastructure and feed it many instances of FixedEoS?
