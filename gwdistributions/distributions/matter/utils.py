"""a module that houses basic utilities common when dealing with matter parameters and EOS properties
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from numpy import genfromtxt ### used for I/O

from gwdistributions.distributions.base import (SamplingDistribution, PowerLaw, Fixed)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

DEFAULT_RULE = 'random'

#-------------------------------------------------

class MassDomainError(Exception):
    pass

class FixedEoSSamplingDistribution(object):
    """a dummy object that labels child classes as FixedEoSSamplingDistributions
    """

class StableBranch(object):
    """an object representing a stable branch of neutron star stellar sequences
    """
    def __init__(self, mass, **columns):
        self._mass = mass
        self._mass_min = be.min(mass)
        self._mass_max = be.max(mass)
        self._columns = columns

    @property
    def mass(self):
        return self._mass

    @property
    def mass_domain(self):
        return (self._mass_min, self._mass_max)

    @property
    def columns(self):
        return self._columns

    def spans(self, mass):
        return (self._mass_min <= mass) & (mass <= self._mass_max)

    def __call__(self, mass, column):
        assert column in self._columns, 'column=%s not understood!'%column
        if self.spans(mass):
            return be.interp(mass, self._mass, self._columns[column])
        else:
            raise MassDomainError

#-------------------------------------------------

class PowerLawWithBoundedMass(SamplingDistribution):
    """a parent class that defines methods for matter properties that depend on mass
    """
    _normalized = False
    _scalar_variates = ('param',)
    _param_prefixes = ('min', 'max', 'pow')
    _param_defaults = (0, 1, 0, 1.0, 3.0) ### include matter_mass_source defaults

    @staticmethod
    def default(mass1):
        return 0.0

    @property
    def _params(self):
        param = self._scalar_variates[0]
        return tuple(param if prefix is None else '%s_%s'%(prefix, param) for prefix in self._param_prefixes) \
            + ('min_matter_mass_source', 'max_matter_mass_source')

    def _init_values(self, **kwargs):
        self._values = [kwargs.pop(key, default) for key, default in zip(self._params, self._param_defaults)]
        if kwargs: ### there were unrecognized kwargs
            raise TypeError('unknown kwargs : '+', '.join(kwargs.keys()))
    
    def _rvs(self, mass1, size=1):
        return self._static_rvs(mass1, self.default(mass1), *self.params, size=size)

    @staticmethod
    def _static_rvs(mass1, default, minp, maxp, powp, matter_mass_min, matter_mass_max, size=1):
        if (matter_mass_min <= mass1) & (mass1 <= matter_mass_max):
            return PowerLaw._static_rvs(minp, maxp, powp, size=size)
        else:
            return Fixed._static_rvs(default, size=size)

    def _logprob(self, p, mass1, minp, maxp, powp, matter_mass_min, matter_mass_max):
        return be.where(
            (matter_mass_min <= mass1) & (mass1 <= matter_mass_max),
            PowerLaw._logprob(p, minp, maxp, powp),
            Fixed._logprob(p, self.default(mass1))
        )

#-------------------------------------------------

def path2branches(path, mass_column, central_density_column, columns=[], verbose=False):
    """load relations between macroscopic observables for a particular EOS from a file
    """
    ### load the data
    if verbose:
        print('reading EOS from: '+path)
    if path.endswith('csv') or path.endswith('csv.gz'):
        ans = genfromtxt(path, names=True, delimiter=',')
    elif path.endswith('txt') or path.endswith('txt.gz') or path.endswith('dat') or path.endswith('dat.gz'):
        ans = genfromtxt(path, names=True)
    else:
        raise ValueError('filetype not understood for path=%s!'%path)

    for column in [mass_column, central_density_column] + columns:
        assert column in ans.dtype.names, 'required column=%s not present in %s!'%(column, path)

    ### divide into stable branches
    # sort by increasing central density
    assert be.all(be.diff(ans[central_density_column]) >= 0), \
        'central_density=%s must monotonically increase within %s'%(central_density_column, path)

    # iterate to determine stable branches
    branches = []
    branch = None
    for i in range(len(ans)-1):
        if ans[mass_column][i+1] < ans[mass_column][i]: ### not on a stable branch
            if branch: ### on a stable branch
                branches.append(branch)
                branch = None
        elif branch: ### already on a stable branch
            branch.append(i+1)
        else: ### not on a stable branch
            branch = [i, i+1]
    if branch is not None: ### on a stable branch
        branches.append(branch)

    # convert to objects and return
    return [StableBranch(ans[mass_column][branch], **dict((col, ans[col][branch]) for col in columns)) \
        for branch in branches]

#------------------------

def rvs_from_branches(mass, columns, defaults, branches, size=1, multivalued_rule=DEFAULT_RULE):
    """implements logic for how to draw values from stable branches
    """
    assert isinstance(mass, (int, float)), 'mass must be a scalar!'
    assert len(columns) == len(defaults), 'columns and defaults must have the same length'

    # figure out which values correspond to this mass
    samples = massbranches2samples(mass, columns, branches)
    Nsamp = len(samples)

    if Nsamp <= 1: ### single-valued answer
        if Nsamp == 1: ### we have a match
            ans = samples[0]
        else:
            ans = defaults

    else: ### multivalued
        ans = select_from_multivalued(samples, rule=multivalued_rule, size=size)

    return be.array(ans).reshape((size, len(columns)))

def massbranches2samples(mass, columns, branches):
    samples = []
    for branch in branches:
        if branch.spans(mass):
            samples.append([branch(mass, col) for col in columns])
    return samples

def select_from_multivalued(samples, rule=DEFAULT_RULE, size=1):
    if rule == 'min':
        assert len(samples[0])==1, 'can only sample with rule="min" for a single column at a time'
        return be.min(be.array(samples))

    elif rule == 'max':
        assert len(samples[0])==1, 'can only sample with rule="max" for a single column at a time'
        return be.max(be.array(samples))

    elif rule == 'random':
        return [samples[ind] for ind in be.randint(len(samples), size=size)]

    else:
        raise ValueError('rule=%s not understood!'%rule)

#------------------------

def logprob_from_singlevalued(X, values):
    logprob = be.where(X == values, 0.0, -be.infty)
    if len(values) > 1: ### more than one column, so we need to sum
        logprob = be.sum(logprob, axis=1)
    return logprob

def logprob_from_multivalued(X, samples, rule=DEFAULT_RULE):
    if rule == 'min':
        assert len(samples[0])==1, 'can only sample with rule="min" for a single column at a time'
        return logprob_from_singlevalued(x, be.min(samples))

    elif rule == 'max':
        assert len(samples[0])==1, 'can only sample with rule="min" for a single column at a time'
        return logprob_from_singlevalued(X, be.max(samples))

    elif rule == 'random':
        ### sum over each branch, giving each branch equal probability
        norm = be.log(len(samples))
        probs = be.exp(logprob_from_singlevalued(X, samples[0]) - norm)
        for sample in samples[1:]:
            probs += be.exp(logprob_from_singlevalued(X, sample) - norm)
        return be.log(probs)
        
    else:
        raise ValueError('rule=%s not understood!'%rule)
        
#------------------------

def logprob_from_branches(X, mass, columns, defaults, branches, multivalued_rule=DEFAULT_RULE):
    """implements logic to evaluate logprob that matches how we sample with rvs_from_branches
    NOTE: currently only works with scalar values for mass
    """
    assert isinstance(mass, (int, float)), 'mass must be a scalar!'
    assert len(columns) == len(defaults), 'columns and defaults must have the same length'

    # figure out which values correspond to this mass
    samples = massbranches2samples(mass, columns, branches)

    Nsamp = len(samples)

    if Nsamp <= 1:
        if Nsamp == 0:
            samples = defaults
        else:
            samples = samples[0]

        return logprob_from_singlevalued(X, samples)

    else:
        return prob_from_multivalued(X, samples, rule=multivalued_rule)
