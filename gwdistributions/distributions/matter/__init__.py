"""distributions for properties of compact objects that contain matter, such as tidal deformabilities and radii
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .tides import *
from .radii import *
