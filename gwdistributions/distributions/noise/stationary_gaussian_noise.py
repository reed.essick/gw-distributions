"""a module that houses distributions that generate noise for GW observations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

#-------------------------------------------------

class StationaryGaussianComplexFrequencyNoise(SamplingDistribution):
    """generate and store noise realizations from individual detectors within a network
    note that we return freqs as a random variate even though it is deterministic. This is done so that \
event's will record the frequency vector corresponding to the noise realization.
    """
    _params = ('network', 'flow', 'fhigh', 'deltaf')

    def _init_values(self, network=None, flow=8, fhigh=2048, deltaf=1./8):
        assert network is not None, 'must specify a network!'
        self._values = [network, flow, fhigh, deltaf]

    @staticmethod
    def _noise_name(name):
        return '%s_%s'%(names.name('complex_frequency_noise'), name)

    @property
    def _vector_variates(self):
        network, fmin, fmax, deltaf = self.params
        return tuple([self._noise_name('freqs')] + [self._noise_name(detector.name) for detector in network])

    @property
    def freqs(self):
        network, fmin, fmax, deltaf = self.params
        return be.arange(fmin, fmax+deltaf, deltaf)

    def _rvs(self, size=1):
        network, fmin, fmax, deltaf = self.params
        freqs = self.freqs
        ans = []
        for ind in range(size):
            ans.append( [freqs] + [detector.draw_noise(freqs) for detector in network.detectors] )
        return be.array(ans, dtype=complex) # NOTE, this makes freqs complex, but we can live with that...

    def _logprob(self, *args):
        network = self.params[0]
        assert len(args) == len(network)+5, 'must specify one vector (%d) per detector in the network (%d)' % \
            (len(args), len(network))

        freqs = args[0].real ### just to make sure we handle casting correctly
        Nfreqs = len(freqs)

        logp = 0.0 ### iterate over detectors, assuming noise in each is independent
        for data, detector in zip(args[1:len(network)], network.detectors):
            assert Nfreqs == len(data), 'mismatch between length of freqs (%d) and data for detector=%s (%s)' % \
                (Nfreqs, detector.name, len(data))
            logp += detector.logprob(freqs, data)

        return logp
