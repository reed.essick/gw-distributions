"""a module that houses routines for adding noise to observations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from .stationary_gaussian_noise import *
