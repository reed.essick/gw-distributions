"""declaration of zodiac defined by the International Astronomical Union (IAU)
https://www.iau.org/public/themes/constellations/
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from numpy import genfromtxt
from pkg_resources import resource_filename

from gwdistributions.backends import numerics as be

from .utils import (FixedConstellationFromDisk, DEFAULT_NUM_AREA_POINTS)

from gwdistributions.distributions.base import Mixture
from gwdistributions.distributions.location.angles.utils import RADecDistribution

#-------------------------------------------------

with open(resource_filename(__name__, 'iau-constellation-names.txt'), 'r') as obj:
    KNOWN_IAU_CONSTELLATIONS = obj.read().strip().split()

#------------------------

class IAUConstellation(FixedConstellationFromDisk, RADecDistribution):
    """a class that represents one of the International Astronomical Union's constellations
    """

    def _init_values(self, name=None, num_points=DEFAULT_NUM_AREA_POINTS):
        self._name = name
        FixedConstellationFromDisk._init_values(self, path=name, num_points=num_points)

    @property
    def name(self):
        return self._name

    @staticmethod
    def _path2vertices(name, verbose=False):
        """loads vertex locations for the constellation "name"
        """
        assert name in KNOWN_IAU_CONSTELLATIONS, \
            'name=%s not a known IAU constellation. Must be one of : '%name + ', '.join(KNOWN_IAU_CONSTELLATIONS)

        path = resource_filename(__name__, '%s.csv.gz'%name)
        if verbose:
            print('loading IAUConstellation(%s) from %s'%(name, path))
        data = genfromtxt(path, delimiter=',', names=True)
        return be.transpose(be.array([data['right_ascension'], data['declination']]))

#-------------------------------------------------

class IAUConstellationMixture(Mixture, RADecDistribution):
    """a mixture model over all IAU constellations. Mixture weights are the relative probability \
that a signal came from a particular constellation (should be proportional to the constellation's area for isotropy)
    """

    def __init__(self, num_points=DEFAULT_NUM_AREA_POINTS, verbose=False, **params):

        # make an instance of each constellation
        args = []
        for name in KNOWN_IAU_CONSTELLATIONS:
            if verbose:
                print('instantiating %s with num_points=%d'%(name, num_points))
            obj = IAUConstellation(name, num_points=num_points)
            args.append( (obj, obj.area) )

        # now delegate to mixture to finish setting up
        Mixture.__init__(self, *args)

        # now update params (if specified)
        self.update(**params)

    @property
    def iau_names(self):
        return tuple(self._param_name2iau_name(name) for name in self.param_names)

    def _param_name2iau_name(self, name):
        return KNOWN_IAU_CONSTELLATIONS[int(name[12:])]
