"""basic declaration of Constellation object
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from gwdistributions.distributions.location.angles.utils import RADecDistribution
from gwdistributions.backends import numerics as be

#-------------------------------------------------

DEFAULT_NUM_AREA_POINTS = 100000 ### used to estimate area via monte carlo sampling from bounding box

#-------------------------------------------------

class FixedConstellation(RADecDistribution):
    """\
Assumes a uniform distribution within a user-defined polygon.
    """
    _normalized = True
    _vertices = ( ### these do not change for FixedConstellation
        (-be.pi, +0.25*be.pi),
        (-be.pi, -0.25*be.pi),
        (+be.pi, -0.25*be.pi),
        (+be.pi, +0.25*be.pi),
    )

    def _init_values(self, num_points=DEFAULT_NUM_AREA_POINTS):
        RADecDistribution._init_values(self)
        # set the rest of the properties
        self._init_vertices(self._vertices)
        self._init_bounding_box() # compute bounding box
        self._init_area(num_points=num_points) # compute area

    def _init_vertices(self, vertices):
        vertices = be.array(vertices, dtype=float)

        # check dec boundaries
        assert be.all(be.abs(vertices[:,1])) <= 0.5*be.pi, \
            'declinations must all be between [-pi/2, +pi/2]'

        # make sure smallest RA is within (0, 2pi)
        minra = be.min(vertices[:,0])
        vertices = be.transpose(be.array([vertices[:,0] - ((minra // (2*be.pi)) * 2*be.pi), vertices[:,1]]))

        # record the result
        self._vertices = vertices

    def _init_bounding_box(self):
        vertices = self.vertices
        minx = be.min(vertices[:,0])
        maxx = be.max(vertices[:,0])
        miny = be.min(vertices[:,1])
        maxy = be.max(vertices[:,1])
        self._bounding_box = be.array([(minx, miny), (maxx, maxy)])

        # assumes minx \in [0, 2.pi)
        self._wraps_high = maxx >= 2*be.pi ### need to wrap around RA when checking for points in contains

    def _init_area(self, num_points=100000):
        """monte carlo sample to estimate the area
        """
        _, count = self._draw_and_count(size=num_points)
        (min_ra, min_dec), (max_ra, max_dec) = self.bounding_box
        self._area = (max_ra - min_ra) * (be.sin(max_dec) - be.sin(min_dec)) * num_points / float(count)

    #---

    def _domain(self):
        (min_ra, min_dec), (max_ra, max_dec) = self.bounding_box
        if max_ra >= 2*be.pi: ### this wraps around, so any ra might be possible...
            min_ra = 0
            max_ra = 2*be.pi
        return {'right_ascension':(min_ra, max_ra), 'declination':(min_dec, max_dec)}

    @property
    def vertices(self):
        return self._vertices

    @property
    def edges(self):
        return self._vertices2edges(self.vertices)

    @staticmethod
    def _vertices2edges(vertices):
        """map a set of vertices into a set of segments that each never cross x=2*pi
    assumes vertices fall within x \in [0, 4*pi)
        """
        N = len(vertices)
        edges = []
        for (x, y), (X, Y) in [(vertices[i], vertices[(i+1)%N]) for i in range(N)]:
            if (x == 2*be.pi) and (X == 2*be.pi): ### special case
                edges.append( ((x, y), (X, Y)) )

            elif (x < 2*be.pi) == (X < 2*be.pi): ### segment does not cross the border
                edges.append( ((x % (2*be.pi), y), (X % (2*be.pi), Y)) )

            else: # segment crosses the border, so we split it into two edges
                if X < x: # make sure the smallest x is called 'x"
                    x, X = X, x
                    y, Y = Y, y
                y2pi = y + (2*be.pi - x) * (Y-y)/(X-x)
                edges.append( ((0, y2pi), (X % (2*be.pi), Y)) )
                edges.append( ((x, y), (2*be.pi, y2pi)) )
        return be.array(edges)

    @property
    def bounding_box(self):
        return self._bounding_box

    @property
    def area(self):
        return self._area

    def contains(self, ra, dec):
        """determines whether the points fall within the polygon by first checking the bounding box and \
then checking the polygon via ray tracing
        """
        return be.where(self._bounding_box_contains(ra, dec), self._polygon_contains(ra, dec), False)

    def _bounding_box_contains(self, ra, dec):
        """checks whether the polygon's bounding box contains the points
        """
        ra = ra % (2*be.pi) # undo periodicity
        (min_ra, min_dec), (max_ra, max_dec) = self.bounding_box
        contained_dec = (min_dec <= dec) & (dec <= max_dec)
        contained_ra = (min_ra <= ra) & (ra <= max_ra)
        if max_ra > 2*be.pi: ### wrap to check periodicity
            ra += 2*be.pi
            contained_ra |= (min_ra <= ra) & (ra <= max_ra)
        return contained_dec & contained_ra

    def _polygon_contains(self, ra, dec):
        """checks whether the polygon contains the points via "ray tracing". Defines vertices and edges as part of the polygon
        """
        # process input arguments
        return_scalar = be.shape(ra) == ()
        if return_scalar:
            ra = [ra]
            dec = [dec]

        Nsmp = len(ra)
        ra = be.array(ra, dtype=float) % (2*be.pi) # undo periodicity
        dec = be.array(dec, dtype=float)

        # expect more eval points than edges (or at least a small-ish number of edges)
        # iterate over edges directly, then use array manipulation for points
        num_edges_crossed = be.zeros(Nsmp, dtype=int) ### the number of edges each sample crosses via ray tracing

        ### NOTE: the "dec crossing" criterion ignores the possible effect of geodesics.
        ### This is probably fine for our purposes, but could be improved in the future
        for (ra1, dec1), (ra2, dec2) in self.edges:

            # check RA crossing
            min_ra = min(ra1, ra2)
            max_ra = max(ra1, ra2)
            crosses_ra = (min_ra <= ra) & (ra < max_ra) ### assumes min_ra, max_ra \in [0, 2pi)

            # now check Dec crossing where appropriate
            num_edges_crossed += be.where(
                crosses_ra, # points fall between this segment's RA bounds
                (dec <= dec1 + (ra-ra1)*(dec2-dec1)/(ra2-ra1)), # shoot ray "north" (increasing dec)
                0,
            )

        # now count the number of edges crossed for each sample
        ans = (num_edges_crossed % 2) > 0 # crosses an odd number of segments --> within the shape

        # return
        if return_scalar:
            return ans[0]

        return ans

    #---

    def _rvs(self, **kwargs):
        ans, count = self._draw_and_count(**kwargs)
        return ans

    def _draw_and_count(self, size=1, max_iter=be.infty, max_draw=1e6):
        """rejection sampling with samples drawn within the bounding box
        """
        size = int(size)
        max_draw = int(max_draw)
        ans = []

        count = 0
        N = 0
        it = 0
        num_draw = size
        max_draw = size if size > max_draw else max_draw ### never draw too many

        # pre-compute some useful things based on the bounding box
        (min_ra, min_dec), (max_ra, max_dec) = self.bounding_box
        diff_ra = max_ra - min_ra
        max_sindec = be.sin(max_dec)
        min_sindec = be.sin(min_dec)
        diff_sindec = max_sindec - min_sindec

        # iterate and rejection sample
        while (N < size) and (it < max_iter):
            # draw samples from distributions within bounding box that are uniform over the sky
            ra = (min_ra + diff_ra * be.random(size=num_draw) ) % (2*be.pi) ### undo periodicity
            dec = be.arcsin(min_sindec + diff_sindec * be.random(size=num_draw))

            # select those that fall within the polygon
            selected = self._polygon_contains(ra, dec) ### I drew from the bounding box, so check isn't needed
            Nselected = be.sum(selected)

            n = min(Nselected, size-N)

            selected_fraction = float(n)/Nselected if Nselected else 1
            count += selected_fraction * num_draw ### increment by approx fraction that we actuall keep

            # record
            ans += [(ra, dec) for ra, dec, selected in zip(ra, dec, selected) if selected][:n]

            # increment
            N += n
            it += 1

            # update num_draw based on acceptance rate
            frac = 1.*max(1, n) / num_draw
            num_draw = int(min(max_draw, (size-N) / frac))

        if it == max_iter:
            raise RuntimeError('too many iterations (%d) without drawing %d samples'%(max_iter, size))

        return be.array(ans), count

    def _logprob(self, ra, dec, *ignored):
        return be.where(self.contains(ra, dec), be.log(be.cos(dec)) - be.log(self.area), -be.infty)

#------------------------

class FixedConstellationFromDisk(FixedConstellation):
    """an extension of FixedConstellation that knows how to load vertices from disk
    """

    def _init_values(self, path=None, num_points=DEFAULT_NUM_AREA_POINTS):
        if path is None:
            raise ValueError('must supply a path!')
        self._vertices = self._path2vertices(path)
        FixedConstellation._init_values(self, num_points=num_points)

    @staticmethod
    def _path2vertices(path):
        """instructions to load vertices from disk. Should be overwritten by child classes
        """
        raise NotImplementedError

#------------------------

class Constellation(FixedConstellation):
    """an extension of FixedConstellation that supports dynamic assignment of vertices
    """
    _params = ('vertices',)

    def _init_values(self, vertices=None):
        if vertices is None:
            raise ValueError('must supply vertices as an iterable of (RA, Dec) points!')
        if len(vertices) < 3:
            raise ValueError('must supply at least 3 elements within vertices!')

        # set the rest of the properties
        self._init_vertices(vertices)
        self._values = [self._vertices]

        self._init_bounding_box() # compute bounding box
        self._init_area() # compute area

    @property
    def vertices(self):
        return self.params[0]

    #---

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        self._init_values(vertices=new)

    def __setitem__(self, name, value):
        if name != 'vertices': ### only a single parameter allowed
            raise KeyError('could not find parameter=%s'%name)
        self._init_values(vertices=value)
