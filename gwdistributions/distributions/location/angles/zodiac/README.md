The International Astronomical Union (IAU) constellation vertices are defined in:

  * iau-constellations.dat

This file was taken from <https://pbarbier.com/constellations/bound_ed.dat>, which in turn is based on

  * Eugene Delporte, *Delimitation Scientifique des Constellations*, Cambridge University Press, 1930.

For convenience of later instantiation, iau-constellations.dat was converted into separate csv.gz files for each constellation via

  * ./iau-constellations-dat2csv

These CSVs are what is loaded (dynamically) within iau.py's IAUConstellation.
