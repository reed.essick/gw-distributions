"""a module that implements RA, Dec distributions based on the healpix decomposition
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from gwdistributions.backends import numerics as be

from gwdistributions.distributions import Mixture
from gwdistributions.distributions.location.angles.utils import RADecDistribution
from .utils import FixedConstellation

try:
    import healpy
except ImportError:
    healpy = None

#-------------------------------------------------

class HealpixConstellation(FixedConstellation):
    """a class that represents a single pixel in a Healpy decomposition of the sky
    """
    _params = ('pix', 'nside')

    def _init_values(self, pix, nside=1):
        if healpy is None:
            raise ImportError('could not import healpy!')
        assert (0 <= pix) and (pix <= healpy.nside2npix(nside))

        self._values = [pix, nside]

        theta, phi = healpy.vec2ang(be.transpose(be.array(healpy.boundaries(nside, pix))))
        vertices = be.transpose(be.array([phi, 0.5*be.pi - theta]))

        # check to see if this pixel touches a pole
        if be.any(be.abs(vertices[:,1]) == 0.5*be.pi):
            vertices = list(vertices)
            for ind, (ra, dec) in enumerate(vertices):
                if be.abs(dec) == 0.5*be.pi:
                    vertices = vertices[:ind] \
                        + [(vertices[ind-1][0], dec), (vertices[ind+1][0], dec)] \
                        + vertices[ind+1:]
                    break
            vertices = be.array(vertices)

        # check to see if any vertices got wrapped when they shouldn't have
        if be.max(be.abs(vertices[1:,0] - vertices[:-1,0])) > be.pi:
             vertices = be.transpose(be.array([vertices[:,0] % (2*be.pi), vertices[:,1]]))
             vertices = be.transpose(be.array([
                 vertices[:,0] + be.where(vertices[:,0] < be.pi, 2*be.pi, 0),
                 vertices[:,1],
             ]))

        self._init_vertices(vertices)
        self._init_bounding_box()

    @property
    def _area(self):
        _, nside = self.params
        return healpy.nside2pixarea(nside)

#-------------------------------------------------

class HealpixConstellationMixture(Mixture, RADecDistribution):
    """a class that represents the distribution over RA, Dec with a healpix decomposition
    note that this is implemented directly rather than as an actual Mixture over HealpixConstellation
    """

    def __init__(self, nside=1, verbose=False, **params):
        if healpy is None:
            raise ImportError('could not import healpy!')

        # make an instance of each constellation
        args = []
        for pix in range(healpy.nside2npix(int(nside))):
            if verbose:
                print('instantiating pix=%d with nside=%d'%(pix, nside))
            obj = HealpixConstellation(pix, nside=nside)
            args.append( (obj, obj.area) )

        # now delegate to mixture to finish setting up
        Mixture.__init__(self, *args)

        # now update params (if specified)
        self.update(**params)
