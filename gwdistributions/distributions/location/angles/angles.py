"""source distributions for how events are distributed through space
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import (RADistribution, DecDistribution, RADecDistribution)
from gwdistributions.distributions.base import (Uniform, Fixed)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class IsotropicRA(RADistribution):
    """\
Random event orientation distributed isotropically on the sky
    p(RA) = (1/2*pi)
where
    0 <= RA <= 2*pi
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return Uniform._static_rvs(0, 2*be.pi, size=size)

    @staticmethod
    def _logprob(ra):
        return Uniform._logprob(ra, 0, 2*be.pi)

class IsotropicDec(DecDistribution):
    """\
Random event orientation distributed isotropically on the sky
    p(Dec) = cos(Dec)/2
where
    -pi/2 <= Dec <= pi/2
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return be.arcsin(Uniform._static_rvs(-1, +1, size=size))

    @staticmethod
    def _logprob(dec):
        return Uniform._logprob(be.clip(be.sin(dec), -1, +1), -1, +1) + be.log(be.clip(be.cos(dec), 0, 1))

class IsotropicRADec(RADecDistribution):
    """\
Random event orientation distributed isotropically on the sky
    p(RA, Dec) = (1/4*pi) * cos(Dec)
where
    0 <= RA <= 2*pi
    -pi/2 <= Dec <= pi/2
    """
    _normalized = True
    _parmas = ()

    @staticmethod
    def _rvs(size=1):
        return be.transpose(be.array([IsotropicRA._rvs(size=size)[:,0], IsotropicDec._rvs(size=size)[:,0]]))

    @staticmethod
    def _logprob(ra, dec):
        return IsotropicRA._logprob(ra) + IsotropicDec._logprob(dec)

#------------------------

class OptimalDec(DecDistribution):
    """\
Assumes the detector arms are aligned with the coordinate system, so zenith is at RA=0, Dec=pi/2.
Note that this is only optimally oriented in the long-wavelength approximation!
    p(Dec) =  delta(Dec - pi/2)
    """
    _normalized = False
    _params = ()

    @staticmethod
    def _rvs(size=1):
        return Fixed._static_rvs(0.5*be.pi, size=size)

    @staticmethod
    def _logprob(dec):
        return Fixed._logprob(dec, 0.5*be.pi)

class OptimalRADec(RADecDistribution):
    """\
Assumes the detector arms are aligned with the coordinate system, so zenith is at RA=0, Dec=pi/2.
Note that this is only optimally oriented in the long-wavelength approximation!
    p(RA, Dec) = (1/2pi) delta(Dec - pi/2)
    """
    _normalized = False
    _params = ()

    @staticmethod
    def _rvs(size=1):
        return be.transpose(be.array([IsotropicRA._rvs(size=size)[:,0], OptimalDec._rvs(size=size)[:,0]]))

    @staticmethod
    def _logprob(ra, dec):
        return IsotropicRA._logprob(ra) + OptimalDec._logprob(dec)

#-------------------------------------------------

class HemisphereRADec(RADecDistribution):
    """\
a very simple distribution that splits the sky into a northern and southern hemisphere.
parameters describe the relative rate of samples from each hemisphere. This model only uses the fraction of events \
that come from the northern hemisphere (north) and automatically computes the fraction that comes from the south as (1-north)

Note that this could also have been implemented with a Mixture object
    """
    _normalized = True
    _params = ('north',)

    def _init_values(self, north=0.5):
        self._values = [north]

    def _rvs(self, size=1):

        # draw from uniform distrib over the sky
        ra, dec = be.transpose(IsotropicRADec._rvs(size=size))

        # now assign points to each hemisphere
        north = self.params[0]
        dec = be.abs(dec) * (1 - 2*(be.random(size=size) > north))

        # return
        return be.transpose(be.array([ra, dec]))

    @staticmethod
    def _prob(ra, dec, north):
        return be.where(0 <= dec, north, 1-north) * be.clip(be.cos(dec), 0, 1) / (2*be.pi)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

#-------------------------------------------------

class VectorRADec(RADecDistribution):
    """\
a simple model that parametrizes the distribution over (RA,Dec) via a dipole vector with magnitude less than 1

    p(RA, Dec) = cos(Dec) * (1 + dot(b, Omega))/4*pi
    """
    _normalized = True
    _params = ('vector_magnitude', 'vector_pole', 'vector_azimuth')

    def _init_values(self, vector_magnitude=0.5, vector_pole=0.0, vector_azimuth=0.0):
        self._values = [vector_magnitude, vector_pole, vector_azimuth]

    def _rvs(self, size=1, max_iter=be.infty, max_draw=1e6):
        ans = []

        ans_ra = []
        ans_dec = []
        N = 0

        it = 0
        num_draw = size ### the number of samples we draw each epoch
                        ### updated based on estimate of acceptance fraction
        max_draw = size if size > max_draw else max_draw ### never draw too many

        mag, theta, phi = self.params
        max_rate = (1+mag)/(4*be.pi)

        while (N < size) and (it < max_iter):
            # draw from Isotropic distribution
            ra, dec = be.transpose(IsotropicRADec._rvs(size=num_draw))

            # compute weights
            # do not include jacobian within rejection sampling!
            # we've already drawn from an isotropic distrib
            weights = self._rate(ra, dec, mag, theta, phi) / max_rate

            # draw random subset from weights
            selected = be.random(size=num_draw) < weights

            ### record the samples
            n = min(be.sum(selected), size-N)

            ans_ra += list(ra[selected][:n])
            ans_dec += list(dec[selected][:n])

            # increment the number of samples found
            N += n

            # increment iteration number
            it += 1

            # update num_draw based on approximate acceptance rate
            frac = 1.* max(1, n) / num_draw
            num_draw = int(min(max_draw, (size-N) / frac)) ### never draw too many in a single epoch

        if it == max_iter:
            raise RuntimeError('too many iterations (%d) without drawing %d samples'%(max_iter, size))

        return be.transpose(be.array([ans_ra, ans_dec]))

    @staticmethod
    def _rate(ra, dec, mag, theta, phi):
        """compute the rate density (no jacobian for dec)
        """
        # compute the cartesian components of the dipole
        z = be.cos(theta) * mag
        sin_theta = be.sin(theta)
        x = mag * sin_theta * be.cos(phi)
        y = mag * sin_theta * be.sin(phi)

        # compute the cartesian components of the test points
        Z = be.sin(dec)
        cos_dec = be.cos(dec)
        X = cos_dec * be.cos(ra)
        Y = cos_dec * be.sin(ra)

        # compute probability and return
        return (1 + be.clip(X*x + Y*y + Z*z, -1, +1)) / (4*be.pi)

    @staticmethod
    def _prob(ra, dec, mag, theta, phi):
        return be.clip(be.cos(dec), 0, 1) * VectorRADec._rate(ra, dec, mag, theta, phi)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

#-------------------------------------------------

class SphericalHarmonicRADec(RADecDistribution):
    """\
a class that supports calculation of spherical harmonic-based distributions over the unit sphere
child classes can extend how they map the sum over spherical harmonics to a probability distribution
this class represents the distribution as the absolute value of the sum over spherical harmonics

    p(RA, Dec) = cos(Dec) * |\sum_{lm} Y_{lm}(RA, Dec) * b_{lm}|

with the requirement that

    b_{l,+m} = conjugate(b_{l,-m})

so that the sum over spherical harmonics is real.

b_{lm} are passed as kwargs at instantiation with the form : b_1_0 = val for the l=1, m=0 coefficient
    """
    _param_tmp = 'b_%d_%d' # used to name parameters

    def _init_values(self, automatically_normalize=True, **b_l_m):

        self._normalized = automatically_normalize ### numerically normalize

        assert len(b_l_m), 'must specify at least one spherical harmonic coefficient'

        ### parse the kwargs
        setup = dict()
        for key, val in b_l_m.items():
            l, m = [int(_) for _ in key.split('_')[1:]]
            assert l >= 0, 'spherical harmonics not defined for negative l'
            assert m <= l, 'spherical harmonics not defined for m > l'
            assert m >= 0, 'we only support (complex) coefficients for m>=0'
            if m == 0:
                assert complex(val).imag == 0, 'm=0 coefficients must be purely real!'

            if l not in setup:
                setup[l] = dict()
            setup[l][m] = val

        # now iterate through and structure params, values
        params = []
        values = []
        self._max_l = max(setup.keys())
        for l in range(self._max_l+1):
            for m in range(l+1):
                params.append(self._param_tmp%(l,m))
                if l in setup:
                    val = setup[l].get(m, 0.0 + 0.0j)
                else:
                    val = 0.0 + 0.0j
                values.append(val)

        self._params = tuple(params)
        self._values = values
        self._compute_norm()

    #---

    # need these in order to correctly update the numeric normalization

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==self.num_params
        self._values[:] = new
        self._compute_norm()

    def __setitem__(self, name, value):
        RADecDistribution.__setitem__(self, name, value)
        self._compute_norm()

    def update(self, **kwargs):
        RADecDistribution.update(self, **kwargs)
        self._compute_norm()

    #---

    def _compute_norm(self, num_samples=100000):
        """compute the numeric normalization via monte carlo sampling
        """
        if self.normalized:
            # draw a bunch of samples from an isotropic distribution
            ra, dec = be.transpose(IsotropicRADec._rvs(size=num_samples))

            # set norm=1 and then compute probs
            probs = self._sum2prob(self._ra_dec2sum(ra, dec, self._max_l, *self.params))

            # monte carlo sum to determin the norm
            self._norm = 4*be.pi * be.sum(probs) / num_samples

            # store the max prob that we saw (for rejection sampling)
            self._max_sum2prob = be.max(probs)

        else:
            self._norm = 1.0
            self._max_sum2prob = be.infty ### NOTE: will break rejection sampling...

    @property
    def norm(self):
        return self._norm

    #---

    def _rvs(self, size=1, max_iter=be.infty, max_draw=1e6):
        """performed via rejection sampling (may be inefficient!)
        """
        ans = []

        ans_ra = []
        ans_dec = []
        N = 0

        it = 0
        num_draw = size ### the number of samples we draw each epoch
                        ### updated based on estimate of acceptance fraction
        max_draw = size if size > max_draw else max_draw ### never draw too many

        while (N < size) and (it < max_iter):
            # draw from Isotropic distribution
            ra, dec = be.transpose(IsotropicRADec._rvs(size=num_draw))

            # compute weights
            # do not include jacobian within rejection sampling!
            # we've already drawn from an isotropic distrib
            weights = self._sum2prob(self._ra_dec2sum(ra, dec, self._max_l, *self.params)) / self._max_sum2prob

            # draw random subset from weights
            selected = be.random(size=num_draw) < weights

            ### record the samples
            n = min(be.sum(selected), size-N)

            ans_ra += list(ra[selected][:n])
            ans_dec += list(dec[selected][:n])

            # increment the number of samples found
            N += n

            # increment iteration number
            it += 1

            # update num_draw based on approximate acceptance rate
            frac = 1.* max(1, n) / num_draw
            num_draw = int(min(max_draw, (size-N) / frac)) ### never draw too many in a single epoch

        if it == max_iter:
            raise RuntimeError('too many iterations (%d) without drawing %d samples'%(max_iter, size))

        return be.transpose(be.array([ans_ra, ans_dec]))

    #---

    @staticmethod
    def _ra_dec2sum(ra, dec, max_l, *b_l_m):
        """compute the sum over spherical harmonics at positions given by ra, dec
        expect b_l_m to be a 1D array of predictable length (based on the maximum l)
        """
        ans = 0.0
        ind = 0
        pole = 0.5*be.pi - dec
        azimuth = ra
        for l in range(max_l+1):
            for m in range(l+1):
                b = b_l_m[ind]
                sph = be.spherical_harmonic(m, l, azimuth, pole)
                if m == 0:
                    ans = ans + b.real*sph.real
                else:
                    # twice the real part of blm*Ylm because of our convention for blm
                    ans = ans + 2*(b.real*sph.real - b.imag*sph.imag)
                ind += 1
        return ans

    @staticmethod
    def _sum2prob(sph_sum):
        """encodes how we map the sum over spherical harmonics to a probability
child classes should overwrite this
default is to take absolute value of the sum (which is constrained to be real
        """
        return be.abs(sph_sum)

    def _prob(self, ra, dec, *b_l_m):
        return be.clip(be.cos(dec), 0, 1) * self._sum2prob(self._ra_dec2sum(ra, dec, self._max_l, *b_l_m)) / self.norm

    def _logprob(self, *args):
        return be.log(self._prob(*args))

#-----------

class SquaredSphericalHarmonicRADec(SphericalHarmonicRADec):
    """
    p(RA, Dec) ~ cos(Dec) * (\sum_{lm} Y_{lm}(RA, Dec) * b_{lm})**2
"""

    @staticmethod
    def _sum2prob(sph_sum):
        return sph_sum**2

#-----------

class ExponentialSphericalHarmonicRADec(SphericalHarmonicRADec):
    """
    p(RA, Dec) ~ cos(Dec) * exp(\sum_{lm} Y_{lm}(RA, Dec) * b_{lm})
"""

    @staticmethod
    def _sum2prob(sph_sum):
        return be.exp(sph_sum)
