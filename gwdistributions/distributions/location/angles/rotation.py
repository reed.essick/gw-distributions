"""a module that provides utilities to rotate distributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import (RADistribution, DecDistribution, RADecDistribution)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class RotatedRADec(RADecDistribution):
    """\
a wrapper class that performs a coordinate transformation and then evaluates the probability, draws samples, etc from another RADecDistribution
"""
    _extra_params = ('euler_alpha', 'euler_beta', 'euler_gamma')

    def _init_values(self, dist=None, euler_alpha=0.0, euler_beta=0.0, euler_gamma=0.0):
        if not isinstance(dist, RADecDistribution):
            raise ValueError('must pass an RADistribution as dist')
        self._dist = dist
        self._params = self._extra_params + dist._params
        self._extra_values = [euler_alpha, euler_beta, euler_gamma]

    @property
    def normalized(self):
        return self.dist.normalized

    @property
    def dist(self):
        return self._dist

    #---

    # need these in order to correctly update the numeric normalization

    @property
    def _values(self):
        return self._extra_values + self.dist.params

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==self.num_params
        self._extra_values[:] = new[:3]
        self.dist.params = new[3:] ### update underlying distribution

    def __setitem__(self, name, value):
        if name in self._extra_params:
            self._extra_values[self._param2ind[name]] = value ### make sure to update the correct attribute!
        else:
            self.dist[name] = value

    #---

    @staticmethod
    def rotate(ra, dec, alpha, beta, gamma):
        """maps RA, Dec into the rotated coordinate frame
        """
        theta = 0.5*be.pi - dec
        phi = ra

        # rotate about z by alpha
        phi = phi - alpha

        # rotate about x by beta
        # convert to cartesian
        z = be.cos(theta)
        sintheta = be.sin(theta)
        x = sintheta * be.cos(phi)
        y = sintheta * be.sin(phi)

        # apply rotation about x
        sinbeta = be.sin(beta)
        cosbeta = be.cos(beta)

        y, z = +y*cosbeta + z*sinbeta, -y*sinbeta + z*cosbeta

        # convert back to spherical
        theta = be.arccos(z)
        phi = be.arctan2(y, x)

        # rotate about z by gamma
        phi = phi - gamma

        # return (as longitude and colatitude)
        return phi % (2*be.pi), 0.5*be.pi - be.clip(theta, 0, be.pi)

    @staticmethod
    def irotate(ra, dec, alpha, beta, gamma):
        """maps the rotated coordinate frame into RA, Dec (the inverse of rotate())
        """
        theta = 0.5*be.pi - dec
        phi = ra

        # undo rotation about z by gamma
        phi = phi + gamma

        # undo rotation about x by beta
        # convert to cartesian
        z = be.cos(theta)
        sintheta = be.sin(theta)
        x = sintheta * be.cos(phi)
        y = sintheta * be.sin(phi)

        # apply rotation about x
        sinbeta = be.sin(beta)
        cosbeta = be.cos(beta)

        y, z = +y*cosbeta - z*sinbeta, +y*sinbeta + z*cosbeta

        # convert back to spherical
        theta = be.arccos(z)
        phi = be.arctan2(y, x)

        # undo rotation about z by alpha
        phi = phi + alpha

        # return (as longitude and colatitude)
        return phi % (2*be.pi), 0.5*be.pi - be.clip(theta, 0, be.pi)

    #---

    def _rvs(self, size=1):
        return be.transpose(be.array(self.irotate(*be.transpose(self.dist.rvs(size=size)), *self._extra_values)))

    def _prob(self, ra, dec, euler_alpha, euler_beta, euler_gamma, *args):
        rotated_ra, rotated_dec = self.rotate(ra, dec, euler_alpha, euler_beta, euler_gamma)
        cos_rotated_dec = be.clip(be.cos(rotated_dec), 0, 1)

        # include jacobian, and handle the limit of prob, cos(rotated_dec) --> 0 simultaneously
        return be.where((cos_rotated_dec == 0),
            0.0, ### we are allowed to zero out this point because it has measure zero (happens at isolated points)
            self.dist.prob(rotated_ra, rotated_dec) * be.clip(be.cos(dec), 0, 1) / cos_rotated_dec,
        )

    def _logprob(self, *args):
        return be.log(self._prob(*args))
