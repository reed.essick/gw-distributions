"""basic declarations of source distributions for how events are distributed through space
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import SamplingDistribution
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class RADistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over RA
    """
    _scalar_variates = ('right_ascension',)

    @staticmethod
    def _domain():
        return {'right_ascension': (0.0, 2*be.pi)}

class DecDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over Dec
    """
    _scalar_variates = ('declination',)

    @staticmethod
    def _domain():
        return {'declination': (-0.5*be.pi, +0.5*be.pi)}

class RADecDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over RA and Dec
    """
    _scalar_variates = RADistribution._scalar_variates + DecDistribution._scalar_variates

    @staticmethod
    def _domain():
        return dict(list(RADistribution._domain().items()) + list(DecDistribution._domain().items()))
