"""source distributions for how events are distributed through space
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import (SamplingDistribution, PowerLaw)
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

#-------------------------------------------------

class ComovingDistanceDistribution(SamplingDistribution):
    """a child class used to declare things are comoving_distance distributions
    """
    _scalar_variates = ('comoving_distance',)

    def _domain(self):
        return {'comoving_distance': (0.0, +be.infty)}

class LuminosityDistanceDistribution(SamplingDistribution):
    """a child class used to declare things are luminosity_distance distributions
    """
    _scalar_variates = ('luminosity_distance',)

    def _domain(self):
        return {'luminosity_distance': (0.0, +be.infty)}

#------------------------

class PowerLawComovingDistance(ComovingDistanceDistribution, PowerLaw):
    """\
Power law in comoving_distance between min and max with exponent pow
if pow == -1:
    p(DC) = DC**-1 /log(max/min)
else:
    p(DC) = (pow+1) * DC**pow / (max**(pow+1) - min**(pow+1))
where
    min <= DC <= max
    """

class PowerLawLuminosityDistance(LuminosityDistanceDistribution, PowerLaw):
    """\
Power law in luminosity_distance between min and max with exponent pow
if pow == -1:
    p(DL) = DL**-1 /log(max/min)
else:
    p(DL) = (pow+1) * DL**pow / (max**(pow+1) - min**(pow+1))
where
    min <= DL <= max
    """
