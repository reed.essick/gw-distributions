"""source distributions for how events are distributed through space
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import (SamplingDistribution, PowerLaw, TruncatedGaussian, Beta)
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

from gwdistributions.utils.cosmology import \
    (Cosmology, PLANCK_2018_Ho, PLANCK_2018_OmegaMatter, PLANCK_2018_OmegaLambda, PLANCK_2018_OmegaRadiation, DEFAULT_DZ)

#-------------------------------------------------

class RedshiftDistribution(SamplingDistribution):
    """a child class used to declare things are redshift distributions
    """
    _scalar_variates = ('redshift',)

    def _domain(self):
        return {'redshift': (0.0, +be.infty)}

#------------------------

class PowerLawRedshift(RedshiftDistribution, PowerLaw):
    """\
Power law in redshift between z_min and z_max with exponent z_pow
if z_pow == -1:
    p(z) = z**-1 /log(z_max/z_min)
else:
    p(z) = (z_pow+1) * z**z_pow / (z_max**(z_pow+1) - z_min**(z_pow+1))
where
    z_min <= z <= z_max
    """

class PowerLaw1plusRedshift(RedshiftDistribution, PowerLaw):
    """\
Power law in (1+z) between z_min and z_max with exponent z_pow
if z_pow == -1:
    p(z) = (1+z)**-1 /log((1+z_max)/(1+z_min))
else:
    p(z) = (z_pow+1) * (1+z)**z_pow / ((1+z_max)**(z_pow+1) - (1+z_min)**(z_pow+1))
where
    z_min <= z <= z_max
    """

    def _rvs(self, size=1):
        m, M, p = self.params
        return PowerLaw._static_rvs(1+m, 1+M, p, size=size) - 1 ### sample in power law for (1+z)

    @staticmethod
    def _logprob(z, zmin, zmax, zpow):
        return PowerLaw._logprob(1+z, 1+zmin, 1+zmax, zpow)

#-------------------------------------------------

class LocalMergerRateRedshiftDistribution(RedshiftDistribution):
    """a redshift distribution that builds in cosmological scaling
    """
    cosmological = True ### used to control option lookup when parsing objects from INI
    normalized = True ### numerically normalized!
    _regular_params = ('min_redshift', 'max_redshift') ### child classes should overwrite this
    _default_params = dict(min_redshift=0, max_redshift=10) ### default values for regular params
    dz = DEFAULT_DZ ### used to integrate out cosmology

    _cosmology_params = ( ### only load these once (param names are static!)
        names.name('ho'),
        names.name('omega_matter'),
        names.name('omega_radiation'),
        names.name('omega_lambda'),
    )

    def _init_values(self,
            **kwargs
        ):
        z_min = kwargs.get('min_redshift', self._default_params['min_redshift']),
        z_max = kwargs.get('max_redshift', self._default_params['max_redshift']),

        # deal with annoying camel case issues from Python's config parser
        ho, omega_matter, omega_radiation, omega_lambda = self._cosmology_params

        Ho = kwargs.get(ho, kwargs.get(ho.lower(), PLANCK_2018_Ho))
        OmegaMatter = kwargs.get(omega_matter, kwargs.get(omega_matter.lower(), PLANCK_2018_OmegaMatter))
        OmegaRadiation = kwargs.get(omega_radiation, kwargs.get(omega_radiation.lower(), PLANCK_2018_OmegaRadiation))
        OmegaLambda = kwargs.get(omega_lambda, kwargs.get(omega_lambda.lower(), PLANCK_2018_OmegaLambda))

        # order parameters
        self._params = self._regular_params + self._cosmology_params
        values = [kwargs.get(param, self._default_params[param]) for param in self._regular_params]
        values += [Ho, OmegaMatter, OmegaRadiation, OmegaLambda]
        self._values = values

        # init interpolators
        self._check_z_bounds()
        self._init_cosmology()
        self._init_interpolators()

    def _check_z_bounds(self):
        zmin, zmax = self.params[:2]
        assert zmin < zmax, 'min_redshift (%.6f) must be smaller than max_redshift (%.6f)'%(zmin, zmax)

    def _init_cosmology(self):
        zmin, zmax = self.params[:2]
        Ho, OmegaMatter, OmegaRadiation, OmegaLambda = self.params[-4:]
        cosmology = Cosmology(Ho, OmegaMatter, OmegaRadiation, OmegaLambda)
        cosmology.extend(max_z=zmax, dz=self.dz) ### integrate out the cosmology

        self._cosmology = cosmology

    @property
    def cosmology(self):
        return self._cosmology

    def _init_interpolators(self):
        ### set up z-grid
        ### this is more complicated, but it provides better performance than just a rigid number of grid points
        ### that is, the precision naturally matches the precision within the cosmology
        zmin, zmax = self.params[:2]

        self.cosmology.extend(max_z=zmax) ### extend to zmax

        z = list(self.cosmology.z[(zmin <= self.cosmology.z)*(self.cosmology.z <= zmax)])
        if min(z) > zmin:
            z.insert(0, zmin)
        if max(z) < zmax:
            z.append(zmax)
        z = be.array(z)

        ### compute the rest of it
        logpdf = self.cosmology.logdVcdz(z) - be.log(1. + z) + be.log(self.local_merger_rate(z, *self.params))
        pdf = be.exp(logpdf - be.max(logpdf)) # we are going to normalize this anyway...

        cdf = be.array([0.] + list(be.cumtrapz(pdf, x=z)))

        # numerically normalize interpolators
        pdf = pdf / cdf[-1]
        cdf = cdf / cdf[-1]

        # store the results
        self._z = z
        self._pdf = pdf
        self._cdf = cdf

    @staticmethod
    def local_merger_rate(z, *args):
        """child classes should overwrite this!
        """
        return be.ones(be.shape(z), dtype=float) ### just default to constant local merger rate

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==self.num_params
        self._values[:] = new
        self._check_z_bounds()
        self._init_cosmology()
        self._init_interpolators()

    def __setitem__(self, name, value):
        SamplingDistribution.__setitem__(self, name, value)
        self._check_z_bounds()
        if name in self._cosmology_params:
            self._init_cosmology() ### only re-integrate Friedman equations if we touch the cosmology
        self._init_interpolators() ### always update the interpolators

    def update(self, **new):
        cosmo = False
        for name, value in new.items(): ### do this ourselves instead of delegating
                                        ### to avoid re-integrating cosmology repeatedly
                                        ### within each call of self.__setitem__ if we
                                        ### update multiple cosmology params at once
            cosmo |= name in self._cosmology_params
            SamplingDistribution.__setitem__(self, name, value)

        if cosmo: ### touched a cosmology parameter
            self._init_cosmology()

        ### always update the interpolators
        self._init_interpolators()

    def _domain(self):
        zmin, zmax = self.params[:2]
        return {'redshift': (zmin, zmax)}

    def _rvs(self, size=1):
        """inverse transform sampling
        """
        return be.interp(be.random(size=size), self._cdf, self._z).reshape((size,1))

    ### NOTE: not a static method and therefore this object will not play nicely within autoconditioning...
    def _prob(self, z, *args, safe=False):
        if safe: ### check to make sure args make sense (wrapped this way so jax.jit is happy)
            assert len(args)==len(self.params)
            matches = True
            for arg, param in zip(args, self.params):
                matches &= be.all(arg==param)
            assert matches, 'at least one of the arguments does not match internal params!'

        zmin, zmax = args[:2]
        return be.where((zmin <= z) & (z <= zmax), be.interp(z, self._z, self._pdf), 0.0)

    def _logprob(self, *args):
        return be.log(self._prob(*args))

#------------------------

class LocalMergerRatePowerLaw1plusRedshift(LocalMergerRateRedshiftDistribution):
    """\
local merger rate is a power law in (1+z)
    f(z) ~ (1+z)**pow
with
    z_min <= z <= z_max
and
    p(z) ~ f(z) * (dVc/dz) * (1+z)**-1
    """
    _regular_params = ('min_redshift', 'max_redshift', 'pow_redshift')
    _default_params = dict(min_redshift=0, max_redshift=10, pow_redshift=0.0)

    @staticmethod
    def local_merger_rate(z, zmin, zmax, zpow, Ho, OmegaMatter, OmegaRadiation, OmegaLambda):
        return (1. + z)**zpow

#------------------------

class LocalMergerRateTruncatedGaussianRedshift(LocalMergerRateRedshiftDistribution):
    """\
local merger rate is modeled as a truncated Gaussian
    f(z) ~ exp(-0.5*(z-mean_z)**2/stdv_z**2)
with
    min_z <= z <= max_z
and
    p(z) ~ f(z) * (dVc/dz) * (1+z)**-1
    """
    _regular_params = ('min_redshift', 'max_redshift', 'mean_redshift', 'stdv_redshift')
    _default_params = dict(min_redshift=0, max_redshift=10, mean_redshift=1.0, stdv_redshift=1.0)

    @staticmethod
    def local_merger_rate(z, zmin, zmax, zmean, zstdv, Ho, OmegaMatter, OmegaRadiation, OmegaLambda):
        """NOTE, we do not use distributions.base.TruncatedGaussian because we do not need to call the \
Gaussian CDF since the probability density is numerically normalized anyway
        """
        return be.where(
            (zmin <= z) & (z <= zmax),
            be.exp(-0.5 * (z-zmean)**2 / zstdv**2), ### numerically normalized within LocalMergerRateRedshift logic
            0,
        )

#------------------------

class LocalMergerRateBetaRedshift(LocalMergerRateRedshiftDistribution):
    """\
local merger rate is modeled as a Beta distribution
    f(z) ~ (z - min_z)**(alpha_z - 1) * (max_z - z)**(beta_z -1)
with
    min_z <= z <= max_z
and
    p(z) ~ f(z) * (dVc/dz) * (1+z)**-1

Note, because the probability distribution is numerically normalized on a grid, this object requires
    alpha_redshift, beta_redshift > 1.0
because otherwise the numerical normalization fails (there is a grid point with diverging local merger rate)
    """
    _regular_params = ('min_redshift', 'max_redshift', 'alpha_redshift', 'beta_redshift')
    _default_params = dict(min_redshift=0, max_redshift=10, alpha_redshift=1.0, beta_redshift=1.0)

    @staticmethod
    def local_merger_rate(z, zmin, zmax, zalpha, zbeta, Ho, OmegaMatter, OmegaRadiation, OmegaLambda):
        """NOTE, we do not use distributions.base.Beta because we do not need to call the Gaussian CDF since \
the probability density is numerically normalized anyway
        """
        return be.where(
            (zmin <= z) & (z <= zmax),
            (z - zmin)**(zalpha - 1) * (zmax - z)**(zbeta - 1),
            0,
        )

#------------------------

### Redshift distributions derived from SFRs

class LocalMergerRateMadauDickinsonRedshift(LocalMergerRateRedshiftDistribution):
    """\
Redshift distribution between z_min and z_max that follows the Madau-Dickinson star-formation rate (SFR)
    p(z) ~ d(Vc)/dz * (1+z)**-1 * sfr(z)
where
    sfr(z) ~ (1 + z)**num_exp / (1 + (den_coef * (1 + z))**den_exp)
and
    z_min <= z <= z_max
Parametrization and default parameters are taken from Eq 15 of https://arxiv.org/pdf/1403.0007.pdf
    """
    _regular_params = ('min_redshift', 'max_redshift', 'num_exp', 'den_coef', 'den_exp')
    _default_params = dict(min_redshift=0, max_redshift=10, num_exp=2.7, den_coef=1/2.9, den_exp=5.6)

    @staticmethod
    def local_merger_rate(z, zmin, zmax, num_exp, den_coef, den_exp, Ho, OmegaMatter, OmegaRadiation, OmegaLambda):
        """Madau-Dickinson SFR (up to a multiplicative factor)
        """
        return (1 + z)**num_exp / (1 + (den_coef * (1 + z))**den_exp)
