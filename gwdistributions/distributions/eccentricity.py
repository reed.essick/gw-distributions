"""source distributions for how events' eccentricies are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import (SamplingDistribution, Zero, Uniform, PowerLaw, TruncatedGaussian, TruncatedLogNormal)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class EccentricityDistribution(SamplingDistribution):
    """a child class used to declar things are distributions over eccentricity
    """
    _scalar_variates = ('eccentricity',)

    @staticmethod
    def _domain():
        return {'eccentricity': (0.0, 1.0)}

#-------------------------------------------------

class ZeroEccentricity(EccentricityDistribution, Zero):
    """\
zero eccentricity
    p(eccentricity) = delta(eccentricity)
    """

class UniformEccentricity(EccentricityDistribution, Uniform):
    """\
uniform eccentricity distribuiton
    p(ecc) = 1/(max_eccentricity - min_eccentricity)
    """

    def _domain(self):
        return Uniform._domain(self)

class PowerLawEccentricity(EccentricityDistribution, PowerLaw):
    """a truncated power law in eccentricity.
    """

    def _domain(self):
        return PowerLaw._domain(self)

class TruncatedGaussianEccentricity(EccentricityDistribution, TruncatedGaussian):
    """implements a Gaussian distribution between bounds
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

class TruncatedLogNormalEccentricity(EccentricityDistribution, TruncatedLogNormal):
    """implements a log-normal distribution between bounds
    """

    def _domain(self):
        return TruncatedLogNormal._domain(self)
