"""source distributions for how polar spins components are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import SpinDistribution
from gwdistributions.distributions.base import (Zero, Uniform, PowerLaw, TruncatedGaussian, Beta)
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class Spin1AzimuthalAngleDistribution(SpinDistribution):
    """a child class used to declare things are distributions over the spin's azimuthal angle
    """
    _scalar_variates = ('spin1_azimuthal_angle',)

    def _domain(self):
        return {self._scalar_variates[0]: (0.0, 2*be.pi)}

class Spin2AzimuthalAngleDistribution(Spin1AzimuthalAngleDistribution):
    __doc__ = Spin1AzimuthalAngleDistribution.__doc__
    _scalar_variates = ('spin2_azimuthal_angle',)

#------------------------

class Spin1PolarAngleDistribution(SpinDistribution):
    """a child class used to declare things are the spin's polar angle
    """
    _scalar_variates = ('spin1_polar_angle',)

    def _domain(self):
        return {self._scalar_variates[0]: (0.0, be.pi)}

class Spin2PolarAngleDistribution(Spin1PolarAngleDistribution):
    __doc__ = Spin1PolarAngleDistribution.__doc__
    _scalar_variates = ('spin2_polar_angle',)

#------------------------

class Spin1MagnitudeDistribution(SpinDistribution):
    """a child class used to declare things are distributions over the spin's magnitudes
    """
    _scalar_variates = ('spin1_magnitude',)

    def _domain(self):
        return {self._scalar_variates[0]: (0.0, 1.0)}

class Spin2MagnitudeDistribution(Spin1MagnitudeDistribution):
    __doc__ = Spin1MagnitudeDistribution.__doc__
    _scalar_variates = ('spin2_magnitude',)

#------------------------

class PolarSpin1Distribution(SpinDistribution):
    """a child class used to declare distributions over the polar spin components
    """
    _scalar_variates = \
        Spin1AzimuthalAngleDistribution._scalar_variates \
        + Spin1PolarAngleDistribution._scalar_variates \
        + Spin1MagnitudeDistribution._scalar_variates

    def _domain(self):
        azimuth, pole, magnitude = self._scalar_variates # extract names
        return {
            azimuth: (0.0, 2*be.pi),
            pole: (0.0, be.pi),
            magnitude: (0.0, 1.0),
        }

class PolarSpin2Distribution(PolarSpin1Distribution):
    _scalar_variates = \
        Spin2AzimuthalAngleDistribution._scalar_variates \
        + Spin2PolarAngleDistribution._scalar_variates \
        + Spin2MagnitudeDistribution._scalar_variates

#------------------------

class PolarSpin1Spin2Distribution(SpinDistribution):
    """a child class used to declare distributions over polar spin componets of 2 objects
    """
    _scalar_variates = \
        Spin1AzimuthalAngleDistribution._scalar_variates \
        + Spin1PolarAngleDistribution._scalar_variates \
        + Spin1MagnitudeDistribution._scalar_variates \
        + Spin2AzimuthalAngleDistribution._scalar_variates \
        + Spin2PolarAngleDistribution._scalar_variates \
        + Spin2MagnitudeDistribution._scalar_variates

    def _domain(self):
        azimuth1, pole1, mag1, azimuth2, pole2, mag2 = self._scalar_variates # extract names
        return {
            azimuth1: (0.0, 2*be.pi),
            pole1: (0.0, be.pi),
            mag1: (0.0, 1.0),
            azimuth2: (0.0, 2*be.pi),
            pole2: (0.0, be.pi),
            mag2: (0.0, 1.0),
        }

#-------------------------------------------------

class IsotropicSpin1AzimuthalAngle(Spin1AzimuthalAngleDistribution):
    """\
Generate azimuthal angles that correspond to isotropic orientations
    p(azimuth) = 1/2*pi   where 0 <= azimuth < 2*pi
    """
    _normalized = True

    @staticmethod
    def _rvs(size=1):
        return Uniform._static_rvs(0, 2*be.pi, size=size)

    @staticmethod
    def _logprob(azimuth):
        return Uniform._logprob(azimuth, 0, 2*be.pi)

class IsotropicSpin2AzimuthalAngle(Spin2AzimuthalAngleDistribution, IsotropicSpin1AzimuthalAngle):
    __doc__ = IsotropicSpin1AzimuthalAngle.__doc__
    pass

#-------------------------------------------------

class IsotropicSpin1PolarAngle(Spin1PolarAngleDistribution):
    """\
Generate polar angles that correspond to istotropic orientations
    p(pole) = sin(pole)/2  where 0 <= pole <= pi
    """
    _normalized = True

    @staticmethod
    def _rvs(size=1):
        return be.arccos(Uniform._static_rvs(-1, +1, size=size))

    def _prob(pole):
        return be.where((0<=pole) & (pole <= be.pi), 0.5*be.sin(pole), 0.)

    @staticmethod
    def _logprob(pole):
        return Uniform._logprob(be.cos(pole), -1, +1) + be.log(be.clip(be.sin(pole), 0, 1))

class IsotropicSpin2PolarAngle(Spin2PolarAngleDistribution, IsotropicSpin1PolarAngle):
    __doc__ = IsotropicSpin1PolarAngle.__doc__

#------------------------

class AlignedSpin1PolarAngle(Spin1PolarAngleDistribution, Zero):
    """\
Generate polar angles that correspond to aligned spins
    p(pole) ~ delta(pole=0)
    """

    def _domain(self):
        return Zero._domain(self)

class AlignedSpin2PolarAngle(Spin2PolarAngleDistribution, AlignedSpin1PolarAngle):
    __doc__ = AlignedSpin1PolarAngle.__doc__

#------------------------

class TruncatedGaussianSpin1PolarAngle(Spin1PolarAngleDistribution, TruncatedGaussian):
    """\
Generates polar angles drawn from a truncated Gaussian distribution
    p(pole) ~ exp(-0.5*(pole-mean)**2/stdv**2)
where
    min_pole <= pole <= max_pole
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

class TruncatedGaussianSpin2PolarAngle(Spin2PolarAngleDistribution, TruncatedGaussianSpin1PolarAngle):
    __doc__ = TruncatedGaussianSpin1PolarAngle.__doc__

#------------------------

class TruncatedGaussianSpin1CosPolarAngle(Spin1PolarAngleDistribution):
    """\
Generates cos(pole) from a truncated Gaussian distribution
    p(pole) ~ |sin(pole)| * exp(-0.5*(cos(pole) - mean)**2/stdv**2)
where
    min_cospole <= cos(pole) <= max_cospole
The factor of |sin(pole)| is the jacobian between "cos(pole)" and "pole"
    """
    _normalized = True
    _params = (
        'min_spin1_cos_polar_angle',
        'max_spin1_cos_polar_angle',
        'mean_spin1_cos_polar_angle',
        'stdv_spin1_cos_polar_angle',
    )

    def _init_values(
            self,
            min_spin1_cos_polar_angle=-1.0,
            max_spin1_cos_polar_angle=+1.0,
            mean_spin1_cos_polar_angle=+1.0,
            stdv_spin1_cos_polar_angle=0.05,
        ):
        self._values = [
            min_spin1_cos_polar_angle,
            max_spin1_cos_polar_angle,
            mean_spin1_cos_polar_angle,
            stdv_spin1_cos_polar_angle,
        ]

    def _domain(self):
        amin, amax, _, _ = self.params
        return {self._scalar_variates[0]: (be.arccos(amax), be.arccos(amin))}

    def _rvs(self, size=1):
        return be.arccos(TruncatedGaussian._static_rvs(*self.params, size=size))

    @staticmethod
    def _logprob(pole, min_cospole, max_cospole, mean_cospole, stdv_cospole):
        cospole = be.cos(pole)
        return TruncatedGaussian._logprob(be.cos(pole), min_cospole, max_cospole, mean_cospole, stdv_cospole) \
            + be.log(be.abs(be.sin(pole))) ### include jacobian from cos(pole) to pole

class TruncatedGaussianSpin2CosPolarAngle(Spin2PolarAngleDistribution, TruncatedGaussianSpin1CosPolarAngle):
    __doc__ = TruncatedGaussianSpin1CosPolarAngle.__doc__
    _params = (
        'min_spin2_cos_polar_angle',
        'max_spin2_cos_polar_angle',
        'mean_spin2_cos_polar_angle',
        'stdv_spin2_cos_polar_angle',
    )

    def _init_values(
            self,
            min_spin2_cos_polar_angle=-1.0,
            max_spin2_cos_polar_angle=+1.0,
            mean_spin2_cos_polar_angle=+1.0,
            stdv_spin2_cos_polar_angle=0.05,
        ):
        self._values = [
            min_spin2_cos_polar_angle,
            max_spin2_cos_polar_angle,
            mean_spin2_cos_polar_angle,
            stdv_spin2_cos_polar_angle,
        ]

#------------------------

class PowerLawSpin1PolarAngle(Spin1PolarAngleDistribution, PowerLaw):
    """\
Generates polar angles drawn form a power law
if pow == -1
    p(pole) = pole**-1 / log(max_pole/min_pole)
else:
    p(pole) = (pow+1) * pole**pow / (max_pole**(pow+1) - min_pole(pow+1))
where
    min_pole <= pole <= max_pole
    """
    _param_defaults = (0.0, be.pi, 0.0)

    def _domain(self):
        return PowerLaw._domain(self)

class PowerLawSpin2PolarAngle(Spin2PolarAngleDistribution, PowerLawSpin1PolarAngle):
    __doc__ = PowerLawSpin1PolarAngle.__doc__

#------------------------

class PowerLawSpin1CosPolarAngle(Spin1PolarAngleDistribution):
    """\
Generates polar angles drawn from a power law in |cos(pole)|.
Note, this has support for both positive and negative cos(pole).

if pow == -1
    p(|cos(pole)|) = |cos(pole)|**-1 / log(max_cospole/min_cospole)
else:
    p(|cos(pole)|) = (pow+1) * |cos(pole)|**pow / (max_cospole**(pow+1) - min_cospole(pow+1))
where
    min_cospole <= |cos(pole)| <= max_cospole
and
    p(pole) = |sin(pole)| * p(|cos(pole)|) / 2
    """
    _normalized = True
    _params = ('min_spin1_cos_polar_angle', 'max_spin1_cos_polar_angle', 'pow_spin1_cos_polar_angle')

    def _init_values(
            self,
            min_spin1_cos_polar_angle=-1.0,
            max_spin1_cos_polar_angle=+1.0,
            pow_spin1_cos_polar_angle=0.0,
        ):
        self._values = [
            min_spin1_cos_polar_angle,
            max_spin1_cos_polar_angle,
            pow_spin1_cos_polar_angle,
        ]

    def _domain(self):
        _, max_cospole, _ = self.params
        min_pole = be.arccos(max_cospole)
        return {self._scalar_variates[0]: (min_pole, be.pi-min_pole)}

    def _rvs(self, size=1):
        return be.arccos(be.where(be.random(size) > 0.5, +1, -1) \
            * PowerLaw._static_rvs(*self.params, size=size)[:,0]).reshape((size,1))

    @staticmethod
    def _logprob(pole, min_cospole, max_cospole, pow_cospole):
        cospole = be.abs(be.cos(pole))
        return PowerLaw._logprob(be.abs(be.cos(pole)), min_cospole, max_cospole, pow_cospole) \
            - be.log(2) + + be.log(be.abs(be.sin(pole))) ### include jacobian from "cos(pole)" to "pole"

class PowerLawSpin2CosPolarAngle(Spin2PolarAngleDistribution, PowerLawSpin1CosPolarAngle):
    __doc__ = PowerLawSpin1CosPolarAngle.__doc__
    _params = ('min_spin2_cos_polar_angle', 'max_spin2_cos_polar_angle', 'pow_spin2_cos_polar_angle')

    def _init_values(
            self,
            min_spin2_cos_polar_angle=-1.0,
            max_spin2_cos_polar_angle=+1.0,
            pow_spin2_cos_polar_angle=0.0,
        ):
        self._values = [
            min_spin2_cos_polar_angle,
            max_spin2_cos_polar_angle,
            pow_spin2_cos_polar_angle,
        ]

#------------------------

class BetaSpin1PolarAngle(Spin1PolarAngleDistribution, Beta):
    """\
generates scaled/shifted Beta distributed spin polar angles such that the support of the Beta distribuiton \
(usually between 0 and 1) is between
    min_spin_polar_angle <= cos(pole) <= max_spin_polar_angle
and
    p(pole) ~ (pole - min_pole)**(alpha_pole - 1) * (max_pole - pole)**(beta_pole - 1)
    """

    def _domain(self):
        return Beta._domain(self)

class BetaSpin2PolarAngle(Spin2PolarAngleDistribution, BetaSpin1PolarAngle):
    __doc__ = BetaSpin1PolarAngle.__doc__

#------------------------

class BetaSpin1CosPolarAngle(Spin1PolarAngleDistribution):
    """\
generates scaled/shifted Beta distributed cos(polar angles) so that the support of the Beta distribution \
(usually between 0 and 1) is between
    min_spin_cos_polar_angle <= cos(pole) <= max_spin_cos_polar_angle
and
    p(pole) ~ |sin(pole)| * (cos(pole) - min_cospole)**(alpha - 1) * (max_cospole - cos(pole))**(beta - 1)
    """
    _normalized = True
    _params = (
        'min_spin1_cos_polar_angle',
        'max_spin1_cos_polar_angle',
        'alpha_spin1_cos_polar_angle',
        'beta_spin1_cos_polar_angle',
    )

    def _init_values(
            self,
            min_spin1_cos_polar_angle=0.0,
            max_spin1_cos_polar_angle=be.pi,
            alpha_spin1_cos_polar_angle=1.0,
            beta_spin1_cos_polar_angle=1.0
        ):
        self._values = [
            min_spin1_cos_polar_angle,
            max_spin1_cos_polar_angle,
            alpha_spin1_cos_polar_angle,
            beta_spin1_cos_polar_angle,
        ]

    def _domain(self):
        mincosp, maxcosp, _, _ = self.params
        return {self._scalar_variates[0]:(be.arccos(maxcosp), be.arccos(mincosp))}

    def _rvs(self, size=1):
        return be.arccos(Beta._static_rvs(*self.params, size=size))

    @staticmethod
    def _logprob(pole, min_cospole, max_cospole, alpha_cospole, beta_cospole):
        return Beta._logprob(be.cos(pole), min_cospole, max_cospole, alpha_cospole, beta_cospole) \
            + be.log(be.abs(be.sin(pole)))

class BetaSpin2CosPolarAngle(Spin2PolarAngleDistribution, BetaSpin1CosPolarAngle):
    __doc__ = BetaSpin1CosPolarAngle.__doc__
    _params = (
        'min_spin2_cos_polar_angle',
        'max_spin2_cos_polar_angle',
        'alpha_spin2_cos_polar_angle',
        'beta_spin2_cos_polar_angle',
    )

    def _init_values(
            self,
            min_spin2_cos_polar_angle=0.0,
            max_spin2_cos_polar_angle=be.pi,
            alpha_spin2_cos_polar_angle=1.0,
            beta_spin2_cos_polar_angle=1.0
        ):
        self._values = [
            min_spin2_cos_polar_angle,
            max_spin2_cos_polar_angle,
            alpha_spin2_cos_polar_angle,
            beta_spin2_cos_polar_angle,
        ]

#-------------------------------------------------

class ZeroSpin1Magnitude(Spin1MagnitudeDistribution, Zero):
    """\
Generate spin magnitudes corresponding to non-spinning systems
    p(|spin|) ~ delta(|spin|=0)
    """

    def _domain(self):
        return Zero._domain(self)

class ZeroSpin2Magnitude(Spin2MagnitudeDistribution, ZeroSpin1Magnitude):
    pass

#------------------------

class PowerLawSpin1Magnitude(Spin1MagnitudeDistribution, PowerLaw):
    """\
Generate spin magnitudes that follow a power law
if pow == -1
    p(mag) = mag**-1 / log(max_mag/min_mag)
else:
    p(mag) = (pow+1) * mag**pow / (max_mag**(pow+1) - min_mag**(pow+1))
where
    min_mag <= mag <= max_mag
    """

    def _domain(self):
        return PowerLaw._domain(self)

class PowerLawSpin2Magnitude(Spin2MagnitudeDistribution, PowerLawSpin1Magnitude):
    __doc__ = PowerLawSpin1Magnitude.__doc__

#------------------------

class TruncatedGaussianSpin1Magnitude(Spin1MagnitudeDistribution, TruncatedGaussian):
    """\
Generates spin magnitudes drawn from a truncated Gaussian distribution
    p(mag) ~ exp(-0.5*(mag-mean)**2/stdv**2)
where
    min_mag <= mag <= max_mag
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

class TruncatedGaussianSpin2Magnitude(Spin2MagnitudeDistribution, TruncatedGaussianSpin1Magnitude):
    __doc__ = TruncatedGaussianSpin1Magnitude.__doc__

#------------------------

class BetaSpin1Magnitude(Spin1MagnitudeDistribution, Beta):
    """\
generates scaled/shifted Beta distributed spin magnitudes such that the support of the Beta distribuiton \
(usually between 0 and 1) is between
    min_spin_magnitude <= magnitude <= max_magnitude
and
    p(magnitude) ~ (magnitude - min_magnitude)**(alpha_magnitude - 1) * (max_magnitude - pole)**(beta_magnitude - 1)
    """

    def _domain(self):
        return Beta._domain(self)

class BetaSpin2Magnitude(Spin2MagnitudeDistribution, BetaSpin1Magnitude):
    __doc__ = BetaSpin1Magnitude.__doc__

#------------------------

class BetaSpin1MagnitudeGivenMass1(Spin1MagnitudeDistribution):
    """
generates scaled/shifted Beta distributed spin magnitudes like BetaSpin1Magnitude conditioned on mass1_source
if mass1_source < mbreak_spin1_magnitude:
    BetaSpin1Mangitude(min_spin1_magnitude_low, max_spin1_magnitude_low, alpha_spin1_magnitude_low, beta_spin1_magnitude_low)
else:
    BetaSpin1Mangitude(min_spin1_magnitude_high, max_spin1_magnitude_high, alpha_spin1_magnitude_high, beta_spin1_magnitude_high)
    """
    _normalized = Beta._normalized
    _params = (
        'mbreak_spin1_magnitude',
        'min_spin1_magnitude_low',
        'max_spin1_magnitude_low',
        'alpha_spin1_magnitude_low',
        'beta_spin1_magnitude_low',
        'min_spin1_magnitude_high',
        'max_spin1_magnitude_high',
        'alpha_spin1_magnitude_high',
        'beta_spin1_magnitude_high',
    )
    _required = ('mass1_source',)

    def _init_values(
            self,
            mbreak_spin1_magnitude=1.0,
            min_spin1_magnitude_low=0.0,
            max_spin1_magnitude_low=1.0,
            alpha_spin1_magnitude_low=1.0,
            beta_spin1_magnitude_low=1.0,
            min_spin1_magnitude_high=0.0,
            max_spin1_magnitude_high=1.0,
            alpha_spin1_magnitude_high=1.0,
            beta_spin1_magnitude_high=1.0,
        ):
        self._values = [
            mbreak_spin1_magnitude,
            min_spin1_magnitude_low,
            max_spin1_magnitude_low,
            alpha_spin1_magnitude_low,
            beta_spin1_magnitude_low,
            min_spin1_magnitude_high,
            max_spin1_magnitude_high,
            alpha_spin1_magnitude_high,
            beta_spin1_magnitude_high,
        ]

    def _domain(self, mass1_source):
        mbreak, ml, Ml, _, _, mh, Mh, _, _ = self.params
        if mass1_source <= mbreak:
            return dict([(self._scalar_variates[0], (ml, Ml))])
        else:
            return dict([(self._scalar_variates[0], (mh, Mh))])

    def _rvs(self, mass1_source, size=1):
        mbreak, ml, Ml, al, bl, mh, Mh, ah, bh = self.params
        return self._static_rvs(mass1_source, *self.params, size=size)

    @staticmethod
    def _static_rvs(m1, mb, ml, Ml, al, bl, mh, Mh, ah, bh, size=1):
        if m1 <= mb:
            return Beta._static_rvs(ml, Ml, al, bl, size=size)
        else:
            return Beta._static_rvs(mh, Mh, ah, bh, size=size)

    @staticmethod
    def _logprob(
            spin1_mag,
            mass1_source,
            mbreak,
            min_low,
            max_low,
            alpha_low,
            beta_low,
            min_high,
            max_high,
            alpha_high,
            beta_high,
        ):
        return be.where(
            mass1_source <= mbreak,
            Beta._logprob(spin1_mag, min_low, max_low, alpha_low, beta_low),
            Beta._logprob(spin1_mag, min_high, max_high, alpha_high, beta_high),
        )

class BetaSpin2MagnitudeGivenMass2(Spin2MagnitudeDistribution, BetaSpin1MagnitudeGivenMass1):
    __doc__ = BetaSpin1MagnitudeGivenMass1.__doc__.replace('spin1', 'spin2').replace('mass1', 'mass2')

    _normalized = BetaSpin1MagnitudeGivenMass1._normalized
    _params = tuple(_.replace('spin1', 'spin2') for _ in BetaSpin1MagnitudeGivenMass1._params)
    _required = tuple(_.replace('mass1', 'mass2') for _ in BetaSpin1MagnitudeGivenMass1._required)

    def _init_values(
            self,
            mbreak_spin2_magnitude=1.0,
            min_spin2_magnitude_low=0.0,
            max_spin2_magnitude_low=1.0,
            alpha_spin2_magnitude_low=1.0,
            beta_spin2_magnitude_low=1.0,
            min_spin2_magnitude_high=0.0,
            max_spin2_magnitude_high=1.0,
            alpha_spin2_magnitude_high=1.0,
            beta_spin2_magnitude_high=1.0,
        ):
        BetaSpin1MagnitudeGivenMass1._init_values(
            self,
            mbreak_spin1_magnitude=mbreak_spin2_magnitude,
            min_spin1_magnitude_low=min_spin2_magnitude_low,
            max_spin1_magnitude_low=max_spin2_magnitude_low,
            alpha_spin1_magnitude_low=alpha_spin2_magnitude_low,
            beta_spin1_magnitude_low=beta_spin2_magnitude_low,
            min_spin1_magnitude_high=min_spin2_magnitude_high,
            max_spin1_magnitude_high=max_spin2_magnitude_high,
            alpha_spin1_magnitude_high=alpha_spin2_magnitude_high,
            beta_spin1_magnitude_high=beta_spin2_magnitude_high,
        )

    def _rvs(self, mass2_source, size=1):
        return BetaSpin1MagnitudeGivenMass1._static_rvs(mass2_source, *self.params, size=size)

#-------------------------------------------------

class IsotropicPowerLawPolarSpin1(PolarSpin1Distribution):
    """isotropic spin direction and power-law spin magnitude
    """
    _normalized = IsotropicSpin1PolarAngle._normalized \
        and IsotropicSpin2AzimuthalAngle._normalized \
        and PowerLawSpin1Magnitude._normalized
    _params = ('min_spin1_magnitude', 'max_spin1_magnitude', 'pow_spin1_magnitude')

    def _init_values(self, min_spin1_magnitude=0.0, max_spin1_magnitude=1.0, pow_spin1_magnitude=0.0):
        self._values = [min_spin1_magnitude, max_spin1_magnitude, pow_spin1_magnitude]

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(min_mag, max_mag, pow_mag, size=1):
        pol = IsotropicSpin1PolarAngle._rvs(size=size)[:,0]
        azm = IsotropicSpin2AzimuthalAngle._rvs(size=size)[:,0]
        mag = PowerLawSpin1Magnitude._static_rvs(min_mag, max_mag, pow_mag, size=size)[:,0]
        return be.transpose(be.array([azm, pol, mag]))

    @staticmethod
    def _logprob(azimuth, pole, mag, min_mag, max_mag, pow_mag):
        return IsotropicSpin2AzimuthalAngle._logprob(azimuth) \
            + IsotropicSpin1PolarAngle._logprob(pole) \
            + PowerLawSpin1Magnitude._logprob(mag, min_mag, max_mag, pow_mag)

class IsotropicPowerLawPolarSpin2(PolarSpin2Distribution, IsotropicPowerLawPolarSpin1):
    __doc__ = IsotropicPowerLawPolarSpin1.__doc__
    _normalized = IsotropicPowerLawPolarSpin1._normalized
    _params = ('min_spin2_magnitude', 'max_spin2_magnitude', 'pow_spin2_magnitude')

    def _init_values(self, min_spin2_magnitude=0.0, max_spin2_magnitude=1.0, pow_spin2_magnitude=0.0):
        self._values = [min_spin2_magnitude, max_spin2_magnitude, pow_spin2_magnitude]

#------------------------

class IsotropicPowerLawPolarSpin1Spin2(PolarSpin1Spin2Distribution):
    """isotropic spin direction and power-law spin magnitude
    """
    _normalized = IsotropicPowerLawPolarSpin1._normalized and IsotropicPowerLawPolarSpin2._normalized
    _params = IsotropicPowerLawPolarSpin1._params + IsotropicPowerLawPolarSpin2._params

    def _init_values(
            self,
            min_spin1_magnitude=0.0,
            max_spin1_magnitude=1.0,
            pow_spin1_magnitude=0.0,
            min_spin2_magnitude=0.0,
            max_spin2_magnitude=1.0,
            pow_spin2_magnitude=0.0,
        ):
        self._values = [
            min_spin1_magnitude,
            max_spin1_magnitude,
            pow_spin1_magnitude,
            min_spin2_magnitude,
            max_spin2_magnitude,
            pow_spin2_magnitude,
        ]

    def _rvs(self, size=1):
        min_mag1, max_mag1, pow_mag1, min_mag2, max_mag2, pow_mag2 = self.params
        spin1 = IsotropicPowerLawPolarSpin1._static_rvs(min_mag1, max_mag1, pow_mag1, size=size)
        spin2 = IsotropicPowerLawPolarSpin2._static_rvs(min_mag2, max_mag2, pow_mag2, size=size)
        return be.transpose(be.array([spin1[:,0], spin1[:,1], spin1[:,2], spin2[:,0], spin2[:,1], spin2[:,2]]))

    @staticmethod
    def _logprob(azm1, pol1, mag1, azm2, pol2, mag2, min_mag1, max_mag1, pow_mag1, min_mag2, max_mag2, pow_mag2):
        return IsotropicPowerLawPolarSpin1._logprob(azm1, pol1, mag1, min_mag1, max_mag1, pow_mag1) \
            + IsotropicPowerLawPolarSpin2._logprob(azm2, pol2, mag2, min_mag2, max_mag2, pow_mag2)
