"""source distributions for how spins are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.distributions.base import SamplingDistribution

#-------------------------------------------------

class SpinDistribution(SamplingDistribution):
    """a child class used to declare things are distributions over the spin
    """
