"""source distributions for how cartesian spin components are distributed
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .polar import * ### used to delegate some sampling logic for isotropic distributions
                     ### also grabs things like backends.numerics as be

#-------------------------------------------------

class Spin1zDistribution(SpinDistribution):
    """a child class that specifies the distribution over the component of the spins aligned with \
the orbital angular momentum
    """
    _scalar_variates = ('spin1z',)

    def _domain(self):
        return {self._scalar_variates[0]:(-1.0, +1.0)}

class Spin2zDistribution(Spin1zDistribution):
    __doc__ = Spin1zDistribution.__doc__
    _scalar_variates = ('spin2z',)

#------------------------

class CartesianSpin1Distribution(SpinDistribution):
    """a child class used to declare distributions over cartesian spin components
    """
    _scalar_variates = ('spin1x', 'spin1y', 'spin1z')

    def _domain(self,):
        return dict((key, (-1.0, +1.0)) for key in self._scalar_variates)

class CartesianSpin2Distribution(CartesianSpin1Distribution):
    __doc__ = CartesianSpin1Distribution.__doc__
    _scalar_variates = ('spin2x', 'spin2y', 'spin2z')

#------------------------

class CartesianSpin1Spin2Distribution(CartesianSpin2Distribution, CartesianSpin1Distribution):
    """a child class used to declare distributions over cartesian spin components for 2 objects
    """
    _scalar_variates = ('spin1x', 'spin1y', 'spin1z', 'spin2x', 'spin2y', 'spin2z')

#-------------------------------------------------

class PowerLawSpin1z(Spin1zDistribution):
    """\
Generate distribution over spinz (useful for aligned spins) based on PowerLawSpin1Magnitude.
    p(spinz) = PowerLawSpinMagnitude(|spinz|) / 2
and the fator of 2 comes from the fact that the sign of spinz is randomly assigned
    """
    _normalized = True
    _params = ('min_spin1z', 'max_spin1z', 'pow_spin1z')

    def _init_values(self, min_spin1z=0.0, max_spin1z=1.0, pow_spin1z=0.0):
        self._values = [min_spin1z, max_spin1z, pow_spin1z]

    def _domain(self):
        amin, amax, apow = self.params
        return {self._scalar_variates[0]: (-amax, +amax)}

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(amax, amin, apow, size=1):

        ### draw |spinz| according to power law
        ### flip spinz's sign randomly
        return PowerLaw._static_rvs(amin, amax, apow, size=size) \
            * be.sign(1 - 2*be.random(size=(size,1)))

    @staticmethod
    def _logprob(spinz, amin, amax, apow):
        spinz = be.abs(spinz)
        return PowerLaw._logprob(be.abs(spinz), amin, amax, apow) - be.log(2)

class PowerLawSpin2z(Spin2zDistribution, PowerLawSpin1z):
    __doc__ = PowerLawSpin1z.__doc__
    _params = ('min_spin2z', 'max_spin2z', 'pow_spin2z')

    def _init_values(self, min_spin2z=0.0, max_spin2z=1.0, pow_spin2z=0.0):
        self._values = [min_spin2z, max_spin2z, pow_spin2z]

#-------------------------------------------------

class ZeroCartesianSpin1(CartesianSpin1Distribution):
    """\
A distribution of spin components that is isotropic in direction but fixed to zero magnitude
    p(spinx, spiny, spinz) = delta(spinx=0) * delta(spiny=0) * delta(spinz=0)
    """
    _normalized = False
    _params = ()

    def _domain(self):
        return dict((_, (0.0, 0.0)) for _ in self._scalar_variates)

    def _rvs(self, size=1):
        return be.zeros((size, 3))

    @staticmethod
    def _logprob(spinx, spiny, spinz):
        """Note: because of the jacobian between spin magnitude and (cartesian) spin components, \
we don't rely on ZeroSpinMagnitude
        """
        return be.where((spinx==0) & (spiny==0) & (spinz==0), 0., -be.infty)

class ZeroCartesianSpin2(CartesianSpin2Distribution, ZeroCartesianSpin1):
    __doc__ = ZeroCartesianSpin1.__doc__

#------------------------

class AlignedPowerLawCartesianSpin1(CartesianSpin1Distribution):
    """\
A distribution of cartesian spin components that assumes aligned spins (spinx=spiny=0) with spinz described \
by a power law
    p(spinx, spiny, spinz) = delta(spinx=0) * delta(spiny=0)* PowerLawSpinz(spinz)
    """
    _normalized = True
    _params = ('min_spin1z', 'max_spin1z', 'pow_spin1z')

    def _init_values(self, min_spin1z=0.0, max_spin1z=1.0, pow_spin1z=0.0):
        self._values = [min_spin1z, max_spin1z, pow_spin1z]

    def _domain(self):
        ans = dict((_, (0.0, 0.0)) for _ in self._scalar_variates[:2])
        M = self.params[1]
        ans[self._scalar_variates[2]] = (-M, +M)
        return ans

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(amin, amax, apow, size=1):
        return be.transpose(be.array([
            be.zeros(size), ### zero spinx
            be.zeros(size), ### zero spiny
            PowerLawSpin1z._static_rvs(amax, amin, apow, size=size)[:,0], # draw |spinz| according to 
        ]))                                                              # power law with sign randomly assigned

    @staticmethod
    def _logprob(spinx, spiny, spinz, amin, amax, apow):
        return be.where(
            (spinx==0) & (spiny==0),
            PowerLawSpin1z._logprob(spinz, amin, amax, apow),
            -be.infty
        )

class AlignedPowerLawCartesianSpin2(CartesianSpin2Distribution, AlignedPowerLawCartesianSpin1):
    __doc__ = AlignedPowerLawCartesianSpin1.__doc__
    _params = ('min_spin2z', 'max_spin2z', 'pow_spin2z')

    def _init_values(self, min_spin2z=0.0, max_spin2z=1.0, pow_spin2z=0.0):
        self._values = [min_spin2z, max_spin2z, pow_spin2z]

#------------------------

class IsotropicPowerLawCartesianSpin1(CartesianSpin1Distribution):
    """\
A distribution of cartesian spin components that is isotropic in orientation and has a magnitude that \
follows a power law
    p(spinx, spiny, spinz) = (1/4*pi*|spin|**2) * p(|spin|)
where p(|spin|) is as descrbed in PowerLawSpinMagnitude. The other factor is the jacobian from polar to \
cartesian components.
    """
    _normalized = True
    _params = ('min_spin1_magnitude', 'max_spin1_magnitude', 'pow_spin1_magnitude')

    def _init_values(self, min_spin1_magnitude=0.0, max_spin1_magnitude=1.0, pow_spin1_magnitude=0.0):
        self._values = [min_spin1_magnitude, max_spin1_magnitude, pow_spin1_magnitude]

    def _domain(self):
        _, M, _ = self.params
        return dict((_, (-M, +M)) for _ in self._scalar_variates)

    def _rvs(self, size=1):
        return self._static_rvs(*self.params, size=size)

    @staticmethod
    def _static_rvs(amin, amax, apow, size=1):
        a = PowerLawSpin1Magnitude._static_rvs(amin, amax, apow, size=size)[:,0]

        azimuth = IsotropicSpin1AzimuthalAngle._rvs(size=size)[:,0]
        pole = IsotropicSpin1PolarAngle._rvs(size=size)[:,0]

        sinpole = be.sin(pole)

        sx = a * sinpole * be.cos(azimuth)
        sy = a * sinpole * be.sin(azimuth)
        sz = a * be.cos(pole)

        return be.transpose(be.array([sx, sy, sz]))

    @staticmethod
    def _logprob(spinx, spiny, spinz, a_min, a_max, a_pow):
        """Note: includes jacobian from spin magnitude to cartesian spin components, which blows up as \
|spin|**-2 as |spin| --> 0
We also only rely on PowerLawSpinMagnitude and re-implement the PDFs from IsotropicSpinAzimuthalAngle and \
IsotropicSpinPolarAngle because they are so simple
        """
        a = (spinx**2 + spiny**2 + spinz**2)**0.5
        return be.where(
            (a_min <= a) & (a <= a_max),
            PowerLawSpin1Magnitude._logprob(a, a_min, a_max, a_pow) - be.log(4*be.pi*a**2), ### Includes Jacobian; handle divergence as a --> 0 better?
            -be.infty
        )

class IsotropicPowerLawCartesianSpin2(CartesianSpin2Distribution, IsotropicPowerLawCartesianSpin1):
    __doc__ = IsotropicPowerLawCartesianSpin1.__doc__
    _params = ('min_spin2_magnitude', 'max_spin2_magnitude', 'pow_spin2_magnitude')

    def _init_values(self, min_spin2_magnitude=0.0, max_spin2_magnitude=1.0, pow_spin2_magnitude=0.0):
        self._values = [min_spin2_magnitude, max_spin2_magnitude, pow_spin2_magnitude]

#-------------------------------------------------

class ZeroCartesianSpin1Spin2(CartesianSpin1Spin2Distribution):
    """\
Generate spins for 2 objects that are identically zero
    p(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z) = \
delta(spin1x=0) * delta(spin1y=0) * delta(spin1z=0) \
* delta(spin2x=0) * delta(spin2y=0) * delta(spin2z=0)
    """
    _normalized = ZeroCartesianSpin1._normalized and ZeroCartesianSpin2._normalized
    _params = ()

    def _domain(self):
        return dict((_, (0.0, 0.0)) for _ in self._scalar_variates)

    def _rvs(self, size=1):
        return be.zeros((size, 6), dtype=float)

    @staticmethod
    def _logprob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        return ZeroCartesianSpin1._logprob(spin1x, spin1y, spin1z) + ZeroCartesianSpin2._logprob(spin2x, spin2y, spin2z)

#------------------------

class AlignedPowerLawCartesianSpin1Spin2(CartesianSpin1Spin2Distribution):
    """\
Generate spins for 2 objects that are aligned with the orbital angular momentum. The magnitudes follow power laws \
within the specified bounds
    p(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z) = \
AlignedPowerLawSpin(spin1x, spin1y, spin1z) * AlignedPowerLawSpin(spin2x, spin2y, spin2z)
with the additional requirements
    min_spin1z <= spin1z <= max_spin1z
    min_spin2z <= spin2z <= max_spin2z
    """
    _normalized = AlignedPowerLawCartesianSpin1._normalized and AlignedPowerLawCartesianSpin2._normalized
    _params = ('min_spin1z', 'max_spin1z', 'pow_spin1z', 'min_spin2z', 'max_spin2z', 'pow_spin2z')

    def _init_values(
            self,
            min_spin1z=-1.0,
            max_spin1z=+1.0,
            pow_spin1z=0.0,
            min_spin2z=-1.0,
            max_spin2z=+1.0,
            pow_spin2z=0.0,
        ):
        self._values = [min_spin1z, max_spin1z, pow_spin1z, min_spin2z, max_spin2z, pow_spin2z]

    def _domain(self):
        _, M1, _, _, M2, _ = self.params
        ans = dict((_, (0.0, 0.0)) for _ in self._scalar_variates[:2] + self._scalar_variates[3:5])
        ans[self._scalar_variates[2]] = (-M1, +M1)
        ans[self._scalar_variates[5]] = (-M2, +M2)
        return ans

    def _rvs(self, size=1):
        a1min, a1max, a1pow, a2min, a2max, a2pow = self.params
        spin1 = AlignedPowerLawCartesianSpin1._static_rvs(a1min, a1max, a1pow, size=size)
        spin2 = AlignedPowerLawCartesianSpin2._static_rvs(a2min, a2max, a2pow, size=size)
        return be.transpose(be.array([spin1[:,0], spin1[:,1], spin1[:,2], spin2[:,0], spin2[:,1], spin2[:,2]]))

    @staticmethod
    def _logprob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, a1min, a1max, a1pow, a2min, a2max, a2pow):
        return \
            AlignedPowerLawCartesianSpin1._logprob(spin1x, spin1y, spin1z, a1min, a1max, a1pow) \
            + AlignedPowerLawCartesianSpin2._logprob(spin2x, spin2y, spin2z, a2min, a2max, a2pow)

#------------------------

class IsotropicPowerLawCartesianSpin1Spin2(CartesianSpin1Spin2Distribution):
    """\
Generate isotropically distributed spin orientations. The magnitudes are distributed according to a power law.
    p(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z) = \
IsotropicPowerLawSpin(spin1x, spin1y, spin1z) * IsotropicPowerLawSpin(spin2x, spin2y, spin2z)
with
    min_spin1_magnitude <= (spin1x**2 + spin1y**2 + spin1z**2)**0.5 <= max_spin1_magnitude
    min_spin2_magnitude <= (spin2x**2 + spin2y**2 + spin2z**2)**0.5 <= max_spin2_magnitude
    """
    _normalized = IsotropicPowerLawCartesianSpin1._normalized and IsotropicPowerLawCartesianSpin2._normalized
    _params = (
        'min_spin1_magnitude',
        'max_spin1_magnitude',
        'pow_spin1_magnitude',
        'min_spin2_magnitude',
        'max_spin2_magnitude',
        'pow_spin2_magnitude',
    )

    def _init_values(
            self,
            max_spin1_magnitude=1.0,
            max_spin2_magnitude=1.0,
            min_spin1_magnitude=0.0,
            min_spin2_magnitude=0.0,
            pow_spin1_magnitude=0.0,
            pow_spin2_magnitude=0.0,
        ):
        self._values = [
            min_spin1_magnitude,
            max_spin1_magnitude,
            pow_spin1_magnitude,
            min_spin2_magnitude,
            max_spin2_magnitude,
            pow_spin2_magnitude,
        ]

    def _domain(self):
        _, M1, _, _, M2, _ = self.params
        ans = dict((_, (-M1, +M1)) for _ in self._scalar_variates[:3])
        ans.update(dict((_, (-M2, +M2)) for _ in self._scalar_variates[3:]))
        return ans

    def _rvs(self, size=1):
        a1min, a1max, a1pow, a2min, a2max, a2pow = self.params
        spin1 = IsotropicPowerLawCartesianSpin1._static_rvs(a1min, a1max, a1pow, size=size)
        spin2 = IsotropicPowerLawCartesianSpin2._static_rvs(a2min, a2max, a2pow, size=size)
        return be.transpose(be.array([spin1[:,0], spin1[:,1], spin1[:,2], spin2[:,0], spin2[:,1], spin2[:,2]]))

    @staticmethod
    def _logprob(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, a1min, a1max, a1pow, a2min, a2max, a2pow):
        return \
            IsotropicPowerLawCartesianSpin1._logprob(spin1x, spin1y, spin1z, a1min, a1max, a1pow) \
            + IsotropicPowerLawCartesianSpin2._logprob(spin2x, spin2y, spin2z, a2min, a2max, a2pow)
