"""a module housing distributions over effective spins (conditioned on masses)
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from .utils import SpinDistribution
from gwdistributions.distributions.base import (Uniform, TruncatedGaussian, Beta)
from gwdistributions.transforms.spin import PolarSpins2Chi
from gwdistributions.backends import numerics as be
from .polar import IsotropicSpin1PolarAngle

#-------------------------------------------------

class ChiEffDistribution(SpinDistribution):
    """a child class used to declare things are distributions over the effective aligned spin (chi_eff)
    """
    _scalar_variates = ('chi_eff',)

    def _domain(self):
        return {self._scalar_variates[0]: (-1.0, +1.0)}

#------------------------

# declare sub-classes that are conditioned on different ways of representing the masses
# this is supported because we always generate samples by iterating through all distributions
# and only then do we apply transforms

class ChiEffGivenAsymmetricMassRatioDistribution(ChiEffDistribution):
    """a child class used to declare things are distributions over the effective aligned spin (chi_eff)
    """
    _required = ('asymmetric_mass_ratio',)

class ChiEffGivenComponentMassDistribution(ChiEffDistribution):
    """a child class used to declare things are distributions over the effective aligned spin (chi_eff)
    """
    _required = ('mass1_source', 'mass2_source')

#-------------------------------------------------

class BetaChiEff(ChiEffDistribution, Beta):
    """assumes chi_eff is drawn from a scaled/shifted Beta distributed such that the support of the \
Beta distribuiton (usually between 0 and 1) is between
    min_chi_eff <= chi_eff <= max_chi_eff
and
    p(chi_eff) ~ (chi_eff - min_chi_eff)**(alpha_chi_eff - 1) * (max_chi_eff - chi_eff)**(beta_chi_eff - 1)
    """

    def _domain(self):
        return Beta._domain(self)

#-------------------------------------------------


class TruncatedGaussianChiEff(ChiEffDistribution, TruncatedGaussian):
    """\
Generates chi_eff drawn from a truncated Gaussian distribution
    p(chi_eff) ~ exp(-0.5*(chi_eff-mean_chi_eff)**2/stdv_chi_eff**2)
where
    min_chi_eff <= chi_eff <= max_chi_eff
    """

    def _domain(self):
        return TruncatedGaussian._domain(self)

#-------------------------------------------------

class IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio(ChiEffGivenAsymmetricMassRatioDistribution):
    """\
The induced distribution of chi_eff when the (6D) spin components are iid isotropically following \
power-laws in magnitudes. This is additionally conditioned on the asymmetric mass ratio. See 
    https://dcc.ligo.org/LIGO-P2100112
for full derivations and more details.
    """
    _normalized = True
    _params = ('max_spin_magnitude',)

    def _init_values(self, max_spin_magnitude=+1.0):
        self._values = [max_spin_magnitude]

    def _domain(self, q):
        return {'chi_eff': (-1.0, +1.0)}

    def _rvs(self, q, size=1):
        return self._static_rvs(*self.params, q, size=size)

    @staticmethod
    def _static_rvs(max_mag, q, size=1):

        # draw samples from iid spin distributions
        pole1 = IsotropicSpin1PolarAngle._rvs(size=size)
        pole2 = IsotropicSpin1PolarAngle._rvs(size=size)
        mag1 = Uniform._static_rvs(0, max_mag, size=size)
        mag2 = Uniform._static_rvs(0, max_mag, size=size)

        # now compute chi_eff
        return PolarSpins2Chi._chi_eff(1.0, mag1*be.cos(pole1), q, mag2*be.cos(pole2))

    @staticmethod
    def _logprob(chi_eff, q, max_spin_mag):
        chi = be.abs(chi_eff) ### only depends on absolute magnitude of chi_eff
        return be.where( # based on Eq 10 of https://dcc.ligo.org/LIGO-P2100112
            chi >= max_spin_mag,
            -be.infty,
            be.where( # chi < max_spin_mag
                chi == 0,
                IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B21(chi, q, max_spin_mag),
                be.where( # chi != 0
                    chi < max_spin_mag*(1-q)/(1+q),
                    be.where(
                        chi < q*max_spin_mag/(1+q),
                        IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B20(chi, q, max_spin_mag),
                        IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B22(chi, q, max_spin_mag),
                    ),
                    be.where( # chi > max_spin_mag*(1-q)/(1+q)
                        chi < q*max_spin_mag/(1+q),
                        IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B26(chi, q, max_spin_mag),
                        be.where( # chi > q*max_spin_mag/(1+q)
                            chi < max_spin_mag/(1+q),
                            IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B27(chi, q, max_spin_mag),
                            IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._B29(chi, q, max_spin_mag),
                        ),
                    ),
                ),
            ),
        )

    @staticmethod
    def _B20(chi, q, amax):
        """Eq. B20 from https://dcc.ligo.org/LIGO-P2100112
        """
        ans = q*amax*(4 + 2*be.log(amax) - be.log(q**2*amax**2 - (1+q)**2 * chi**2)) \
            - 2*(1+q)*chi * be.arctanh((1+q)*chi/(q*amax)) \
            + (1+q)*chi * (be.dilogarithm(-q*amax/((1+q)*chi)).real - be.dilogarithm(q*amax/((1+q)*chi)).real)

        return be.log(1+q) - be.log(4*q*amax**2) + be.log(ans)

    @staticmethod
    def _B21(chi, q, amax):
        """Eq. B21 from https://dcc.ligo.org/LIGO-P2100112
        """
        return be.log(1+q) + be.log(1-be.log(q)) - be.log(2*amax)

    @staticmethod
    def _B22(chi, q, amax):
        """Eq. B22 from https://dcc.ligo.org/LIGO-P2100112
        """
        ans = 4*q*amax + 2*q*amax*be.log(amax) \
            - 2*(1+q)*chi*be.arctanh(q*amax/((1+q)*chi)) \
            - q*amax*be.log((1+q)**2*chi**2 - q**2*amax**2) \
            + (1+q)*chi*(be.dilogarithm(-q*amax/((1+q)*chi)).real - be.dilogarithm(q*amax/((1+q)*chi)).real)

        return be.log(1+q) - be.log(4*q*amax**2) + be.log(ans)

    @staticmethod
    def _B26(chi, q, amax):
        """Eq. B26 from https://dcc.ligo.org/LIGO-P2100112
        """
        ans = 2*(1+q)*(amax - chi) - (1+q)*chi*be.log(amax)**2 \
            + (amax + (1+q)*chi*be.log((1+q)*chi))*be.log(q*amax/(amax-(1+q)*chi)) \
            - (1+q)*chi*be.log(amax)*(2 + be.log(q) - be.log(amax - (1+q)*chi)) \
            + q*amax * be.log(amax/(q*amax - (1+q)*chi)) \
            + (1+q)*chi*be.log((amax-(1+q)*chi)*(q*amax - (1+q)*chi)/q) \
            + (1+q)*chi*(be.dilogarithm(1 - amax/((1+q)*chi)).real - be.dilogarithm(q*amax/((1+q)*chi)).real)

        return be.log(1+q) - be.log(4*q*amax**2) + be.log(ans)

    @staticmethod
    def _B27(chi, q, amax):
        """Eq. B27 from https://dcc.ligo.org/LIGO-P2100112
        """
        ans = -chi*be.log(amax)**2 + 2*(1+q)*(amax-chi) \
            + q*amax*be.log(amax/((1+q)*chi - q*amax)) + amax*be.log(q*amax/(amax - (1+q)*chi)) \
            - chi*be.log(amax)*(2*(1+q) - be.log((1+q)*chi) - q*be.log((1+q)*chi/amax)) \
            + (1+q)*chi*be.log((-q*amax + (1+q)*chi)*(amax - (1+q)*chi)/q) \
            + (1+q)*chi*be.log(amax/((1+q)*chi))*be.log((amax - (1+q)*chi)/q) \
            + (1+q)*chi*(be.dilogarithm(1-amax/((1+q)*chi)).real - be.dilogarithm(q*amax/((1+q)*chi)).real)

        return be.log(1+q) - be.log(4*q*amax**2) + be.log(ans)

    @staticmethod
    def _B29(chi, q, amax):
        """Eq. B29 from https://dcc.ligo.org/LIGO-P2100112
        """
        ans = 2*(1+q)*(amax-chi) - (1+q)*chi*be.log(amax)**2 \
            + be.log(amax)*(amax - 2*(1+q)*chi - (1+q)*chi*be.log(q/((1+q)*chi - amax))) \
            - amax*be.log(((1+q)*chi - amax)/q) \
            + (1+q)*chi*be.log(((1+q)*chi - amax)*((1+q)*chi - q*amax)/q) \
            + (1+q)*chi*be.log((1+q)*chi)*be.log(q*amax/((1+q)*chi - amax)) \
            - q*amax*be.log(((1+q)*chi - q*amax)/amax) \
            + (1+q)*chi*(be.dilogarithm(1 - amax/((1+q)*chi)).real - be.dilogarithm(q*amax/((1+q)*chi)).real)

        return be.log(1+q) - be.log(4*q*amax**2) + be.log(ans)

class IsotropicUniformMagnitudeChiEffGivenComponentMass(ChiEffGivenComponentMassDistribution):
    """\
The induced distribution of chi_eff when the (6D) spin components are isotropically distributed following \
power-laws in magnitudes. This is additionally conditioned on the (source-frame) component masses, and delegates \
to IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio. See 
    https://dcc.ligo.org/LIGO-P2100112
for full derivations and more details.
    """
    _normalized = IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._normalized
    _params = ('max_spin_magnitude',)

    def _init_values(self, max_spin_magnitude=+1.0):
        self._values = [max_spin_magnitude]

    def _domain(self, mass1_source, mass2_source):
        return IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._domain(self, mass2_source/mass1_source)

    def _rvs(self, mass1_source, mass2_source, size=1):
        q = mass2_source / mass1_source
        return IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._static_rvs(*self.params, q, size=size)

    @staticmethod
    def _logprob(chi_eff, mass1_source, mass2_source, max_spin_mag):
        q = mass2_source / mass1_source
        return IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._logprob(chi_eff, q, max_spin_mag)
