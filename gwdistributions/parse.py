"""a module that houses various I/O utility functions for reading/writing sets of events from disk
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import sys
if sys.version_info.major < 3:
    from ConfigParser import ConfigParser ### Python 2
else:
    from configparser import ConfigParser ### Python 3

from .generators import EventGenerator
from . import distributions
from .distributions.base import Mixture ### handled as a special case
from .distributions.mass import (Mass1Distribution, ComponentMassPairing) ### handled as a special case
from .distributions.location.angles import RotatedRADec
from . import transforms
from .utils import subclasses
from .backends import names

### nonstandard libraries
try:
    import gwdetectors # https://git.ligo.org/reed.essick/gw-detectors
except ImportError:
    gwdetectors = None

#-------------------------------------------------

def samplingdistribution_factory():
    return subclasses(distributions.SamplingDistribution)

def attributetransformation_factory():
    return subclasses(transforms.AttributeTransformation)

#-------------------------------------------------
# I/O associated with parsing config files
#-------------------------------------------------

NETWORK_OPTION_NAME = 'network' ### special case
def parse_config_section(section, config, verbose=False, catch_network_exceptions=False):
    """advanced parsing logic useful for interpreting a specific section
    """
    ans = dict()
    for k in config.options(section):
        try: ### maybe it's an integer
            ans[k] = config.getint(section, k)
            continue
        except ValueError:
            pass

        try: ### maybe a float
            ans[k] = config.getfloat(section, k)
            continue
        except ValueError:
            pass

        try: ### maybe a bool
            ans[k] = config.getboolean(section, k)
            continue
        except:
            pass

        ### special cases
        if k == NETWORK_OPTION_NAME: ### delegate to gw-detectors
            if gwdetectors is None:
                raise ImportError('could not import gwdetectors')

            try:
                ans[k] = gwdetectors.parse(config.get(section, k), verbose=verbose)
                continue

            except Exception as e: ### fall back to default behavior (interpret option as a string
                if verbose:
                    print('WARNING!: error encountered when parsing network (%s) with gwdetectors\n%s' \
                        % (config.get(section, k), str(e)))

                if not catch_network_exceptions:
                    raise e

        ### just grab it as a string
        val = config.get(section, k).split() ### split this into a list by default
        if len(val) == 1: # just return the single string
            val = val[0]
        ans[k] = val

    if verbose:
        for key, val in ans.items():
            print('    %s : %s (%s)' % (key, val, val.__class__.__name__))

    return ans

#------------------------

NAMING_CONVENTION_SECTION_NAME = 'Naming Convention'
NAMING_CONVENTION_OPTION = 'convention'
def parse_naming_convention(config, verbose=False):
    """allow user to specify a naming convention in section=%s via the protected key=%s and \
to specify names for other variates as "key = value" pairs
    """%(NAMING_CONVENTION_SECTION_NAME, NAMING_CONVENTION_OPTION)

    options = config.options(NAMING_CONVENTION_SECTION_NAME)

    # first set up the names for a convention (if specified)
    if NAMING_CONVENTION_OPTION in options:
        options.remove(NAMING_CONVENTION_OPTION) ### remove this so we don't have to worry about it latter
        names.use_convention(
            config.get(NAMING_CONVENTION_SECTION_NAME, NAMING_CONVENTION_OPTION),
            verbose=verbose,
        )

    # now iterate through the remaining options, setting the name for each key, value pair
    for option in options:
        names.use(option, config.get(NAMING_CONVENTION_SECTION_NAME, option), verbose=verbose)

#------------------------

COSMOLOGY_SECTION_NAME = 'Cosmology'
COSMOLOGY_OPTIONS = ['ho', 'omegamatter', 'omegaradiation', 'omegalambda']
def parse_cosmology(config, verbose=False):
    """allow users to specify cosmological parameters (%s) in a single section=%s
    """%(', '.join(COSMOLOGY_OPTIONS), COSMOLOGY_SECTION_NAME)

    kwargs = dict()
    if config.has_section(COSMOLOGY_SECTION_NAME):
        if verbose:
            print('loading cosmological parameters from: [%s]'%COSMOLOGY_SECTION_NAME)

        for option in COSMOLOGY_OPTIONS:
            if config.has_option(COSMOLOGY_SECTION_NAME, option):
                kwargs[option] = config.getfloat(COSMOLOGY_SECTION_NAME, option)

    return kwargs

#------------------------

CONDITIONAL_OPTION = 'check'
def parse_conditional(section, config, verbose=False):
    """attempt to interpet the section as a conditional with option=%s
    """%CONDITIONAL_OPTION
    assert config.has_option(section, CONDITIONAL_OPTION), \
        'sections representing conditionals must have a single option called "%s"'%CONDITIONAL_OPTION

    string = config.get(section, CONDITIONAL_OPTION)
    if verbose:
        print('instantiating conditional [%s] %s'%(section, string))

    ### using eval is not great, but it should allow for a lot of flexibility
    return eval(string)

#------------------------

def parse_mixture_section(section, config, verbose=False, allow_empty_configs=False):
    """parse a mixture model from a section. This should consist of a single option (path) that points to a separate config. \
That separate config should contain a section for each component of the mixture model. These components should have the additional \
option (beyond everything else they need) called 'mixing_fraction' that we additionally read in.
    """
    args = []
    for option in config.options(section): ### iterate through all the options in this config
        path, mixing_fraction = config.get(section, option).split()
        mixing_fraction = float(mixing_fraction)

        if verbose:
            print('reading [%s] config with mixing_fraction=%.3e from: %s'%(option, mixing_fraction, path))

        # instantiate an EventGenerator based on the subpopulation and add it to the mixture modle
        args.append( (parse(path, verbose=verbose, allow_empty_configs=allow_empty_configs), mixing_fraction) )

    # intantiate Mixture model and return
    return Mixture(*args)

#------------------------

MASS_PAIRING_PATH_NAME = 'path'
def parse_mass_pairing_section(section, config, verbose=False, Verbose=False):
    """parse a mass pairing from a section. This should consist of a single option (%s) that points to a separte config. \
That config should contain a single section that defines a Mass1Distribution. \
Additional options for the pairing function can be specified alongside "%s"
    """%(MASS_PAIRING_PATH_NAME, MASS_PAIRING_PATH_NAME)

    verbose |= Verbose

    # parse section in the normal way
    kwargs = parse_config_section(section, config, verbose=Verbose)

    # instantiate Mass1Distribution
    assert MASS_PAIRING_PATH_NAME in kwargs, \
        'ComponentMassPairing sections must have an option: '+MASS_PAIRING_PATH_NAME
    path = kwargs.pop(MASS_PAIRING_PATH_NAME)
    if verbose:
        print('loading [Mass1Distribution] from : '+path)
    dists, trans, conds = parse_config(path, verbose=verbose, allow_empty_configs=False)
    assert len(trans)==0, 'cannot specify transforms in Mass1Distribution config for ComponentMassPairing!'
    assert len(conds)==0, 'cannot specify transforms in Mass1Distribution config for ComponentMassPairing!'
    assert (len(dists)==1), 'must specify a single distribution in config for ComponentMassPairing'
    assert dists[0].num_variates == 1, 'distribution must be 1 dimensional'
    assert dists[0].variates[0] == names.name('mass1_source'), \
        'must specify a distribution with variates = (%s,) for ComponentMassPairing' % names.name('mass1_source')

    kwargs['massdist'] = dists[0] ### based on name declared in distributions.mass.pairing

    # return
    return kwargs

#------------------------

ROTATION_PATH_NAME = 'path'
def parse_rotation_section(section, config, verbose=False, Verbose=False):
    """parse a rotation from a section. This should consist of a single option (%s) that points to a separate config. \
That config should contain a single section that declares an RADecDistribuiton. \
Additional options for the rotation can be specified alongside "%s"
    """%(ROTATION_PATH_NAME, ROTATION_PATH_NAME)

    verbose |= Verbose

    # parse section in the normal way
    kwargs = parse_config_section(section, config, verbose=Verbose)

    # instantiate Mass1Distribution
    assert ROTATION_PATH_NAME in kwargs, \
        'RotatedRADec sections must have an option: '%ROTATION_PATH_NAME
    path = kwargs.pop(ROTATION_PATH_NAME)
    if verbose:
        print('loading [RADecDistribution] from : '+path)
    distributions, transforms, conditionals = parse_config(path, verbose=verbose, allow_empty_configs=False)
    assert len(transforms)==0, 'cannot specify transforms in RADecDistribution config for RotatedRADec!'
    assert len(conditionals)==0, 'cannot specify transforms in RADecDistribution config for RotatedRADec!'
    assert (len(distributions)==1), 'must specify a single distribution in config for RotatedRADec'
    assert isinstance(distributions[0], distributions.RADecDistribution), \
        'must specify an instance of RADecDistribution for RotatedRADec'

    kwargs['dist'] = distributions[0] ### based on name declared in distributions.location.angles.RotateRADec

    # return
    return kwargs

#-------------------------------------------------

def parse_config(path, verbose=False, Verbose=False, allow_empty_configs=False):
    """parse the configuration from disk into separate args needed to construct an Event Generator
    """
    verbose |= Verbose

    ### load the config
    if verbose:
        print('reading config from: '+path)
    config = ConfigParser()
    config.read(path)

    ### figure out which distributions and transforms are available
    samplingdistributions = samplingdistribution_factory()
    attributetransformations = attributetransformation_factory()

    # special cases
    component_mass_pairings = ['ComponentMassPairing'] + list(subclasses(ComponentMassPairing).keys())
    radec_rotations = ['RotatedRADec'] + list(subclasses(RotatedRADec).keys())

    ### iterate through sections, instantiating either a distribution or transform for each
    distributions = []
    transforms = []
    conditionals = []
    for name in config.sections(): ### iterate through config's sections
        if name == NAMING_CONVENTION_SECTION_NAME: ### set the naming convention
            parse_naming_convention(config, verbose=verbose)

        elif name.split()[0] == Mixture.__name__: ### condition in this way to support multiple mixture models within the same config
            if verbose:
                print('instantiating [%s]'%name)

            distrib = parse_mixture_section(
                name,
                config,
                verbose=verbose,
                allow_empty_configs=allow_empty_configs,
            )

            if Verbose:
                print('    required:\n        %s' % ('\n        '.join(distrib.required)))
                print('    scalar_variates\n        %s' % ('\n        '.join(distrib.scalar_variates)))
                print('    vector_variates\n        %s' % ('\n        '.join(distrib.vector_variates)))

            distributions.append(distrib)

        elif name in samplingdistributions:
            if verbose:
                print('instantiating SamplingDistribution [%s]'%name)
            kwargs = dict()
            if samplingdistributions[name].cosmological:          ### look for cosmological parameters
                kwargs.update(parse_cosmology(config, verbose=verbose))

            ### add/overwrite based on what is in this section
            if name in component_mass_pairings:
                kwargs.update(parse_mass_pairing_section(name, config, verbose=verbose, Verbose=Verbose))
            elif name in radec_rotations:
                kwargs.update(parse_rotation_section(name, config, verbose=verbose, Verbose=Verbose))
            else:
                kwargs.update(parse_config_section(name, config, verbose=Verbose))

            distrib = samplingdistributions[name](**kwargs) ### make the object

            if Verbose:
                print('    required:\n        %s' % ('\n        '.join(distrib.required)))
                print('    scalar_variates\n        %s' % ('\n        '.join(distrib.scalar_variates)))
                print('    vector_variates\n        %s' % ('\n        '.join(distrib.vector_variates)))

            distributions.append(distrib)

        elif name in attributetransformations:
            if verbose:
                print('instantiating AttributeTransformation [%s]'%name)

            kwargs = dict()
            if attributetransformations[name].cosmological:       ### look for cosmological parameters
                kwargs.update(parse_cosmology(config, verbose=verbose))
            ### add/overwrite based on what is in this section
            kwargs.update(parse_config_section(name, config, verbose=Verbose))

            trans = attributetransformations[name](**kwargs)

            if Verbose:
                print('    required:\n        %s' % ('\n        '.join(trans.required)))
                print('    scalar_variates\n        %s' % ('\n        '.join(trans.scalar_variates)))
                print('    vector_variates\n        %s' % ('\n        '.join(trans.vector_variates)))

            transforms.append(attributetransformations[name](**kwargs))

        else:
            try: ### try to parse this as if it was an arbitrary conditional
                conditionals.append(parse_conditional(name, config, verbose=verbose))
            except:
                if verbose:
                    print('section [%s] not understood; skipping'%name)

    if not allow_empty_configs:
        assert len(distributions) + len(transforms) + len(conditionals), \
            'could not identify any distributions, transformations, or condionals within %s' % path

    ### return
    return distributions, transforms, conditionals

#------------------------

def parse(path, verbose=False, Verbose=False, allow_empty_configs=False, **kwargs):
    """parse the config into a standard EventGenerator
    """
    verbose |= Verbose
    return EventGenerator(
        *parse_config(path, verbose=verbose, Verbose=Verbose, allow_empty_configs=allow_empty_configs),
        **kwargs
    )
