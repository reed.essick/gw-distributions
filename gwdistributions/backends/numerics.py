"""a module that houses the numeric backends needed thorughout the library. All numeric routines should import the backend by first importing this module. In this way, all modules will automatically use the same shared global name for the backend
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import sys
SELF = sys.modules[__name__] ### used to reference this module

#-------------------------------------------------

### fallback libraries for required functionality
try:
    from scipy import __version__ as scipy_version
except ImportError:
    scipy_version = None

try:
    from scipy import integrate
except ImportError:
    integrate = None

try:
    from scipy import special
except ImportError:
    special = None

try:
    from scipy import stats
except ImportError:
    stats = None

try:
    import numpy
except ImportError:
    numpy = None

try:
    from numpy import random as numpy_random
except ImportError:
    numpy_random = None

#-------------------------------------------------

# a mapping for functions that must be imported
# if these are not available with a particular backend, they will fall back to another library
REQUIRED = {
    'scipy.special' : (special, ['erf', 'sph_harm', 'spence']),
    'scipy.stats' : (stats, ['beta', 'norm']),
    'numpy': (numpy, ['ndim', 'shape', 'interp', 'min', 'max', 'inf', 'pi', 'isclose']),
    'numpy.random': (numpy_random, ['random', 'normal', 'randint', 'seed']),
}

ALIASES = {
    'inf' : ('infty',)
}

# deal with versioning issues for scipy
if scipy_version is not None:
    if scipy_version < '1.14.0':
        REQUIRED['scipy.integrate'] = (integrate, ['cumtrapz'])

    else:
        REQUIRED['scipy.integrate'] = (integrate, ['cumulative_trapezoid'])
        ALIASES['cumulative_trapezoid'] = ('cumtrapz',)

#------------------------

# a list of all the functions we import from different backends
# these will not be imported if they are not present in the backend
IMPORTED = [
    # basic mathematical operations
    'abs',
    'log',
    'exp',
    'sin',
    'cos',
    'arcsin',
    'arccos',
    'arctan2',
    'arctanh',
    'sign',
    'ceil',
    # array manipulation
    'sum',
    'cumsum',
    'prod',
    'where',
    'all',
    'any',
    'transpose', 
    'diff',
    'argsort',
    'unique',
    'clip',
    # array instantiation
    'array',
    'arange',
    'ones',
    'zeros',
    'empty',
    'linspace',
    'sign',
    'meshgrid',
    # fourier transform libraries
    'fft',
    ### the following may be renamed based on NATIVE_NAMES
    'ArrayType',
]

#------------------------

# mappings between the names in modules to what we want to call them after they are imported
NATIVE_NAMES = {
    'numpy' : {
        'ArrayType' : 'ndarray',
        'random' : 'random.random',
        'normal' : 'random.normal',
        'randint': 'random.randint',
        'seed' : 'random.seed',
    },
    'jax' : {
        'ArrayType' : 'numpy.ndarray',
        'erf' : 'scipy.special.erf',
        'sph_harm' : 'scipy.special.sph_harm',
        'infty' : 'numpy.inf',
        'spence' : 'scipy.special.spence',
        'beta' : 'scipy.stats.beta',
        'norm' : 'scipy.stats.norm',
    },
    'cupy' : {
        'ArrayType' : 'ndarray',
        'random' : 'random.random',
        'normal' : 'random.normal',
        'randint': 'random.randint',
        'seed' : 'random.seed',
    },
}

SUFFIX = {
    'numpy' : '',
    'jax' : 'numpy.',
    'cupy' : '',
}

#-------------------------------------------------

DEFAULT_BACKEND = 'numpy'

#-------------------------------------------------

def use(bknd_name, verbose=False, Verbose=False):
    '''
    determine which numerical backend will be used. We currently support
    - numpy
    - jax
    - cupy
    '''
    verbose |= Verbose

    ### record the backend that was loaded
    setattr_with_alias('BACKEND', bknd_name, verbose=Verbose)

    ### delete any previously imported functions
    if verbose:
        print('removing previously imported functions')
    remove = []
    for _, attrs in REQUIRED.values():
        remove += attrs
    remove += IMPORTED

    for attr in remove:
        if hasattr(SELF, attr):
            if verbose:
                print('    removing previous declartion of: '+attr)
            delattr(SELF, attr)

    ### import functions
    if verbose:
        print('importing utilities from: '+bknd_name)
    names = NATIVE_NAMES.get(bknd_name, dict())
    suffix = SUFFIX[bknd_name]

    if bknd_name == 'numpy':
        import numpy as bknd

    elif bknd_name == 'jax':
        import jax as bknd

    elif bknd_name == 'cupy':
        import cupy as bknd

    else:
        raise ValueError('WARNING: backend=%s not undestood!'%bknd_name)

    # actually do the importing

    ### the following will always be imported. If they are not present in the backend they will be
    ### imported from a backup library
    for fallback_name, (fallback, attrs) in REQUIRED.items():
        for attr in attrs:
            native = names.get(attr, suffix+attr)
            try:
                if verbose:
                    print('    importing %s.%s as %s'%(bknd_name, native, attr))
                setattr_with_alias(attr, fetch(bknd, native), verbose=Verbose)

            except ImportError:
                native = NATIVE_NAMES[fallback_name].get(attr, attr) if fallback_name in NATIVE_NAMES else attr
                if verbose:
                    print('        could not import %s.%s. Falling back to %s.%s' % \
                        (bknd_name, native, fallback_name, attr))
                try:
                    setattr_with_alias(attr, fetch(fallback, native), verbose=Verbose)
                except:
                    raise ImportError('could not import %s! Not available in fallback library %s' % \
                        (attr, fallback_name))

    ### the following will only be imported if they are present in the backend
    for attr in IMPORTED:
        native = names.get(attr, suffix+attr) ### the name of the attribute in the library
        try:
            if verbose:
                print('    importing %s.%s as %s'%(bknd_name, native, attr))
            setattr_with_alias(attr, fetch(bknd, native), verbose=Verbose)

        except ImportError:
            if verbose:
                print('        could not import %s'%attr)

    ### FIXME?
    # special cases where function signatures change depending on backend...
    if bknd_name == 'jax':
        if verbose:
            print('    setting custom signature for spherical_harmonic')

        def spherical_harmonic(m, l, azimuth, pole):
            """jax is peculiar about how values are passed, and the jit compiler requires n_max to be declared
            assumes m, l, l_max are integers while azimuth, pole are either ints, floats, or vectors
            """
            if not shape(pole):
                pole = [pole]
                azimuth = [azimuth]
            N = len(pole)
            return sph_harm(array([m]*N), array([l]*N), array(azimuth), array(pole), n_max=l)

    else:
        def spherical_harmonic(m, l, azimuth, pole):
            return sph_harm(m, l, azimuth, pole)

    setattr_with_alias('spherical_harmonic', spherical_harmonic, verbose=Verbose) ### set to be outside this function's scope

def setattr_with_alias(key, val, verbose=False):
    if verbose:
        print('        setting : %s -> %s' % (val, key))
    setattr(SELF, key, val)

    if key in ALIASES:
        for v in ALIASES[key]:
            if verbose:
                print('        aliasing : %s -> %s' % (key, v))
            setattr(SELF, v, val)

def fetch(module, attr):
    '''
    support look-up within submodules
    '''
    sequence = attr.split('.')
    for ATTR in attr.split('.'):
        if hasattr(module, ATTR):
            module = getattr(module, ATTR)
        else:
            raise ImportError('could not find attr=%s'%attr)
    return module

#-------------------------------------------------

def dilogarithm(z):
    return spence(1-z+0j) # follow definition of dilogarithm as PolyLog[2,z]

#-------------------------------------------------

use(DEFAULT_BACKEND) ### set the backend
