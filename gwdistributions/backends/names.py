"""a module that houses standardized names for variates
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import sys
SELF = sys.modules[__name__] ### used to reference this module

import copy

import string ### used to generate random naming conventions
import random

from . import numerics as be

#-------------------------------------------------

CONVENTIONS = dict()

#-------------------------------------------------

### default naming convention
default = dict()
CONVENTIONS['default'] = default

# metadata for gwdio
default['date'] = 'date'
default['num_accepted'] = 'num_accepted'
default['total_generated'] = 'total_generated'

# probablity names
default['logprob'] = 'lnpdraw' ### this is the attribute name used to store the pdf value of each draw

# masses
for mass in ['mass', 'mass1', 'mass2', 'chirp_mass', 'total_mass']:
    for frame in ['source', 'detector']:
        name = mass+"_"+frame
        default[name] = name

# mass ratios
default['symmetric_mass_ratio'] = 'eta'
default['asymmetric_mass_ratio'] = 'q'

# spins
for spherical_component in ['azimuthal_angle', 'polar_angle', 'magnitude']:
    for mass_component in ['', '1', '2']:
        name = 'spin'+mass_component+'_'+spherical_component
        default[name] = name

for cartesian_component in ['x', 'y', 'z']:
    for mass_component in ['', '1', '2']:
        name = 'spin' + mass_component + cartesian_component
        default[name] = name

default['chi_eff'] = 'chi_eff'
default['chi_p'] = 'chi_p'

# orientation
default['inclination'] = 'inclination'
default['polarization'] = 'polarization'
default['coalescence_phase'] = 'coa_phase'

# location
default['right_ascension'] = 'right_ascension' ### celestial coordinates
default['declination'] = 'declination'

default['colatitude'] = 'theta'                ### geographic coordinates
default['longitude'] = 'phi'

default['redshift'] = 'z'
default['comoving_distance'] = 'comoving_distance'
default['luminosity_distance'] = 'luminosity_distance'

default['dcomoving_distance_dredshift'] = 'dcomoving_distance_dredshift'
default['dluminosity_distance_dredshift'] = 'dluminosity_distance_dredshift'

default['effective_luminosity_distance'] = 'eff_dist'

# cosmology
default['ho'] = 'Ho'
default['omega_matter'] = 'OmegaMatter'
default['omega_radiation'] = 'OmegaRadiation'
default['omega_lambda'] = 'OmegaLambda'

# eccentricity
default['eccentricity'] = 'eccentricity'

# time
default['time'] = 'time'
default['geocenter_time'] = default['time']+'_geocenter'

# matter
for param in ['radius', 'tidal_deformability']:
    for mass_component in ['1', '2']:
        name = param + mass_component
        default[name] = name

# SNR / detection

default['snr'] = 'snr'
default['observed_snr'] = 'observed_'+default['snr']
default['observed_phase_maximized_snr'] = 'observed_phase_maximized_'+default['snr']

default['threshold_observed_snr'] = 'threshold_'+default['observed_snr']

### frequency parameters
for param in ['snr', 'waveform']:
    for freq in ['flow', 'fhigh']:
        name = param + "_" + freq
        default[name] = name

default['waveform_fref'] = 'fref'
default['waveform_delta_f'] = 'deltaf'
default['waveform_delta_t'] = 'deltat'

### approximant parameters
default['approximant'] = 'approximant'
default['taper'] = 'taper'
default['mass_boundary'] = 'mass_boundary'

for mass_range in ['bns', 'bhns', 'bbh']:
    name = mass_range + '_approximant'
    default[name] = name

### network parameters
default['network'] = 'network'

#-------------------------------------------------

### LIGOLW XML naming convention
ligolw_xml = copy.copy(default) ### use the default values for everything as a starting point
CONVENTIONS['ligolw_xml'] = ligolw_xml

# overwrite a few special cases

# mass
ligolw_xml['mass1_detector'] = 'mass1'
ligolw_xml['mass2_detector'] = 'mass2'
ligolw_xml['chirp_mass'] =  'mchirp'

# orientation
ligolw_xml['coalescence_phase'] = 'phi0'

# location
ligolw_xml['right_ascension'] = 'longitude'
ligolw_xml['declination'] = 'latitude'
ligolw_xml['luminosity_distance'] = 'distance'

ligolw_xml['effective_luminosity_distance'] = 'eff_dist'

# time
ligolw_xml['time'] = 'end_time'
ligolw_xml['geocenter_time'] = 'geocent_end_time'

# SNR / detection

ligolw_xml['snr_flow'] = 'f_lower'
ligolw_xml['snr_fhigh'] = 'f_final'
ligolw_xml['waveform_fref'] = 'f_final'

### approximant parameters
ligolw_xml['approximant'] = 'waveform'

#-------------------------------------------------

KNOWN_VARIATES = set()
for key, val in CONVENTIONS.items():
    KNOWN_VARIATES = KNOWN_VARIATES.union(set(val.keys()))

CONVENTION = dict()    # mapping from "raw name" to naming convention
INVCONVENTION = dict() # inverse mapping

#-------------------------------------------------

def use_convention(convention, verbose=False, Verbose=False):
    """set the names declared in this module to follow a standard convention
    """
    if convention == RANDOM_CONVENTION_NAME: ### used for unit tests
        use_random_convention(verbose=verbose, Verbose=Verbose)

    else:
        verbose |= Verbose
        if verbose:
            print('using naming convention: '+convention)
        for key, val in CONVENTIONS[convention].items():
            use(key, val, verbose=Verbose)

def use(key, val, verbose=False, must_be_known=True):
    """set the name for a key to val
    """
    if must_be_known:
        assert key in KNOWN_VARIATES, 'setting name for unknown variate=%s'%key
    if verbose:
        print('setting name: %s -> %s'%(key, val))
    CONVENTION[key] = val
    INVCONVENTION[val] = key

#------------------------

RANDOM_CONVENTION_NAME = 'RANDOM'
def use_random_convention(extra=[], verbose=False, Verbose=False):
    """generate a random name for each variate. This is used for unit tests
    """
    verbose |= Verbose
    if verbose:
        print('generating random naming convention')
    for variate in sorted(set(KNOWN_VARIATES) | set(extra)):
        val = _random_string()
        use(variate, val, verbose=Verbose, must_be_known=False)

def _random_string(size=8):
    return ''.join(random.choice(string.ascii_letters) for _ in range(size))

#------------------------

def name(key):
    """look up the name associated with this key
    """
    return CONVENTION.get(key, key)

def invname(name):
    """look up the key associated with name
    """
    return INVCONVENTION.get(name, name)

#-----------

LOGPROB_GIVEN_NAME = 'GIVEN' ### a protected name
def logprob_name(variates=[], required=[]):
    """special case of mangled names for factored probabilities
    NOTE! this does not support automatic renaming unless variates==required==[]
    """
    ans = name('logprob')
    if variates:
        ans = "_".join(name(_) for _ in [ans]+_logprob_ordering(variates))
    if required:
        ### NOTE: assumes no variate will ever be called "GIVEN"
        ans = "_".join(name(_) for _ in [ans, LOGPROB_GIVEN_NAME]+_logprob_ordering(required))
    return ans

def _logprob_ordering(variates):
    """wrap this into a function so that other libraries can call it without knowing exactly how it is accomplished
    """
    return sorted(variates)

#-------------------------------------------------

### set the naming convention to the default
use_convention('default')
