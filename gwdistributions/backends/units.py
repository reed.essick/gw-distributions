"""define common parameters with dimension-full units
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

### define units in SI
C_SI =  299792458.0
PC_SI = 3.085677581491367e+16
MPC_SI = PC_SI * 1e6
G_SI = 6.6743e-11
MSUN_SI = 1.9884099021470415e+30

### define units in CGS
G_CGS = G_SI * 1e+3
C_CGS = C_SI * 1e2
PC_CGS = PC_SI * 1e2
MPC_CGS = MPC_SI * 1e2
MSUN_CGS = MSUN_SI * 1e3
