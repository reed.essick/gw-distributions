"""a module that houses utility functions, mostly involving the automatic extraction of logprob from events
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from . import names
from . import numerics as be

# used for auto-logprob over effective spins a la https://dcc.ligo.org/LIGO-T2200294
from gwdistributions.distributions.spin import IsotropicPowerLawPolarSpin1Spin2
from gwdistributions.distributions.spin import IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio

from gwdistributions.transforms.spin import CartesianSpin2PolarSpin

#-------------------------------------------------

def _events2fields(events):
    if isinstance(events, be.ArrayType): # structured array
        return events.dtype.names
    else:
        return events.keys() # dictionary

def _fields2logprob_fields(fields):
    ans = dict()
    for field in fields:
        if names.name('logprob') in field:
            divided = field.split(names.LOGPROB_GIVEN_NAME)
            if len(divided) > 1:
                ans[field] = (divided[0][:-1], divided[1])
            else:
                ans[field] = (divided[0], [])
    return ans

#-------------------------------------------------

def variates2logprob(variates, events, events_fields=None, required=None, logprob_fields=None, verbose=False):
    """identify a set of logprob fields that correspond to exactly the variates and required specified.
    If no such set is possible, raise an exception.

    return logprob(variates|required)
    """
    if required is None:
        required = []

    if events_fields is None:
        events_fields = _events2fields(events)

    if logprob_fields is None:
        logprob_fields = _fields2logprob_fields(events_fields)

    if verbose:
        message = 'attempting to construct\n    logprob(%s' % (', '.join(variates))
        if required:
            message = message + '|%s' % (', '.join(required))
        message = message + ')\nbased on\n    %s' % ('\n    '.join(logprob_fields))
        print(message)

    # now, let's map the requested variates to their corresponding logprob_fields
    present_var = dict((field, []) for field in logprob_fields)
    present_req = dict((field, []) for field in logprob_fields)

    missing = []

    for variate in variates:
        assert variate not in required, 'conflict between variate (%s) and required (%s)' % \
            (variate, ', '.join(required))

        # make sure each variate belongs to one and only one logprob_var
        sel = [field for field in logprob_fields.keys() if '_'+variate in logprob_fields[field][0]]
        if len(sel) == 1:
            present_var[sel[0]].append(variate)

        elif len(sel) == 0:
            missing.append(variate)
            if verbose:
                print('could not find (%s) within variates from:\n    %s' % \
                    (variate, '\n    '.join(logprob_fields.keys())))

        else:
            raise ValueError('found multiple instances of (%s) within variates from:\n    %s' % \
                (variate, '\n    '.join(logprob_fields.keys())))

        # associate variates with required
        for field in logprob_fields.keys():
            if variate in logprob_fields[field][1]:
                present_req[field].append(variate)

    # associated required with logprob_fields
    for variate in required:
        for field in logprob_fields.keys():
            if variate in logprob_fields[field][1]:
                present_req[field].append(variate)

    # finally, let's see if there is a match
    # for there to be a match, the computed logprob_name for each logprob_field with non-empty present_var
    # must exactly match what we compute based on present_var and present_req
    selected_fields = []
    for field in logprob_fields.keys():
        if present_var[field]: ### only look at fields defined over variates we care about
            predicted = names.logprob_name(variates=present_var[field], required=present_req[field])
            assert predicted == field, 'predicted name (%s) must exactly match existing name (%s)' % \
                (predicted, field)
            selected_fields.append(field)
    selected_fields.sort()

    # figure out which variables are accounted for
    present = []
    for val in present_var.values():
        present += val
    present.sort()

    if verbose:
        print('identified %d logprob fields with exact matches' % len(selected_fields))
        for field in selected_fields:
            print('    %s'%field)
        print('corresponding to %d variates' % len(present))
        for field in present:
            print('    %s'%field)
        print('with %d required' % len(required))
        for field in required:
            print('    %s'%field)

    # now actually compute logprob
    logprob = be.zeros(len(events), dtype=float)
    for field in selected_fields:
        logprob += events[field]

    # some variates did not have an exact match, so we look for known variable transformations
    if missing:
        logprob += _variates2logprob_jacobians(
            missing,
            present+required, # we have already accounted for these!
            events,
            events_fields=events_fields,
            logprob_fields=logprob_fields,
            verbose=verbose,
        )

    # return
    return logprob

#------------------------

def _variates2logprob_jacobians(missing, present, events, events_fields=None, logprob_fields=None, verbose=False):
    """a helper function that attempts to compute logprob for "missing" variates from the \
logprob_fields and checks for overlap with variates already accounted for in "present". \
This implements a few common variable transformations, but is not meant to be an exhaustive set.
    """
    if not missing: # there are no missing variates
        return be.zeros(len(events), dtype=float) # return correct shape

    if events_fields is None:
        events_fields = _events2fields(events)

    if logprob_fields is None:
        logprob_fields = _fields2logprob_fields(events_fields)

    if verbose:
        print('searching for known coordinate transformations between:\n    %s\nto compute logprob from:\n    %s' % \
            ('\n    '.join(missing), '\n    '.join(logprob_fields.keys())))
        if present:
            print('with present:\n    %s' % ('\n    '.join(present)))
        else:
            print('with no present variates')

    # iterate through known transformations
    # the following is not particularly elegant...

    #--------------------

    # detector-frame <--> source-frame masses

    # individual masses separately
    for mass_component in ['', '1', '2']:
        mass = 'mass'+mass_component

        # detector-frame mass <-- source-frame mass, redshift
        mass_det_name = names.name(mass+'_detector')
        if mass_det_name in missing:
            redshift_name = names.name('redshift')
            mass_src_name = names.name(mass+'_source')
            logp_name = names.logprob_name([mass_src_name])

            for logprob_field in logprob_fields.keys():

                if logp_name == logprob_fields[logprob_field][0]:

                    # quickly sanity check existing fields
                    required_and_present = [field for field in present \
                        if field in logprob_fields[logprob_field][1]]

                    if logprob_fields[logprob_field][1] and ('_'+'_'.join(names._logprob_ordering(required_and_present)) != logprob_fields[logprob_field][1]):
                        if verbose:
                            print('cannot convert from %s -> %s with %s because required not present in (%s)' % \
                                (mass_det_name, mass_src_name, logprob_field, ', '.join(present)))

                    elif mass_src_name in present:
                        if verbose:
                            print('conversion from %s -> %s conflicts with existing variate' % \
                                (mass_det_name, mass_src_name))

                    elif redshift_name not in events_fields:
                        if verbose:
                            print('conversion from %s -> %s not possible because %s not present' % \
                                (mass_det_name, mass_src_name, redshift_name))

                    else:
                        if verbose:
                            print('computing logprob(%s) from %s and %s' % \
                                (mass_det_name, logprob_field, redshift_name))

                        # remove from list of "missing"
                        present.append(mass_det_name)
                        present.append(mass_src_name)
                        missing.remove(mass_det_name)

                        # compute this contribution to logprob and recurse
                        # p(m_det) = p(m_src) * |dm_src/dm_det| = p(m_src) / (1+z)
                        return events[logprob_field] - be.log(1+events[redshift_name]) \
                            + _variates2logprob_jacobians(
                                missing,
                                present,
                                events,
                                events_fields=events_fields,
                                logprob_fields=logprob_fields,
                                verbose=verbose,
                            )

        # source-frame mass <-- detector-frame mass, redshift
        mass_src_name = names.name(mass+'_source')
        if mass_src_name in missing:
            redshift_name = names.name('redshift')
            mass_det_name = names.name(mass+'_detector')
            logp_name = names.logprob_name([mass_det_name])

            for logprob_field in logprob_fields:
                if logp_name == logprob_field.split(names.LOGPROB_GIVEN_NAME)[0].strip('_'):

                    # quickly sanity check existing fields
                    required_and_present = [field for field in present \
                        if field in logprob_fields[logprob_field][1]]
                    if logprob_fields[logprob_field][1] and ('_'+'_'.join(names._logprob_ordering(required_and_present)) != logprob_fields[logprob_field][1]):
                        if verbose:
                            print('cannot convert from %s -> %s with %s because required not present in (%s)' % \
                                (mass_src_name, mass_det_name, logprob_field, ', '.join(present)))

                    if mass_det_name in present:
                        if verbose:
                            print('conversion from %s -> %s conflicts with existing variate' % \
                                (mass_src_name, mass_det_name))

                    elif redshift_name not in events_fields:
                        if verbose:
                            print('conversion from %s -> %s not possible because %s not present' % \
                                (mass_src_name, mass_det_name, redshift_name))

                    else:
                        if verbose:
                            print('computing logprob(%s) from %s and %s' % \
                                (mass_src_name, logprob_field, redshift_name))

                        # remove from list of "missing"
                        present.append(mass_src_name)
                        present.append(mass_det_name)
                        missing.remove(mass_src_name)

                        # compute this contribution to logprob and recurse
                        # p(m_src) = p(m_det) * |dm_det/dm_src| = p(m_det) * (1+z)
                        return events[logprob_field] + be.log(1+events[redshift_name]) \
                            + _variates2logprob_jacobians(
                                missing,
                                present,
                                events,
                                events_fields=events_fields,
                                logprob_fields=logprob_fields,
                                verbose=verbose,
                            )

    # both masses together

    # detector-frame mass <-- source-frame mass, redshift
    mass1_det_name = names.name('mass1_detector')
    mass2_det_name = names.name('mass2_detector')
    mass1_src_name = names.name('mass1_source')
    mass2_src_name = names.name('mass2_source')

    mass1_det_missing = mass1_det_name in missing
    mass2_det_missing = mass2_det_name in missing
    mass1_src_missing = mass1_src_name in missing
    mass2_src_missing = mass2_src_name in missing

    if (mass1_det_missing or mass1_src_missing) and (mass2_det_missing or mass2_src_missing):

        # simple sanity check
        if mass1_det_missing and mass1_src_missing:
            raise ValueError('cannot handle both detector- and source-frame mass1 missing (%s and %s)' % \
                (mass1_det_name, mass1_src_name))

        if mass2_det_missing and mass2_src_missing:
            raise ValueError('cannot handle both detector- and source-frame mass2 missing (%s and %s)' % \
                (mass2_det_name, mass2_src_name))

        # now go hunting for matching logpdraw
        missing_masses = []
        sought_masses = []

        redshift_pow = 0 # used to count how many powers of (1+z) we need to multiply by

        if mass1_det_missing:
            missing_masses.append(mass1_det_name)
            sought_masses.append(mass1_src_name)
            redshift_pow -= 1
        else:
            missing_masses.append(mass1_src_name)
            sought_masses.append(mass1_det_name)
            redshift_pow += 1

        if mass2_det_missing:
            missing_masses.append(mass2_det_name)
            sought_masses.append(mass2_src_name)
            redshift_pow -= 1
        else:
            missing_masses.append(mass2_src_name)
            sought_masses.append(mass2_det_name)
            redshift_pow += 1

        logp_name = names.logprob_name(sought_masses)
        for logprob_field in logprob_fields.keys():

            if logp_name == logprob_fields[logprob_field][0]: # we have a match!
                redshift_name = names.name('redshift')

                # sanity check present
                required_and_present = [field for field in present \
                    if field in logprob_fields[logprob_field][1]]
                if logprob_fields[logprob_field][1] and ('_'+'_'.join(names._logprob_ordering(required_and_present)) != logprob_fields[logprob_field][1]):
                    if verbose:
                        print('cannot convert from (%s) -> (%s) with %s because required not present in (%s)' % \
                            (', '.join(missing_masses), ', '.join(sought_masses), logprob_field, ', '.join(present)))

                elif any([mass in present for mass in sought_masses]):
                    if verbose:
                        print('cannot convert from (%s) -> (%s) because of conflict with present (%s)' % \
                            (', '.join(missing_masses), ', '.join(sought_masses), ', '.join(present)))

                elif (redshift_pow != 0) and (redshift_name not in events_fields):
                    print('HERE?')
                    if verbose:
                        print('conversion from %s -> %s not possible because %s not present' % \
                            (', '.join(missing_masses), ', '.join(sought_masses), redshift_name))

                else:
                    if verbose:
                        message = 'computing logprob(%s) from (%s), %s' % \
                                (', '.join(missing_masses), logprob_field, ', '.join(sought_masses))
                        if redshift_pow != 0:
                            message = message + ' and %s' % redshift_name
                        print(message)

                    # remove from list of "missing"
                    for mass_name in sought_masses:
                        present.append(mass_name)

                    for mass_name in missing_masses:
                        present.append(mass_name)
                        missing.remove(mass_name)

                    # compute this contribution to logprob and recurse
                    # p(m_src) = p(m_det) * |dm_det/dm_src| = p(m_det) * (1+z)
                    ans = events[logprob_field]
                    if redshift_pow != 0:
                        ans = ans + redshift_pow * be.log(1+events[redshift_name])
                    return ans \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

    #--------------------

    ### asymmetric_mass_ratio <-- mass2 | mass1
    q_name = names.name('asymmetric_mass_ratio')

    if q_name in missing:
        # look for logprob(mass2|mass1)
        for frame in ['source', 'detector']: # check both possibilities
            mass1_name = names.name('mass1_'+frame)
            mass2_name = names.name('mass2_'+frame)

            logp_name = names.logprob_name([mass2_name]) ### FIXME? also check for logprob(m1|m2)?
            for logprob_field in logprob_fields.keys():
                if logp_name == logprob_fields[logprob_field][0]:

                    # quickly sanity-check the existing fields
                    required_and_present = [field for field in present \
                        if field in logprob_fields[logprob_field][1]]
                    if logprob_fields[logprob_field][1] and ('_'+'_'.join(names._logprob_ordering(required_and_present)) != logprob_fields[logprob_field][1]):
                        if verbose:
                            print('cannot convert from (%s) -> (%s) with %s because required not present in (%s)' % \
                                (mass2_name, q_name, logprob_field, ', '.join(present)))

                    elif mass2_name in present:
                        if verbose:
                            print('conversion from %s -> %s|%s conflicts with existing variate %s' % \
                                (q_name, mass2_name, mass1_name, mass2_name))

                    elif mass1_name not in events_fields:
                        if verbose:
                            print('conversion from %s -> %s|%s not possible because %s not present' % \
                                (q_name, mass2_name, mass1_name, mass1_name))

                    elif mass2_name not in events_fields:
                        if verbose:
                            print('conversion from %s -> %s|%s not possible because %s not present' % \
                                (q_name, mass2_name, mass1_name, mass2_name))

                    else:
                        if verbose:
                            print('computing logprob(%s) from %s, %s' % \
                                (q_name, logprob_field, mass1_name))

                        # remove from list of "missing"
                        present.append(q_name)
                        present.append(mass2_name)
                        missing.remove(q_name)

                        # compute this contribution to logprob and recurse
                        # p(q) = p(m2|m1) * |dm2/dq| = p(m2|m1) * m1
                        return events[logprob_field] + be.log(events[mass1_name]) \
                            + _variates2logprob_jacobians(
                                missing,
                                present,
                                events,
                                events_fields=events_fields,
                                logprob_fields=logprob_fields,
                                verbose=verbose,
                            )

    #-------

    ### mass2 | mass1 <-- asymmetric_mass_ratio
    for frame in ['source', 'detector']:
        mass2_name = names.name('mass2_'+frame)
        if mass2_name in missing:
            q_name = names.name('asymmetric_mass_ratio')
            logp_name = names.logprob_name([q_name])
            if logp_name in logprob_fields:
                mass1_name = names.name('mass1_'+frame)

                # quickly sanity check the existing fields
                if q_name in present:
                    if verbose:
                        print('conversion from %s|%s -> %s conflicts with existing variate %s' % \
                            (mass2_name, mass1_name, q_name, q_name))

                elif mass1_name not in events_fields:
                    if verbose:
                        print('conversion from %s|%s -> %s not possible because %s is not present' % \
                            (mass2_name, mass1_name, q_name, mass1_name))

                else:
                    if verbose:
                        print('computing logprob(%s) from %s, %s' % \
                            (mass2_name, logp_name, mass1_name))

                    # remove from list of "missing"
                    present.append(mass2_name)
                    present.append(q_name)
                    missing.remove(mass2_name)

                    # compute this contribution to logprob and recurse
                    # p(m2|m1) = p(q) * |dq/dm2| = p(q) / m1
                    return events[logp_name] - be.log(events[mass1_name]) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

    ### FIXME: more generally support the transformation between (mass1, mass2) and (mass1, q) or (mass2, q)?

    #--------------------

    ### cartesian <--> polar spin components

    # spin components separately
    for mass_component in ['', '1', '2']:
        spin = 'spin'+mass_component

        ### cartesian spin components <-- polar spin components
        spinx_name = names.name(spin+'x')
        spiny_name = names.name(spin+'y')
        spinz_name = names.name(spin+'z')
        if (spinx_name in missing) and (spiny_name in missing) and (spinz_name in missing):
            mag_name = names.name(spin+'_magnitude')
            pol_name = names.name(spin+'_polar_angle')
            azm_name = names.name(spin+'_azimuthal_angle')

            # check reasonable combos of polar angles
            ### FIXME? do not require an exact match? Only look for variates and not required?

            for logp_names in [
                    (names.logprob_name([mag_name]), names.logprob_name([pol_name]), names.logprob_name([azm_name])),
                    (names.logprob_name([mag_name]), names.logprob_name([pol_name, azm_name])),
                    (names.logprob_name([mag_name, pol_name, azm_name]),),
                ]:
                if all([logp_name in logprob_fields for logp_name in logp_names]):

                    # quickly sanity-check the existing fields
                    for _name in [mag_name, pol_name, azm_name]:
                        if _name in present:
                            if verbose:
                                old = ', '.join([spinx_name, spiny_name, spinz_name])
                                new = ', '.join([mag_name, pol_name, azm_name])
                                print('conversion from %s -> %s conflicts with existing variate %s' % \
                                    (old, new, _name))
                            break

                    else:
                        if verbose:
                            print('computing logprob(%s, %s, %s) from %s, %s, %s' % \
                                (spinx_name, spiny_name, spinz_name, ', '.join(logp_names), mag_name, pol_name))

                        # remove from list of "missing"
                        for _name in [mag_name, pol_name, azm_name]:
                            present.append(_name)

                        for _name in [spinx_name, spiny_name, spinz_name]:
                            present.append(_name)
                            missing.remove(_name)

                        # compute contribution to logprob and recurse
                        # p(x,y,z) = p(m,p,a) |dm*dp*da/dx*dy*dz| = p(m,p,a) / (m**2 * sin(p))
                        ans = be.zeros(len(events), dtype=float)
                        for logp_name in logp_names:
                            ans += events[logp_name]

                        return ans - 2*be.log(events[mag_name]) \
                            - be.log(be.clip(be.sin(events[pol_name]), 0, 1)) \
                            + _variates2logprob_jacobians(
                                missing,
                                present,
                                events,
                                events_fields=events_fields,
                                logprob_fields=logprob_fields,
                                verbose=verbose,
                            )

        ### polar spin components <-- cartesian spin components
        mag_name = names.name(spin+'_magnitude')
        pol_name = names.name(spin+'_polar_angle')
        azm_name = names.name(spin+'_azimuthal_angle')
        if (mag_name in missing) and (pol_name in missing) and (azm_name in missing):
            spinx_name = names.name(spin+'x')
            spiny_name = names.name(spin+'y')
            spinz_name = names.name(spin+'z')

            ### FIXME? do not look for an exact match? only look for variates and not required?

            logp_name = names.logprob_name([spinx_name, spiny_name, spinz_name])
            if logp_name in logprob_fields:

                # quickly sanity-check the existing fields
                for _name in [spinx_name, spiny_name, spinz_name]:
                    if _name in present:
                        if verbose:
                            old = ', '.join([mag_name, pol_name, azm_name])
                            new = ', '.join([spinx_name, spiny_name, spinz_name])
                            print('conversion from %s -> %s conflicts with existing variate %s' % \
                                (old, new, _name))
                        break

                else:
                    if verbose:
                        print('computing logprob(%s, %s, %s) from %s, %s, %s, %s' % \
                            (mag_name, pol_name, azm_name, logp_name, spinx_name, spiny_name, spinz_name))

                    # remove from list of "missing"
                    for _name in [spinx_name, spiny_name, spinz_name]:
                        present.append(_name)

                    for _name in [mag_name, pol_name, azm_name]:
                        present.append(_name)
                        missing.remove(_name)

                    # compute contribution to logprob and recurse
                    # p(m,p,a) = p(x,y,z) |dx*dy*dz/dm*dp*da| = p(x,y,z) * (m**2 * sin(p))
                    mag = events[spinx_name]**2 + events[spiny_name]**2 + events[spinz_name]**2
                    cos = events[spinz_name] / mag**0.5
                    return events[logp_name] + be.log(mag) \
                        + 0.5*be.log(1-cos**2) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

    # spin components jointly

    # cartesian <-- polar

    spin1x_name = names.name('spin1x')
    spin1y_name = names.name('spin1y')
    spin1z_name = names.name('spin1z')

    spin2x_name = names.name('spin2x')
    spin2y_name = names.name('spin2y')
    spin2z_name = names.name('spin2z')

    if (spin1x_name in missing) and (spin1y_name in missing) and (spin1z_name in missing) \
        and (spin2x_name in missing) and (spin2y_name in missing) and (spin2z_name in missing):

        spin1_mag_name = names.name('spin1_magnitude')
        spin1_pol_name = names.name('spin1_polar_angle')
        spin1_azm_name = names.name('spin1_azimuthal_angle')

        spin2_mag_name = names.name('spin2_magnitude')
        spin2_pol_name = names.name('spin2_polar_angle')
        spin2_azm_name = names.name('spin2_azimuthal_angle')

        ### FIXME? do not look for an exact match? only look for variates and not required?

        logp_name = names.logprob_name([spin1_mag_name, spin1_pol_name, spin1_azm_name, \
            spin2_mag_name, spin2_pol_name, spin2_azm_name])

        if logp_name in logprob_fields:

            # quickly sanity-check the existing fields
            for _name in [spin1_mag_name, spin1_pol_name, spin1_azm_name, spin2_mag_name, spin2_pol_name, spin2_azm_name]:
                if _name in present:
                    if verbose:
                        old = ', '.join([spin1x_name, spin1y_name, spin1z_name, spin2x_name, spin2y_name, spin2z_name])
                        new = ', '.join([spin1_mag_name, spin1_pol_name, spin1_azm_name, spin2_mag_name, spin2_pol_name, spin2_azm_name])
                        print('conversion from %s -> %s conflicts with existing variate %s' % \
                            (old, new, _name))
                    break

                else:
                    if verbose:
                        print('computing logprob(%s, %s, %s) from %s, %s, %s, %s' % \
                            (spin1x_name, spin1y_name, spin1z_name, \
                             spin2x_name, spin2y_name, spin2z_name, \
                             logp_name, \
                             spin1_mag_name, spin1_pol_name, spin1_azm_name, \
                             spin2_mag_name, spin2_pol_name, spin2_azm_name, \
                             ))

                    # remove from list of "missing"
                    for _name in [spin1_mag_name, spin1_pol_name, spin1_azm_name, \
                        spin2_mag_name, spin2_pol_name, spin2_azm_name]:
                        present.append(_name)

                    for _name in [spin1x_name, spin1y_name, spin1z_name, \
                        spin2x_name, spin2y_name, spin2z_name]:
                        present.append(_name)
                        missing.remove(_name)

                    # compute contribution to logprob and recurse
                    # do this for both components
                    # p(x,y,z) = p(m,p,a) |dx*dy*dz/dm*dp*da|**-1 = p(x,y,z) / (m**2 * sin(p))
                    return events[logp_name] - 2*be.log(events[spin1_mag_name]) - 2*be.log(events[spin2_mag_name]) \
                        - be.log(be.clip(be.sin(events[spin1_pol_name]), 0, 1)) \
                        - be.log(be.clip(be.sin(events[spin2_pol_name]), 0, 1)) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

    # polar <-- cartesian
    spin1_mag_name = names.name('spin1_magnitude')
    spin1_pol_name = names.name('spin1_polar_angle')
    spin1_azm_name = names.name('spin1_azimuthal_angle')

    spin2_mag_name = names.name('spin2_magnitude')
    spin2_pol_name = names.name('spin2_polar_angle')
    spin2_azm_name = names.name('spin2_azimuthal_angle')

    if (spin1_mag_name in missing) and (spin1_pol_name in missing) and (spin1_azm_name in missing) \
        and (spin2_mag_name in missing) and (spin2_pol_name in missing) and (spin2_azm_name in missing):

        spin1x_name = names.name('spin1x')
        spin1y_name = names.name('spin1y')
        spin1z_name = names.name('spin1z')

        spin2x_name = names.name('spin2x')
        spin2y_name = names.name('spin2y')
        spin2z_name = names.name('spin2z')

        ### FIXME? do not look for an exact match? only look for variates and not required?

        logp_name = names.logprob_name([spin1x_name, spin1y_name, spin1z_name, spin2x_name, spin2y_name, spin2z_name])

        if logp_name in logprob_fields:

            # quickly sanity-check the existing fields
            for _name in [spin1x_name, spin1y_name, spin1z_name, spin2x_name, spin2y_name, spin2z_name]:
                if _name in present:
                    if verbose:
                        old = ', '.join([spin1_mag_name, spin1_pol_name, spin1_azm_name, spin2_mag_name, spin2_pol_name, spin2_azm_name])
                        new = ', '.join([spin1x_name, spin1y_name, spin1z_name, spin2x_name, spin2y_name, spin2z_name])
                        print('conversion from %s -> %s conflicts with existing variate %s' % \
                            (old, new, _name))
                    break

                else:
                    if verbose:
                        print('computing logprob(%s, %s, %s, %s, %s, %s) from %s, %s, %s, %s, %s, %s, %s' % \
                            (spin1_mag_name, spin1_pol_name, spin1_azm_name, \
                             spin2_mag_name, spin2_pol_name, spin2_azm_name, \
                             logp_name, \
                             spin1x_name, spin1y_name, spin1z_name, \
                             spin2x_name, spin2y_name, spin2z_name, \
                             ))

                    # remove from list of "missing"
                    for _name in [spin1x_name, spin1y_name, spin1z_name, \
                        spin2x_name, spin2y_name, spin2z_name]:
                        present.append(_name)

                    for _name in [spin1_mag_name, spin1_pol_name, spin1_azm_name, \
                        spin2_mag_name, spin2_pol_name, spin2_azm_name]:
                        present.append(_name)
                        missing.remove(_name)

                    # compute contribution to logprob and recurse
                    # do this for both components
                    # p(m,p,a) = p(x,y,z) |dx*dy*dz/dm*dp*da| = p(x,y,z) * (m**2 * sin(p))
                    mag1 = events[spin1x_name]**2 + events[spin1y_name]**2 + events[spin1z_name]**2
                    mag2 = events[spin2x_name]**2 + events[spin2y_name]**2 + events[spin2z_name]**2
                    cos1 = events[spin1z_name] / mag1**0.5
                    cos2 = events[spin2z_name] / mag2**0.5

                    return events[logp_name] \
                        + be.log(mag1) \
                        + be.log(mag2) \
                        + 0.5*be.log(1-cos1**2) \
                        + 0.5*be.log(1-cos2**2) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

    #--------------------

    ''' The following is NOT tested and therefore is not supported

    ### polar_angle <--> cos(polar_angle)

    for mass_component in ['', '1', '2']:

        ### polar_angle <-- cos(polar_angle)
        pol_name = names.name('spin'+mass_component+'_polar_angle')
        if pol_name in missing:
            cos_pol_name = names.name('cos_spin'+mass_component+'_polar_angle')

            ### FIXME? do not require an exact match?

            logp_name = names.logprob_name([cos_pol_name])
            if logp_name in logprob_fields:
                # sanity check
                if cos_pol_name in present:
                    if verbose:
                        print('conversion from %s -> %s conflicts with existing variate' % \
                            (pol_name, cos_pol_name))

                else:
                    if verbose:
                        print('computing logprob(%s) from %s, %s' % \
                            (pol_name, logp_name, cos_pol_name))

                    # remove from list of "missing"
                    present.append(cos_pol_name)
                    present.append(pol_name)
                    missing.remove(pol_name)

                    # compute contribution to logprob and recurse
                    # p(pol) = p(cos) *|dcos/dpol| = p(cos) sin(pol)
                    return events[logp_name] + 0.5*be.log(1-be.clip(events[col_pol_name]**2, 0, 1)) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

        ### cos(polar_angle) <-- polar_angle
        cos_pol_name = names.name('cos_spin'+mass_component+'_polar_angle')
        if cos_pol_name in missing:

            ### FIXME? do not require an exact match?

            logp_name = names.logprob_names.name([pol_name])
            if logp_name in logprob_fields:
                # sanity check
                if pol_name in present:
                    if verbose:
                        print('conversion from %s -> %s conflicts with existing variate' % \
                            (cos_pol_name, pol_name))

                else:
                    if verbose:
                        print('computing logprob(%s) from %s, %s' % \
                            (cos_pol_name, logp_name, pol_name))

                    # remove from list of "missing"
                    present.append(pol_name)
                    present.append(cos_pol_name)
                    missing.remove(cos_pol_name)

                    # compute contribution to logprob and recurse
                    # p(cos) = p(pol) *|dpol/dcos| = p(pol) / sin(pol)
                    return events[logp_name] - be.log(be.clip(be.sin(events[pol_name]), 0, 1)) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )
    '''

    #--------------------

    ### effective spins via the procedure from https://dcc.ligo.org/LIGO-T2200294

    chi_eff_name = names.name('chi_eff')
    if chi_eff_name in missing:
        try:
            # look for logprob(polar spin components)
            polar_names = [names.name(a+"_"+b) for a in ['spin1', 'spin2'] for b in ['magnitude', 'polar_angle', 'azimuthal_angle']]

            # draw probability
            logprob_polar = variates2logprob(
                polar_names,
                events,
                events_fields=events_fields,
                required=present,
                logprob_fields=logprob_fields,
                verbose=verbose,
            )

            # compute logprob(polar spin components|isotropic, uniform in mag)
            try:
                logprob_polar_iso = _logprob_polar_iso(events, events_fields)

                # compute logprob(chi_eff|q) or logprob(chi_eff|mass1_source, mass2_source)
                try:
                    return _logprob_chi_eff(events, events_fields, present) - logprob_polar_iso + logprob_polar

                except Exception as e:
                    if verbose:
                        print('cannont compute effective draw probability for chi_eff because we cannot \
compute p(chi_eff|q) or p(chi_eff|m1, m2).', e)

            except Exception as e:
                if verbose:
                    print('cannot compute effective draw proability for chi_eff even though we computed the \
draw probability for polar spin components because we cannot find the fields associated with the polar or \
cartesian spin components.', e)

        except Exception as e:
            if verbose:
                print('cannot compute effective draw probabilty for chi_eff because we cannot find the \
draw probability for the polar spin components.', e)

    #--------------------

    ### redshift --> luminosity distance
    z_name = names.name('redshift')
    if z_name in missing:
        dl_name = names.name('luminosity_distance')

        ## FIXME? do not require an exact match?

        logp_name = names.logprob_name([dl_name])

        if logp_name in logprob_fields:
            # sanity check
            if dl_name in present:
                if verbose:
                    print('conversion from %s -> %s conflicts with existing variate' % \
                        (z_name, dl_name))

            else:

                # check for presence of jacobian (non-trivial to compute on-the-fly)
                jac_name = names.name('dluminosity_distance_dredshift')
                if jac_name in events_fields:
                    if verbose:
                        print('computing logprob(%s) from %s, %s' % \
                            (z_name, logp_name, jac_name))

                    # remove from list of "missing"
                    present.append(dl_name)
                    present.append(z_name)
                    missing.remove(z_name)

                    # compute contribution and recurse
                    # p(z) = p(DL) * |dDL/dz|
                    return events[logp_name] + be.log(events[jac_name]) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

                elif verbose:
                    print('could not find jacobian from %s -> %s (%s) within events: %s' % \
                        (z_name, dl_name, jac_name, ', '.join(events_fields)))

    ### luminosity_distance --> redshift
    dl_name = names.name('luminosity_distance')
    if dl_name in missing:
        logp_name = names.logprob_name([z_name])

        ### FIXME? do not require an exact match?

        if logp_name in logprob_fields:
            # sanity check
            if z_name in present:
                if verbose:
                    print('conversion from %s -> %s conflicts with existing variate' % \
                        (z_name, dl_name))

            else:

                # check for presence of jacobian (non-trivial to compute on-the-fly)
                jac_name = names.name('dluminosity_distance_dredshift')
                if jac_name in events_fields:
                    if verbose:
                        print('computing logprob(%s) from %s, %s' % \
                            (dl_name, logp_name, jac_name))

                    # remove from list of "missing"
                    present.append(z_name)
                    present.append(dl_name)
                    missing.remove(dl_name)

                    # compute contribution and recurse
                    # p(DL) = p(z) / |dDL/dz|
                    return events[logp_name] - be.log(events[jac_name]) \
                        + _variates2logprob_jacobians(
                            missing,
                            present,
                            events,
                            events_fields=events_fields,
                            logprob_fields=logprob_fields,
                            verbose=verbose,
                        )

                elif verbose:
                    print('could not find jacobian from %s -> %s (%s) within events: %s' % \
                        (dl_name, z_name, jac_name, ', '.join(events_fields)))

    #--------------------

    # if we have not returned, then there must be missing variates for which we did not find a transform
    raise ValueError('could not find transforms for (%s) compatible with existing variates (%s) and logprob (%s)' % \
        (', '.join(sorted(missing)), ', '.join(sorted(present)), ', '.join(sorted(logprob_fields))))

#------------------------

def _logprob_polar_iso(events, events_fields):
    """a helper function to compute a known reference distribution defined over 
    spin1, spin2 magnitudes, polar_angles, and azimuthal_angles
    """

    # check whether polar components exist in events_fields
    polar_components = []
    for spin in ['spin1', 'spin2']:
        # do polar components exist?
        mag_name = names.name(spin+'_magnitude')
        pol_name = names.name(spin+'_polar_angle')
        azm_name = names.name(spin+'_azimuthal_angle')
        if (mag_name in events_fields) and (pol_name in events_fields) and (azm_name in events_fields):
            polar_components += [events[azm_name], events[pol_name], events[mag_name]]

        else:
            # do cartesian components exist
            spinx_name = names.name(spin+'x')
            spiny_name = names.name(spin+'y')
            spinz_name = names.name(spin+'z')
            if (spinx_name in events_fields) and (spiny_name in events_fields) and (spinz_name in events_fields):
                polar_components += list(CartesianSpin2PolarSpin.cartesian2polar(
                    events[spinx_name], events[spiny_name], events[spinz_name],
                ))

            else:
                raise RuntimeError('could not find cartesian or polar %s components'%spin)

    # compute logprob(polar|iso,uni)
    return IsotropicPowerLawPolarSpin1Spin2._logprob(*polar_components, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0)

#------------------------

def _logprob_chi_eff(events, events_fields, present):
    """a helper function to compute chi_eff's conditioned marginal distribution
    """
    q_name = names.name('asymmetric_mass_ratio')

    mass1_src_name = names.name('mass1_source')
    mass2_src_name = names.name('mass2_source')

    mass1_det_name = names.name('mass1_detector')
    mass2_det_name = names.name('mass2_detector')

    if (q_name in present) \
        or ((mass1_src_name in present) and (mass2_src_name in present)) \
        or ((mass1_det_name in present) and (mass2_det_name in present)):

        # compute asymmetric mass ratio
        if q_name in events_fields:
            q = events[q_name]

        elif (mass1_src_name in events_fields) and (mass2_src_name in events_fields):
            q = events[mass2_src_name] / events[mass1_src_name]

        elif (mass1_det_name in events_fields) and (mass2_det_name in events_fields):
            q = events[mass2_det_name] / events[mass1_det_name]

        else:
            raise ValueError('could not compute p(chi_eff|q, iso, uni) because I could not compute q \
from events_fields: %s' % (', '.join(events_fields)))

        # compute p(chi_eff|q, iso)
        chi_eff_name = names.name('chi_eff')
        assert chi_eff_name in events_fields, 'cannot compute p(chi_eff|q,iso,uni) because chi_eff not in \
events_fields : %s' % (', '.join(events_fields))

        # use max_spin_magnitude = 1.0 to make sure we always have full coverage of possible spin space
        return IsotropicUniformMagnitudeChiEffGivenAsymmetricMassRatio._logprob(events[chi_eff_name], q, 1.0)

    else:
        raise ValueError('could not compute marginal probability for chi_eff because could not condition on \
mass ratio or (source- or detector-frame) component masses')
