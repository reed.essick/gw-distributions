"""a module that houses various population models useful for generating synthetic GW events and general population inference.
Submodules include
    io : basic I/O utilities
    distributions : implementation of distributions over single-event parameters
    transforms : transformations between different single-event parameters (eg: luminosity distanct -> redshift)
    event : definition of the basic Event class, which stores the parameters of an event drawn from distributions
    generators : infrastructure to automatically draw and process large sets of events
"""
__author__ = "Reed Essick <reed.essick@gmail.com>, Chris Pankow <chris.pankow@ligo.org>"

#-------------------------------------------------

__version__ = '0.0.0'
