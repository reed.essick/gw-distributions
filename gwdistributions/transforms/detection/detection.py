"""transformations relevant for modeling detection (e.g., signal-to-noise ratios)
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.transforms.base import AttributeTransformation
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

from gwdistributions.event import \
    (Event, DEFAULT_FLOW, DEFAULT_FHIGH)

from .waveform import ProjectedStrain
from gwdistributions.distributions.noise import StationaryGaussianComplexFrequencyNoise

#------------------------

### import a few distributions needed here
from scipy.stats import (norm, ncx2)

def normal_survival_function(x, mean=0.0, stdv=1.0):
    """\
the survival function: Prob(X >= x) for a normal variable with mean x0 and unit variance
    """
    return norm.sf(x, loc=mean, scale=stdv)

def noncentral_chi_survival_function(x, noncentrality_parameter=0., num_degrees_of_freedom=1):
    """\
The CDF of the non-central chi distribution with non-centrality parameter x0
This is based on the CDF of the corresponding non-central chi-squared random variable with \
the same number of degrees of freedom non-centrality parameter x0**2
    """
    return ncx2.sf(x**2, df=num_degrees_of_freedom, nc=noncentrality_parameter**2) ### ordering should be the same 

#------------------------

class OptimalSNR(AttributeTransformation):
    """associate the SNRs for this event
    """
    _params = (
        names.name('network'),
        names.name('snr_flow'),
        names.name('snr_fhigh'),
    )

    def _init_values(self,
            network=None,
            snr_flow=DEFAULT_FLOW,
            snr_fhigh=DEFAULT_FHIGH,
        ):
        self._values = [network, snr_flow, snr_fhigh]

    @property
    def network(self):
        return self.params[0]

    #---

    @property
    def _network_snr_name(self):
        return self._snr_name('net')

    @staticmethod
    def _snr_name(name):
        return '%s_%s'%(names.name('snr'), name)

    #---

    @property
    def _required(self):
        ans = []
        if self.network is not None:
            ans += [ProjectedStrain._strain_name(det.name) for det in self.network.detectors]
        return tuple(ans+list(Event._wavefreqs))

    @property
    def _scalar_variates(self):
        variates = []
        if self.network is not None:
            variates += [self._snr_name(detector.name) for detector in self.network]
        variates += [self._network_snr_name, names.name('snr_flow'), names.name('snr_fhigh')]
        return tuple(variates)

    #---

    def transform(self, event):
        network, snr_flow, snr_fhigh = self.params

        ### zero the waveform outside of flow, fhigh
        freqs = event['waveform_freqs']
        keep = (snr_flow <= freqs) & (freqs <= snr_fhigh)
        freqs = freqs[keep]

        ### now iterate and compute SNR for each detector
        snr_net = 0.
        for detector, strain_name in zip(network.detectors, self.required):
            snr = detector.snr(freqs, event[strain_name][keep])
            event[self._snr_name(detector.name)] = snr
            snr_net += snr**2 ### sum in quadrature
        event[self._network_snr_name] = snr_net**0.5

        event['snr_flow'] = snr_flow
        event['snr_fhigh'] = snr_fhigh

#------------------------

class ObservedSNR(AttributeTransformation):
    """\
A transform that computes the filter response of the optimal template in the presence of stationary gaussian noise by directly filtering the waveform against the waveform + noise
    """
    _params = (
        names.name('network'),
        names.name('obs_snr_flow'),
        names.name('obs_snr_fhigh'),
    )

    def _init_values(self,
            network=None,
            obs_snr_flow=DEFAULT_FLOW,
            obs_snr_fhigh=DEFAULT_FHIGH,
        ):
        self._values = [network, obs_snr_flow, obs_snr_fhigh]

    @property
    def network(self):
        return self.params[0]

    #---

    @property
    def _network_obs_snr_name(self):
        return self._obs_snr_name('net')

    @staticmethod
    def _obs_snr_name(name):
        return '%s_%s'%(names.name('observed_snr'), name)

    @property
    def _network_obs_snr_phs_name(self):
        return self._obs_snr_phs_name('net')

    @staticmethod
    def _obs_snr_phs_name(name):
        return '%s_%s'%(names.name('observed_phase_maximized_snr'), name)

    #---

    @property
    def _required(self):
        ans = []
        if self.network is not None:
            for detector in self.network.detectors:
                name = detector.name
                ans += [ProjectedStrain._strain_name(name), StationaryGaussianComplexFrequencyNoise._noise_name(name)]
        return tuple(ans + list(Event._wavefreqs))

    @property
    def _scalar_variates(self):
        variates = [self._network_obs_snr_name, self._network_obs_snr_phs_name]
        if self.network is not None:
            for detector in self.network.detectors:
                name = detector.name
                variates += [self._obs_snr_name(name), self._obs_snr_phs_name(name)]
        return tuple(variates + [names.name('obs_snr_flow'), names.name('obs_snr_fhigh')])

    #---

    def transform(self, event):
        network, snr_flow, snr_fhigh = self.params

        ### zero the waveform outside of flow, fhigh
        freqs = event['freqs']
        keep = (snr_flow <= freqs) & (freqs <= snr_fhigh)
        freqs = freqs[keep]

        ### now iterate and compute SNR for each detector
        obs_snr_net = 0.0 ### set up network SNR cumulative counters
        obs_phs_net = 0.0

        # iterate over detectors
        for dnd, detector in enumerate(network.detectors):
            name = detector.name

            strain = event[ProjectedStrain._strain_name(name)][keep]
            noise = event[StationaryGaussianComplexFrequencyNoise._noise_name(name)][keep]

            # compute filter response
            snr = detector.filter(freqs, strain+noise, strain) # complex filter response

            obs_snr = snr.real ### with fixed phase at coalescence
            obs_snr2 = obs_snr**2
            obs_snr_phs2 = obs_snr2 + snr.imag**2 ### maximized over phase at coalescence

            # record single-IFO values
            event[self._obs_snr_name(name)] = obs_snr
            event[self._obs_snr_phs_name(name)] = obs_snr_phs2**0.5

            # increment network values
            obs_snr_net += obs_snr2
            obs_phs_net += obs_snr_phs2

        # record network analogs
        event[self._network_obs_snr_name] = obs_snr_net**0.5
        event[self._network_obs_snr_phs_name] = obs_phs_net**0.5

        # record bounds on SNR integrals
        event['obs_snr_flow'] = snr_flow
        event['obs_snr_fhigh'] = snr_fhigh

#------------------------

class ObservedSNRGivenOptimalSNR(AttributeTransformation):
    """\
A transform that computes observed SNRs (realizations of random variables) based on the Optimal SNR
This computes the (standard) matched filter SNR and the matched-filter SNR maximized over coalescence phase.
The analogous "network" SNRs are computed as quadrature sums of the single-IFO SNRs.

Users should instantiate this object with a list of detector names (strings).
These should match the detectors in the network used within OptimalSNR
    """
    _params = (
        names.name('detectors'), ### should be a list of strings
    )

    def _init_values(self, detectors=[]):
        if isinstance(detectors, str): ### a single detector
            detectors = [detectors]
        self._values = [detectors]

    @property
    def detectors(self):
        return self._values[0]

    @property
    def num_detectors(self):
        return len(self.detectors)

    @property
    def _required(self):
        return tuple(OptimalSNR._snr_name(name) for name in self.detectors) ### relies on OptimalSNR

    @property
    def _network_obs_snr_name(self):
        return self._obs_snr_name('net')

    @staticmethod
    def _obs_snr_name(name):
        return '%s_%s'%(names.name('observed_snr'), name)

    @property
    def _network_obs_snr_phs_name(self):
        return self._obs_snr_phs_name('net')

    @staticmethod
    def _obs_snr_phs_name(name):
        return '%s_%s'%(names.name('observed_phase_maximized_snr'), name)

    @property
    def _scalar_variates(self):
        variates = [self._network_obs_snr_name, self._network_obs_snr_phs_name]
        for name in self.detectors:
            variates += [self._obs_snr_name(name), self._obs_snr_phs_name(name)]
        return tuple(variates)

    def transform(self, event):
        obs_snr_net = 0.0 ### set up network SNR cumulative counters
        obs_phs_net = 0.0

        # iterate over detectors
        required = self.required
        detectors = self.detectors
        for opt_key, name in zip(required, detectors):
            real, imag = be.normal(size=2) ### zero-mean, unit variance noise realizations
            obs_snr = event[opt_key] + real ### standard matched-filter
            obs_snr2 = obs_snr**2
            obs_phs2 = (obs_snr2 + imag**2) ### (square of) matched-filter max'd over coalescence phase

            # record single-IFO values
            event[self._obs_snr_name(name)] = obs_snr
            event[self._obs_snr_phs_name(name)] = obs_phs2**0.5

            # increment network values
            obs_snr_net += obs_snr2
            obs_phs_net += obs_phs2

        # record network analogs
        event[self._network_obs_snr_name] = obs_snr_net**0.5
        event[self._network_obs_snr_phs_name] = obs_phs_net**0.5

#------------------------

class DetectionProbabilityGivenOptimalSNR(AttributeTransformation):
    """\
A transform that computes the probability of detecting a signal based on a threshold on the observed SNR.
This is done by computing the survival function above some threshold on the observed SNR based on the expected
distribution of the observed SNR given the optimal SNR under stationary Gaussian noise.
Computes single-IFO survival functions using "threshold_observed_snr" for both the matched-filter SNR and the matched-filter SNR maximized over coalescence phase.
Computes the survival function for the "network" analogs using "threshold_observed_snr_net".

Users should instantiate this object with a list of detector names (strings).
These should match the detectors in the network used within OptimalSNR.
Users should also specify the two thresholds as parameters.
    """
    _params = (
        names.name('detectors'), # should be a list of strings
        names.name('threshold_observed_snr')+"_net",
        names.name('threshold_observed_snr'),
    )

    def _init_values(self, detectors=[], threshold_snr_net=10.0, threshold_snr=10.0):
        self._values = [detectors, threshold_snr, threshold_snr_net]

    @property
    def detectors(self):
        return self._values[0]

    @property
    def num_detectors(self):
        return len(self.detectors)

    @property
    def threshold_snr(self):
        return self._values[1]

    @property
    def threshold_snr_net(self):
        return self._values[2]

    @property
    def _required(self):
        return tuple(OptimalSNR._snr_name(name) for name in self.detectors) ### relies on OptimalSNR

    @property
    def _scalar_variates(self):
        variates = [self._network_pdet_snr_name, self._network_pdet_snr_phs_name]
        for name in self.detectors:
            variates += [self._pdet_snr_name(name), self._pdet_snr_phs_name(name)]
        return tuple(variates)

    @property
    def _network_pdet_snr_name(self):
        return self._pdet_snr_name('net')

    @staticmethod
    def _pdet_snr_name(name):
        return '%s_%s'%(names.name('pdet'), ObservedSNRGivenOptimalSNR._obs_snr_name(name))

    @property
    def _network_pdet_snr_phs_name(self):
        return self._pdet_snr_phs_name('net')

    @staticmethod
    def _pdet_snr_phs_name(name):
        return '%s_%s'%(names.name('pdet'), ObservedSNRGivenOptimalSNR._obs_snr_phs_name(name))

    def transform(self, event):
        single_ifo_threshold = self.threshold_snr
        network_threshold = self.threshold_snr_net

        # compute and store survival functions of single-IFO SNRs
        opt_snr_net = 0.0
        for name, old_key in zip(self.detectors, self.required):
            opt_snr = event[old_key]

            # increment the network counter
            opt_snr_net += opt_snr**2

            # compute survival function of matched-filter SNR
            # this variable is has mean = opt_snr and unit variance
            event[self._pdet_snr_name(name)] = normal_survival_function(
                single_ifo_threshold,
                mean=opt_snr,
                stdv=1.,
            )

            # compute survival function of phase-maximized matched-filter SNR
            # this variable is non-central chi-distributed with 2 degrees of freedom
            # and non-centrality parameter = opt_snr
            event[self._pdet_snr_phs_name(name)] = noncentral_chi_survival_function(
                single_ifo_threshold,
                num_degrees_of_freedom=2,
                noncentrality_parameter=opt_snr,
            )

        # now compute survival function for network SNRs
        opt_snr_net = opt_snr_net**0.5
        num_detectors = self.num_detectors

        # compute the "matched-filter network SNR"
        # this is a non-central chi-distributed variable with "num_detectors" degrees of freedom
        # and non-centrality parameter = opt_snr_net
        event[self._network_pdet_snr_name] = noncentral_chi_survival_function(
            network_threshold,
            num_degrees_of_freedom=num_detectors,
            noncentrality_parameter=opt_snr_net,
        )

        # compute the "phase-maximized matched-filter network SNR"
        # this is a non-central chi-distributed variable with "2*num_detectors" degrees of freedom
        # and non-centrality parameter = opt_snr_net
        event[self._network_pdet_snr_phs_name] = noncentral_chi_survival_function(
            network_threshold,
            num_degrees_of_freedom=2*num_detectors,
            noncentrality_parameter=opt_snr_net,
        )
