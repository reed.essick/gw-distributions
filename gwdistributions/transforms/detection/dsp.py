"""transformations relevant for modeling detection (e.g., signal-to-noise ratios)
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import warnings

from gwdistributions.transforms.base import AttributeTransformation
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

#-------------------------------------------------

DEFAULT_HALF_TUKEY_ALPHA = 0.25

DEFAULT_BUTTERWORTH_KNEE = None
DEFAULT_BUTTERWORTH_ORDER = 0.0

DEFAULT_HALF_TUKEY_KNEE = None

DEFAULT_RAMP_DURATION = None
DEFAULT_RAMP_HALF_TUKEY_ALPHA = None

DEFAULT_TREF_CONVENTION = 'lalsimulation'
KNOWN_TREF_CONVENTIONS = ['peak_hp_amplitude', 'lalsimulation']

#-------------------------------------------------

def butterworth_lowpass(N, knee, order):
    """generate a butterworth low-pass filter based on a knee and an order
    filter = (1 + (x/knee)**(2*order))**-0.5
    """
    x = be.linspace(0, 1, N)
    return (1 + (x/knee)**(2*order))**-0.5

def butterworth_highpass(N, knee, order):
    """generate a butterworth high-pass filter based on a knee and an order
    """
    return 1.0 - butterworth_lowpass(N, knee, order)

#------------------------

def half_tukey(N, alpha=DEFAULT_HALF_TUKEY_ALPHA):
    """genrate a tukey window over only the first half of the data
    """
    # Special cases
    if alpha <= 0:
        return be.ones(N) #rectangular window
    elif alpha >= 1:
        raise ValueError('alpha >= 1 not allowed in half_tukey window')

    # Normal case
    x = be.linspace(0, 1, N)
    w = be.ones(x.shape)

    # first condition 0 <= x < alpha/2
    first_condition = x<alpha/2
    w[first_condition] = 0.5 * (1 + be.cos(2*be.pi/alpha * (x[first_condition] - alpha/2) ))

    return w

#-------------------------------------------------

class FreqArray2TimeArray(AttributeTransformation):
    """transform a frequency array into a time array with additional signal-conditioning properties
    """
    _always_required = (
        names.name('geocenter_time'),
    )

    _params = (
        'butterworth_highpass_knee',
        'butterworth_highpass_order',
        'half_tukey_highpass_knee',
        'butterworth_lowpass_knee',
        'butterworth_lowpass_order',
        'half_tukey_lowpass_knee',
        'ramp_up_duration',
        'ramp_up_half_tukey_alpha',
        'ramp_down_duration',
        'ramp_down_half_tukey_alpha',
        'tref_convention',
        'times_name',
        'freqs_name',
        'freq_series',
    )

    def _init_values(self,
            butterworth_highpass_knee=DEFAULT_BUTTERWORTH_KNEE,
            butterworth_highpass_order=DEFAULT_BUTTERWORTH_ORDER,
            half_tukey_highpass_knee=DEFAULT_HALF_TUKEY_KNEE,
            butterworth_lowpass_knee=DEFAULT_BUTTERWORTH_KNEE,
            butterworth_lowpass_order=DEFAULT_BUTTERWORTH_ORDER,
            half_tukey_lowpass_knee=DEFAULT_HALF_TUKEY_KNEE,
            ramp_up_duration=DEFAULT_RAMP_DURATION,
            ramp_up_half_tukey_alpha=DEFAULT_RAMP_HALF_TUKEY_ALPHA,
            ramp_down_duration=DEFAULT_RAMP_DURATION,
            ramp_down_half_tukey_alpha=DEFAULT_RAMP_HALF_TUKEY_ALPHA,
            tref_convention=DEFAULT_TREF_CONVENTION,
            times_name=names.name('waveform_times'),
            freqs_name=names.name('waveform_freqs'),
            freq_series=[],
        ):
        assert tref_convention in KNOWN_TREF_CONVENTIONS, 'tref_convention=%s not understood; must be one of: %s' % \
            (tref_convention, ', '.join(KNOWN_TREF_CONVENTIONS))

        self._values = [
            butterworth_highpass_knee,
            butterworth_highpass_order,
            half_tukey_highpass_knee,
            butterworth_lowpass_knee,
            butterworth_lowpass_order,
            half_tukey_lowpass_knee,
            ramp_up_duration,
            ramp_up_half_tukey_alpha,
            ramp_down_duration,
            ramp_down_half_tukey_alpha,
            tref_convention,
            times_name,
            freqs_name,
            freq_series,
        ]

    @property
    def butterworth_highpass_knee(self):
        return self._values[0]

    @property
    def butterworth_highpass_order(self):
        return self._values[1]

    @property
    def half_tukey_highpass_knee(self):
        return self._values[2]

    @property
    def butterworth_lowpass_knee(self):
        return self._values[3]

    @property
    def butterworth_lowpass_order(self):
        return self._values[4]

    @property
    def half_tukey_lowpass_knee(self):
        return self._values[5]

    @property
    def ramp_up_duration(self):
        return self._values[6]

    @property
    def ramp_up_half_tukey_alpha(self):
        return self._values[7]

    @property
    def ramp_down_duration(self):
        return self._values[8]

    @property
    def ramp_down_half_tukey_alpha(self):
        return self._values[9]

    @property
    def tref_convention(self):
        return self._values[10]

    @property
    def times_name(self):
        return self._values[-3]

    @property
    def freqs_name(self):
        return self._values[-2]

    @property
    def freq_series(self):
        return self._values[-1]

    #---

    @property
    def _required(self):
        ans = self._always_required + (self.freqs_name,) + tuple(self.freq_series)
        if self.tref_convention == 'peak_hp_amplitude':
            ans = ans + (names.name('hp'),)

        elif self.tref_convention == 'lalsimulation':
            ans = ans + (names.name('waveform_tref'),)

        return ans

    @property
    def _vector_variates(self):
        return (self.times_name,) + tuple(self._time_series_name(name) for name in self.freq_series)

    @staticmethod
    def _time_series_name(fd_name):
        return 'time_domain_'+fd_name

    #---

    def _freq_filters(self, freqs, fNyquist, verbose=False):

        N = len(freqs)
        ffilter = be.ones(N, dtype=complex) # start with a trivial filter

        if self.butterworth_highpass_knee is not None:
            if verbose:
                print('''\
        generating frequency-domain Butterworth high-pass filter
             knee = %.1f Hz
            order = %.1f''' % (self.butterworth_highpass_knee, self.butterworth_highpass_order))

            ffilter *= butterworth_highpass(
                N,
                self.butterworth_highpass_knee/fNyquist,
                self.butterworth_highpass_order,
            )

        if self.half_tukey_highpass_knee is not None:
            highpass_alpha = 2 * self.half_tukey_highpass_knee / fNyquist
            if verbose:
                print('''\
        generating frequency-domain half-tukey high-pass filter
             knee = %.1f Hz --> alpha = %.3f''' % (self.half_tukey_highpass_knee, highpass_alpha))

            ffilter *= half_tukey(N, alpha=highpass_alpha)

        if self.butterworth_lowpass_knee is not None:
            if verbose:
                print('''\
        generating frequency-domain Butterworth low-pass filter
             knee = %.1f Hz
            order = %.1f''' % (self.butterworth_lowpass_knee, self.butterworth_lowpass_order))

            ffilter *= butterworth_lowpass(
                N,
                self.butterworth_lowpass_knee/fNyquist,
                self.butterworth_lowpass_order,
            )

        if self.half_tukey_lowpass_knee is not None:
            lowpass_alpha = 2 * (fNyquist - self.half_tukey_lowpass_knee) / fNyquist
            if verbose:
                print('''\
        generating frequency-domain half-tukey high-pass filter
             knee = %.1f Hz --> alpha = %.3f''' % (self.half_tukey_lowpass_knee, lowpass_alpha))

            ffilter *= half_tukey(N, alpha=lowpass_alpha)[::-1] ### reverse order to roll-off high frequencies

        # return
        return ffilter

    #---

    def _time_filters(self, times, verbose=False):

        delta_t = times[1]-times[0] # NOTE: assumes more than a single sample, which is probably safe
        N = len(times)
        dur = N*delta_t

        window = be.ones(N, dtype=float) # begin with a trivial filter

        # generate tukey window to smooth out waveform at very early times

        ### beginning of the waveform
        if self.ramp_up_duration or self.ramp_up_half_tukey_alpha:
            if self.ramp_up_duration is not None:
                alpha = 2*self.ramp_up_duration/dur

            elif self.ramp_up_half_tukey_alpha is not None:
                alpha = self.ramp_up_half_tukey_alpha

            if verbose:
                print('''\
            generating time-domain ramp-up half-tukey window with alpha = %.3f (%.3f sec)''' % \
                (alpha, 0.5*dur*alpha))

            window *= half_tukey(N, alpha=alpha)

        ### end of the waveform
        if self.ramp_down_duration or self.ramp_down_half_tukey_alpha:
            if self.ramp_down_duration is not None:

                print(self.ramp_down_duration, dur)

                alpha = 2 * self.ramp_down_duration / dur

            elif self.ramp_down_half_tukey_alpha is not None:
                alpha = self.ramp_down_half_tukey_alpha

            if verbose:
                print('''\
            generating time-domain ramp-down half-tukey windows with alpha = %.3f (%.3f sec)''' % \
                (alpha, 0.5*dur*alpha))

            window *= half_tukey(N, alpha=alpha)[::-1] # reverse order

        # return
        return window

    #---

    @staticmethod
    def _peak_time(strain_fd, times, freq_filters):
        strain_td = FreqArray2TimeArray.ifft(strain_fd*freq_filters) 
        return times[be.argmax(be.abs(strain))] # should be accurate to within delta_t

    @staticmethod
    def _phase(times, freqs, t0, gps, delta_t):

        ### compute the phase-lag required to put tref at the correct gps time at geocenter
        # we shift the times by an integer number of delta_t (so they still line up with the conceptual global time array)
        gps_dt = int(round(gps/delta_t, 0)) * delta_t
        t0_dt = int(round(t0/delta_t, 0)) * delta_t

        times += gps_dt - t0_dt # shift the reference array by an integer number of delta_t
        tref = gps_dt + t0-t0_dt # tref occurs at "gps_dt + t0%dt = gps_dt + t0-t0_dt"

        # compute the remaining phase to account for offset smaller than delta_t
        phase = be.exp(2j*be.pi*freqs*(gps-tref)) # defined so that t=tref -> t=gps

        return times, phase

    #---

    @staticmethod
    def ifft(data, delta_t=1):
        return be.fft.irfft(data) / delta_t

    #---

    def transform(self, event, verbose=False):

        ### set up the filters that will be applied in the frequency domain
        freqs = event[self.freqs_name]

        assert be.min(freqs) == 0, 'low frequencies appear to be truncated in freqs=%s, which will corrupt ifft' % self.freqs_name

        fNyquist = be.max(freqs)
        deltaf = freqs[1] - freqs[0] # NOTE: assumes more than one sample, which is probably safe

        deltat = 0.5/fNyquist
        times = be.arange(0, 1./deltaf, deltat)

        # delegate to compute filters and phasing

        ffilter = self._freq_filters(freqs, fNyquist, verbose=verbose)   # extra filters to apply in the frequency domain
        tfilter = self._time_filters(times, verbose=verbose)             # smoothing filters for the time domain

        # compute phase shift to align signal at the desired time

        ### figure out the reference time
        if self.tref_convention == 'peak_hp_amplitude':
            t0 = self._peak_time(
                event[names.name('hp')],
                times,
                ffilter,
            )

        elif self.tref_convention == 'lalsimulation':
            t0 = event[names.name('waveform_tref')]

        else:
            raise RuntimeError('tref_convention=%s not understood!' % self.tref_convention)

        if verbose:
            print('        aligning waveform at : waveform_tref=%.9f (convention=%s)' % \
                (t0, self.tref_convention))

        ### compute the phase needed to set the reference time to match gps
        gps = event[names.name('geocenter_time')]
        times, phase = self._phase(times, freqs, t0, gps, deltat)

        ffilter *= phase # include phase shift within freq filters

        # iterate over freq arrays, computing time-domain array for each
        for fd_name in self.freq_series:
            event[self._time_series_name(fd_name)] = self.ifft(event[fd_name]*ffilter, delta_t=deltat) * tfilter

        # also record times
        event[names.name('waveform_times')] = times

#------------------------

class SumArrays(AttributeTransformation):
    """compute the sum of vectors (e.g., signal + noise = data)
    combinations should be passed as kwargs with
        key = output_vector_name
        val = [input_vector_name1, input_vector_name2, ...]
    """
    _params = ()

    def _init_values(self, **mapping):
        self._values = [mapping]

    @property
    def mapping(self):
        return self._values[0]

    @property
    def _required(self):
        ans = []
        for val in self.mapping.values(): # get the names of the vectors that will be included in any sum
            ans += val
        return tuple(sorted(set(ans)))

    @property
    def _vector_variates(self):
        return tuple(sorted(self.mapping.keys()))

    def transform(self, event, check_arrays=False):
        for key, val in self.mapping.items():
            vecs = [event[_] for _ in val]

            if check_arrays: # some quick sanity checks
                assert len(vecs) > 1, 'must have more than one series to sum'

                shape = be.shape(vecs[0])
                dtype = vecs[0].dtype

                for vec in vecs[1:]:
                    assert vec.dtype == dtype, 'dtype mismatch! %s vs %s' % (vec.dtype, dtype)
                    _shape = be.shape(vec)
                    assert (len(_shape) == len(shape)) and be.all(_shape == shape), \
                        'shape mismatch between arrays! %s vs %s' % (_shape, shape)

            # sum vectors
            event[key] = be.sum(vecs, axis=0) ### assumes vectors will be of the same length
