"""transformations relevant for modeling detection (e.g., signal-to-noise ratios)
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.transforms.base import AttributeTransformation
from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

from gwdistributions.event import \
    (Event, DEFAULT_FLOW, DEFAULT_FHIGH, DEFAULT_DELTAF, DEFAULT_APPROXIMANT, DEFAULT_TAPER, DEFAULT_MBAND_THRESHOLD)

#------------------------

DEFAULT_MASS_BOUNDARY = 3.0
DEFAULT_BNS_APPROXIMANT = 'IMRPhenomPv2_NRTidalv2'
DEFAULT_BHNS_APPROXIMANT = 'IMRPhenomPv2_NRTidalv2'
DEFAULT_BBH_APPROXIMANT = DEFAULT_APPROXIMANT

#-------------------------------------------------

class Waveform(AttributeTransformation):
    """compute the waveform given the system parameters
    """
    _required = Event._required4waveform
    _params = ( ### only read in once because params are supposed to be static
        names.name('waveform_flow'),
        names.name('waveform_fhigh'),
        names.name('waveform_delta_f'),
        names.name('waveform_fref'),
        names.name('approximant'),
        names.name('mass_boundary'),
        names.name('bns_approximant'),
        names.name('bhns_approximant'),
        names.name('bbh_approximant'),
        names.name('waveform_taper'),
        names.name('truncate_low_frequencies'),
        names.name('truncate_high_frequencies'),
        names.name('phenomxhm_mband_threshold'),
        names.name('phenomxphm_mband_threshold'),
    )

    def _init_values(self,
            waveform_flow=None,
            waveform_fhigh=4096,
            waveform_delta_f=DEFAULT_DELTAF,
            waveform_fref=None,
            approximant=None,
            mass_boundary=DEFAULT_MASS_BOUNDARY,
            bns_approximant=DEFAULT_BNS_APPROXIMANT,
            bhns_approximant=DEFAULT_BHNS_APPROXIMANT,
            bbh_approximant=DEFAULT_BBH_APPROXIMANT,
            taper=DEFAULT_TAPER,
            truncate_low_frequencies=True,
            truncate_high_frequencies=True,
            phenomxphm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
            phenomxhm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
        ):
        self._values = [waveform_flow, waveform_fhigh, waveform_delta_f, waveform_fref, approximant, \
            mass_boundary, bns_approximant, bhns_approximant, bbh_approximant, taper, \
            truncate_low_frequencies, truncate_high_frequencies, phenomxhm_mband_threshold, phenomxphm_mband_threshold]

   #---

    @property
    def network(self):
        return self._values[0]

    @property
    def taper(self):
        return self._values[-5] ### NOTE: this must match the order in self._values!

    @property
    def phenomxhm_mband_threshold(self):
        return self._values[-2] ### NOTE: this must match the order in self._values!

    @property
    def phenomxphm_mband_threshold(self):
        return self._values[-1] ### NOTE: this must match the order in self._values!

    #---

    @property
    def _vector_variates(self):
        return Event._wavepolarizations + Event._wavefreqs

    @property
    def _scalar_variates(self):
        variates = [ # based on what is added within call to event._freq_domain_waveform
            names.name('waveform_flow'),
            names.name('waveform_fhigh'),
            names.name('waveform_fref'),
            names.name('waveform_tref'),
            names.name('waveform_delta_f'),
            names.name('approximant'),
        ]
        if self.taper is not None:
            variates.append(names.name('waveform_taper'))

        if self.phenomxhm_mband_threshold is not None:
            variates.append(names.name('phenomxhm_mband_threshold'))

        if self.phenomxphm_mband_threshold is not None:
            variates.append(names.name('phenomxphm_mband_threshold'))

        return tuple(variates)

    #---

    @staticmethod
    def _total_mass2flow(total_mass):
        return 1899. / total_mass ### approximate limit for some EOB waveform generation

    @staticmethod
    def _component_masses2approximant(
            mass1,
            mass2,
            mass_boundary=DEFAULT_MASS_BOUNDARY,
            bns_approximant=DEFAULT_BNS_APPROXIMANT,
            bhns_approximant=DEFAULT_BHNS_APPROXIMANT,
            bbh_approximant=DEFAULT_BBH_APPROXIMANT,
        ):
        if mass1 <= mass_boundary: ### "BNS range"
            return bns_approximant
        else:
            if mass2 <= mass_boundary: ### NSBH range
                return bhns_approximant
            else: ### BBH range
                return bbh_approximant

    #---

    def transform(self, event):
        wave_flow, wave_fhigh, deltaf, fref, approximant, mass_boundary, bns, bhns, bbh, taper, trunc_low, trunc_high, xhm, xphm = self.params

        if wave_flow is None: ### automatically choose wave_flow to match waveform and SNR requirements
            wave_flow = self._total_mass2flow(event['mass1_detector']+event['mass2_detector'])

        if fref is None: ### automatically set this to wave_flow so that all waveform generation requirements are met
            fref = wave_flow

        if approximant is None:
            if hasattr(event, names.name('mass1_source')) and hasattr(event, names.name('mass2_source')):
                ### if available, pick this based on the source-frame masses
                mass1, mass2 = event['mass1_source'], event['mass2_source']
            else:
                mass1, mass2 = event['mass1_detector'], event['mass2_detector']
            approximant = self._component_masses2approximant(
                mass1,
                mass2,
                mass_boundary=mass_boundary,
                bns_approximant=bns,
                bhns_approximant=bhns,
                bbh_approximant=bbh,
            )

        ### compute the waveform
        # NOTE! Event objects automatically cache the waveform vectors and scalars, so we do not need to do that here

        event.waveform(
            flow=wave_flow,
            fhigh=wave_fhigh,
            deltaf=deltaf,
            fref=fref,
            approximant=approximant,
            taper=taper,
            truncate_low_frequencies=trunc_low,
            truncate_high_frequencies=trunc_high,
            phenomxhm_mband_threshold=xhm,
            phenomxphm_mband_threshold=xphm,
        )

#------------------------

class ProjectedStrain(AttributeTransformation):
    """a transform that takes the strain in the wave-frame and projects it into the detectors
    """
    _required = Event._wavepolarizations + Event._wavefreqs + Event._extrinsic4waveform
    _params = (
        names.name('network'),
    )

    def _init_values(self,
            network=None,
        ):
        self._values = [network]

    @property
    def network(self):
        return self.params[0]

    @property
    def _vector_variates(self):
        if self.network is None:
            return tuple()
        else:
            return tuple(self._strain_name(detector.name) for detector in self.network.detectors)

    @staticmethod
    def _strain_name(name):
        return '%s_%s'%(names.name('strain'), name)

    def transform(self, event):

        # grab the waveform from the event
        hp = event['hp']
        hx = event['hx']
        hvx = event['hvx']
        hvy = event['hvy']
        hb = event['hb']
        hl = event['hl']
        freqs = event['waveform_freqs']

        ### project into each detector and compute SNR
        ra = event['right_ascension']
        dec = event['declination']
        psi = event['polarization']
        time = event['geocenter_time']

        snr_net = 0.
        for detector in self.network:
            ### FIXME: change this so that waveform is rolled-off with the same high-pass filter as the actual data?
            event[self._strain_name(detector.name)] = \
                detector.project(freqs, time, ra, dec, psi, hp=hp, hx=hx, hvx=hvx, hvy=hvy, hb=hb, hl=hl)
