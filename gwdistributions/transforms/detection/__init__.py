"""a module to hold transformations related to detection
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

from .waveform import *
from .dsp import *
from .detection import *
