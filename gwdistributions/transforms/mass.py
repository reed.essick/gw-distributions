"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import AttributeTransformation

#-------------------------------------------------

### component mass to mass ratios

class Mass1AsymmetricMassRatio2Mass2SymmetricMassRatio(AttributeTransformation):
    """compute mass2 and eta=mass1*mass2/(mass1+mass2)**2 from mass1 and q=mass2/mass1
Users can specify which mass they want to use (source-frame or detector frame) by passing either
    source_or_detector = 'source'
or
    source_or_detector = 'detector'
    """
    _params = ('source_or_detector',) ### should be a string

    def _init_values(self, source_or_detector='source'):
        self._values = [source_or_detector]

    @property
    def _scalar_variates(self):
        return ('mass2_'+self.params[0], 'symmetric_mass_ratio')

    @property
    def _required(self):
        return ('mass1_'+self.params[0], 'asymmetric_mass_ratio')

    def transform(self, event):
        q = event['asymmetric_mass_ratio']
        event[self.scalar_variates[0]] = event[self.required[0]] * q
        event['symmetric_mass_ratio'] = q / (1.0 + q)**2

class ComponentMass2MassRatios(AttributeTransformation):
    """compute mass ratio based on component masses
Users can specify which mass they want to use (source-frame or detector frame) by passing either
    source_or_detector = 'source'
or
    source_or_detector = 'detector'
    """
    _params = ('source_or_detector',)
    _scalar_variates = ('asymmetric_mass_ratio', 'symmetric_mass_ratio')

    def _init_values(self, source_or_detector='source'):
        self._values = [source_or_detector]

    @property
    def _required(self):
        sod = self.params[0]
        return ('mass1_'+sod, 'mass2_'+sod)

    def transform(self, event):
        m1 = event[self.required[0]]
        m2 = event[self.required[1]]
        event['asymmetric_mass_ratio'] = m2 / m1
        event['symmetric_mass_ratio'] = m1 * m2 / (m1 + m2)**2

#------------------------

### component mass to chirp mass

class ComponentMass2ChirpMassTotalMass(AttributeTransformation):
    """compute chirp mass and total mass from component masses
Users can specify which mass they want to use (source-frame or detector frame) by passing either
    source_or_detector = 'source'
or
    source_or_detector = 'detector'
    """
    _params = ('source_or_detector',)

    def _init_values(self, source_or_detector='source'):
        self._values = [source_or_detector]

    @property
    def _required(self):
        source_or_detector = self.params[0]
        return ('mass1_'+source_or_detector, 'mass2_'+source_or_detector)

    @property
    def _scalar_variates(self):
        source_or_detector = self.params[0]
        return ('chirp_mass_'+source_or_detector, 'total_mass_'+source_or_detector)

    def transform(self, event):
        m1 = event[self.required[0]]
        m2 = event[self.required[1]]
        mtot = m1 + m2
        event[self.scalar_variates[0]] = (m1*m2)**0.6 / mtot**0.2
        event[self.scalar_variates[1]] = mtot

class ChirpMassTotalMass2ComponentMass(AttributeTransformation):
    """compute component masses from chirp mass and total mass
Users can specify which mass they want to use (source-frame or detector frame) by passing either
    source_or_detector = 'source'
or
    source_or_detector = 'detector'
    """
    _params = ('source_or_detector',)

    def _init_values(self, source_or_detector='source'):
        self._values = [source_or_detector]

    @property
    def _required(self):
        source_or_detector = self.params[0]
        return ('chirp_mass_'+source_or_detector, 'total_mass_'+source_or_detector)

    @property
    def _scalar_variates(self):
        source_or_detector = self.params[0]
        return ('mass1_'+source_or_detector, 'mass2_'+source_or_detector)

    def transform(self, event):
        mc = event[self.required[0]]
        mt = event[self.required[1]]
        b = (mt/mc)**(5./3) - 2
        q = 0.5 * (b - (b**2 - 4)**0.5) ### asymmetric mass ratio
        m1 = mc * (1+q)**0.2 / q**0.6
        event[self.scalar_variates[0]] = m1
        event[self.scalar_variates[1]] = q * m1

#------------------------

### source frame to detector frame

class SourceMass2DetectorMass(AttributeTransformation):
    """compute the detector frame mass from the source frame mass.
    which masses are transformed is set dynamically
    """

    def _init_values(self, mass_names=None):
        if mass_names is not None:
            self._required = tuple(mass_names) + ('redshift',)
            self._scalar_variates = tuple(name.replace('source', 'detector') for name in mass_names)
        AttributeTransformation._init_values(self)

    def transform(self, event):
        zp1 = 1 + event['redshift']
        for src, det in zip(self.required[:-1], self.scalar_variates):
            event[det] = event[src] * zp1

class DetectorMass2SourceMass(AttributeTransformation):

    def _init_values(self, mass_names=None):
        if mass_names is not None:
            self._required = tuple(mass_names) + ('redshift',)
            self._scalar_variates = tuple(name.replace('detector', 'source') for name in mass_names)
        AttributeTransformation._init_values(self)

    def transform(self, event):
        zp1 = 1 + event['redshift']
        for det, src in zip(self.required[:-1], self.scalar_variates):
            event[src] = event[det] / zp1
