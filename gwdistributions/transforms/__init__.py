"""a module that houses transformations for single-event GW parameters
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import *

from .location import *
from .mass import *
from .spin import *

from .detection import *
