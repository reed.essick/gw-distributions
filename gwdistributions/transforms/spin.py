"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import AttributeTransformation
from gwdistributions.backends import numerics as be

#-------------------------------------------------

class CartesianSpin2PolarSpin(AttributeTransformation):
    """\
eats a set of cartesian spin coordinates and compute the corresponding polar coordinates.
"spin_names" should be a list of which spin vectors are to be converted (e.g., "spin1" instead of "spin1x")
    """

    def _init_values(self, spin_names=None):
        if spin_names is not None:
            required = []
            scalars = []
            for name in spin_names:
                cart, pole = self._name2components(name)
                required += cart
                scalars += pole
            self._required = tuple(required)
            self._scalar_variates = tuple(scalars)
        self._num_names = len(self._required) // 3
        AttributeTransformation._init_values(self)

    @staticmethod
    def _name2components(name):
        return [name+'x', name+'y', name+'z'], [name+'_azimuthal_angle', name+'_polar_angle', name+'_magnitude']

    @staticmethod
    def cartesian2polar(x, y, z):
        """return azm, pol, mag
        """
        mag = (x**2 + y**2 + z**2)**0.5
        pol = be.arccos(z/mag)
        azm = be.arctan2(y, x) % (2*be.pi)
        return azm, pol, mag

    def transform(self, event):
        required = self.required
        scalars = self.scalar_variates
        for ind in range(self._num_names):
            sa, sp, sm = scalars[3*ind:3*(ind+1)] # extract polar names
            event[sa], event[sp], event[sm] = self.cartesian2polar(*[event[_] for _ in required[3*ind:3*(ind+1)]])

class PolarSpin2CartesianSpin(AttributeTransformation):
    """\
eats a set of polar spin coordinates and computes the corresponding cartesian coordinates
"spin_names" should be a list of which spin vectors are to be converted (e.g., "spin1" instead of "spin1x")
    """

    def _init_values(self, spin_names=None):
        if spin_names is not None:
            required = []
            scalars = []
            for name in spin_names:
                cart, pole = CartesianSpin2PolarSpin._name2components(name)
                required += pole
                scalars += cart
            self._required = tuple(required)
            self._scalar_variates = tuple(scalars)
        self._num_names = len(self._required) // 3
        AttributeTransformation._init_values(self)

    @staticmethod
    def polar2cartesian(azm, pol, mag):
        """return x, y, z
        """
        sinpol = be.sin(pol)
        return mag*sinpol*be.cos(azm), mag*sinpol*be.sin(azm), mag*be.cos(pol)

    def transform(self, event):
        required = self.required
        scalars = self.scalar_variates
        for ind in range(self._num_names):
            sx, sy, sz = scalars[3*ind:3*(ind+1)] # extract cartesian names
            event[sx], event[sy], event[sz] = self.polar2cartesian(*[event[_] for _ in required[3*ind:3*(ind+1)]])

#------------------------

class PolarSpins2Chi(AttributeTransformation):
    """\
Computes chi_eff and chi_p as a function of component masses and spins
    chi_eff = (mass1*spin1z + mass2*spin2z) / (mass1 + mass2)
    chi_p = max(spin1p, (3*mass1 + 4*mass2)/(4*mass1 + 3*mass2)*(mass2/mass1)*spin2p)
where
    spin1z = spin1_magnitude*cos(spin1_polar_angle)
    spin2z = spin2_magnitude*cos(spin2_polar_angle)
    spin1p = spin1_magnitude*sin(spin1_polar_angle)
    spin2p = spin2_magnitude*sin(spin2_polar_angle)

The definition of chi_p is introduced in
    Schmidt+ PRD 91, 023043 (2015)
    https://journals.aps.org/prd/abstract/10.1103/PhysRevD.91.024043
    """
    _required = (
        'mass1_source',
        'mass2_source',
        'spin1_magnitude',
        'spin1_polar_angle',
        'spin2_magnitude',
        'spin2_polar_angle',
    )
    _scalar_variates = ('chi_eff', 'chi_p')

    @staticmethod
    def transform(event):
        m1 = event['mass1_source']
        m2 = event['mass2_source']
        s1z = event['spin1_magnitude']*be.cos(event['spin1_polar_angle'])
        s2z = event['spin2_magnitude']*be.cos(event['spin2_polar_angle'])
        event['chi_eff'] = PolarSpins2Chi._chi_eff(m1, s1z, m2, s2z)

        s1p = event['spin1_magnitude']*be.sin(event['spin1_polar_angle'])
        s2p = event['spin2_magnitude']*be.sin(event['spin2_polar_angle'])
        event['chi_p'] = PolarSpins2Chi._chi_p(m1, s1p, m2, s2p)

    @staticmethod
    def _chi_eff(m1, s1z, m2, s2z):
        return (m1*s1z + m2*s2z) / (m1 + m2)

    @staticmethod
    def _chi_p(m1, s1p, m2, s2p):
        q = m2/m1
        return be.max([s1p, (3+4*q)/(4+3*q)*q*s2p], axis=0)

class CartesianSpins2Chi(AttributeTransformation):
    """\
Computes chi_eff and chi_p as a function of component masses and spins
    chi_eff = (mass1*spin1z + mass2*spin2z) / (mass1 + mass2)
    chi_p = max(spin1p, (3*mass1 + 4*mass2)/(4*mass1 + 3*mass2)*(mass2/mass1)*spin2p)
where
    spin1p = (spin1x**2 + spin1y**2)**0.5
    spin2p = (spin2x**2 + spin2y**2)**0.5
The definition of chi_p is introduced in
    Schmidt+ PRD 91, 023043 (2015)
    https://journals.aps.org/prd/abstract/10.1103/PhysRevD.91.024043
    """
    _required = (
        'mass1_source',
        'mass2_source',
        'spin1x',
        'spin1y',
        'spin1z',
        'spin2x',
        'spin2y',
        'spin2z',
    )
    _scalar_variates = PolarSpins2Chi._scalar_variates

    @staticmethod
    def transform(event):
        m1 = event['mass1_source']
        m2 = event['mass2_source']
        s1z = event['spin1z']
        s2z = event['spin2z']
        event['chi_eff'] = PolarSpins2Chi._chi_eff(m1, event['spin1z'], m2, event['spin2z'])

        s1p = (event['spin1x']**2 + event['spin1y']**2)**0.5
        s2p = (event['spin2x']**2 + event['spin2y']**2)**0.5
        event['chi_p'] = PolarSpins2Chi._chi_p(m1, s1p, m2, s2p)
