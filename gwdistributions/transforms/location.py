"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from numpy import pi

try:
    from lal.lal import GreenwichMeanSiderealTime as GMST
except ImportError:
    GMST = None

from gwdistributions.backends import numerics as be
from gwdistributions.backends import names
from gwdistributions.backends.units import MPC_CGS

from gwdistributions.utils.cosmology import \
    (Cosmology, PLANCK_2018_Ho, PLANCK_2018_OmegaMatter, PLANCK_2018_OmegaRadiation, PLANCK_2018_OmegaLambda)

from .base import AttributeTransformation

#-------------------------------------------------

class Celestial2Geographic(AttributeTransformation):
    """computes the Geographic coordinates of the event from the Celestial coordinates and the time
    """
    _required = ('right_ascension', 'declination', 'geocenter_time')
    _scalar_variates = ('colatitute', 'longitude')

    @staticmethod
    def transform(event):
        if GMST is None:
            raise ImportError('lal.lal.GreenwichMeanSiderealTime is not available!')

        event['longitude'] = (event['right_ascension'] - GMST(event['geocenter_time']))%(2*pi)
        event['colatitute'] = 0.5*pi - event['declination']

#-------------------------------------------------

class CosmologicalTransformation(AttributeTransformation):
    """a dummy class that deals with instantiation so that child classes can inherit a consistent set of routines
    """
    cosmological = True ### used to control option lookup when parsing objects from INI
    _params = ( ### only load these once (param names are static!)
        names.name('ho'),
        names.name('omega_matter'),
        names.name('omega_radiation'),
        names.name('omega_lambda'),
    )

    def _init_values(self, **kwargs):

        # deal with annoying camel case issues from Python's config parser
        # these are the names used to grab values from kwargs
        ho, omega_matter, omega_radiation, omega_lambda = self._params

        Ho = kwargs.get(ho, kwargs.get(ho.lower(), PLANCK_2018_Ho))
        OmegaMatter = kwargs.get(omega_matter, kwargs.get(omega_matter.lower(), PLANCK_2018_OmegaMatter))
        OmegaRadiation = kwargs.get(omega_radiation, kwargs.get(omega_radiation.lower(), PLANCK_2018_OmegaRadiation))
        OmegaLambda = kwargs.get(omega_lambda, kwargs.get(omega_lambda.lower(), PLANCK_2018_OmegaLambda))
        self._values = [Ho, OmegaMatter, OmegaRadiation, OmegaLambda]

        self._cosmology = Cosmology(Ho, OmegaMatter, OmegaRadiation, OmegaLambda)

    @property
    def cosmology(self):
        return self._cosmology

#------------------------

class Redshift2ComovingDistance(CosmologicalTransformation):
    """compute comoving distance based on a redshift
    """
    _required = ('redshift',)
    _scalar_variates = ('comoving_distance', 'dcomoving_distance_dredshift') ### assumed to be in Mpc

    def transform(self, event):
        event['comoving_distance'] = self.cosmology.z2Dc(event['redshift']) / MPC_CGS ### return this in Mpc
        event['dcomoving_distance_dredshift'] = self.cosmology.dDcdz(event['redshift']) / MPC_CGS

#------------------------

class Redshift2LuminosityDistance(CosmologicalTransformation):
    """compute luminosity distance based on a redshift.
    Also computes the jacobian between luminosity distance and redshift
    """
    _required = ('redshift',)
    _scalar_variates = ('luminosity_distance', 'dluminosity_distance_dredshift') ### assumed to be in Mpc

    def transform(self, event):
        event['luminosity_distance'] = self.cosmology.z2DL(event['redshift']) / MPC_CGS ### return this in Mpc
        event['dluminosity_distance_dredshift'] = self.cosmology.dDLdz(event['redshift']) / MPC_CGS

class LuminosityDistance2Redshift(CosmologicalTransformation):
    """compute redshift based on luminosity distance
    **WARNING**: this is implemented in a way that is known to be computationally expensive
    """
    _required = ('luminosity_distance',) ### assumed to be in Mpc
    _scalar_variates = ('redshift', 'dluminosity_distance_dredshift')

    def transform(self, event):
        event['redshift'] = self.cosmology.DL2z(event['luminosity_distance'] * MPC_CGS) # cosmology works in CGS
        event['dluminosity_distance_dredshift'] = self.cosmology.dDLdz(event['redshift']) / MPC_CGS

#-------------------------------------------------

class EffectiveDistance(AttributeTransformation):
    """associate the effective distance in each detector for this event
    """
    _required = (
        'luminosity_distance',
        'geocenter_time',
        'right_ascension',
        'declination',
        'polarization',
        'inclination',
    )
    _params = ('network',)

    def _init_values(self, network=None):
        self._values = [network]

    @property
    def network(self):
        return self._values[0]

    @property
    def _scalar_variates(self):
        if self.network is None:
            return ()
        return tuple([self._eff_dist_name(detector.name) for detector in self.network])

    @staticmethod
    def _eff_dist_name(name):
        return '%s_%s'%(names.name('effective_luminosity_distance'), name)

    def transform(self, event):
        cosi = be.cos(event['inclination'])
        cosi2_1 = 0.5*(1+cosi**2)
        dist = event['luminosity_distance']
        freqs = be.array([0.]) ### NOTE: we compute the antenna response in the long-wavelength-approximation 
                                ### by passing freqs=0
        for attr, detector in zip(self.scalar_variates, self.network):
            Fp, Fx = detector.response(
                freqs,
                event['geocenter_time'],
                event['right_ascension'],
                event['declination'],
                event['polarization'],
            )[:2]
            ### appropriate expression for 2 polarizations in long-wavelength approx
            event[attr] = dist * ((cosi2_1 * Fp[0,0].real)**2 + (cosi * Fx[0,0].real)**2)**0.5
