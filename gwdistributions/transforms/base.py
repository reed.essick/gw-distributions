"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from gwdistributions.backends import names
from gwdistributions.utils import Parameters

#-------------------------------------------------

class AttributeTransformation(object):
    """a simple class that transforms Event attributes and add them to the object in place
    """
    cosmological = False ### used to control option lookup when parsing objects from INI
    _required = ()
    _scalar_variates = ()
    _vector_variates = ()
    _params = ()

    def __init__(self, *args, **kwargs):
        self._init_values(*args, **kwargs)
        self._param2ind = dict((name, ind) for ind, name in enumerate(self._params))
        self._parameters = Parameters(self)

    def _init_values(self, *args, **kwargs):
        self._values = [] ### a list, which is empty by default

    def check(self, event):
        for attr in self._required:
            assert hasattr(event, names.invname(attr)), 'Event does not have attribute "%s" \nEvent only has:\n    %s' % \
                (attr, '\n    '.join(sorted(event.attributes)))

    def __call__(self, event):
        self.check(event)
        self.transform(event)

    @property
    def required(self): ### allow for dynamic renaming
        return tuple(names.name(_) for _ in self._required)

    @property
    def scalar_variates(self): ### allow for dynamic renaming
        return tuple(names.name(_) for _ in self._scalar_variates)

    @property
    def vector_variates(self): ### allow for dynamic renaming
        return tuple(names.name(_) for _ in self._vector_variates)

    @property
    def variates(self):
        return self.scalar_variates + self.vector_variates

    @property
    def params(self):
        return self._values

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, new):
        '''expects the new parameters to be a dictionary, which we then pass to self.update
        '''
        self.update(**new)

    def __getitem__(self, name):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        return self._values[self._param2ind[name]]

    def __setitem__(self, name, value):
        if name not in self._params:
            raise KeyError('could not find parameter=%s'%name)
        self._values[self._param2ind[name]] = value
 
    def __items__(self):
        return list(zip(self._params, self._values))

    @staticmethod
    def transform(event):
        pass

    def update(self, **kwargs):
        for name, param in kwargs.items():
            self[name] = param
