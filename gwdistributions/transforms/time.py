"""a module for computing times relevant for events
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .base import AttributeTransformation
from gwdistributions.backends import names

#-------------------------------------------------

class TimeOfArrival(AttributeTransformation):
    """associate the time of arrival in each detector for this event
    """
    _required = ('geocenter_time', 'right_ascension', 'declination') ### time and location on the sky
    _params = ('network',)

    def _init_values(self, network=None):
        self._values = [network]

    @property
    def network(self):
        return self._values[0]

    @property
    def _scalar_variates(self):
        if self.network is None:
            return ()
        return tuple([self._dt_name(detector.name) for detector in self.network])

    @staticmethod
    def _dt_name(name):
        return '%s_%s'%(names.name('time'), name)

    def transform(self, event):
        for attr, detector in zip(self.scalar_variates, self.network):
            event[attr] = event['geocenter_time'] \
                + detector.dt(event['geocenter_time'], event['right_ascension'], event['declination'])
