"""a module housing basic testing logic for SamplingDistributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import sys

from .utils import *
from .distribution import (SamplingDistributionTest, SamplingDistributionTester)
from gwdistributions.distributions import Constellation

#-------------------------------------------------

DEFAULT_NUM_VERTICES = 5
DEFAULT_RA_PRIOR_RANGE = (0, 2*be.pi)
DEFAULT_DEC_PRIOR_RANGE = (-0.5*be.pi, +0.5*be.pi)

#-------------------------------------------------

class ConstellationTest(SamplingDistributionTest):
    """an object that performs and summarizes the tests of Constellation. Also handles I/O and reporting
    """

    def __init__(self, constellation, numevents, *required):
        assert isinstance(constellation, Constellation), \
            'ConstellationTest can only test objects of type "Constellation"'
        SamplingDistributionTest.__init__(self, constellation, numevents, *required)

    def plot(self, path, verbose=False, **kwargs):
        """plot the comparison between the drawn samples and the predicted distribution
        """
        if not os.path.exists(os.path.dirname(path)):
            try:
                os.makedirs(os.path.dirname(path))
            except: ### another process may have made this directory if we run in parallel
                pass
        if verbose:
            print('plotting results from %d events'%len(self.samples))

        ### delegate to make a nice corner plot
        ### expect this to be a 2x2 grid
        fig = self._multivariate_plot(verbose=verbose, contour=False, label=False, **kwargs)

        ### now annotate the joint distribution to show the constellation boundaries
        ax = fig.axes[-1] ### this should be the joint distrib for the way we construct a 2x2 grid

        # plot bounding box and polygon as overlays
        (min_ra, min_dec), (max_ra, max_dec) = self.dist.bounding_box
        bb_vertices = [(min_ra, min_dec), (min_ra, max_dec), (max_ra, max_dec), (max_ra, min_dec)]
        bb_edges = Constellation._vertices2edges(bb_vertices) ### list of edges that never cross x=2*pi

        for verts, edges, color, marker in [
              (bb_vertices, bb_edges, 'g', 's'),
              (self.dist.vertices, self.dist.edges, 'b', 'o'),
            ]:
            for (x, y), (X, Y) in edges:
                ax.plot([x,X], [y,Y], color=color)

            for x, y in verts:
                x = x % (2*be.pi)
                ax.plot([x], [y], markerfacecolor='none', markeredgecolor=color, marker=marker)

        # shade forbidden parts of the plot
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()

        if xmin < 0:
            ax.fill_between([xmin, 0], [ymin]*2, [ymax]*2, color='k', alpha=0.15)
        if xmax > 2*be.pi:
            ax.fill_between([2*be.pi, xmax], [ymin]*2, [ymax]*2, color='k', alpha=0.15)
        if ymin < -0.5*be.pi:
            ax.fill_between([xmin, xmax], [ymin]*2, [-0.5*be.pi]*2, color='k', alpha=0.15)
        if ymax > +0.5*be.pi:
            ax.fill_between([xmin, xmax], [+0.5*be.pi]*2, [ymax]*2, color='k', alpha=0.15)

        ax.set_xlim(xmin=xmin, xmax=xmax)
        ax.set_ylim(ymin=ymin, ymax=ymax)

        # save
        if verbose:
            print('    saving: '+path)
        fig.savefig(path, **kwargs)
        plt.close(fig)

#------------------------

class ConstellationTester(SamplingDistributionTester):
    """an object that tests Constellation to see if it self-consistently define probabilistic models. \
This is done by randomly drawing hyperparameters from prior distributions and then comparing the predicted probability distribution to what is obtained from drawing a set of samples from the distribution
    """

    num_variates = 2 ### this will always be the case

    def __init__(
            self,
            ra_prior,
            dec_prior,
            num_vertices=DEFAULT_NUM_VERTICES,
        ):
        self._ra_prior = ra_prior
        self._dec_prior = dec_prior
        self._num_vertices = num_vertices
        self.tests = []

    def _draw_vertices(self):

        ### generate sets of vertices
        vertices = be.array([[self._ra_prior(), self._dec_prior()] for _ in range(self._num_vertices)], dtype=float)

        ### now order them in a reasonable way
        mean_ra = be.sum(vertices[:,0]) / self._num_vertices
        mean_dec = be.sum(vertices[:,1]) / self._num_vertices

        angles = be.arctan2(vertices[:,1]-mean_dec, vertices[:,0]-mean_ra) ### the angles around the mean
        order = be.argsort(angles)

        vertices = vertices[order] ### sort by increasing angles around the mean

        ### return
        return vertices

    def run(self, verbose=False, num_trials=DEFAULT_NUM_TRIALS, num_samples=DEFAULT_NUM_SAMPLES, poly_tmp=None):
        """generate random realizations of Constellation objects and associated tests
        """
        if poly_tmp is not None:
            outdir = os.path.dirname(poly_tmp)
            if not os.path.exists(outdir):
                os.makedirs(outdir)

        for trial in range(num_trials):
            vertices = self._draw_vertices() ### draw params
            if verbose:
                print('trial=%d'%trial)
                print('    vertices :')
                for tup in vertices:
                    print('        (RA=%.3f, Dec=%.3f)'%tuple(tup))

            if poly_tmp is not None: # plot the vertices and edges
                fig = plt.figure()
                ax = fig.gca()
                for x, y in vertices:
                    ax.plot([x], [y], marker='o', color='b')
                verts = list(vertices) + [vertices[0]]
                for (x, y), (X, Y) in zip(verts[:-1], verts[1:]):
                    ax.plot([x,X], [y,Y], color='b')

                xmin, xmax = ax.get_xlim()
                ymin, ymax = ax.get_ylim()
                if xmin < 0:
                    ax.fill_between([xmin, 0], [ymin]*2, [ymax]*2, color='k', alpha=0.25)
                if xmax > 2*be.pi:
                    ax.fill_between([2*be.pi, xmax], [ymin]*2, [ymax]*2, color='k', alpha=0.25)
                ax.set_xlim(xmin=xmin, xmax=xmax)
                ax.set_ylim(ymin=ymin, ymax=ymax)

                figname = poly_tmp % trial
                if verbose:
                    print('    '+figname)
                fig.savefig(figname)
                plt.close(fig)

            test = ConstellationTest(Constellation(vertices=vertices), num_samples) ### create test object
            self.tests.append(test)                            ### hold onto a reference
        return self.tests

#-------------------------------------------------

class ConstellationMixtureTest(ConstellationTest):
    """an object that performs some basic tests of a constellation mixture model
    """

    def __init__(self, constellation, numevents, *required):
        SamplingDistributionTest.__init__(self, constellation, numevents, *required)

    def check_membership(self, verbose=False):
        self._membership = []
        N = len(self.samples)
        for ind, sample in enumerate(self.samples):
            if verbose:
                sys.stdout.write('\r    checking membership for sample %6d / %6d : %s'%(ind, N, sample))
                sys.stdout.flush()

            membership = [ind for ind in range(self.dist.num_components) \
                if self.dist.distributions[ind].prob(*sample) != 0]

            assert len(membership) == 1, 'sample must have nonzero probability for exactly one component'
            self._membership.append(membership[0])

        if verbose:
            sys.stdout.write('\n')
            sys.stdout.flush()

    def plot(self, path, verbose=False, **kwargs):
        """plot the comparison between the drawn samples and the predicted distribution
        """
        if not os.path.exists(os.path.dirname(path)):
            try:
                os.makedirs(os.path.dirname(path))
            except: ### another process may have made this directory if we run in parallel
                pass
        if verbose:
            print('plotting results from %d events'%len(self.samples))

        ### delegate to make a nice corner plot
        ### expect this to be a 2x2 grid
        fig = self._multivariate_plot(verbose=verbose, contour=False, label=False, **kwargs)

        # add edges for each component of the mixture
        ax = fig.axes[-1] # should be the joint distrib
        for dist in self.dist.distributions:
            for (x, y), (X, Y) in dist.edges:
                ax.plot([x,X], [y, Y], color='b', alpha=0.25) ### shade these by mixing fraction

        # shade forbidden parts of the plot
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()

        if xmin < 0:
            ax.fill_between([xmin, 0], [ymin]*2, [ymax]*2, color='k', alpha=0.15)
        if xmax > 2*be.pi:
            ax.fill_between([2*be.pi, xmax], [ymin]*2, [ymax]*2, color='k', alpha=0.15)
        if ymin < -0.5*be.pi:
            ax.fill_between([xmin, xmax], [ymin]*2, [-0.5*be.pi]*2, color='k', alpha=0.15)
        if ymax > +0.5*be.pi:
            ax.fill_between([xmin, xmax], [+0.5*be.pi]*2, [ymax]*2, color='k', alpha=0.15)

        ax.set_xlim(xmin=xmin, xmax=xmax)
        ax.set_ylim(ymin=ymin, ymax=ymax)

        # save
        if verbose:
            print('    saving: '+path)
        fig.savefig(path, **kwargs)
        plt.close(fig)

#------------------------

class ConstellationMixtureTester(ConstellationTester):
    """an object that runs a series of tests of ConstellationMixture objects
    """

    def __init__(self, instantiator, **instantiator_kwargs):
        self._instantiator = instantiator
        self._instantiator_kwargs = instantiator_kwargs
        self.tests = []

    def make_dist(self):
        return self._instantiator(**self._instantiator_kwargs)

    def run(self, num_trials=DEFAULT_NUM_TRIALS ,num_samples=DEFAULT_NUM_SAMPLES, verbose=False):
        for trial in range(num_trials):
            if verbose:
                sys.stdout.write('\r    generating trial: %d' % trial)
                sys.stdout.flush()

            dist = self.make_dist() # make a new instance
            for ind in range(dist.num_components): # set the mixing fractions to be random numbers
                dist[dist._mixture_frac_tmp%ind] = be.random()
            test = ConstellationMixtureTest(dist, num_samples) ### create test object
            self.tests.append(test)                            ### hold onto a reference

        if verbose:
            sys.stdout.write('\n')
            sys.stdout.flush()

        return self.tests

    def check_membership(self, verbose=False):
        """make sure drawn samples belong to exactly one component of the mixture
        """
        for trial, test in enumerate(self.tests):
            if verbose:
                print('checking membership for trial=%d'%trial)
            test.check_membership(verbose=verbose)
