"""a module housing basic testing logic for SamplingDistributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import os

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
plt.rcParams['font.family'] = 'serif'
if matplotlib.__version__ < '1.3.0':
    plt.rcParams['text.usetex'] = True

from scipy.stats import kstest
from scipy.integrate import cumtrapz

import copy

from numpy import savetxt ### used to write out randomly generated EOS

from gwdistributions.backends import numerics as be
from gwdistributions.backends import names

#-------------------------------------------------

DEFAULT_NUM_TRIALS = 1
DEFAULT_NUM_SAMPLES = 1000
DEFAULT_NUM_GRID_POINTS = 10000 ### could scale badly for multi-variate plots

#-------------------------------------------------

class FixedDistribution(object):

    def __init__(self, x):
        self.x = x

    def __call__(self):
        return self.x

class UniformDistribution(object):

    def __init__(self, xmin, xmax):
        self.xmin = xmin
        self.xmax = xmax

    def __call__(self):
        return self.xmin + (self.xmax-self.xmin)*be.random()

class UniformComplexDistribution(UniformDistribution):

    def __init__(self, xmin, xmax):
        self.xmin = xmin
        self.xmax = xmax

    def __call__(self):
        return UniformDistribution.__call__(self) + 1j*UniformDistribution.__call__(self)

#-------------------------------------------------

class DomainError(Exception):
    """raised when a random sample does not fall within the declared domain
    """

    def __init__(self, var, minimum, maximum, bad_samples):
        self.var = var
        self.bounds = (minimum, maximum)
        self.bad_samples = bad_samples

class ProbShapeError(Exception):
    """raised when the shape of the returned probability does not match the samples that were used
    """
    def __init__(self, num_sample, len_prob):
       Exception.__init__(self, 'len(prob)=%d does not match len(sample)=%d'%(len_prob, num_sample))

class ProbError(Exception):
    """raised when a random sample corresponds to zero probability within the same model
    """
    def __init__(self, samples, prob):
        message = 'probability of drawn samples is not positive definite: prob(%s) = %s'%(samples, prob)
        Exception.__init__(self, message)
