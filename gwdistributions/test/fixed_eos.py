"""a module housing basic testing logic for SamplingDistributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import os

from .utils import *
from .distribution import (SamplingDistributionTest, SamplingDistributionTester)
from gwdistributions.distributions import FixedEoSSamplingDistribution

#-------------------------------------------------

class FixedEoSSamplingDistributionTest(SamplingDistributionTest):
    """a class that specifically tests the behavior of FixedEoS distributions
    """

    def _draw_samples(self, size=1):
        ans = []
        for i in range(size):
            required = [_() for _ in self.required]
            ans.append( list(self.dist.rvs(*required, size=1)[0]) + required )
        return be.array(ans).reshape((size, self.dist.num_variates+len(self.required)))

    def plot(self, path, verbose=False, num_grid_points=DEFAULT_NUM_GRID_POINTS, **kwargs):
        """make a scatter plot of the samples vs the associated masses. Overlay the EOS relation as one line per branch
        """
        assert self.dist.num_variates == 1, 'only know how to plot one-dimensional FixedEoS distributions!'
        assert len(self.dist.required) == 1, 'only know how to plot FixedEoS distributions that have len(required)==1'

        ### make a figure
        ndim = 2 ### has to be this way if we made it past the assertion statements
        nbins = max(10, int(0.5*len(self.samples)**0.5))
        fig = plt.figure(figsize=(2.5*ndim, 2.5*ndim))

        #---

        ### joint distribution: scatter and overlay
        if verbose:
            print('plotting joint distribution as scatter')
        ax = plt.subplot(ndim, ndim, 3)

        # scatter plot of samples
        m = self.samples[:,1]    ### extract this way because matplotlib complains otherwise (no idea why...)
        x = self.samples[:,0] ### extract this in this way because we know num_variates == 1
        ax.plot(m, x, markeredgecolor='grey', markerfacecolor='none', alpha=0.15, marker='o', markersize=3, linestyle='none')

        # plot default values
        m_default = be.linspace(be.min(m), be.max(m), 1001)
        ax.plot(m_default, self.dist.default(m_default), color='k', linestyle='dashed', alpha=0.5)

        # decorate
        ax.set_xlabel(self.dist.required[0])
        ax.set_ylabel(self.dist.variates[0])

        xmin, xmax = ax.get_xlim()
        dx = (xmax-xmin)*0.05
        ax.set_xlim(xmin=xmin-dx, xmax=xmax+dx)

        ymin, ymax = ax.get_ylim()
        dy = (ymax-ymin)*0.05
        ax.set_ylim(ymin=ymin-dy, ymax=ymax+dy)

        #---

        if verbose:
            print('plotting projected histograms')

        ### projected histograms over mass
        ax_m = plt.subplot(ndim, ndim, 1)

        bins_m = be.linspace(xmin, xmax, nbins)
        ax_m.hist(m, bins=bins_m, histtype='step', color='grey', orientation='vertical')

        ### projected histograms over X
        ax_x = plt.subplot(ndim, ndim, 4)

        bins_x = be.linspace(ymin, ymax, nbins)
        ax_x.hist(x, bins=bins_x, histtype='step', color='grey', orientation='horizontal')

        #---

        ### iterate through samples / branches
        if verbose:
            print('iterating over branches; plotting overlay')

        # overlay branches
        for ind, branch in enumerate(self.dist.branches):
            min_branch_mass, max_branch_mass = branch.mass_domain
            branch_m = branch.mass
            branch_x = list(branch.columns.values())[0] ### NOTE: assumes we only store a single column
                                                        ### for this branch

            ### add to joint distribution
            color = ax.plot(branch_m, branch_x, linestyle='solid')[0].get_color()
            
            ### figure out which samples fall on this curvemake projected histograms of samples that fall on this branch!
            branch_m_samples = []
            branch_x_samples = []
            for X, M in zip(x, m):
                if (M < min_branch_mass) or (max_branch_mass < M): ### not on this branch
                    continue

                if abs(be.interp(M, branch_m, branch_x) - X) < 1e-3 * abs(X): ### belongs to this branch
                    branch_m_samples.append(M)
                    branch_x_samples.append(X)

            if len(branch_m_samples): ### something to plot
                ax_m.hist(branch_m_samples, bins=bins_m, histtype='step', color=color)
                ax_x.hist(branch_x_samples, bins=bins_x, histtype='step', color=color, orientation='horizontal')

            elif verbose:
                print('    WARNING: no sample found on branch %d'%ind)

            ### add labels
            fig.text(0.95, 0.90-ind*0.05, 'branch %d'%ind, ha='right', va='top', color=color, fontsize=10)

        #---

        # finish decorating projected histograms

        ax_m.set_xlim(ax.get_xlim())

        plt.setp(ax_m.get_xticklabels(), visible=False)
        plt.setp(ax_m.get_yticklabels(), visible=False)

        ax_x.set_ylim(ax.get_ylim())

        plt.setp(ax_x.get_xticklabels(), visible=False)
        plt.setp(ax_x.get_yticklabels(), visible=False)

        #---

        plt.subplots_adjust(hspace=0.03, wspace=0.03, left=0.13, right=0.95, bottom=0.1, top=0.90)

        ### save the figure
        if verbose:
            print('    saving: '+path)
        fig.savefig(path, **kwargs)
        plt.close(fig)

#------------------------

class FixedEoSSamplingDistributionTester(SamplingDistributionTester):
    """a class that specifically tests the behavior of FixedEoS distributions
    """

    def __init__(self, samplingdistribution_constructor, columns, paramprior):
        assert issubclass(samplingdistribution_constructor, FixedEoSSamplingDistribution), 'must pass a constructor for a FixedEoSSamplingDistribution!'
        self.dist = samplingdistribution_constructor ### note, should be callable to make more objects!
        self.dist_columns = columns

        for param in self.dist._required:
            assert names.name(param) in paramprior, 'required=%s not in paramprior!'%param

        self.prior = paramprior
        if 'num_branches' not in paramprior:
            paramprior['num_branches'] = FixedDistribution(1)

        self.tests = []

    @staticmethod
    def _generate_eos(num_branches, mass_min, mass_max, x_min, x_max, num_points_per_branch=100):
        """generate a random EOS with the requested number of branches that span the specified mass range
        """
        affine = []
        mass = []
        column = []

        dm = mass_max - mass_min
        for branch in range(num_branches):
            ### monotonically increasing affine parameter (central density)
            a = be.arange(num_points_per_branch) + branch*num_points_per_branch

            ### draw random mass coverage
            m = mass_min + be.random()*0.5*dm
            M = mass_max - be.random()*0.5*dm
            m0 = 0.5*(M+m)

            m = be.linspace(m, M, num_points_per_branch)

            ### compute macroscopic property
            # approximate the range 
            x0 = 0.5*(x_min+x_max)
            dx = x_max - x_min if x_max != x_min else 1.

            # a cubic polynomial
            x = x0 + dx*(be.random() + be.random()*(m-m0) + be.random()*(m-m0)**2 + be.random()*(m-m0)**3)

            # add to holders
            affine += list(a)
            mass += list(m)
            column += list(x)

        # return
        return be.array(affine), be.array(mass), be.array(column)

    def run(
            self,
            mass_min=1.0,
            mass_max=3.0,
            outdir='.',
            tag='',
            verbose=False,
            num_trials=DEFAULT_NUM_TRIALS,
            num_samples=DEFAULT_NUM_SAMPLES,
        ):
        """draw parameters from the hyperprior, draw many samples, compare to predicted distribution
        """
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        if not tag:
            tag = "FixedEoSSamplingDistributionTester"
        path = os.path.join(outdir, tag + '-%d.csv.gz')

        for trial in range(num_trials):
            if verbose:
                print('trial=%d'%trial)

            # make an EoS
            affine, M, X = self._generate_eos(self.prior['num_branches'](), mass_min, mass_max, self.dist.default(mass_min), self.dist.default(mass_max))
            eos = path%trial
            if verbose:
                print('    eos path: %s'%eos)
            savetxt(eos, list(zip(affine, M, X)), header='affine,M,X', comments='', delimiter=',')

            # make the actual test object
            dist = self.dist(eos, **self.dist_columns)
            test = FixedEoSSamplingDistributionTest(dist, num_samples, *[self.prior[names.name(param)] for param in self.dist._required])
            self.tests.append(test)

        return self.tests

    def plot(self, verbose=False, outdir='.', tag='', figtype='png', **kwargs):
        """plot the tests
        """
        if not tag:
            tag = "FixedEoSSamplingDistributionTester"
        path = os.path.join(outdir, tag + '-%d.' + figtype)
        for trial, test in enumerate(self.tests):
            figname = path%trial
            if verbose:
                print('plotting trial=%d : %s'%(trial, figname))
            test.plot(figname, verbose=verbose, **kwargs)
