"""a module housing basic testing logic for SamplingDistributions
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import *
from gwdistributions.distributions import SamplingDistribution

#-------------------------------------------------

class SamplingDistributionTest(object):
    """an object that performs and summarizes the tests of SamplingDistributions. Also handles I/O and reporting
    """

    def __init__(self, samplingdistribution, numevents, *required):
        self.dist = copy.deepcopy(samplingdistribution) ### so we don't accidentally share references
        self.required = required
        self.samples = self._draw_samples(size=numevents)
        self._stached_compare = False

    def _draw_samples(self, size=1):
        return self.dist.rvs(*self.required, size=size)

    def check_prob(self, verbose=False):
        args = list(be.transpose(self.samples)) + list(self.required)
        prob = self.dist.prob(*args)

        if len(prob) != len(self.samples):
            raise ProbShapeError(len(self.samples), len(prob))

        elif verbose:
            print('    dist.prob produced the correct shape (matches samples)')
            (len(self.samples), len(prob))

        if be.any(prob <= 0):
            truth = prob <= 0
            raise ProbError(self.samples[truth], prob[truth])

        elif be.any(prob != prob):
            truth = prob != prob
            raise ProbError(self.samples[truth], prob[truth])

        elif verbose:
            print('    all samples produce probabilities that are strictly positive definite')

    def check_bounds(self, verbose=False):
        domain = self.dist.domain(*self.required)
        samples = self.samples
        for ind, var in enumerate(self.dist.variates):
            m, M = domain[var]
            if be.any(samples[:,ind] < m) or be.any(samples[:,ind] > M):
                raise DomainError(var, m, M, samples[(samples[:,ind]<m) | (M<samples[:,ind]),ind])
            elif verbose:
                print('    all samples for %s fall within declared bounds : [%.6e, %.6e]'%(var, m, M))

    def _get_bounds(self, ind=0):
        """determine the bounds for histograms, grids based on the delcared domain within the SamplingDistribution
        """
        xmin, xmax = self.dist.domain(*self.required)[self.dist.variates[ind]]
        if xmin == -be.infty: ### futz this to be based on observed samples
            xmin = be.min(self.samples[:,ind]) - 0.1*(be.max(self.samples[:,ind]) - be.min(self.samples[:,ind]))
        if xmax == +be.infty:
            xmax = be.max(self.samples[:,ind]) + 0.1*(be.max(self.samples[:,ind]) - be.min(self.samples[:,ind]))
        if xmin == xmax:
            xmin -= 1.0
            xmax += 1.0
        return xmin, xmax

    def compare(self, verbose=False, num_grid_points=DEFAULT_NUM_GRID_POINTS):
        """compare the observed drawn samples with the predicted PDF, CDF using \
    1D distribution --> KS 2-sided test
    ND distribution --> not implemented! (will raise an exception
        """
        if self._stached_compare:
            return self._stached_compare

        if self.dist.num_variates == 1: ### univariate distribution! We can compute a KS test

            ### compute cdf everywhere on a grid
            xmin, xmax = self._get_bounds(ind=0)
            grid = be.linspace(xmin, xmax, num_grid_points)

            pdf = self.dist.prob(grid, *self.required)
            cdf = be.array([0] + list(cumtrapz(pdf, x=grid)))
            if not self.dist.normalized:
                cdf = cdf / cdf[-1]

            foo = lambda x: be.interp(x, grid, cdf) ### needs to be a callable to play nicely with scipy.stats.kstest

            ### compute 1D KS test
            stat, pvalue = kstest(self.samples[:,0], foo)

            ### report the statistics
            if verbose:
                print('''\
1D KS test for: %s
    statistic = %.6e
    pvalue    = %.6e'''%(self.dist.variates[0], stat, pvalue))

            self._stached_compare = (stat, pvalue) ### remember this

            return stat, pvalue

        else:
            if verbose:
                print("WARNING: We don't know how best to quantify consistency of multidimensional distributions")
            return None

    def plot(self, path, verbose=False, num_grid_points=DEFAULT_NUM_GRID_POINTS, **kwargs):
        """plot the comparison between the drawn samples and the predicted distribution
        """
        if not os.path.exists(os.path.dirname(path)):
            try:
                os.makedirs(os.path.dirname(path))
            except: ### another process may have made this directory if we run in parallel
                pass
        if verbose:
            print('plotting results from %d events'%len(self.samples))

        if self.dist.num_variates == 1: ### simple univariate plot
            fig = self._univariate_plot(verbose=verbose, num_grid_points=num_grid_points)
        else:
            fig = self._multivariate_plot(verbose=verbose, num_grid_points=num_grid_points)

        if verbose:
            print('    saving: '+path)
        fig.savefig(path, **kwargs)
        plt.close(fig)

    def _univariate_plot(self, verbose=False, num_grid_points=DEFAULT_NUM_GRID_POINTS):
        """stacked differential and cumulative histograms annotated with model parameters, KS test
        """
        fig = plt.figure(figsize=(5,8))
        ax2 = plt.subplot(2,1,1)
        ax1 = plt.subplot(2,1,2)

        ### figure out limits of plot
        xmin, xmax = self._get_bounds(ind=0)

        ### plot a histogram of samples
        if verbose:
            print('plotting histogram of samples')

        ax1.hist(
            self.samples[:,0],
            bins=be.linspace(xmin, xmax, max(10, int(0.5*len(self.samples)**0.5))),
            histtype='stepfilled',
            color='k',
            alpha=0.5,
            density=True,
        )

        ax2.hist(
            self.samples[:,0],
            bins=be.linspace(xmin, xmax, 10*len(self.samples)),
            histtype='stepfilled',
            color='k',
            alpha=0.5,
            cumulative=+1,
            density=True,
        )

        ### compute pdf, cdf everywhere on the grid
        if verbose:
            print('computing predicted PDF, CDF')

        grid = be.linspace(xmin, xmax, num_grid_points)

        pdf = self.dist.prob(grid, *self.required)
        cdf = be.array([0] + list(cumtrapz(pdf, x=grid)))

        if not self.dist.normalized:
            ax2.set_title('predicted distributions\nnumerially normalized!')
            pdf = pdf / cdf[-1]
            cdf = cdf / cdf[-1]

        # plot predicted distributions
        ax1.plot(grid, pdf, color='r')
        ax2.plot(grid, cdf, color='r')
         
        # compute KS test
        if verbose:
            print('performing KS test')

        stat, pvalue = self.compare(num_grid_points=num_grid_points)
        ax2.text(xmin, 1, '1D KS stat = %.3e\n1D KS pval = %.3e'%(stat, pvalue), ha='left', va='top')

        ### decorate
        if verbose:
            print('decorating')

        # add label for model parameters
        s = []
        for par, val in zip(self.dist.param_names, self.dist.params):
            s.append('%s = %.3e'%(par, val))
        for par, val in zip(self.dist.required, self.required):
            s.append('%s = %.3e'%(par, val))

        ax1.text(xmin, 0, '\n'.join(s), ha='left', va='bottom')

        # set bounds, etc
        dx = 0.05*(xmax-xmin)
        ax1.set_xlim(xmin=xmin-dx, xmax=xmax+dx)
        ax2.set_xlim(ax1.get_xlim())

        ymin, ymax = ax1.get_ylim()
        dy = 0.05*(ymax-ymin)
        ax1.set_ylim(ymin=0-dy)
        ax2.set_ylim(ymin=-0.05, ymax=1.05)

        ax1.set_xlabel(self.dist.variates[0])
        plt.setp(ax2.get_xticklabels(), visible=False)

        ax1.set_ylabel('p(%s)'%self.dist.variates[0])
        ax2.set_ylabel('P(%s)'%self.dist.variates[0])

        plt.subplots_adjust(hspace=0.05, left=0.15, right=0.95, bottom=0.10, top=0.93)

        ### return
        return fig

    def _multivariate_plot(self, verbose=False, num_grid_points=DEFAULT_NUM_GRID_POINTS, contour=True, label=True):
        """multidimensional distribution --> produce a corner plot for visual inspection (but do not know how to quantify this!)
        """
        ndim = self.dist.num_variates
        fig = plt.figure(figsize=(2.5*ndim, 2.5*ndim))

        # create grid
        if verbose:
            print('constructing %d-dimensional grid with %d grid points per dimension'%(ndim, num_grid_points))

        grids = [be.linspace(*self._get_bounds(ind=ind), num=num_grid_points) for ind in range(ndim)]
        vols = be.array([grids[ind][1]-grids[ind][0] for ind in range(ndim)])

        if verbose:
            print('computing pdf')

        meshgrid = be.meshgrid(*grids, indexing='ij')
        meshgrid = [_.flatten() for _ in meshgrid]
        pdf = self.dist.prob(*(meshgrid + list(self.required)))

        if be.any(pdf!=pdf) or be.any(pdf < 0):
            truth = (pdf!=pdf)
            if be.any(truth):
                print('nan in pdf!')
                for name, vect in zip(self.dist.variates, meshgrid):
                    print(name, vect[truth])

            truth = (pdf<0)
            if be.any(truth):
                print('negative pdf!')
                for name, vect in zip(self.dist.variates, meshgrid):
                    print(name, vect[truth])

            raise ValueError

        pdf = pdf.reshape(tuple(num_grid_points for _ in range(ndim)))

        if not self.dist.normalized:
            fig.suptitle('predicted distributions\nnumerially normalized!')
            # numerically normalize the pdf
            pdf = pdf / (be.sum(pdf) * be.prod(vols))

        # iterate and plot comparison between samples and marginals
        nbins = max(10, int(0.5*len(self.samples)**0.5))
        for row in range(ndim):
            ### plot 1D marginal (row==col)
            ax = plt.subplot(ndim, ndim, 1 + row*ndim + row)

            ymin = grids[row][0] ### named like this because it will be the y-axis in joint distributions
            ymax = grids[row][num_grid_points-1]

            # histogram the samples
            if verbose:
                print('plotting histogram of: '+self.dist.variates[row])

            ax.hist(
                self.samples[:,row],
                bins=be.linspace(ymin, ymax, nbins),
                histtype='stepfilled',
                color='k',
                alpha=0.5,
                density=True,
            )

            # plot predicted distribution
            if verbose:
                print('numerically marginalizing and plotting 1D marginal PDF of: '+self.dist.variates[row])

            ax.plot(
                grids[row],
                be.sum(pdf, axis=tuple(ind for ind in range(ndim) if ind!=row)) * be.prod(vols)/vols[row], ### numerically marginalize
                color='r',
            )
    
            # set limits
            dy = 0.05*(ymax-ymin)
            ax.set_xlim(xmin=ymin-dy, xmax=ymax+dy)
            ax.set_ylim(ymin=0.0)

            # decorate
            plt.setp(ax.get_yticklabels(), visible=False)

            if row == ndim-1: ### include xlabels
                ax.set_xlabel(self.dist.variates[row])
            else:
                plt.setp(ax.get_xticklabels(), visible=False)

            ### iterate and plot 2D marginals
            for col in range(row):
                ax = plt.subplot(ndim, ndim, 1 + row*ndim + col)

                xmin = grids[col][0]
                xmax = grids[col][num_grid_points-1]

                # scatter plot the samples
                ### FIXME: also plot a histogram/heatmap?
                if verbose:
                    print('plotting scatter of %s vs %s'%(self.dist.variates[col], self.dist.variates[row]))

                ax.plot(self.samples[:,col], self.samples[:,row], color='k', alpha=0.10, marker='.', linestyle='none')

                # plot predicted distribution
                if verbose:
                    print('plotting predicted 2D marginal PDF of %s vs %s'%(self.dist.variates[col], self.dist.variates[row]))

                if contour: # include contours in 2D marginal plots

                    ### numerically marginalize
                    pdf_2d = be.sum(pdf, axis=tuple(ind for ind in range(ndim) if (ind!=row) and (ind!=col)))
                    pdf_2d *= be.prod(vols)/(vols[row]*vols[col])

                    ax.contour(
                        grids[col],
                        grids[row],
                        be.transpose(pdf_2d), ### marginalize
                        colors='r',
                        levels=self._pdf2levels(pdf_2d.flatten()),
                    )

                # set limits
                dx = 0.05*(xmax-xmin)
                ax.set_xlim(xmin=xmin-dx, xmax=xmax+dx)
                ax.set_ylim(ymin=ymin-dy, ymax=ymax+dy)

                # decorate
                if row == ndim-1: ### include xlabel
                    ax.set_xlabel(self.dist.variates[col])
                else:
                    plt.setp(ax.get_xticklabels(), visible=False)

                if col == 0: ### include ylabel
                    ax.set_ylabel(self.dist.variates[row])
                else:
                    plt.setp(ax.get_yticklabels(), visible=False)

        plt.subplots_adjust(hspace=0.03, wspace=0.03, left=0.13, right=0.95, bottom=0.1, top=0.90)

        if label: # add label for model parameters
            s = []
            for par, val in zip(self.dist.param_names, self.dist.params):
                if isinstance(val, complex):
                    s.append('%s = %.3e + j*%.3e'%(par, val.real, val.imag))
                else:
                    s.append('%s = %.3e'%(par, val))
            for par, val in zip(self.dist.required, self.required):
                if isinstance(val, complex):
                    s.append('%s = %.3e + j*%.3e'%(par, val.real, val.imag))
                else:
                    s.append('%s = %.3e'%(par, val))

            fig.text(0.95, 0.90, '\n'.join(s), ha='right', va='top')

        return fig

    @staticmethod
    def _pdf2levels(pdf, levels=[0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]):
        if not levels:
            return []

        order = pdf.argsort()[::-1] ### largest to smallest
        cpdf = be.cumsum(pdf[order]) ### cumulative distribution
        cpdf = cpdf / be.sum(pdf)

        ans = []
        for level in levels: ### iterate through levels, returning the pdf value associated with that confidence
            ind = order[cpdf<=level]
            if len(ind):
                ans.append(pdf[ind[-1]])
            else: ### nothing is smaller than the first level, so we just add the first element
                  ### this issue should go away if we increase the number of samples in the kde...
                ans.append(pdf[order[0]])

        return be.unique(be.array(sorted(ans))) ### return increasing order, make sure there are no repeats

#------------------------

class SamplingDistributionTester(object):
    """an object that tests SamplingDistributions to see if they self-consistently define probabilistic models. \
This is done by randomly drawing hyperparameters from prior distributions and then comparing the predicted probability distribution to what is obtained from drawing a set of samples from the distribution
    """

    def __init__(self, samplingdistribution, paramprior):
        assert isinstance(samplingdistribution, SamplingDistribution), 'must pass an instance of SamplingDistribution'
        self.dist = samplingdistribution

        for param in self.dist.param_names:
            if param not in paramprior: ### no prior specified, so fix this to the current value
                paramprior[param] = FixedDistribution(self.dist[param])
            assert param in paramprior, 'param=%s not in paramprior!'%param

        for param in self.dist.required: ### make sure we run through name.names
            assert param in paramprior, 'required=%s not in paramprior!'%param

        self.prior = paramprior

        self.tests = []

    @property
    def num_variates(self):
        return self.dist.num_variates ### call it this way to make inheritance of "plot" simpler

    def _check_params_setter(self):
        """check that setter for dist.params works correctly
        """
        params = [self.prior[param]() for param in self.dist.param_names]
        self.dist.params = params                                         ### update distribution
        assert len(self.dist.params) == len(params)
        for n, p, P in zip(self.dist.param_names, self.dist.params, params):
            assert p==P, n+' not updated correctly by params.setter'
        print('dist.params.setter correctly updates distribution')

    def _check_setitem(self):
        """check that dist.__setitem__ works correctly
        """
        params = [self.prior[param]() for param in self.dist.param_names]
        for n, P in zip(self.dist.param_names, params):
            self.dist[n] = P        
        assert len(self.dist.params) == len(params)
        for n, p, P in zip(self.dist.param_names, self.dist.params, params):
            assert p==P, n+' not updated correctly by __setitem__'
            print('dist.__setitem__ correctly updates : '+n)

    def _check_update(self):
        """check that dist.update works correctly
        """
        params = [self.prior[param]() for param in self.dist.param_names]
        self.dist.update(**dict(zip(self.dist.param_names, params)))
        assert len(self.dist.params) == len(params)
        for n, p, P in zip(self.dist.param_names, self.dist.params, params):
            assert p==P, n+' not updated correctly by update'
        print('dist.update correctly updates distribution')

    def run(self, verbose=False, num_trials=DEFAULT_NUM_TRIALS, num_samples=DEFAULT_NUM_SAMPLES):
        """draw parameters from the hyperprior, draw many samples, compare to predicted distribution
        """
        # check that we update parameters correctly
        self._check_params_setter()
        self._check_setitem()
        self._check_update()

        for trial in range(num_trials):
            params = [self.prior[param]() for param in self.dist.param_names] ### draw params
            required = [self.prior[param]() for param in self.dist.required]  ### draw required
            self.dist.params = params                                         ### update distribution
            if verbose:
                print('trial=%d'%trial)
                print('    params  : %s'%repr(params))
                print('    required: %s'%repr(required))
            test = SamplingDistributionTest(self.dist, num_samples, *required) ### create test object
            self.tests.append(test)                                            ### hold onto a reference
        return self.tests

    def check_prob(self, verbose=False):
        """make sure the pdf is not zero for any of the drawn samples
        """
        for trial, test in enumerate(self.tests):
            if verbose:
                print('checking prob for trial=%d'%trial)
            test.check_prob(verbose=verbose)

    def check_bounds(self, verbose=False):
        """make sure that each trial's samples obey the declared domain
        """
        for trial, test in enumerate(self.tests):
            if verbose:
                print('checking bounds for trial=%d'%trial)
            test.check_bounds(verbose=verbose)

    def plot(self, verbose=False, outdir='.', tag='', figtype='png', **kwargs):
        """plot the tests
        """
        if not tag:
            tag = "SamplingDistributionTester"
        path = os.path.join(outdir, tag + '-%d.' + figtype)
        for trial, test in enumerate(self.tests):
            figname = path%trial
            if verbose:
                print('plotting trial=%d : %s'%(trial, figname))
            test.plot(figname, verbose=verbose, **kwargs)

        ### plot a cumulative histogram of pvalues from trials
        if self.num_variates == 1:
            if verbose:
                print('plotting distribution of 1D KS pvalues')

            pvalues = [test.compare()[1] for test in self.tests] ### relies on how we return compare for 1D KS test

            fig = plt.figure()
            ax = fig.gca()

            # cumulative histogram of pvalues
            N = len(pvalues)
            pvalues = sorted(pvalues)
            ax.step([0]+pvalues+[1], [0]+list(be.arange(1., N+1)/N)+[1], color='k', where='post')

            # expectations based on binomial distribution
            faircoin = be.linspace(0, 1, 1001)
            ax.plot(faircoin, faircoin, color='r')

            s = (faircoin*(1-faircoin)/N)**0.5 ### approx to the standard deviation from binomial distrib
            for i in [1, 2, 3]:
                ax.fill_between(faircoin, faircoin-i*s, faircoin+i*s, color='r', alpha=0.25)

            ### decorate
            ax.set_xlabel('pvalue')
            ax.set_ylabel('cumulative fraction of trials')

            ax.set_xlim(xmin=-0.01, xmax=1.01)
            ax.set_ylim(ax.get_xlim())

            ### save
            figname = os.path.join(outdir, tag + '-pvalues.' + figtype)
            if verbose:
                print('    saving: '+figname)
            fig.savefig(figname)
            plt.close(fig)
