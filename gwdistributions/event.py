"""a module that defines the Event class, which abstracts the concept of a GW event and retains knowledge of its properies and the associated waveform
"""
__author__ = "Chris Pankow <chris.pankow@ligo.org>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import warnings

try:
    import lal ### used to generate dictionaries for tidal parameters
except ImportError:
    lal = None ### flag this; we can't generate parameter dictionaries but we can still generate distributions

try:
    import lalsimulation ### used to generate waveforms
except ImportError:
    lalsimulation = None ### flag this; we can't generate waveforms but we can still generate distributions

from .backends import numerics as be
from .backends import names
from .backends.units import (C_SI, G_SI, MPC_SI, MSUN_SI)

#-------------------------------------------------

DEFAULT_FLOW = 10.0 # Hz
DEFAULT_FHIGH = 2048. # Hz
DEFAULT_DELTAF = 0.125 # Hz
DEFAULT_DELTAT = 1/4096.0 # sec
DEFAULT_FREF = 10. # Hz

DEFAULT_APPROXIMANT = "IMRPhenomXPHM"

DEFAULT_TAPER = None
DEFAULT_MBAND_THRESHOLD = None

TIME_DOMAIN_NAME = 'time'
FREQ_DOMAIN_NAME = 'frequency'

#-------------------------------------------------
# Event classes
#-------------------------------------------------

class Event(object):
    """an object representing a GW event. Knows how to compute the waveform for the event and \
retains some basic knowledge of event parameters
    """

    _domain = None ### describes the cached waveform

    _required4waveform = (
        'mass1_detector',
        'mass2_detector',
        'spin1x',
        'spin1y',
        'spin1z',
        'spin2x',
        'spin2y',
        'spin2z',
        'luminosity_distance',
        'inclination',
        'coalescence_phase',
        'eccentricity',
    )

    _extra4waveform = (
        'tidal_deformability1',
        'tidal_deformability2',
        'dchim2',
        'dchi0',
        'dchi1',
        'dchi2',
        'dchi3',
        'dchi4',
        'dchi5',
        'dchi5L',
        'dchi6',
        'dchi6L',
        'dchi7',
    )

    _extrinsic4waveform = ('right_ascension', 'declination', 'polarization', 'geocenter_time')

    _required = _required4waveform + _extrinsic4waveform

    _wavepolarizations = (
        'hx',  # tensor polarizations ### FIXME: declare standard names for these in backends.names?
        'hp',
        'hvx', # vector polarizations
        'hvy',
        'hb',  # scalar polarizations
        'hl',
    )
    _wavefreqs = (
        'waveform_freqs',
    )
    _wavetimes = (
        'waveform_times',
    )

    _wavescalars = (
        'waveform_flow',  # Hz
        'waveform_fhigh', # Hz
        'waveform_fref',  # Hz
        'waveform_tstart', # Hz
        'approximant',
        'waveform_taper',
        'domain',
    )

    @property
    def _wavevectors(self):
        return self._wavepolarizations + self._wavefreqs + self._wavetimes

    #--------------------

    @staticmethod
    def _lalname(key):
        """mappings from my naming convention to LALDict naming convention \
(see lalsimulation/lib/LALSimInspiralWaveformParams.c). Supports dynamic renaming within gw-distributions
        """

        # check tidal deformability
        for ind in [1, 2]:
            if key == names.name('tidal_deformability%d' % ind):
                return 'lambda%d' % ind

        # check non-GR params

        # TIGER dChi parameters
        for order in [2]: # negative orders
            if key == names.name('dchim%d' % order):
                return "dchi_minus%d" % order

        for order in range(8): # positive orders
            if key == names.name('dchi%d' % order):
                return 'dchi%d' % order

        for order in [5, 6]: # log terms
            if key == names.name('dchi%dL' % order):
                return 'dchi%dl' % order

        # if you haven't returned yet, raise an error
        raise KeyError('no known LALDict key corresponding to : '+key)

    @staticmethod
    def _default(key):
        """dynamic mapping between attribute and default value
        """
        if key in [
                names.name('inclination'),
                names.name('eccentricity'),
                names.name('coalescence_phase'),
                names.name('polarization'),
                names.name('spin1x'),
                names.name('spin1y'),
                names.name('spin1z'),
                names.name('spin2x'),
                names.name('spin2y'),
                names.name('spin2z'),
                names.name('tidal_deformability1'),
                names.name('tidal_deformability2'),
            ] \
                + [names.name('dchim%d'%ind) for ind in [2]] \
                + [names.name('dchi%d'%ind) for ind in range(8)] \
                + [names.name('dchi%dL'%ind) for ind in [5, 6]] \
            :
            return 0.0

        else:
            raise KeyError('no default value available for %s'%key)

    #--------------------

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            self[k] = v

    #--------------------

    def __getitem__(self, item):
        return getattr(self, names.invname(item))

    def __setitem__(self, item, value):
        setattr(self, names.invname(item), value)

    _attributes2ignore = ('_domain',) ### don't include these within list of attributes
    @property
    def attributes(self):
        """return a list of attributes that are interesting (i.e., not method names)
        """
        return tuple(names.name(_) for _ in vars(self).keys() if _ not in self._attributes2ignore)

    @property
    def scalar_attributes(self):
        return tuple(_ for _ in self.attributes if isinstance(self[_], (int, float, str)))

    @property
    def vector_attributes(self):
        return tuple(_ for _ in self.attributes if not isinstance(self[_], (int, float, str)))

    #--------------------

    @property
    def required4waveform(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._required4waveform)

    @property
    def extra4waveform(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._extra4waveform)

    @property
    def extrinsic4waveform(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._extrinsic4waveform)

    @property
    def required(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._required)

    @property
    def wavevectors(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._wavevectors)

    @property
    def waveattrs(self): ### support dynamic renaming
        return tuple(names.name(_) for _ in self._waveattrs)

    @property
    def domain(self):
        return self._domain

    #--------------------

    def _check(self):
        """check to make sure we have all the required attributes needed for this event
        """
        for k in self.required:
            assert hasattr(self, k), 'Event does not have attribute "%s"'%k

    def _check4waveform(self, use_defaults=False):
        """check to make sure we have all the required attributes needed for waveform \
generation defined for this event
        also extracts the extra parameters needed for calls to lalsimulation.SimInspiralFD, \
lalsimulation.SimInspiralTD
        Note: these must be passed as a LALDict
        """
        for k in self.required4waveform:
            if not hasattr(self, names.invname(k)):
                if use_defaults:
                    warnings.warn('using default : %s=%.6e'%(k, self._default(k)))
                    self[k] = self._default(k)
                else:
                    raise AttributeError('Event does not have attribute "%s"'%k)

        if lal is None:
            raise ImportError('could not import lal!')

        params = lal.CreateDict()

        for k in self.extra4waveform:
            if hasattr(self, names.invname(k)):
                lal.DictInsertREAL8Value(params, self._lalname(k), self[k])

            elif use_defaults:
                warnings.warn('using default : %s=%.6e'%(k, self._default(k)))
                self[k] = self._default(k)
                lal.DictInsertREAL8Value(params, self._lalname(k), self[k])
        
        return params

    #--------------------

    def _set_mband_thresholds(
            self,
            params,
            phenomxhm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
            phenomxphm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
        ):
        """set the multi-banding thresholds for calls involving IMRPhenomX waveforms
        """
        if phenomxhm_mband_threshold is not None:
            lalsimulation.SimInspiralWaveformParamsInsertPhenomXHMThresholdMband(params, phenomxhm_mband_threshold)
            self['phenomxhm_mband_threshold'] = phenomxhm_mband_threshold

        if phenomxphm_mband_threshold is not None:
            lalsimulation.SimInspiralWaveformParamsInsertPhenomXPHMThresholdMband(params, phenomxphm_mband_threshold)
            self['phenomxphm_mband_threshold'] = phenomxphm_mband_threshold

        return params

    #--------------------

    @property
    def freqs(self):
        assert self.domain == FREQ_DOMAIN_NAME, 'must have already computed waveform \
in the frequency domain to access freqs'
        return self['waveform_freqs']

    @property
    def deltaf(self):
        assert self.domain == FREQ_DOMAIN_NAME, 'must have already computed waveform \
in the frequency domain to access deltaf'
        return self['waveform_delta_f']

    @property
    def times(self):
        assert self.domain == TIME_DOMAIN_NAME, 'must have already computed waveform \
in the time domain to access times'
        return self['waveform_times']

    @property
    def deltat(self):
        assert self.domain == TIME_DOMAIN_NAME, 'must have already computed waveform \
in the time domain to access deltat'
        return self['waveform_delta_t']

    @property
    def waveform_duration(self):
        '''compute the signal duration in the detector-frame. Requires a waveform to have already been generated
In the frequency domain, we just estimate the duration from a 0-PN scaling with chirp mass between flow and fhigh.
In the time domain, we simply check the length of the abscissa (time in seconds) for the computed waveform
        '''
        if self.domain == FREQ_DOMAIN_NAME:
            return self.__frequency_domain_waveform_duration(
                m1 = self['mass1_detector'],    # solar masses
                m2 = self['mass2_detector'],    # solar masses
                flow = self['waveform_flow'],   # Hz
                fhigh = self['waveform_fhigh'], # Hz
            )

        elif self.domain == TIME_DOMAIN_NAME:
            return len(self['waveform_times']) * self['waveform_delta_t']

        else:
            ### NOTE: we may want to change this in the future...
            raise RuntimeError('must compute waveform before we can estimate the template duration')

    @staticmethod
    def _frequency_domain_waveform_duration(m1, m2, flow, fhigh):
        '''estimate the template duration from a 0-PN scaling with chirp mass between flow and fhigh
        masses should be specified in solar masses; frequencies in Hz
        '''
        mc = (m1*m2)**0.6 / (m1+m2)**0.2 * MSUN_SI ### chirp mass
        return (5./256) * be.pi**(-8./3) * (G_SI * mc / C_SI**3)**(-5./3) * (flow**(-8./3) - fhigh**(-8./3)) # sec

    #--------------------

    def waveform(
            self,
            use_defaults=False,
            flow=DEFAULT_FLOW,
            fhigh=DEFAULT_FHIGH,
            deltaf=DEFAULT_DELTAF,
            deltat=DEFAULT_DELTAT,
            fref=DEFAULT_FREF,
            approximant=DEFAULT_APPROXIMANT,
            taper=DEFAULT_TAPER,
            truncate_low_frequencies=True,
            truncate_high_frequencies=True,
            domain=FREQ_DOMAIN_NAME,
            phenomxhm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
            phenomxphm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
        ):
        """
        Generate, and internally store, the h_+ and h_x polarizations for an event.
        """
        generate_waveform = False

        # test to see if we need to regenerate waveform
        generate_waveform |= (domain != self.domain)
        for key in ['hp', 'hx', 'hvx', 'hvy', 'hp', 'hl', 'waveform_freqs']:
            generate_waveform |= not hasattr(self, key)

        # now delegate if needed
        if generate_waveform:
            if domain == FREQ_DOMAIN_NAME:
                return self._frequency_domain_waveform(
                    use_defaults=use_defaults,
                    flow=flow,
                    fhigh=fhigh,
                    deltaf=deltaf,
                    fref=fref,
                    approximant=approximant,
                    taper=taper,
                    truncate_low_frequencies=truncate_low_frequencies,
                    phenomxhm_mband_threshold=phenomxhm_mband_threshold,
                    phenomxphm_mband_threshold=phenomxphm_mband_threshold,
                )

            elif domain == TIME_DOMAIN_NAME:
                return self._time_domain_waveform(
                    use_defaults=use_defaults,
                    flow=flow,
                    deltat=deltat,
                    fref=fref,
                    approximant=approximant,
                    taper=taper,
                    phenomxhm_mband_threshold=phenomxhm_mband_threshold,
                    phenomxphm_mband_threshold=phenomxphm_mband_threshold,
                )

            else:
                raise ValueError('domain=%s not understood!'%domain)

        else:
            return self['hp'], self['hx'], self['hvx'], self['hvy'], self['hb'], self['hl'], self['waveform_freqs']

    def delete_attr(self, attr, verbose=False):
        if hasattr(self, attr):
            if verbose:
                print('removing : '+attr)
            delattr(self, attr)

    _waveattrs2ignore = ('domain,') ### do not delete these attrs
    def delete_waveform(self, verbose=False):
        for attr in self._wavescalars + self._wavevectors:
            if attr not in self._waveattrs2ignore:
                self.delete_attr(attr, verbose=verbose)
        self._domain = None ### reset this so we will regenerate waveform next time

    def delete_wavescalars(self, verbose=False):
        for attr in self._wavescalars:
            if attr not in self._waveattrs2ignore:
                self.delete_attr(attr, verbose=verbose)
        self._domain = None ### reset this so we will regenerate waveform next time

    def delete_wavevectors(self, verbose=False):
        """delete only the vectors (the expensive parts of the waveform
        """
        for attr in self.wavevectors:
            if attr not in self._waveattrs2ignore:
                self.delete_attr(attr, verbose=verbose)
        self._domain = None ### reset this so we will regenerate waveform next time

    def _frequency_domain_waveform(
            self,
            use_defaults=False,
            flow=DEFAULT_FLOW,
            fhigh=DEFAULT_FHIGH,
            deltaf=DEFAULT_DELTAF,
            fref=DEFAULT_FREF,
            approximant=DEFAULT_APPROXIMANT,
            taper=DEFAULT_TAPER,
            truncate_low_frequencies=True,
            truncate_high_frequencies=True,
            phenomxhm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
            phenomxphm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
        ):
        if lalsimulation is None:
            raise ImportError('could not import lalsimulation!')

        params = self._check4waveform(use_defaults=use_defaults)
        params = self._set_mband_thresholds(
            params,
            phenomxhm_mband_threshold=phenomxhm_mband_threshold,
            phenomxphm_mband_threshold=phenomxphm_mband_threshold,
        )

        if taper != DEFAULT_TAPER: ### only warn once
            warnings.warn('\nsetting taper=%s although this is not used within waveform generation. \
Please make sure this is intentional!\n'%taper)

        try:
            hp, hx = lalsimulation.SimInspiralFD(
                self['mass1_detector'] * MSUN_SI,
                self['mass2_detector'] * MSUN_SI,
                self['spin1x'],
                self['spin1y'],
                self['spin1z'],
                self['spin2x'],
                self['spin2y'],
                self['spin2z'],
                self['luminosity_distance'] * MPC_SI, ### distance should be specified in Mpc
                self['inclination'],
                self['coalescence_phase'],
                0.0, # longAscNodes (longitude of ascending nodes)
                self['eccentricity'],
                0.0, # meanPerAno (mean anomaly of periastron)
                deltaf,
                flow,
                fhigh,
                fref,
                params,
                lalsimulation.SimInspiralGetApproximantFromString(approximant),
            )
            freqs = be.arange(0, fhigh+deltaf, deltaf)

        except RuntimeError as e:
            print(e)
            for param in self.required4waveform:
                print('   ', param, '=', self[param])

#            print(lal.DictKeys(params), lal.DictValues(params))
            print('    params : ') ; lal.DictPrint(params, 0) ; print('')

            for param, val in [
                    (names.name('waveform_delta_f'), deltaf),
                    (names.name('waveform_flow'), flow),
                    (names.name('waveform_fhigh'), fhigh),
                    (names.name('waveform_fref'), fref),
                    (names.name('approximant'), approximant),
                    (names.name('use_defaults'), use_defaults),
                ]:
                print('   ', param, '=', val)

            raise e

        self._domain = FREQ_DOMAIN_NAME

        keep = be.ones(len(freqs), dtype=bool)

        if truncate_high_frequencies:
            keep *= (freqs <= fhigh)

        if truncate_low_frequencies:
            keep *= (flow <= freqs)

        Nkeep = be.sum(keep)

        self['waveform_freqs'] = freqs[keep]
        self['waveform_delta_f'] = deltaf

        self['hp'] = hp.data.data[keep]
        self['hx'] = hx.data.data[keep]

        # return nontensorial modes, which are zero by default for lalsim waveforms
        self['hvx'], self['hvy'], self['hb'], self['hl'] = be.zeros((4, Nkeep), dtype=float)

        self['waveform_flow'] = flow
        self['waveform_fhigh'] = fhigh
        self['waveform_fref'] = fref

        self['waveform_tref'] = -float(hp.epoch) # measured relative to the start of the timeseries
                                                 # this is a lalsimulation convention

        if taper is not None:
            self['waveform_taper'] = taper

        self['truncate_low_frequencies'] = truncate_low_frequencies
        self['truncate_high_frequencies'] = truncate_high_frequencies

        self['approximant'] = approximant

        return self['hp'], self['hx'], self['hvx'], self['hvy'], self['hb'], self['hl'], self['waveform_freqs']

    def _time_domain_waveform(
            self,
            use_defaults=False,
            flow=DEFAULT_FLOW,
            deltat=DEFAULT_DELTAT,
            fref=DEFAULT_FREF,
            approximant=DEFAULT_APPROXIMANT,
            taper=DEFAULT_TAPER,
            phenomxhm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
            phenomxphm_mband_threshold=DEFAULT_MBAND_THRESHOLD,
        ):
        if lalsimulation is None:
            raise ImportError('could not import lalsimulation!')

        params = self._check4waveform(use_defaults=use_defaults)
        params = self._set_mband_thresholds(
            params,
            phenomxhm_mband_threshold=phenomxhm_mband_threshold,
            phenomxphm_mband_threshold=phenomxphm_mband_threshold,
        )

        if taper != DEFAULT_TAPER:
            warnings.warn('\nsetting taper=%s although this is not used within waveform generation. \
Please make sure this is intentional!\n'%taper)

        try:
            hp, hx = lalsimulation.SimInspiralTD(
                self['mass1_detector'] * MSUN_SI,
                self['mass2_detector'] * MSUN_SI,
                self['spin1x'],
                self['spin1y'],
                self['spin1z'],
                self['spin2x'],
                self['spin2y'],
                self['spin2z'],
                self['luminosity_distance'] * MPC_SI, ### distance should be specified in Mpc
                self['inclination'],
                self['coalescence_phase'],
                0.0, # longAscNodes (longitude of ascending nodes)
                self['eccentricity'],
                0.0, # meanPerAno (mean anomaly of periastron)
                deltat,
                flow,
                fref,
                params,
                lalsimulation.SimInspiralGetApproximantFromString(approximant),
            )
            times = be.arange(0, len(hp.data.data[:])) * deltat

        except RuntimeError as e:
            print(e)
            for param in self.required4waveform:
                print('   ', param, '=', self[param])

#            print(lal.DictKeys(params), lal.DictValues(params))
            print('    params : ') ; lal.DictPrint(params, 0) ; print('')

            for param, val in [
                    (names.name('waveform_delta_t'), deltat),
                    (names.name('waveform_flow'), flow),
                    (names.name('waveform_fref'), fref),
                    (names.name('approximant'), approximant),
                    (names.name('use_defaults'), use_defaults),
                ]:
                print('   ', param, '=', val)

            raise e

        self._domain = TIME_DOMAIN_NAME
        self['waveform_times'] = times
        self['waveform_delta_t'] = deltat

        self['hp'] = hp.data.data[:]
        self['hx'] = hx.data.data[:]

        # return nontensorial modes, which are zero by default for lalsim waveforms
        self['hvx'] = be.zeros(len(hx.data.data[:]), dtype=float)
        self['hvy'] = be.zeros(len(hx.data.data[:]), dtype=float)
        self['hb'] = be.zeros(len(hx.data.data[:]), dtype=float)
        self['hl'] = be.zeros(len(hx.data.data[:]), dtype=float)

        self['waveform_flow'] = flow
        self['waveform_fref'] = fref

        self['waveform_tref'] = -float(hp.epoch) # measured relative to the start of the timeseries
                                                 # this is a lalsimulation convention

        if taper is not None:
            self['waveform_taper'] = taper

        self['approximant'] = approximant

        return self['hp'], self['hx'], self['hvx'], self['hvy'], self['hb'], self['hl'], self['waveform_times']
