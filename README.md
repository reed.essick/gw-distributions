# gw-distributions

A flexible architecture for distributions over single-event parameters.

## TO DO

  * walk through `distributions.py` and `transforms.py` to make sure everything works as expected with other recent changes
  * implement `gwdistributions-sample` and `gwdistributions-weigh`
  * add distributions/mass-pairing functionality from [twixie](https://git.ligo.org/reed.essick/twixie)
  * add factories or some sort of automation to condition distributions that generate one set of variates on another set of variates
    * support a few possible combinations here, such as "taylor expansion", "spline", "step function/bins"

## Executables

`gw-distributions` provides only minimal command-line support, in the expectation that most of the functionality will be imported within external libraries and used directly therein.
These are provided as much as tutorials and/or example code as much as they are expected to be routinely used.

### gwdistributions-sample

A simple script that loads a distribution configuration and draws a specified number of samples.

```
usage: gwdistributions-generate [-h] [-N NUM_SAMPLES] [-o OUTPUT_PATH] [-v]
                                config

a quick script to draw samples from a distribution

positional arguments:
  config

optional arguments:
  -h, --help            show this help message and exit
  -N NUM_SAMPLES, --num-samples NUM_SAMPLES
  -o OUTPUT_PATH, --output-path OUTPUT_PATH
                        the path into which we will write samples. By default,
                        they will be printed to STDOUT as a CSV
  -v, --verbose

```

### gwdistributions-weigh

A simple script that loads a distribution configuration and existing samples, and then weighs those samples by the probability encoded in the distribution.
The new probabilities are included as a new column.

```
usage: gwdistributions-weigh [-h] [-c COLUMN_NAME] [-o OUTPUT_PATH] [-v]
                             inpath config

a quick script to weigh samples by the probability associated with a given
distribution

positional arguments:
  inpath
  config

optional arguments:
  -h, --help            show this help message and exit
  -c COLUMN_NAME, --column-name COLUMN_NAME
                        the column name that will be added to the data,
                        containing the probabilities associated with that
                        event
  -o OUTPUT_PATH, --output-path OUTPUT_PATH
                        the path into which we will write samples. By default,
                        they will be printed to STDOUT as a CSV
  -v, --verbose
```

## Library

The architecture of `gw-distributions` is meant to be extremely modular and automate as much boilerplate code as possible.
As such, users should be able to interchange distributions with zero overhead.
Roughly speaking, the library's logic can be divided into a few simple components

### I/O

**WRITE DESCRIPTION OF HOW THIS IS ACCOMPLISHED**
  * `gwdistributions/io.py`

### distributions and transforms

**WRITE DESCRIPTION OF HOW THIS IS ACCOMPLISHED**
  * `gwdistributions/distributions.py`
  * `gwdistributions/transforms.py`

### events and generators

**WRITE DESCRIPTION OF HOW THIS IS ACCOMPLISHED**
  * `gwdistributions/event.py`
  * `gwdistributions/generators.py`

## Installation

Installation is available through the standard Python syntax

```
python setup.py install --prefix=/path/to/install
```

## Useful References

  * [R. Essick and M. Fishbach, On Reweighing Single-Event Posteriors with Population Priors, LIGO-T1900895 (2021)](https://dcc.ligo.org/LIGO-T1900895/public)
  * [R. Essick, Constructing Mixture Models for Sensitivity Estimates from Subsets of Separate Injections, LIGO-T2100101 (2021)](https://dcc.ligo.org/LIGO-T2100101)
  * [R. Essick and M. Fishbach, On Estimating Rates from Monte-Carlo Integrals over Injection Sets, LIGO-T2000100 (2021)](https://dcc.ligo.org/LIGO-T2000100)
  * [W. Farr, Accuracy Requirements for Empirically Measured Selection Functions, Research Notes of the AAS, Volume 3, Number 5 (2019)](https://iopscience.iop.org/article/10.3847/2515-5172/ab1d5f)

## Authors

This code was developed from [gw_event_gen](https://git.ligo.org/chris-pankow/gw_event_gen.git) and contains contributions from

  * Reed Essick (reed.essick@gmail.com)
  * Chris Pankow (chris.pankow@ligo.org)
  * Eve Chase (eve.chase@ligo.org)
  * Maya Fishbach (maya.fishbach@ligo.org)
  * Amanda Farah (amanda.farah@ligo.org)
